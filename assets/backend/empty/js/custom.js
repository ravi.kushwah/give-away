/*--------------------- Copyright (c) 2020 -----------------------
[Master Javascript]
Project: Admin- Responsive HTML Template 
Version: 1.0.0
Assigned to: Theme Forest
-------------------------------------------------------------------*/
(function($) {
    "use strict";

    /*-----------------------------------------------------
    	Function  Start
    -----------------------------------------------------*/
    var admin = {
        initialised: false,
        version: 1.0,
        mobile: false,
        collapseScreenSize: 991,
        sideBarSize: 1199,
        init: function() {
            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }
            /*-----------------------------------------------------
            	Function Calling
            -----------------------------------------------------*/
            this.loader();
        },
		
		loader: function () {
			jQuery(window).on('load', function() {
				$(".loader").fadeOut();
				$(".spinner").delay(500).fadeOut("slow");
			});
		}
    };

    admin.init();

})(jQuery);

