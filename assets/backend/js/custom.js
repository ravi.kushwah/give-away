/*--------------------- Copyright (c) 2020 -----------------------
[Master Javascript]
-------------------------------------------------------------------*/
(function($) {
    "use strict";

    /*-----------------------------------------------------
    	Function  Start
    -----------------------------------------------------*/
    var admin = {
        initialised: false,
        version: 1.0,
        mobile: false,
        collapseScreenSize: 991,
        sideBarSize: 1199,
        init: function() {
            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }
            /*-----------------------------------------------------
            	Function Calling
            -----------------------------------------------------*/
            this.userToggle();
            this.sideBarToggle();
            this.sideMenu();
            this.sideBarHover();
            this.searchToggle();
            this.rightSlide();
            this.tooltipHover();
            this.nubberSpin();
            this.loader();
            this.checkSiteName();
            this.copyToClipBoard();
            this.deleteWebsite();
            this.leadSettings();
            this.categorySettings();
            this.customerSettings();
            this.agencySettings();
            this.customProductsSettings();
            this.shareButton();
        },

        /*-----------------------------------------------------
            Fix Header User Button
        -----------------------------------------------------*/
		// loader			
		loader: function () {
			jQuery(window).on('load', function() {
				$(".loader").fadeOut();
				$(".spinner").delay(500).fadeOut("slow");
			});
		},
		// loader	
        userToggle: function() {
            var count = 0;
            $('.user-info').on("click", function() {
                if ($(window).width() <= admin.collapseScreenSize) {
                    if (count == '0') {
                        $('.user-info-box').addClass('show');
                        count++;
                    } else {
                        $('.user-info-box').removeClass('show');
                        count--;
                    }
                }
            });

            $(".user-info-box, .user-info").on('click', function(e) {
                if ($(window).width() <= admin.collapseScreenSize) {
                    event.stopPropagation();
                }
            });

            $('body').on("click", function() {
                if ($(window).width() <= admin.collapseScreenSize) {
                    if (count == '1') {
                        $('.user-info-box').removeClass('show');
                        count--;
                    }
                }
            });
        },
        sideBarToggle: function() {
            $(".toggle-btn").on('click', function(e) {
                e.stopPropagation();
                $("body").toggleClass('mini-sidebar');
                $(this).toggleClass('checked');

            });
            $('.sidebar-wrapper').on('click', function(event) {
                event.stopPropagation();
            });
        },
        sideMenu: function() {
            $('.side-menu-wrap ul li').has('.sub-menu').addClass('has-sub-menu');
            $.sidebarMenu = function(menu) {
                var animationSpeed = 300,
                    subMenuSelector = '.sub-menu';
                $(menu).on('click', 'li a', function(e) {
                    var $this = $(this);
                    var checkElement = $this.next();
                    if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
                        checkElement.slideUp(animationSpeed, function() {
                            checkElement.removeClass('menu-show');
                        });
                        checkElement.parent("li").removeClass("active");
                    } else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
                        var parent = $this.parents('ul').first();
                        var ul = parent.find('ul:visible').slideUp(animationSpeed);
                        ul.removeClass('menu-show');
                        var parent_li = $this.parent("li");
                        checkElement.slideDown(animationSpeed, function() {
                            checkElement.addClass('menu-show');
                            parent.find('li.active').removeClass('active');
                            parent_li.addClass('active');
                        });
                    }
                    if (checkElement.is(subMenuSelector)) {
                        e.preventDefault();
                    }
                });
            }
            $.sidebarMenu($('.main-menu'));
            $(function() {
                for (var a = window.location, counting = $(".main-menu a").filter(function() {
                        return this.href == a;
                    }).addClass("active").parent().addClass("active");;) {
                    if (!counting.is("li")) break;
                    counting = counting.parent().addClass("in").parent().addClass("active");
                }
            });
        },
        sideBarHover: function() {
            if ($(window).width() >= admin.sideBarSize) {
                $(".main-menu").hover(function() {
                    $('body').addClass('sidebar-hover');
                }, function() {
                    $('body').removeClass('sidebar-hover');
                });
            }
        },
        searchToggle: function() {
            $('.search-toggle').on("click", function() {
                $('.serch-wrapper').addClass('show-search');
            });
            $('.search-close, .main-content').on("click", function() {
                $('.serch-wrapper').removeClass('show-search');
            });
        },
        rightSlide: function() {
            $(".setting-info").on('click', function(e) {
                e.stopPropagation();
                $("body").toggleClass('open-setting');
            });
            $('body, .close-btn').on('click', function() {
                $('body').removeClass('open-setting');
            });
            $('.slide-setting-box').on('click', function(event) {
                event.stopPropagation();
            });

        },
        tooltipHover: function() {
            if ($('.toltiped').length > 0) {
                $(".toltiped").tooltip();
            }
            if ($('.toltiped-right').length > 0) {
                $(".toltiped-right").tooltip({
                    'placement': 'right',
                });
            }
        },
        nubberSpin: function() {
            if ($('.number-spin').length > 0) {
                $('.number-increase').on('click', function() {
                    if ($(this).prev().val() < 50000) {
                        $(this).prev().val(+$(this).prev().val() + 1);
                    }
                });
                $('.number-decrease').on('click', function() {
                    if ($(this).next().val() > 1) {
                        if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
                    }
                });
            }
        },
        checkSiteName: function(){
            $(document).on('click','.check_site_url',function(){
                let siteURL = $.trim($('input[name="w_siteurl"]').val());
                let textObj = $('.plr_url_text');
                if( siteURL.length > 3 ){
                    var regexp = new RegExp(/^[a-z0-9 ]*$/);
                    if( regexp.test(siteURL) ){
                        textObj.html('<span class="mb-0 badge">Checking</span>');
                        $.ajax({
                            url: baseurl + 'home/checksiteurl', 
                            type: "POST",             
                            data: {'w_siteurl':siteURL,'w_id':$('input[name="w_id"]').val()},
                            success: function(e) {
                                if(e == 0){
                                    textObj.html('<span class="mb-0 badge badge-danger">Not Available</span>');
                                    showNotifications('error','Please, enter different site url. Someone is already using this url.');
                                }
                                else
                                    textObj.html('<span class="mb-0 badge badge-success">Available </span>');
                            }
                        });
                    }
                    else
                        showNotifications('error','Site URL can have letters (a-z) and numbers (0-9) only.');
                }
                else
                    showNotifications('error','Site URL should be atleast 3 characters.');
            });
        },
        copyToClipBoard: function(){
            $(document).on('click','.copy_button',function(){
                navigator.clipboard.writeText($(this).data('tobecopied'));
                showNotifications('success','Content copied successfully to clipboard.');
            });
        },
        deleteWebsite: function(){
            $(document).on('click','.deleteWebsite',function(){
                $('#deleteWebsiteBtn').data('webid',$(this).data('webid'))
                $('#deleteWebsite').modal('show');
            });

            $(document).on('click','#deleteWebsiteBtn',function(){
                let webid = $(this).data('webid');
                $.ajax({
                    url: baseurl + 'home/deleteWebsite', 
                    type: "POST",             
                    data: {'webid':webid},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Website deleted successfully.');
                        else
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
            
                        setTimeout(function(){ location.reload(); }, 3000);
                    }
                });
            });
        },
        shareButton: function(){
            $(document).on('click','.shareButton',function(){
                $('.socialul').attr('id',$(this).data('webid'))
                $('#socialicon').modal('show');
            });
        },
        leadSettings: function(){
            $(document).on('change','.arCls',function(){
               getARList($(this));
            });
            var parts = $(location).attr('href').split("/"),// use like uri segment
            last_part = parts[parts.length-2];
            var queryparams = last_part.split('?')[0];
            // console.log(queryparams)
            if(queryparams == 'lead_settings'){
                
                getARList($('#w_signuplist'));
                getARList($('#w_buyplanlist'));
                getARList($('#w_newsletterlist'));
            }
            
        },
        categorySettings:function(){
            $(document).on('click','.deleteCategory',function(){
                $('#deleteCategoryBtn').data('cateid',$(this).data('cateid'));
                $('#deleteCategory').modal('show');
            });

            $(document).on('click','#deleteCategoryBtn',function(){
                let cateid = $(this).data('cateid');
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_categories/'+webid, 
                    type: "POST",             
                    data: {'delete_cateid':cateid},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Category deleted successfully.');
                        else
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
            
                        setTimeout(function(){ location.reload(); }, 3000);
                    }
                });
            });

            $(document).on('change','.cateCls',function(){
                let sts = $(this).is(":checked") ? 1 : 0 ;
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_categories/'+webid, 
                    type: "POST",             
                    data: {'webid':webid,'sts':sts,'cateid':$(this).attr("id")},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Category status updated successfully.');
                        else
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                    }
                });
            });
            $(document).on('change','.customcateCls',function(){
                let sts = $(this).is(":checked") ? 1 : 0 ;
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_categories/'+webid, 
                    type: "POST",             
                    data: {'webid':webid,'sts':sts,'customcateid':$(this).attr("id")},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Category status updated successfully.');
                        else
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                    }
                });
            });

            $(document).on('click','.cateEdit',function(){
                let cateId = $(this).attr("id");
                let webid = $('#webid').val();
                if( cateId == 0 ){
                    $('.cateAddCls').val('');
                    $('#cate_id').val(0);
                    $('#add_categories_title').text('Add New Category');
                    $('#add_categories').modal('show');
                }
                else{
                    $.ajax({
                        url: baseurl + 'home/web_categories/'+webid, 
                        type: "POST",             
                        data: {'cate_id':cateId},
                        success: function(e) {
                            let resp = JSON.parse(e);
                            $('#cate_name').val(resp.cate_name)         
                            $('#cate_slug').val(resp.cate_slug)   
                            $('#cate_id').val(cateId);
                            $('#add_categories_title').text('Edit Category');
                            $('#add_categories').modal('show');      
                        }
                    });
                }
            });

            $(document).on('click','#submitCateData',function(){
                let cate_id = $.trim($('#cate_id').val());
                let cate_name = $.trim($('#cate_name').val());
                let cate_slug = $.trim($('#cate_slug').val());
                let webid = $('#webid').val();
                if( cate_name != '' && cate_slug != '' ){
                    $.ajax({
                        url: baseurl + 'home/web_categories/'+webid,
                        type: "POST",             
                        data: {'cate_id':cate_id,'cate_name':cate_name,'cate_slug':cate_slug},
                        success: function(e) {
                            if(e == 1)
                                showNotifications('success','Category updated successfully.');
                            else if(e == 2)
                                showNotifications('success','Category added successfully.');
                            else
                                showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                                
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                    });
                }
                else
                    showNotifications('error','Please, fill in the details to continue.');
            });
        },
        customerSettings:function(){
            $(document).on('change','.custCls',function(){
                let sts = $(this).is(":checked") ? 1 : 0 ;
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_customers/'+webid, 
                    type: "POST",             
                    data: {'sts':sts,'custid':$(this).attr("id")},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Customer status updated successfully.');
                        else
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                    }
                });
            });

            $(document).on('click','.custEdit',function(){
                let webid = $('#webid').val();
                let custId = $(this).attr("id");
                if( custId == 0 ){
                    $('.custAddCls').val('');
                    $('#c_id').val(0);
                    $('#add_customer_title').text('Add New Customer');
                    $('#add_customer').modal('show');
                }
                else{
                    $.ajax({
                        url: baseurl + 'home/web_customers/'+webid, 
                        type: "POST",             
                        data: {'custid':custId},
                        success: function(e) {
                            let resp = JSON.parse(e);
                            $('#c_name').val(resp.c_name)         
                            $('#c_email').val(resp.c_email)   
                            $('#c_id').val(custId);
                            $('#add_customer_title').text('Edit Customer');
                            $('#add_customer').modal('show');      
                        }
                    });
                }
            });

            $(document).on('click','#submitCustomerData',function(){
                let custId = $.trim($('#c_id').val());
                let c_name = $.trim($('#c_name').val());
                let c_email = $.trim($('#c_email').val());
                let c_password = $.trim($('#c_password').val());
                let err = 0;
                if( c_name != '' && c_email != '' ){
                    if( custId == 0 ){
                        if( c_password == '' ){
                            showNotifications('error','Please, enter passsword.');
                            err++;
                        }
                    }
                    if( err == 0 )
                    {
                        $.ajax({
                            url: baseurl + 'home/web_customers/'+$('#webid').val(), 
                            type: "POST",             
                            data: {'custid':custId,'c_name':c_name,'c_email':c_email,'c_password':c_password},
                            success: function(e) {
                                if(e == 1)
                                    showNotifications('success','Customer data updated successfully.');
                                else if(e == 2)
                                    showNotifications('success','Customer added successfully.');
                                else if(e == 4)
                                    showNotifications('error','One of your customer already have this email.');
                                else
                                    showNotifications('error','Something went wrong. Please, refresh the page and try again.');   
                                    
                                setTimeout(function(){ location.reload(); }, 3000);
                            }
                        });
                    }
                }
                else{
                    showNotifications('error','Please, fill in the details to continue.');
                }
            });
        },
        agencySettings:function(){
            $(document).on('change','.agencyuserCls',function(){
                let sts = $(this).is(":checked") ? 1 : 0 ;
                $.ajax({
                    url: baseurl + 'home/agency_users/', 
                    type: "POST",             
                    data: {'sts':sts,'userid':$(this).attr("id")},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Agency user status updated successfully.');
                        else
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                    }
                });
            });

            $(document).on('click','.agencyuserClsEdit',function(){
                let userId = $(this).attr("id");
                if( userId == 0 ){
                    $('.agencyuserAddCls').val('');
                    $('#u_id').val(0);
                    $('#add_agencyuser_title').text('Add New Agency User');
                    $('#add_agencyuser').modal('show');
                }
                else{
                    $.ajax({
                        url: baseurl + 'home/agency_users/', 
                        type: "POST",             
                        data: {'userid':userId},
                        success: function(e) {
                            let resp = JSON.parse(e);
                            $('#u_name').val(resp.u_name)         
                            $('#u_email').val(resp.u_email)   
                            $('#u_id').val(userId);
                            $('#add_agencyuser_title').text('Edit Agency User');
                            $('#add_agencyuser').modal('show');      
                        }
                    });
                }
            });

            $(document).on('click','#submitAgencyUserData',function(){
                let userId = $.trim($('#u_id').val());
                let u_name = $.trim($('#u_name').val());
                let u_email = $.trim($('#u_email').val());
                let u_password = $.trim($('#u_password').val());
                let err = 0;
                if( u_name != '' && u_email != '' ){
                    if( userId == 0 ){
                        if( u_password == '' ){
                            showNotifications('error','Please, enter passsword.');
                            err++;
                        }
                    }
                    if( err == 0 )
                    {
                        $.ajax({
                            url: baseurl + 'home/agency_users/', 
                            type: "POST",             
                            data: {'userid':userId,'u_name':u_name,'u_email':u_email,'u_password':u_password},
                            success: function(e) {
                                if(e == 1)
                                    showNotifications('success','Agency User data updated successfully.');
                                else if(e == 2)
                                    showNotifications('success','Agency User added successfully.');
                                else if(e == 4 )
                                    showNotifications('error','One of the User already have this email.');
                                else
                                    showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                                    
                                setTimeout(function(){ location.reload(); }, 3000);
                            }
                        });
                    }
                }
                else
                    showNotifications('error','Please, fill in the details to continue.');
            });
        },
        customProductsSettings:function(){
            $(document).on('click','.custProdEdit',function(){
                let p_id = $(this).attr("id");
                let webid = $('#webid').val();
                if( p_id == 0 ){
                    $('.productsAddCls').val('');
                    $('#p_id').val(0);
                    $('#add_products_title').text('Add Custom GiveAways');
                    $('#add_custom_products').modal('show');
                }
                else{
                    $.ajax({
                        url: baseurl + 'home/custom_products/'+webid, 
                        type: "POST",             
                        data: {'p_id':p_id},
                        success: function(e) {
                            let resp = JSON.parse(e);
                            $('#g_offer_name').val(resp.g_offer_name)         
                            $('#slug').val(resp.slug)   
                            $('#g_payout').val(resp.g_payout)   
                            $('#g_priview').val(resp.g_priview)   
                            $('#g_categories').val(resp.g_categories)  
                            $('#g_countries').val(resp.g_countries)   
                            $('#g_offer_url').val(resp.g_offer_url);
                            $('#g_sign_up_url').val(resp.g_sign_up_url);
                            $('#p_id').attr("p_id",resp.g_id);
                            $('#webid').val(resp.g_webid);
                            $('#add_products_title').text('Edit Custom Give Aways');
                            $('#add_custom_products').modal('show');      
                        }
                    });
                }
            });

            $(document).on('click','.submitProductsData',function(){
                let g_offer_name = $('#g_offer_name').val();
                let slug = $.trim($('#slug').val());
                let g_payout = $.trim($('#g_payout').val());
                let g_priview = $.trim($('#g_priview').val());
                let g_categories = $.trim($('#g_categories').val());
                let g_countries = $.trim($('#g_countries').val());
                let g_offer_url = $.trim($('#g_offer_url').val());
                let g_sign_up_url= $.trim($('#g_sign_up_url').val());
                 let webid = $('#webid').val();
                   let p_id = $(this).attr("p_id");
                if( g_offer_name != '' && slug != '' && g_payout != 0 && g_priview != '' && g_categories != '' && g_countries != '' && g_offer_url != '' && g_sign_up_url != '' ){
                    $.ajax({
                        url: baseurl + 'home/custom_products/'+webid, 
                        type: "POST",             
                        data: {'p_id':p_id,'g_offer_name':g_offer_name,'slug':slug,'g_payout':g_payout,'g_priview':g_priview,'g_categories':g_categories,'g_countries':g_countries,'g_offer_url':g_offer_url,'g_sign_up_url':g_sign_up_url},
                        success: function(e) {
                            if(e == 1)
                                showNotifications('success','Product updated successfully.');
                            else if(e == 2)
                                showNotifications('success','Giva Aways added successfully.');
                            else
                                showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                                
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                    });
                }
                else
                    showNotifications('error','Please, fill in the details to continue.');
            });

            $(document).on('click','.deleteCustomProduct',function(){
                $('#deleteCustomProductBtn').data('custprodid',$(this).data('custprodid'));
                $('#deleteCustomProduct').modal('show');
            });

            $(document).on('click','#deleteCustomProductBtn',function(){
                let custprodid = $(this).data('custprodid');
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/custom_products/'+webid, 
                    type: "POST",             
                    data: {'delete_prodid':custprodid},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Product deleted successfully.');
                        else
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
            
                        setTimeout(function(){ location.reload(); }, 3000);
                    }
                });
            });
        }
    };

    admin.init();

})(jQuery);



$(document).ready(function() {
    $(document).on('click','.close',function(){
        $('.plr_notification').removeClass('plr_success_msg');
        $('.plr_notification').removeClass('plr_error_msg');
    })
    if($("#colorpicker").length > 0) {
        $("#colorpicker").spectrum({
            color: "#0094e7"
        });
    }
});

$(document).ready(function() {
    $('#example').DataTable();
} );

function saveBasicSiteOption(){
    let w_siteurl = $.trim($('input[name="w_siteurl"]').val());
    let w_title = $.trim($('input[name="w_title"]').val());
    if( w_siteurl != '' && w_title != ''){
        let myForm = document.getElementById('setupUploadForm');
        var obj = new FormData(myForm);
        $.ajax({
        url: baseurl + 'home/submitSetupWebsite', 
        type: "POST",             
        data: obj,
        contentType: false,
        cache: false,
        processData:false,  
        success: function(e) {
            if( e == 1 ){
                showNotifications('success','Website generated successfully.');
                setTimeout(function(){
                    location.reload();
                },3000)
            }
            else
                showNotifications('error',e);
        }
        });
    }
    else{
        showNotifications('error','URL and Title  can\'t be empty.');
    }
}

function saveads(){
    let home_page_link = $.trim($('input[name="home_page_link"]').val());
    let home_page_sidebar_ink = $.trim($('input[name="home_page_sidebar_ink"]').val());
    let single_page_top_link = $.trim($('input[name="single_page_top_link"]').val());
    let single_page_bottom_link = $.trim($('input[name="single_page_bottom_link"]').val());
    let home_page_add = $.trim($('input[name="home_page_add"]').val());
    let home_page_sidebar_ad = $.trim($('input[name="home_page_sidebar_ad"]').val());
    let single_page_top_ad = $.trim($('input[name="single_page_top_ad"]').val());
    let single_page_bottom_ad = $.trim($('input[name="single_page_bottom_ad"]').val());
    if( home_page_add != '' && home_page_sidebar_ad != '' && single_page_top_ad != '' && single_page_bottom_ad != '' && home_page_link != '' && home_page_sidebar_ink != '' && single_page_top_link != '' && single_page_bottom_link != ''){
        let myForm = document.getElementById('adsBannerSave');
        var obj = new FormData(myForm);
        $.ajax({
        url: baseurl + 'home/submitAdsData', 
        type: "POST",             
        data: obj,
        contentType: false,
        cache: false,
        processData:false,  
        success: function(e) {
            if( e == 1 ){
                showNotifications('success','Ads Update successfully.');
                setTimeout(function(){
                    location.reload();
                },3000)
            }
            else
                showNotifications('error',e);
        }
        });
    }
    else{
        showNotifications('error','URL and Title can\'t be empty.');
    }
}
function saveProfile(){
    let u_name = $.trim($('#u_name').val());
    if(u_name != ''){
       $.ajax({
          url: baseurl + 'home/profile', 
          type: "POST",             
          data: {'u_name':u_name,'pwd':$('#pwd').val()},
          success: function(e) {
              if(e == 1)
                showNotifications('success','Profile updated successfully.');
              else
              showNotifications('error','Something went wrong. Please, refresh the page and try again.');
 
             setTimeout(function(){ location.reload(); }, 3000);
          }
        });
    }else
    showNotifications('error','Please enter the name.')
 }
 
 function saveCompliance(){
    let err = 0;
    let dataArr = {};
    $('.complianceCls').each(function(){
        if ( $(this).val() == '' )
           err++;
        else
            dataArr[$(this).attr('id')] = $(this).val();
    });
    if(err == 0){
       $.ajax({
          url: baseurl + 'home/compliance_settings', 
          type: "POST",             
          data: dataArr,
          success: function(e) {
              console.log(e)
              if(e == 1)
                showNotifications('success','Compliance Settings saved successfully.');
              else
                showNotifications('error','Something went wrong. Please, refresh the page and try again.');
 
             setTimeout(function(){ location.reload(); }, 3000);
          }
        });
    }else
        showNotifications('error','Please enter the details to save.')
 }

 function saveAdvertisement(){
    let dataArr = {};
    $('.adsCls').each(function(){
        dataArr[$(this).attr('id')] = $(this).val();
    });
    $.ajax({
        url: baseurl + 'home/advertisement_settings', 
        type: "POST",             
        data: dataArr,
        success: function(e) {
            console.log(e)
            if(e == 1)
            showNotifications('success','Advertisement Settings saved successfully.');
            else
            showNotifications('error','Something went wrong. Please, refresh the page and try again.');

            setTimeout(function(){ location.reload(); }, 3000);
        }
    });
 }

 function saveAddonDomain(){
    let dataArr = {};
    if( $('#addon_domain').val() != '' ) {
        dataArr['w_id'] = $('#w_id').val();
        dataArr['addon_domain'] = $('#addon_domain').val();
        $.ajax({
            url: baseurl + 'home/connect_domain/'+$('#w_id').val(), 
            type: "POST",             
            data: dataArr,
            success: function(e) {
                console.log(e)
                if(e == 1)
                    showNotifications('success','Domain saved successfully.');
                else if(e == 2)
                    showNotifications('error','Someone is already using this domain.');
                else if(e == 3)
                    showNotifications('error','Domain is already connected.');
                else if(e == 4)
                    showNotifications('error','Domain format is not correct.');
                else if(e == 5)
                    showNotifications('error','Could not determine the nameserver IP addresses for '+$('#addon_domain').val()+' Please make sure that the domain is registered with a valid domain registrar.');
                else
                    showNotifications('error','Something went wrong. Please, refresh the page and try again.');
    
                setTimeout(function(){ location.reload(); }, 3000);
            }
        });
    }
    else{
        showNotifications('error','Domain can not be empty.')
    }
 }


 function saveLeadSettings(){
    let w_signuplist = $('#w_signuplist').val();
    let signup = $('#signup').val();
    let w_buyplanlist = $('#w_buyplanlist').val();
    let planbuy = $('#planbuy').val();
    let w_newsletterlist = $('#w_newsletterlist').val();
    let newsletter = $('#newsletter').val();
     let customgiveaways = $('#customplan').val();
    let customNewsletter = $('#customNewsletter').val();
    


    let w_signuplist_arr = { 'ar':w_signuplist,'listid':signup };
    let w_buyplanlist_arr = { 'ar':w_buyplanlist,'listid':planbuy };
    let w_newsletterlist_arr = { 'ar':w_newsletterlist,'listid':newsletter };

    $.ajax({
        url: baseurl + 'home/lead_settings/'+$('#w_id').val(), 
        type: "POST",             
        data: { 'customHTMLgiveaways' : customgiveaways , 'customHTMLnews' : customNewsletter , 'w_signuplist' : JSON.stringify(w_signuplist_arr) , 'w_buyplanlist' : JSON.stringify(w_buyplanlist_arr) , 'w_newsletterlist' : JSON.stringify(w_newsletterlist_arr) },
        success: function(e) {
            if(e == 1)
                showNotifications('success','Lead Settings saved successfully.');
            else 
                showNotifications('error','Something went wrong. Please, refresh the page and try again.');

            setTimeout(function(){ location.reload(); }, 3000);
        }
    });
 }

function showNotifications(type, message){
    $('.plr_notification').removeClass('plr_success_msg');
    $('.plr_notification').removeClass('plr_error_msg');
    let img = baseurl+'assets/backend/images/'+type+'.png';
    $('.plr_happy_img img').attr('src',img);
    if( type == 'success' )
        $('.plr_yeah h5').text('Congratulations!');
    else
        $('.plr_yeah h5').text('Oops!');
    $('.plr_yeah p').text(message);
    $('.plr_notification').addClass('plr_'+type+'_msg');
}

function getARList(_this){
    let v = _this.val(); 
    // alert(v);
    if( v != 0  ){
        var type = _this.attr('data-artype'); 
        let nextSel = _this.parent().find('input[type="hidden"]').val() || '';
        $.ajax({
            url: baseurl + 'ar_setting/get_list/'+v, 
            type: "GET",
            success: function(e) {
                var resp = JSON.parse(e);
                if( resp.status !== undefined && resp.status == 1 ){
                    var result = (resp.data);
                    var listData = '';
                    if(result.list.length === undefined) {
                        $.each(result.list, function(key, value) {
                            let t = 
                            listData += '<option value="'+key+'" '+(key == nextSel ? 'selected' : '')+'>'+value+'</option>'
                        })
                        $('#'+type).html('<option value="0">Choose One</option>'+listData);
                    }else{
                        showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                    }
                }
                else{
                    showNotifications('error',resp.error);
                }
            }
        });
    }else{
    //   $('.planbuy').hide();
    //   $('.newsletter').hide();
    //      $('.customplan').show();
    //   $('.customNewsletter').show();
    }
}

function addNewPlan(){
    let planCounter = $('#planCounter').val();
    let planHTML = $('#planHTML_div').html();
    let newPlanCounter = parseInt(planCounter)+1;
    planHTML = planHTML.replace(/_ID/g, '_'+newPlanCounter);
    $('#planCounter').val(newPlanCounter);

    $('#planHTML').append(planHTML);
}

 $(document).on('click','.DeleteSiteLogo',function(){
     var type = $(this).attr('type');
      $.ajax({
        url: baseurl + 'home/deleteSiteLogo', 
        type: "POST",
        data: {'webid':$(this).attr('data-id'),'type':$(this).attr('type')},
        success: function(e) {
            var resp = $.parseJSON(e);
            if(resp.status == 1){
                showNotifications('success','Website Logo deleted successfully.');
                if(resp.type=="logo"){
                    setTimeout(function(){$('.RemoveLogo').attr('src',baseurl +'assets/backend/images/logo.png');}, 200);
                }else if(resp.type=="favicon"){
                    setTimeout(function(){$('.RemovefavIcon').attr('src',baseurl +'assets/backend/images/favicon.png');}, 200);
                }
            }else {
                 showNotifications('error','Something went wrong. Please, refresh the page and try again.');
            }
            setTimeout(function(){$('.plr_notification ').removeClass('plr_success_msg');}, 3000);
        } 
    });
 })
  
function savePlanSettings(){
    var dataArr = {};
    let newPlanCounter = $('#planCounter').val();
    for( let i = 1 ; i < parseInt(newPlanCounter) + 1 ; i++ ){
        var tempArr = {};
        $('.planSettings_'+i).each(function(){
            tempArr[$(this).attr('id')] = $(this).val();
        });
        dataArr['plan_'+i] = JSON.stringify(tempArr);
    }
    $.ajax({
        url: baseurl + 'home/web_paymentplan/'+$('#w_id').val(), 
        type: "POST",
        data: {'paydata':dataArr},
        success: function(e) {
            if(e == 1)
                showNotifications('success','Payment Settings saved successfully.');
            else 
                showNotifications('error','Something went wrong. Please, refresh the page and try again.');

            setTimeout(function(){ location.reload(); }, 3000);
        }
    });
}
    $(document).on('change','.submitGiveaways',function(){
                let sts = $(this).is(":checked") ? 1 : 0 ;
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/GiveawaysActive/'+webid, 
                    type: "POST",             
                    data: {'webid':webid,'sts':sts,'s_id':$(this).attr("id")},
                    success: function(e) {
                        if(e == 1){
                            showNotifications('success','Category status updated successfully.');
                            setTimeout(function(){window.location.reload();},3000);  
                        }else{
                            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                            
                        } 
                    }
                });
            });

function sharethelink(_this){
    var socialUrl = $(_this).data('url');
    var id = $('.socialul').attr('id');
    $.ajax({
        url: baseurl + 'home/getWebsiteSeoData/'+id, 
        type: "GET",
        success: function(e) {
            let resp = JSON.parse(e);
            let shareUrl = baseurl + 'ga/' + resp[0].w_siteurl;
            socialUrl = socialUrl.replace("COM-BLAST-QUES", resp[0].w_title);
            socialUrl = socialUrl.replace("COM-BLAST-ANS",  resp[0].w_title);
            let logoUrl = resp[0].w_logourl == "" ? "" : baseurl + "assets/webupload/" + resp[0].w_logourl;
            socialUrl = socialUrl.replace("COM-BLAST-IMAGE", logoUrl);
            socialUrl = socialUrl.replace("COM-BLAST-URL", encodeURIComponent(shareUrl));
            window.open(socialUrl, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes");
        }
    });
 }
 function add_affiliate(){
        let myForm = document.getElementById('setupUploadForm');
        var obj = new FormData(myForm);
        $.ajax({
            url: baseurl + 'home/add_affiliate_url', 
            type: "POST",             
            data: obj,
            contentType: false,
            cache: false,
            processData:false,  
            success: function(e) {
                if( e == 1 ){
                    showNotifications('success','successfully Update Affiliate Link');
                    setTimeout(function(){
                        location.reload();
                    },3000)
                }else if(e == 2){
                    showNotifications('success','successfully Add Affiliate Link');
                    setTimeout(function(){
                        location.reload();
                    },3000)
                    
                }else{
                    showNotifications('error',e);
                }
            }
        });
}

 
if (typeof MathJax !== 'undefined') { MathJax.Hub.Queue(["Typeset", MathJax.Hub]); }
    var dataTableObj = $('.server_datatable').DataTable({
        searching: true,
        processing: true,
        dom: 'lf<"table-responsive" t >ip',
        language: {
            paginate: {
                previous: '<i class="fa fa-chevron-left"></i>',
                next: '<i class="fa fa-chevron-right"></i>',
            },
            emptyTable: "Record not Found",
            search: 'Search' + ':',

        },
        pageLength: 10,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        responsive: true,
        serverSide: { "regex": true },
        columnDefs: [{
            targets: "_all",
            orderable: false
        }],
        ajax: {
            "url": baseurl + $('.server_datatable').attr('data-url'),
            "type": "POST"
        },
    });
    