$(document).ready(function () {
    
    $('body').on('click', '.autoResponder_connect', function (e) {
        e.preventDefault();
        var responder = $(this).data('responder');
        var data = {
            'responder': responder,
            'apikey': ''
        };
        $('#' + responder).find('span').html('');
        console.log($(this));
        switch (responder) {
            case 'ConstantContact':
                var username = $('#' + responder).find('.username').val();
                var password = $('#' + responder).find('.password').val();
                if (username == '') {
                    // $('#' + responder).find('.username').next('span').html('username is required.');
                    showNotifications('error', 'Username is required.');
                    return
                }
                if (password == '') {
                    // $('#' + responder).find('.password').next('span').html('password is required.');
                    showNotifications('error', 'Password is required.');
                    return
                }
                data.apikey = {
                    'username': username,
                    'password': password
                };
                break;
            case 'SendReach':
                var public_key = $('#' + responder).find('.public_key').val();
                var private_key = $('#' + responder).find('.private_key').val();
                if (public_key == '') {
                    // $('#' + responder).find('.public_key').next('span').html('Public Key is required.');
                    showNotifications('error', 'Public Key is required.');
                    return
                }
                if (private_key == '') {
                    // $('#' + responder).find('.private_key').next('span').html('Private Key is required.');
                    showNotifications('error', 'Private Key is required.');
                    return
                }
                data.apikey = {
                    'public_key': public_key,
                    'private_key': private_key
                };
                break;
            case 'iContact':
                var login_email = $('#' + responder).find('.login_email').val();
                var app_id = $('#' + responder).find('.app_id').val();
                var app_password = $('#' + responder).find('.app_password').val();
                if (login_email == '') {
                    // $('#' + responder).find('.login_email').next('span').html('Login Email is required.');
                    showNotifications('error', 'Login Email is required.');
                    return
                }
                if (app_id == '') {
                    // $('#' + responder).find('.app_id').next('span').html('APP ID is required.');
                    showNotifications('error', 'APP ID is required.');
                    return
                }
                if (app_password == '') {
                    // $('#' + responder).find('.app_password').next('span').html('APP Password is required.');
                    showNotifications('error', 'APP Password is required.');
                    return
                }
                data.apikey = {
                    'login_email': login_email,
                    'app_id': app_id,
                    'app_password': app_password
                };
                break;
            case 'Infusionsoft':
                var client_id = $('#' + responder).find('.client_id').val();
                var client_secret = $('#' + responder).find('.client_secret').val();
                if (client_id == '') {
                    showNotifications('error', 'Client Id is required.');
                    return
                }else if (client_secret == '') {
                    showNotifications('error', 'Client secret is required.');
                    return
                }else{
                    $('form#Infusionsoft').submit();
                }
                // data.apikey = {
                //     'client_id': client_id,
                //     'client_secret': client_secret
                // };
                break;
            case 'Aweber':
                var aweber_code = $('#' + responder).find('.aweber_code').val();
                if (aweber_code == '') {
                    // $('#' + responder).find('.aweber_code').next('span').html('Aweber Code is required.');
                    showNotifications('error', 'Aweber Code is required.');
                    return
                }
                data.apikey = {
                    'aweber_code': aweber_code
                };
                break;
            case 'Sendlane':
                var user_url = $('#' + responder).find('.user_url').val();
                var api_key = $('#' + responder).find('.api_key').val();
                var hash_key = $('#' + responder).find('.hash_key').val();
                if (user_url == '') {
                    // $('#' + responder).find('.user_url').next('span').html('User URL is required.');
                    showNotifications('error', 'User URL is required.');
                    return
                }
                if (api_key == '') {
                    // $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    showNotifications('error', 'API Key is required.');
                    return
                }
                if (hash_key == '') {
                    // $('#' + responder).find('.hash_key').next('span').html('Hash Key is required.');
                    showNotifications('error', 'Hash Key is required.');
                    return
                }
                data.apikey = {
                    'user_url': user_url,
                    'api_key': api_key,
                    'hash_key': hash_key
                };
                break;
            case 'Mailchimp':
                var api_key = $('#' + responder).find('.api_key').val();
                if (api_key == '') {
                    // $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    showNotifications('error', 'API Key is required.');
                    return
                }
                data.apikey = {
                    'api_key': api_key
                };
                break;
            case 'GetResponse':
                var api_key = $('#' + responder).find('.api_key').val();
                if (api_key == '') {
                    // $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    showNotifications('error', 'API Key is required.');
                    return
                }
                data.apikey = {
                    'api_key': api_key
                };
                break;
            case 'ActiveCampaign':
                var api_key = $('#' + responder).find('.api_key').val();
                var api_url = $('#' + responder).find('.api_url').val();
                if (api_url == '') {
                    // $('#' + responder).find('.api_url').next('span').html('API Url is required.');
                    showNotifications('error', 'API Url is required.');
                    return
                }
                if (api_key == '') {
                    // $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    showNotifications('error', 'API Key is required.');
                    return
                }
                data.apikey = {
                    'api_key': api_key,
                    'api_url': api_url
                };
                break;
            case 'ConvertKit':
                var api_key = $('#' + responder).find('.api_key').val();
                var api_secret = $('#' + responder).find('.api_secret').val();
                if (api_key == '') {
                    // $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    showNotifications('error', 'API Key is required.');
                    return
                }
                if (api_secret == '') {
                    // $('#' + responder).find('.api_secret').next('span').html('API Secret is required.');
                    showNotifications('error', 'API Secret is required.');
                    return
                }
                data.apikey = {
                    'api_key': api_key,
                    'api_secret': api_secret
                };
                break;
            case 'MailerLite':
                var api_key = $('#' + responder).find('.api_key').val();
                if (api_key == '') {
                    // $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    showNotifications('error', 'API Key is required.');
                    return
                }
                data.apikey = {
                    'api_key': api_key
                };
                break;
            case 'MarketHero':
                var api_key = $('#' + responder).find('.api_key').val();
                if (api_key == '') {
                    $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    // showNotifications('error', 'API Key is required.');
                    return
                }
                data.apikey = {
                    'api_key': api_key
                };
                break;
            case 'Sharpspring':
                var account_id = $('#' + responder).find('.account_id').val();
                var secret_key = $('#' + responder).find('.secret_key').val();
                if (account_id == '') {
                    // $('#' + responder).find('.account_id').next('span').html('Account Id is required.');
                    showNotifications('error', 'Account Id is required.');
                    return
                }
                if (secret_key == '') {
                    // $('#' + responder).find('.secret_key').next('span').html('Secret Key is required.');
                    showNotifications('error', 'Secret Key is required.');
                    return
                }
                data.apikey = {
                    'account_id': account_id,
                    'secret_key': secret_key
                };
                break;
            case 'Drip':
                var account_id = $('#' + responder).find('.account_id').val();
                var api_token = $('#' + responder).find('.api_token').val();
                if (account_id == '') {
                    // $('#' + responder).find('.account_id').next('span').html('Account Id is required.');
                    showNotifications('error', 'Account Id is required.');
                    return
                }
                if (api_token == '') {
                    // $('#' + responder).find('.api_token').next('span').html('API Token is required.');
                    showNotifications('error', 'API Token is required.');
                    return
                }
                data.apikey = {
                    'account_id': account_id,
                    'api_token': api_token
                };
                break;
            case 'CustomHTML':
                var customhtml = $('#' + responder).find('.customhtml_code').val();
                var form_name = $('#' + responder).find('.form_name').val();
                if (form_name == '') {
                    // $('#' + responder).find('.form_name').next('span').html('Form Name is required.');
                    showNotifications('error', 'Form Name is required.');
                    return
                }
                if (customhtml == '') {
                    // $('#' + responder).find('.customhtml_code').next('span').html('Custom HTML Code is required.');
                    showNotifications('error', 'Custom HTML Code is required.');
                    return
                }
                var form_id = Math.floor(Math.random() * 1000) + 1;
                data.apikey = {
                    'form_id': form_id,
                    'customhtml': customhtml,
                    'form_name': form_name
                };
                localStorage.setItem('acy_customhtml', JSON.stringify(data.apikey));
                break;
            case 'Mailvio':
                var api_key = $('#' + responder).find('.api_key').val();
                if (api_key == '') {
                    // $('#' + responder).find('.api_key').next('span').html('API Key is required.');
                    showNotifications('error', 'API Key is required.');
                    return
                }
                data.apikey = {
                    'api_key': api_key
                };
                break;
            case 'Sendiio':
                var api_token = $('#' + responder).find('.api_token').val();
                var api_secret = $('#' + responder).find('.api_secret').val();
                if (api_key == '') {
                    showNotifications('error', 'API Token is required.');
                    return
                }
                if (api_secret == '') {
                    showNotifications('error', 'API Secret is required.');
                    return
                }
                data.apikey = {
                    'api_token': api_token,
                    'api_secret': api_secret
                };
                break;
            case 'SendFox':
                var access_token = $('#' + responder).find('.access_token').val();
                console.log(responder, access_token)
                if (access_token == '') {
                    $('#' + responder).find('.access_token').next('span').html('Access Token is required.');
                    $.brander.loader(0);
                    return;
                }
                data.apikey = {
                    'access_token': access_token
                };
                break;
                
        }
        $.ajax({
            type: 'post',
            url: baseurl + 'ar_setting/autoresponder',
            data: data,
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status) {
                    showNotifications('success',result.msg);
                    // window.appowls.notifyMessage('success', result.msg);
                    $('#integration_popup').modal('hide');
                } else {
                    showNotifications('error',result.msg);
                    // showNotifications('error', result.msg);
                }
                setTimeout(function(){ location.reload(); }, 3000);
            }
        })
    });

    var autoResponderSelected = '';
    $(document).on('change', 'input[name="autoresponder[]"]', function (e) {
        autoResponderSelected = $(this).attr('id');
        
        console.log($(this));
        // return false;
        $('#confirmPopupBtn').val($(this).val());
        if ($(this).prop('checked')) {
            var a = $(this).val();
        // console.log(a);
            $('#integration_popup').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#integration_popup .modal-body > .acy_append_customHtml').addClass('d-none');
            $('#integration_popup .modal-body > form').addClass('d-none');
            $('#integration_popup .modal-body > .acy_append_customHtml#' + a).removeClass('d-none');
            $('#integration_popup .modal-body > form#' + a).removeClass('d-none');

            if ($('.ms_auto_list_popup').length) {
                $('.ms_auto_list_popup > .autoRespSetting').hide();
                $('.ms_autores_back').removeClass('d-none');
            }
        } else {
            if ($(this).val() == 'CustomHTML') {
                $('#integration_popup').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#integration_popup .modal-body > form').addClass('d-none');
                $('#integration_popup').addClass('acy_active');
                $('#integration_popup').find('div[id="' + $(this).val() + '"]').removeClass('d-none');
                $('.acy_custom_html_appended').find('.custom_html').removeClass('d-none');
                $('.acy_custom_html').find('span.acy_error_msg').html('');
            } else {
                $(this).prop('checked', true);
                var _this = $(this);
                $('#confirmationPopup').find('div.acy_model_dltbtn a.red').attr('data-action', 'AR');
                $('#confirmationPopup').find('div.acy_model_dltbtn a.red').attr('data-value', $(this).val());
                // $('#confirmationPopup').find('div.acy_model_dltbtn a.red').attr('data-this',_this);
                $('#confirmPopup').modal('show');
                $(document).on('click','#confirmPopupBtn',function(){
                let ar = $('#confirmPopupBtn').val();
                // console.log($('#confirmPopupBtn').val()); return false;
                    $.ajax({
                        url: baseurl + 'ar_setting/disconnect_autoresponder', 
                        type: "GET",             
                        data: {'ar':ar},
                        success: function(data) {
                            var resp = typeof data != "object" ? JSON.parse(data) : data;
                            $('#' + autoResponderSelected).prop('checked', false)
                            showNotifications('success', resp.msg);
                            // if(_this.val() == 'AWeber'){
                            //     _this.closest('.ao_autoresponder').attr('href', $('#aweberURL').val()).addClass('ao_autoresponder_anchor');
                            // }
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                    });
                });
            }
        }
        if ($(this).val() == 'CustomHTML') {
            $('.acy_append_customHtml').removeClass('d-none');
            if ($('.ms_auto_list_popup').length) {
                $('.ms_auto_list_popup > .autoRespSetting').hide();
                $('.ms_autores_back').removeClass('d-none');

            }
        }

    });


    // if ($('.ms_auto_list_popup').length) {
    //     $('.ms_autores_back').on('click', function() {
    //         $('.ms_auto_list_popup > .autoRespSetting').css('display', 'flex');
    //         $(this).addClass('d-none');
    //         var aid = $("#integration_popup .modal-body form:visible").attr('id');
    //         $('input[value="' + aid + '"]').prop('checked', false);
    //         $('#integration_popup .modal-body > form').addClass('d-none');
    //         $('.acy_append_customHtml').addClass('d-none');

    //         if (autoResponderSelected == 'customhtml' && $('.acy_custom_html').length) {
    //             $('#' + autoResponderSelected).prop('checked', true);
    //         } else
    //             $('#' + autoResponderSelected).prop('checked', false);
    //     });
    // }

    // $('.ms_add_auto_popup').on('click', function() {
    //     $('#add_autoresponder_popup').modal({
    //         backdrop: 'static',
    //         keyboard: false
    //     });
    //     $('.ms_auto_list_popup > .autoRespSetting').css('display', 'flex');
    //     $('.ms_autores_back').addClass('d-none');
    //     var aid = $("#integration_popup .modal-body form:visible").attr('id');
    //     $('input[value="' + aid + '"]').prop('checked', false);
    //     $('#integration_popup .modal-body > form').addClass('d-none');
    //     $('.acy_append_customHtml').addClass('d-none');
    // });

    $('body').on('click', '.close_autoresponder', function () {
        console.log(418)
        console.log($('#' + autoResponderSelected), autoResponderSelected);
        if (autoResponderSelected == 'customhtml' && $('.acy_custom_html').length) {
            $('#' + autoResponderSelected).prop('checked', true);
        } else
            $('#' + autoResponderSelected).prop('checked', false);
    });

    $('.acy_add_customHtml_form').click(function (e) {
        e.preventDefault();
        console.log(428)
        var max = 0;
        if ($('.acy_custom_html').length) {
            $('.acy_custom_html').each(function () {
                max = Math.max($(this).attr('data-form_id'), max);
            });
            // console.log(max)
            var id = parseInt(max) + parseInt(1);
        } else
            var id = parseInt($('#custom_form_id').val()) + parseInt(1);

        var html = '';
        html += `<div class="acy_custom_html" data-form_id="${id}">
                    <a class="acy_close_custom_html_form close_custom_form" href="" title="Remove Form"><img src="${baseurl + 'assets/images/delete.svg'}"></a>
                    <form action="" id="CustomHTML_${id}" class="custom_html">
                        <div class="hs_right_section">
                            <div class="model_filde_wrap">
                                <div class="form-group">
                                    <label class="ao_label">Form Name </label>
                                    <input type="text" value="" placeholder="Enter Form Name" class="form_name form-control ao_input">
                                    <span class="acy_info">
                                    </span>
                                </div>
                            </div>
                            <div class="model_filde_wrap">
                                <div class="form-group">
                                    <label class="ao_label">Enter your custom HTML form below </label><textarea class="customhtml_code form-control ao_input" placeholder="Enter Custom HTML Code"></textarea>
                                    <span class="acy_info"></span>
                                </div>
                            </div>
                            <div class="model_filde_wrap">
                                <div class="form-group">
                                    <input class="acy_btn dark s_btn acy_store_customhtml" data-type="1" type="submit" data-responder="CustomHTML_${id}">
                                    <a href="javascript:;" class="acy_btn ao_redBtn  s_btn ml-2" data-dismiss="modal">Cancel</a>
                                </div>    
                            </div>
                        </div>
                    </form>
                </div>`;
        $('.acy_custom_html_appended').prepend(html)
    });

    $(document).on('click', '.acy_close_custom_html_form', function (e) {
        e.preventDefault();
        console.log(472)
        $(this).parent().remove()
    });

    $(document).on('click', '.acy_remove_customHTML', function (e) {
        e.preventDefault();
        console.log(478)
        var obj = $(this);
        var form_id = $(this).parent('.acy_custom_html').attr('data-form_id');
        // console.log(form_id)
        $('#confirmationPopup').find('div.acy_model_dltbtn a.red').attr('data-action', 'customHTML-delete');
        $('#confirmationPopup').find('div.acy_model_dltbtn a.red').attr('data-form-id', form_id);
        window.appowls.deletePopup({}, (resp) => {
            if (resp) {
            } else {
                $(this).prop('checked', true)
            }
        });
    });

    $(document).on('click', '#confirmationPopup [data-action="customHTML-delete"]', function () {
        var _this = $(this);
        console.log(494)
        var data = {
            'form_id': $(this).attr('data-form-id'),
            'remove': !0
        };
        $.ajax({
            type: 'post',
            url: baseurl + 'ajax/ar_setting/custom_html',
            data: data,
            success: function (data) {
                var result = jQuery.parseJSON(data);
                // console.log(result); return;
                if (result.status) {
                    $('.acy_custom_html_appended [data-form_id="' + _this.attr('data-form-id') + '"]').remove();
                    window.appowls.notifyMessage('success', result.msg);
                    $('#custom_form_id').val(result.id)
                } else {
                    showNotifications('error', result.msg);
                }
                $('#confirmationPopup').modal('toggle');
            }
        })
    })

    $(document).on('click', '.acy_store_customhtml', function (e) {
        e.preventDefault();
        console.log(520)
        var responder = $(this).data('responder');
        var customhtml = $('#' + responder).find('.customhtml_code').val();
        var form_name = $('#' + responder).find('.form_name').val();
        var form_id = $(this).parents('.acy_custom_html').data('form_id');
        var entry_type = $(this).parents('.acy_custom_html').find('.acy_store_customhtml').attr('data-type');

        if (form_name == '') {
            // $('#' + responder).find('.form_name').next('span').html('Form Name is required.');
            showNotifications('error', 'Form Name is required.');
            return;
        }
        if (customhtml == '') {
            // $('#' + responder).find('.customhtml_code').next('span').html('Custom HTML Code is required.');
            showNotifications('error', 'Custom HTML Code is required.');
            return;
        }
        var data = {
            'form_id': form_id,
            'customhtml': customhtml,
            'form_name': form_name,
            'add': !0,
            'entry_type': entry_type
        };
        var obj = $(this);
        $.ajax({
            type: 'post',
            url: baseurl + 'ajax/ar_setting/custom_html',
            data: data,
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status) {
                    if ($('div #customhtml').val() == 'CustomHTML')
                        $('div #customhtml').prop('checked', true);
                    obj.parents('.acy_custom_html').find('a').addClass('acy_remove_customHTML').removeClass('acy_close_custom_html_form');
                    window.appowls.notifyMessage('success', result.msg);
                    if (entry_type == 1) {
                        $('#custom_form_id').val(result.id)
                        obj.parents('.acy_custom_html').attr('data-form_id', result.id)
                    }
                    // if($('.editPageAutoResponder').length){
                    //     $('#autoresponderSettingModal').modal('toggle');
                    //     $('.autoresponder_list').find('select').append(new Option(form_name, result.id, !0, !0)).trigger('change');
                    //     if($('select[name="autoresponder_name"],select[name="contact_form"]').find('option[value="CustomHTML"]').html()=='' || typeof $('select[name="autoresponder_name"],select[name="contact_form"]').find('option[value="CustomHTML"]').html()== 'undefined'){
                    //         $('select[name="autoresponder_name"],select[name="contact_form"]').append(new Option('Custom HTML', 'CustomHTML', !0, !0)).trigger('change');
                    //     }else{
                    //         $('select[name="autoresponder_name"],select[name="contact_form"]').val('CustomHTML').trigger('change');
                    //     }
                    // }
                    obj.parents('.acy_custom_html').find('.acy_store_customhtml').attr('data-type', '2');
                } else {
                    showNotifications('error', result.msg);
                }
            }
        })
    });

    if($('.ao_sidebar_content.profile_account').length){
        console.log(578)
        var menu = location.href.split('#')[1];
        setTimeout(() => {
            $('.ao_sidebar_content.profile_account [data-menu="'+menu+'"]').trigger('click');
        }, 300);
    }

})


