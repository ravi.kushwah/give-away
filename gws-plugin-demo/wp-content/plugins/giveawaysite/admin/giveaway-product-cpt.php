<?php
// Product Post type
function giveaway_product_post_type() {

	$labels = array(
		'name'                  => _x( 'Products', 'giveaway' ),
		'singular_name'         => _x( 'Product', 'giveaway' ),
		'menu_name'             => __( 'Products', 'giveaway' ),
		'name_admin_bar'        => __( 'Products', 'giveaway' ),
		'archives'              => __( 'Product Archives', 'giveaway' ),
		'attributes'            => __( 'Product Attributes', 'giveaway' ),
		'parent_item_colon'     => __( 'Parent Product:', 'giveaway' ),
		'all_items'             => __( 'All Product', 'giveaway' ),
		'add_new_item'          => __( 'Add New Product', 'giveaway' ),
		'add_new'               => __( 'Add New', 'giveaway' ),
		'new_item'              => __( 'New Product', 'giveaway' ),
		'edit_item'             => __( 'Edit Product', 'giveaway' ),
		'update_item'           => __( 'Update Product', 'giveaway' ),
		'view_item'             => __( 'View Product', 'giveaway' ),
		'view_items'            => __( 'View Products', 'giveaway' ),
		'search_items'          => __( 'Search Product', 'giveaway' ),
		'not_found'             => __( 'Not found', 'giveaway' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'giveaway' ),
		'featured_image'        => __( 'Featured Image', 'giveaway' ),
		'set_featured_image'    => __( 'Set featured image', 'giveaway' ),
		'remove_featured_image' => __( 'Remove featured image', 'giveaway' ),
		'use_featured_image'    => __( 'Use as featured image', 'giveaway' ),
		'insert_into_item'      => __( 'Insert into product', 'giveaway' ),
		'uploaded_to_this_item' => __( 'Uploaded to this product', 'giveaway' ),
		'items_list'            => __( 'Products list', 'giveaway' ),
		'items_list_navigation' => __( 'Products list navigation', 'giveaway' ),
		'filter_items_list'     => __( 'Filter products list', 'giveaway' ),
	);
	$args = array(
		'label'                 => __( 'Product', 'giveaway' ),
		'description'           => __( 'Product Description', 'giveaway' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'giveaway_product', $args );
}
add_action( 'init', 'giveaway_product_post_type', 0 );

// Product Category Taxonomy
function giveaway_product_category_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Category', 'Taxonomy General Name', 'giveaway' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'giveaway' ),
		'menu_name'                  => __( 'Category', 'giveaway' ),
		'all_items'                  => __( 'All Categories', 'giveaway' ),
		'parent_item'                => __( 'Parent Category', 'giveaway' ),
		'parent_item_colon'          => __( 'Parent Category:', 'giveaway' ),
		'new_item_name'              => __( 'New Category Name', 'giveaway' ),
		'add_new_item'               => __( 'Add New Category', 'giveaway' ),
		'edit_item'                  => __( 'Edit Category', 'giveaway' ),
		'update_item'                => __( 'Update Category', 'giveaway' ),
		'view_item'                  => __( 'View Category', 'giveaway' ),
		'separate_items_with_commas' => __( 'Separate Categories with commas', 'giveaway' ),
		'add_or_remove_items'        => __( 'Add or remove Categories', 'giveaway' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'giveaway' ),
		'popular_items'              => __( 'Popular Categories', 'giveaway' ),
		'search_items'               => __( 'Search Categories', 'giveaway' ),
		'not_found'                  => __( 'Not Found', 'giveaway' ),
		'no_terms'                   => __( 'No Categories', 'giveaway' ),
		'items_list'                 => __( 'Categories list', 'giveaway' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'giveaway' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'gw_product_category', array( 'giveaway_product' ), $args );
}
add_action( 'init', 'giveaway_product_category_taxonomy', 0 );

// Product Tag Taxonomy
function giveaway_product_tags_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Tag','giveaway' ),
		'singular_name'              => _x( 'Tag','giveaway' ),
		'menu_name'                  => __( 'Tag', 'giveaway' ),
		'all_items'                  => __( 'All Tags', 'giveaway' ),
		'parent_item'                => __( 'Parent Tag', 'giveaway' ),
		'parent_item_colon'          => __( 'Parent Tag:', 'giveaway' ),
		'new_item_name'              => __( 'New Tag Name', 'giveaway' ),
		'add_new_item'               => __( 'Add New Tag', 'giveaway' ),
		'edit_item'                  => __( 'Edit Tag', 'giveaway' ),
		'update_item'                => __( 'Update Tag', 'giveaway' ),
		'view_item'                  => __( 'View Tag', 'giveaway' ),
		'separate_items_with_commas' => __( 'Separate tag with commas', 'giveaway' ),
		'add_or_remove_items'        => __( 'Add or remove tags', 'giveaway' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'giveaway' ),
		'popular_items'              => __( 'Popular Tags', 'giveaway' ),
		'search_items'               => __( 'Search Tags', 'giveaway' ),
		'not_found'                  => __( 'Not Found', 'giveaway' ),
		'no_terms'                   => __( 'No Tags', 'giveaway' ),
		'items_list'                 => __( 'Tags list', 'giveaway' ),
		'items_list_navigation'      => __( 'Tags list navigation', 'giveaway' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'gw_product_tags', array( 'giveaway_product' ), $args );
}
add_action( 'init', 'giveaway_product_tags_taxonomy', 0 );