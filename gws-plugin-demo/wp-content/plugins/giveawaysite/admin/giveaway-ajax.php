<?php 
// Single Product Banner
add_action('wp_ajax_giveaway_save_settings' , 'giveaway_save_settings');
add_action('wp_ajax_nopriv_giveaway_save_settings','giveaway_save_settings');
function giveaway_save_settings(){
    update_option('top_banner',$_POST['top_banner_img']); 
    update_option('bottom_banner',$_POST['bottom_banner_img']); 
    update_option('gw_color',$_POST['color']);
    update_option('top_banner_link',$_POST['top_banner_link']); 
    update_option('bottom_banner_link',$_POST['bottom_banner_link']); 
    update_option('gw_site_name',$_POST['gw_site_name']); 
    $datas = array('status' => 'true');
    echo json_encode($datas);
    die();
}


// Single Product Banner
add_action('wp_ajax_giveaway_api_add' , 'giveaway_api_add');
add_action('wp_ajax_nopriv_giveaway_api_add','giveaway_api_add');
function giveaway_api_add(){
    $apikey = $_POST['apikey'];
    if(!empty($apikey)){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://giveawaysites.co.in/Api/VerifyAPIKey',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('key' => $apikey),
            CURLOPT_HTTPHEADER => array(
                'Cookie: ci_session=1cf990a2386c3cd4c06546f851fd21cd8bbffa46'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response);
        if($data->status == 1){
            update_option('web_id',$data->data[0]->w_id); 
            update_option('web_userid',$data->data[0]->w_userid); 
            update_option('web_siteurl',$data->data[0]->w_siteurl);
            update_option('api_key',$apikey);
            if(!empty($data->data[0]->customHTMLnews)){
                update_option('customHTMLnews',1);
                update_option('customHTMLnews_html',$data->data[0]->customHTMLnews);
            }
            if($data->data[0]->w_type == 1){
                $link = '';
                if($type == 'product'){
                    $link = 'https://giveawaysites.co.in/Api/general_giveAways';
                } else {
                    $link = 'https://giveawaysites.co.in/Api/gmaes_giveAways';
                }

                $curl = curl_init();
                    curl_setopt_array($curl, array(
                    CURLOPT_URL => $link,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Cookie: ci_session=8af92b98e60e26256c35713a1d0902e0c2204a94'
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                $data = json_decode($response);
                $i = 0;
                if($data->status == 1){
                    $data_type = '';
                    if($data->type == 'General GiveAways'){
                        $data_type = 'Product';
                        foreach($data->Data as $datas){
                            $terms = get_terms([
                                'taxonomy' => 'gw_product_category',
                                'hide_empty' => false,
                            ]);
                            $cat_array = array();
                            foreach($terms as $term){
                                $cat_array[] = $term->name;
                            }
                            $id = wp_insert_post(array(
                                'post_type' => 'giveaway_product',
                                'post_title' => $datas->g_offer_name,
                                'post_status' => 'publish',
                                'post_content' => '',
                            ));
                            if(!empty($id)){
                                $term_id = array();
                                if (in_array($datas->cate_name, $cat_array)){
                                    $terms = get_terms([
                                        'taxonomy' => 'gw_product_category',
                                        'hide_empty' => false,
                                    ]);
                                    foreach($terms as $term){
                                        if($term->name == $datas->cate_name){
                                            $term_id['term_id'] = $term->term_id;
                                            $term_id['term_taxonomy_id'] = $term->term_taxonomy_id;
                                        }
                                    }
                                } else {
                                    $term_id = wp_insert_term(
                                        $datas->cate_name,   // the term 
                                        'gw_product_category', // the taxonomy
                                        array(
                                            'slug'=> $datas->cate_name,
                                        )
                                    );
                                }
                                if(!empty($term_id)){
                                    wp_set_post_terms( $id, $term_id, 'gw_product_category');
                                }                        
                                update_post_meta( $id, 'giveaway_productpayout', $datas->g_payout);
                                update_post_meta( $id, 'giveaway_product_location', $datas->g_countries);
                                update_post_meta( $id, 'giveaway_productlast_date', $datas->g_last_update);
                                update_post_meta( $id, 'giveaway_product_type', 1);
                                update_post_meta( $id, 'giveaway_product_img_link', $datas->g_priview);
                            }
                            $i++;
                        }
                    } else {
                        $data_type = 'Games';
                        foreach($data->Data as $datas){
                            $terms = get_terms([
                                'taxonomy' => 'gw_product_category',
                                'hide_empty' => false,
                            ]);
                            $cat_array = array();
                            foreach($terms as $term){
                                $cat_array[] = $term->name;
                            }
                            $id = wp_insert_post(array(
                                'post_type' => 'giveaway_product',
                                'post_title' => $datas->title,
                                'post_status' => 'publish',
                                'post_content' => '',
                            ));
                            if(!empty($id)){
                                $term_id = array();
                                if (in_array($datas->platform, $cat_array)){
                                    $terms = get_terms([
                                        'taxonomy' => 'gw_product_category',
                                        'hide_empty' => false,
                                    ]);
                                    foreach($terms as $term){
                                        if($term->name == $datas->platform){
                                            $term_id['term_id'] = $term->term_id;
                                            $term_id['term_taxonomy_id'] = $term->term_taxonomy_id;
                                        }
                                    }
                                } else {
                                    $term_id = wp_insert_term(
                                        $datas->platform,   // the term 
                                        'gw_product_category', // the taxonomy
                                        array(
                                            'slug'=> $datas->platform,
                                        )
                                    );
                                }
                                if(!empty($term_id)){
                                    wp_set_post_terms( $id, $term_id, 'gw_product_category');
                                }                        
                                update_post_meta( $id, 'giveaway_productpayout', $datas->worth);
                                update_post_meta( $id, 'giveaway_product_location', $datas->type);      
                                update_post_meta( $id, 'giveaway_productlast_date', '');                      
                                update_post_meta( $id, 'giveaway_product_type', 0);                            
                                update_post_meta( $id, 'giveaway_product_img_link', $datas->thumbnail);
                            }
                            $i++;
                        }
                    }
                }
            }
        }
        $data_text = array('status' => 'true', 'msg'=>'API Key Connect Successfully');
    } else {
        $data_text = array('status' => 'false', 'msg'=>'Invalid API Key');
    }
    echo json_encode($data_text);
    die();
}
// Single Product Banner
add_action('wp_ajax_giveaway_product_import' , 'giveaway_product_import');
add_action('wp_ajax_nopriv_giveaway_product_import','giveaway_product_import');
function giveaway_product_import(){
    $type = $_POST['type'];
    if(!empty($type)){
        $link = '';
        if($type == 'product'){
            $link = 'https://giveawaysites.co.in/Api/general_giveAways';
        } else {
            $link = 'https://giveawaysites.co.in/Api/gmaes_giveAways';
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $link,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: ci_session=8af92b98e60e26256c35713a1d0902e0c2204a94'
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response);
        $i = 0;
        if($data->status == 1){
            $data_type = '';
            if($data->type == 'General GiveAways'){
                $data_type = 'Product';
                foreach($data->Data as $datas){
                    $terms = get_terms([
                        'taxonomy' => 'gw_product_category',
                        'hide_empty' => false,
                    ]);
                    $cat_array = array();
                    foreach($terms as $term){
                        $cat_array[] = $term->name;
                    }
                    $id = wp_insert_post(array(
                        'post_type' => 'giveaway_product',
                        'post_title' => $datas->g_offer_name,
                        'post_status' => 'publish',
                        'post_content' => '',
                    ));
                    if(!empty($id)){
                        $term_id = array();
                        if (in_array($datas->cate_name, $cat_array)){
                            $terms = get_terms([
                                'taxonomy' => 'gw_product_category',
                                'hide_empty' => false,
                            ]);
                            foreach($terms as $term){
                                if($term->name == $datas->cate_name){
                                    $term_id['term_id'] = $term->term_id;
                                    $term_id['term_taxonomy_id'] = $term->term_taxonomy_id;
                                }
                            }
                        } else {
                            $term_id = wp_insert_term(
                                $datas->cate_name,   // the term 
                                'gw_product_category', // the taxonomy
                                array(
                                    'slug'=> $datas->cate_name,
                                )
                            );
                        }
                        if(!empty($term_id)){
                            wp_set_post_terms( $id, $term_id, 'gw_product_category');
                        }                        
                        update_post_meta( $id, 'giveaway_productpayout', $datas->g_payout);
                        update_post_meta( $id, 'giveaway_product_location', $datas->g_countries);
                        update_post_meta( $id, 'giveaway_productlast_date', $datas->g_last_update);
                        update_post_meta( $id, 'giveaway_product_type', 1);
                        update_post_meta( $id, 'giveaway_product_img_link', $datas->g_priview);
                    }
                    $i++;
                }
            } else {
                $data_type = 'Games';
                foreach($data->Data as $datas){
                    $terms = get_terms([
                        'taxonomy' => 'gw_product_category',
                        'hide_empty' => false,
                    ]);
                    $cat_array = array();
                    foreach($terms as $term){
                        $cat_array[] = $term->name;
                    }
                    $id = wp_insert_post(array(
                        'post_type' => 'giveaway_product',
                        'post_title' => $datas->title,
                        'post_status' => 'publish',
                        'post_content' => '',
                    ));
                    if(!empty($id)){
                        $term_id = array();
                        if (in_array($datas->platform, $cat_array)){
                            $terms = get_terms([
                                'taxonomy' => 'gw_product_category',
                                'hide_empty' => false,
                            ]);
                            foreach($terms as $term){
                                if($term->name == $datas->platform){
                                    $term_id['term_id'] = $term->term_id;
                                    $term_id['term_taxonomy_id'] = $term->term_taxonomy_id;
                                }
                            }
                        } else {
                            $term_id = wp_insert_term(
                                $datas->platform,   // the term 
                                'gw_product_category', // the taxonomy
                                array(
                                    'slug'=> $datas->platform,
                                )
                            );
                        }
                        if(!empty($term_id)){
                            wp_set_post_terms( $id, $term_id, 'gw_product_category');
                        }                        
                        update_post_meta( $id, 'giveaway_productpayout', $datas->worth);
                        update_post_meta( $id, 'giveaway_product_location', $datas->type);      
                        update_post_meta( $id, 'giveaway_productlast_date', '');                      
                        update_post_meta( $id, 'giveaway_product_type', 0);                            
                        update_post_meta( $id, 'giveaway_product_img_link', $datas->thumbnail);
                    }
                    $i++;
                }
            }
            $data_text = array('status' => 'true', 'msg'=> $i++.' '.$data_type.' Imported Successfully');
        } else {
            $data_text = array('status' => 'false', 'msg'=> 'Invalid Api Links');
        }
    }
    echo json_encode($data_text);
    die();
}