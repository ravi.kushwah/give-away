<?php
// Enqueue Css and js file
add_action( 'admin_enqueue_scripts', 'giveaway_admin_assets' );
function giveaway_admin_assets() {
    // Css
    wp_enqueue_style('bootstrap', GS_PLUGIN_URL . '/includes/assets/css/bootstrap.min.css', array(), '1.0.0', 'all');
    wp_enqueue_style('toastr.min', GS_PLUGIN_URL . '/includes/assets/css/toastr.min.css', array(), '1.0.0', 'all');
    wp_enqueue_style('admin-custom-style', GS_PLUGIN_URL . '/includes/assets/css/admin-custom-style.css', array(), '1.0.0', 'all');

    // js
    wp_enqueue_media();
    wp_enqueue_script( 'bootstrap', GS_PLUGIN_URL . '/includes/assets/js/bootstrap.bundle.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script( 'toastr.min', GS_PLUGIN_URL . '/includes/assets/js/toastr.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script( 'admin-custum-script', GS_PLUGIN_URL . '/includes/assets/js/admin-custum-script.js', array('jquery'), '1.0.0', true);    

    // ajax
	wp_localize_script( 'admin-custum-script', 'gw_ajaxurl', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}

// Plugin Activation Hook
register_activation_hook(__FILE__, 'giveaway_activation');
function giveaway_activation(){
    // Don't forget to exit() because wp_redirect doesn't exit automatically
    exit( wp_redirect( admin_url( 'admin.php?page=giveaway' ) ) );
}

// Product Post type
require GS_PLUGIN_PATH . '/admin/giveaway-product-cpt.php';

// Product Post type Metabox
require GS_PLUGIN_PATH . '/admin/giveaway-product-meta.php';

// Giveaway widget register
require GS_PLUGIN_PATH . '/admin/giveaway-widget.php';

// Giveaway Ajax File
require_once GS_PLUGIN_PATH . '/admin/giveaway-ajax.php';

// Admin Menu
add_action( 'admin_menu', 'giveaway_admin_menu');
function giveaway_admin_menu(){
    add_menu_page(
        __( ' Giveaway', 'giveaway' ),
        __( 'Giveaway', 'giveaway' ),
        'manage_options',
        'giveaway',
        'giveaway_main_page_callback', 
        GS_PLUGIN_URL.'/includes/assets/images/Giveaway-Sites-icon.png',
        6
    );
}

function giveaway_main_page_callback(){
    echo '<div class="gw_main_wrapper">
        <div class="gw_inner_wrap">
            <div class="gw_head_wrap">
                <a href="javascript:;">
                    <img src="'.GS_PLUGIN_URL.'/includes/assets/images/Giveaway-Sites-Logo.png">
                </a>
                <p>'.esc_html('GiveSites Pro is your one-stop source for incredible online giveaways, freebies, sweepstakes, contests or competitions.', 'giveaway').'</p>
            </div>
            <div class="gw_api_form_wrap">
                <label>'.esc_html('Add Your API Key').'</label>
                <div class="gw_api_form_field">';
                    $api_key = get_option('api_key');
                    $bt_name = !empty($api_key) ? 'Connected' : 'Connect';
                    echo '<input type="text" placeholder="'.esc_html('API Key', 'giveaway').'" id="gw_api_link" value="'.esc_html($api_key).'" class="gw_api_text_field">';
                    echo '<a href="javascript:;" id="gw_api_import" class="gw_btn">
                    <span class="loading" style="display: none;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-loader"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg> Loading..
                    </span><span class="gw_bt_text">'.esc_html($bt_name).'</span></a>
                </div>
            </div>';

            if(!empty($api_key)){ 
                echo '<div class="gw_tab_wrapper">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="settings-tab" data-bs-toggle="tab" data-bs-target="#settings" type="button" role="tab" aria-controls="settings" aria-selected="true">'.esc_html('General settings', 'giveaway').'</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="shortcode-tab" data-bs-toggle="tab" data-bs-target="#shortcode" type="button" role="tab" aria-controls="shortcode" aria-selected="false">'.esc_html('Shortcode', 'giveaway').'</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="widgets-tab" data-bs-toggle="tab" data-bs-target="#widgets" type="button" role="tab" aria-controls="widgets" aria-selected="false">'.esc_html('Widgets', 'giveaway').'</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="video-tab" data-bs-toggle="tab" data-bs-target="#video" type="button" role="tab" aria-controls="video" aria-selected="false">'.esc_html('Video', 'giveaway').'</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                            <div class="gw_section_wrap">
                                    <h4 class="gw_title">'.esc_html('General Settings', 'giveaway').'</h4>
                                
                                    <div class="gw_general_setting">
                                        <div class="gw_form_field_wrap">
                                            <div class="row">       
                                                <div class="col-xl-6 col-lg-12">
                                                    <div class="form-group gw_color_piker form-top-fields">
                                                        <label class="col-form-label">'.esc_html('Choose Color', 'giveaway').'</label>';
                                                        $gw_color = get_option('gw_color');
                                                        $color = !empty($gw_color) ? $gw_color : '#000';
                                                        echo '<input type="color" value="'.esc_attr($color).'" id="gw_color_piker"/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <h4 class="gw_title">'.esc_html('Top Banner Settings', 'giveaway').'</h4>
                                        <div class="gw_form_field_wrap">
                                            <div class="row">
                                                <div class="col-xl-12 col-lg-12">
                                                    <div class="gw_affilate_link">
                                                        <label class="col-form-label">'.esc_html('Enter Banner Link', 'giveaway').'</label>';
                                                        $link = get_option('top_banner_link');
                                                        echo '<input type="text" id="gw_top_affilate_link" value="'.esc_url($link).'">
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-12">
                                                    <div class="gw_upload_wrapper">
                                                        <label class="col-form-label">'.esc_html('Single Product Top Ad Banner', 'giveaway').'</label>
                                                        <div class="gw_upload_box">
                                                            <div class="gw_upload_icon">                                                    
                                                                <a href="javascript:;" class="gw_top_ad_image" data-id="ad_top">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="59" height="50" viewBox="0 0 59 50">
                                                                        <path d="M47.177,41.803 L45.403,41.802 C44.557,41.802 43.868,41.120 43.868,40.281 C43.868,39.875 44.028,39.493 44.318,39.206 C44.608,38.919 44.993,38.761 45.403,38.761 L47.177,38.761 C49.537,38.761 51.750,37.842 53.410,36.172 C55.070,34.504 55.964,32.297 55.928,29.960 C55.855,25.254 51.876,21.427 47.057,21.427 L44.577,21.427 L44.577,18.293 C44.577,9.921 37.766,3.079 29.393,3.041 L29.322,3.041 C21.472,3.041 14.948,8.865 14.148,16.587 L14.018,17.847 L12.743,17.948 C7.320,18.377 3.071,22.941 3.071,28.337 C3.071,34.077 7.786,38.753 13.582,38.761 L13.713,38.761 C14.561,38.761 15.250,39.443 15.250,40.281 C15.250,41.120 14.561,41.802 13.715,41.802 L13.598,41.803 C6.099,41.803 -0.000,35.762 -0.000,28.337 C-0.000,24.931 1.288,21.679 3.625,19.183 C5.537,17.142 7.991,15.756 10.722,15.174 L11.253,15.061 L11.361,14.534 C12.138,10.729 14.091,7.332 17.007,4.709 C20.384,1.672 24.758,-0.000 29.323,-0.000 C39.157,-0.000 47.375,7.918 47.642,17.651 L47.662,18.373 L48.387,18.446 C54.437,19.057 59.000,24.064 59.000,30.094 C59.000,36.550 53.696,41.803 47.177,41.803 ZM41.151,31.226 L40.696,31.440 C39.207,32.141 37.425,31.837 36.261,30.684 L31.095,25.568 L31.095,48.479 C31.095,49.318 30.406,50.000 29.559,50.000 C28.712,50.000 28.024,49.318 28.024,48.479 L28.024,25.568 L22.858,30.684 C22.121,31.413 21.142,31.815 20.100,31.815 C19.510,31.815 18.945,31.689 18.420,31.441 L17.967,31.226 L29.559,19.746 L41.151,31.226 Z" class="cls-1"></path>
                                                                    </svg>
                                                                </a>
                                                            </div>
                                                            <div class="gw_upload_detail">
                                                                <h6>'.esc_html('Supports: JPG, JPEG, PNG', 'giveaway').'</h6>
                                                                <p>'.esc_html('Recommended Size - 1070x190 px').'</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-12">
                                                    <div class="gw_upload_wrapper">
                                                        <label class="col-form-label">'.esc_html('Ad Preview', 'giveaway').'</label>
                                                        <div class="gw_upload_box gw_upload_center">
                                                            <span class="gw_close_btn deletebanner" type="logo" data-id="ad_top" data-url="'.GS_PLUGIN_URL.'">×</span>';
                                                            $image_id = get_option('top_banner');
                                                            $image = wp_get_attachment_image_src( $image_id, 'full' );
                                                            if(!empty($image[0])){
                                                                echo '<img src="'.esc_url($image[0]).'" class="RemoveLogo" id="gw_top_ad_img_preview" alt="Logo Preview">
                                                                <input type="hidden" id="gw_top_ad_img_id" value="'.esc_attr($image_id).'">';
                                                            } else {
                                                                echo '<img src="'.GS_PLUGIN_URL.'/includes/assets/images/banner.jpg" class="RemoveLogo" id="gw_top_ad_img_preview" alt="Logo Preview">
                                                                <input type="hidden" id="gw_top_ad_img_id" value="">';
                                                            }
                                                        echo '</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <h4 class="gw_title">'.esc_html('Bottom Banner Settings', 'giveaway').'</h4>
                                        <div class="gw_form_field_wrap last_form_field">
                                            <div class="row">
                                                <div class="col-xl-12 col-lg-12">
                                                    <div class="gw_upload_wrapper">
                                                        <div class="gw_affilate_link">
                                                            <label class="col-form-label">'.esc_html('Enter Banner Link', 'giveaway').'</label>';
                                                            $link = get_option('bottom_banner_link');
                                                            echo '<input type="text" id="gw_bottom_affilate_link" value="'.esc_url($link).'">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-12">
                                                    <label class="col-form-label">'.esc_html('Single Product Bottom Ad Banner', 'giveaway').'</label>
                                                    <div class="gw_upload_box">
                                                        <div class="gw_upload_icon">
                                                            <a href="javascript:;" class="gw_top_ad_image" data-id="ad_bottom">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="59" height="50" viewBox="0 0 59 50">
                                                                    <path d="M47.177,41.803 L45.403,41.802 C44.557,41.802 43.868,41.120 43.868,40.281 C43.868,39.875 44.028,39.493 44.318,39.206 C44.608,38.919 44.993,38.761 45.403,38.761 L47.177,38.761 C49.537,38.761 51.750,37.842 53.410,36.172 C55.070,34.504 55.964,32.297 55.928,29.960 C55.855,25.254 51.876,21.427 47.057,21.427 L44.577,21.427 L44.577,18.293 C44.577,9.921 37.766,3.079 29.393,3.041 L29.322,3.041 C21.472,3.041 14.948,8.865 14.148,16.587 L14.018,17.847 L12.743,17.948 C7.320,18.377 3.071,22.941 3.071,28.337 C3.071,34.077 7.786,38.753 13.582,38.761 L13.713,38.761 C14.561,38.761 15.250,39.443 15.250,40.281 C15.250,41.120 14.561,41.802 13.715,41.802 L13.598,41.803 C6.099,41.803 -0.000,35.762 -0.000,28.337 C-0.000,24.931 1.288,21.679 3.625,19.183 C5.537,17.142 7.991,15.756 10.722,15.174 L11.253,15.061 L11.361,14.534 C12.138,10.729 14.091,7.332 17.007,4.709 C20.384,1.672 24.758,-0.000 29.323,-0.000 C39.157,-0.000 47.375,7.918 47.642,17.651 L47.662,18.373 L48.387,18.446 C54.437,19.057 59.000,24.064 59.000,30.094 C59.000,36.550 53.696,41.803 47.177,41.803 ZM41.151,31.226 L40.696,31.440 C39.207,32.141 37.425,31.837 36.261,30.684 L31.095,25.568 L31.095,48.479 C31.095,49.318 30.406,50.000 29.559,50.000 C28.712,50.000 28.024,49.318 28.024,48.479 L28.024,25.568 L22.858,30.684 C22.121,31.413 21.142,31.815 20.100,31.815 C19.510,31.815 18.945,31.689 18.420,31.441 L17.967,31.226 L29.559,19.746 L41.151,31.226 Z" class="cls-1"></path>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <div class="gw_upload_detail">
                                                            <h6>'.esc_html('Supports: JPG, JPEG, PNG', 'giveaway').'</h6>
                                                            <p>'.esc_html('Recommended Size - 1070x190 px').'</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-12">
                                                    <div class="gw_upload_wrapper">
                                                        <label class="col-form-label">'.esc_html('Ad Preview', 'giveaway').'</label>
                                                        <div class="gw_upload_box gw_upload_center">
                                                            <span class="gw_close_btn deletebanner" type="logo" data-id="ad_bottom" data-url="'.GS_PLUGIN_URL.'">×</span>';
                                                            $image_id = get_option('bottom_banner');
                                                            $image = wp_get_attachment_image_src( $image_id, 'full' );
                                                            if(!empty($image[0])){
                                                                echo '<img src="'.esc_url($image[0]).'" class="RemoveLogo" id="gw_top_bottom_img_preview" alt="Logo Preview"><input type="hidden" id="gw_top_bottom_img_id" value="'.esc_attr($image_id).'">';
                                                            } else {
                                                                echo '<img src="'.GS_PLUGIN_URL.'/includes/assets/images/banner.jpg" class="RemoveLogo" id="gw_top_bottom_img_preview" alt="Logo Preview">
                                                                <input type="hidden" id="gw_top_bottom_img_id" value="">';
                                                            }
                                                        echo '</div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12">
                                                    <a href="javascript:;" class="gw_btn mt-3" id="gw_save_settings">'.esc_html('Save Settings', 'giveaway').'</a>
                                                </div>                                    
                                            </div>
                                        </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="shortcode" role="tabpanel" aria-labelledby="shortcode-tab">
                            <div class="gw_section_wrap">
                                <h4 class="gw_title">'.esc_html('GiveSites Shortcode', 'giveaway').'</h4>
                                <div class="gw_main_shortcode">
                                    <div class="gw_shortcode_code_wrap">
                                        <span>'.esc_html('Main Shortcode', 'giveaway').'</span>
                                        <h5 class="gw_shortcode_code">[giveaway_product title="Section Title" style="style1" post=2 btn_title="View More" btn_link="https://www.google.com/" pagination=true]</h5>
                                    </div>
                                    <div class="gw_shortcode_code_wrap">
                                        <span>'.esc_html('Shortcode for php Files', 'giveaway').'</span>
                                        <h5 class="gw_shortcode_code">&lt;?php echo do_shortcode("[giveaway_product title="Section Title" style="style2" post=2 btn_title="View More" btn_link="https://www.google.com/" pagination=true]")?&gt;</h5>
                                    </div>
                                </div>
                                <div class="gw_shortcode_note_desc">
                                    <p><strong>'.esc_html('1. title').'</strong> : '.esc_html('Title depicts the Heading you can enter the desired heading, needs to be shown on page.', 'giveaway').'</p>
                                    <p><strong>'.esc_html('2. style').'</strong> : '.esc_html('Style depicts the GiveAway Style', 'giveaway').'</p>
                                    <p><strong>'.esc_html('3. post').'</strong> : '.esc_html('Enter the number of post need to show. Enter numerical value only.', 'giveaway').'</p>
                                    <p><strong>'.esc_html('4. btn_title').'</strong> : '.esc_html('Enter button title here', 'giveaway').'</p>
                                    <p><strong>'.esc_html('5. btn_link').'</strong> : '.esc_html('Enter the link where you want to redirect the page.', 'giveaway').'</p>
                                    <p><strong>'.esc_html('6. Pagination').'</strong> : '.esc_html('To enable pagination enter true, to disable enter false', 'giveaway').'</p>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="widgets" role="tabpanel" aria-labelledby="widgets-tab">
                            <div class="gw_section_wrap">
                                <h4 class="gw_title">'.esc_html('GiveSites Widget', 'giveaway').'<small>'.esc_html('You can change these settings from Widgets option in Appearance', 'giveaway').'</small></h4>
                
                                <div class="gw_main_widget_wrapper">
                                    <div class="gw_vertical_tab">
                                    <div class="d-flex align-items-start">
                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <button class="nav-link active" id="v-pills-product-tab" data-bs-toggle="pill" data-bs-target="#v-pills-product" type="button" role="tab" aria-controls="v-pills-product" aria-selected="true">
                                                '.esc_html('Product', 'giveaway').'
                                            </button>
                                            <button class="nav-link" id="v-pills-newsletter-tab" data-bs-toggle="pill" data-bs-target="#v-pills-newsletter" type="button" role="tab" aria-controls="v-pills-newsletter" aria-selected="false">
                                                '.esc_html('NewsLetter', 'giveaway').'
                                            </button>
                                            <button class="nav-link" id="v-pills-social-tab" data-bs-toggle="pill" data-bs-target="#v-pills-social" type="button" role="tab" aria-controls="v-pills-social" aria-selected="false">
                                                '.esc_html('Socials', 'giveaway').'
                                            </button>
                                        </div>
                                        <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade show active" id="v-pills-product" role="tabpanel" aria-labelledby="v-pills-product-tab">
                                                
                                                <div class="gw_widget_section">
                                                    <h5 class="gw_subtitle">
                                                        <img src="'.GS_PLUGIN_URL.'/includes/assets/images/product.png">
                                                    </h5>
                                                    <div class="gw_widget_description">
                                                        <div class="gw_widget_img">
                                                            <img src="'.GS_PLUGIN_URL.'/includes/assets/images/product_1.png">
                                                        </div>
                                                        <div class="gw_widget_des_note">
                                                            <p><strong>'.esc_html('1. Title').'</strong> '.esc_html('Enter Title here for the Product', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('2. Number Of Product').'</strong> '.esc_html('Please enter how much products do you want to show', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('3. Category').'</strong> '.esc_html('Select Category of the product which you want to show if you will not select random product will show.', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('4. Version').'</strong> '.esc_html('Please select the Light / Dark Mode/Version for the Product', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('5. Style').'</strong> '.esc_html('Please select the style of the Product Design', 'giveaway').'</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="v-pills-newsletter" role="tabpanel" aria-labelledby="v-pills-newsletter-tab">
                                                <div class="gw_widget_section">
                                                    <h5 class="gw_subtitle">
                                                    <img src="'.GS_PLUGIN_URL.'/includes/assets/images/newsletter.png">
                                                    </h5>
                                                    <div class="gw_widget_description">
                                                        <div class="gw_widget_img">
                                                            <img src="'.GS_PLUGIN_URL.'/includes/assets/images/newsletter_1.png">
                                                        </div>
                                                        <div class="gw_widget_des_note">
                                                            <p><strong>'.esc_html('1. Title').'</strong> '.esc_html('Enter Title here for the Newsletter', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('2. Text').'</strong> '.esc_html('Enter description here related to the Newsletter', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('3. Version').'</strong> '.esc_html('Please select the Light / Dark Mode/Version for the Newsletter', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('4. Style').'</strong> '.esc_html('Please select the style of the Newsletter Design', 'giveaway').'</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="v-pills-social" role="tabpanel" aria-labelledby="v-pills-social-tab">
                                            
                                                <div class="gw_widget_section">
                                                    <h5 class="gw_subtitle">
                                                        <img src="'.GS_PLUGIN_URL.'/includes/assets/images/social_w.png">
                                                    </h5>
                                                    <div class="gw_widget_description">
                                                        <div class="gw_widget_img">
                                                            <img src="'.GS_PLUGIN_URL.'/includes/assets/images/social_w_1.png">
                                                        </div>
                                                        <div class="gw_widget_des_note">
                                                            <p><strong>'.esc_html('1. Title').'</strong> '.esc_html('Enter Title here for the social icons', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('2. Facebook Url').'</strong> '.esc_html('Enter Facebook URL here for redirecting it to the facebook', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('3. Twitter Url').'</strong> '.esc_html('Enter Twitter URL here for redirecting it to the Twitter', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('4. Instagram Url').'</strong> '.esc_html('Enter Instagram URL here for redirecting it to the Instagram', 'giveaway').'</p>
                                                            <p><strong>'.esc_html('5. Linkedin Url').'</strong> '.esc_html('Enter Linkedin URL here for redirecting it to the Linkedin', 'giveaway').'</p>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                            <div class="gw_section_wrap">
                                <h4 class="gw_title">'.esc_html('See How it Works', 'giveaway').'</h4>
                            <div class="gw_video_wrapper">

                            <div class="gw_video_section">
                                <div style="padding: 23.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/752991225?h=c73ab31e8f&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                            </div>

                            </div>
                            </div>
                        </div>

                    </div>
                </div>';
            }
        echo '</div>
    </div>';
}

// Custom Css For color
add_action('wp_head', 'giveaway_custom_color_css');
function giveaway_custom_color_css() {
    $gw_color = get_option('gw_color');
    $color = !empty($gw_color) ? $gw_color : '#008fe5';
    echo '<style>
        :root {
            --gw-primary-color: '.$color.';
        }
    </style>';
}

function giveaway_widgets_init() {
 
    register_sidebar( array(
        'name' => __( 'GiveawaySites', 'giveaway' ),
        'id' => 'giveawaysites',
        'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'wpb' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}
 
add_action( 'widgets_init', 'giveaway_widgets_init' );