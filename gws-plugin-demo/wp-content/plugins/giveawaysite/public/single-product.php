<?php
// Single Post Giveaway Product
get_header();
$category = wp_get_object_terms(get_the_ID(), 'gw_product_category');
$cate_array = array();
foreach($category as $cate){
    $cate_array[] = $cate->name;
}
$count = get_post_meta( get_the_ID(), 'post_views_count', true );
$view = !empty($count) ? $count : 0;
$total_views = $view + 1; 
update_post_meta( get_the_ID(), 'post_views_count', $total_views);
$payout = get_post_meta( get_the_ID(), 'giveaway_productpayout', true );
$location = get_post_meta( get_the_ID(), 'giveaway_product_location', true );
$end_date = get_post_meta( get_the_ID(), 'giveaway_productend-date', true );
$affiliate_link = get_post_meta( get_the_ID(), 'giveaway_productaffiliate-link', true );
$type = get_post_meta( get_the_ID(), 'giveaway_product_type', true );
$image_ids = get_post_meta( get_the_ID(), 'giveaway_product_img_link', true );
?>
    <div class="gw_product_gallery">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-12 gw_product_box_list">
                    <div class="row">
                        <div class="col-xl-12">
                            <?php 
                                $image_id = get_option('top_banner');
                                $top_banner_link = get_option('top_banner_link');
                                $image = wp_get_attachment_image_src( $image_id, 'full' );
                                if(!empty($image[0])){
                                    echo '<div class="gw_product_single_add">
                                        <a href="'.esc_attr($top_banner_link).'" target="_blank"><img src="'.esc_url($image[0]).'" alt="product-img"></a>
                                    </div>';
                                }
                            ?>
                            <div class="gw_heading_wrapper">
                                <h2><?php echo esc_html(get_the_title()); ?></h2>
                            </div>
                            <div class="col-lg-12">
                                <div class="gw_article_box gw_horizontal_style gw_product_single_box">
                                    <div class="gw_news_img gw_animation_box">
                                        <span class="gw_product_list_img gw_animation">
                                            <?php 
                                                if(!empty($image_ids)){
                                                    echo '<div class="gw_product_single_add">
                                                        <a href="'.esc_attr($top_banner_link).'" target="_blank"><img src="'.esc_url($image_ids).'" class="gw_animation_img" alt="product-img"></a>
                                                    </div>';
                                                }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="gw_blog_content">
                                        <div class="gw_blog_btn">
                                            <span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17px" height="17px">
                                                        <path fill-rule="evenodd" fill="rgb(121, 121, 121)" d="M15.144,-0.000 L13.555,-0.000 L13.555,1.700 C13.555,2.040 13.290,2.267 13.025,2.267 C12.761,2.267 12.496,2.040 12.496,1.700 L12.496,-0.000 L4.021,-0.000 L4.021,1.700 C4.021,2.040 3.756,2.267 3.491,2.267 C3.226,2.267 2.961,2.040 2.961,1.700 L2.961,-0.000 L1.372,-0.000 C0.578,-0.000 -0.005,0.737 -0.005,1.700 L-0.005,3.740 L16.945,3.740 L16.945,1.700 C16.945,0.737 15.992,-0.000 15.144,-0.000 ZM-0.005,4.930 L-0.005,15.300 C-0.005,16.320 0.578,17.000 1.425,17.000 L15.197,17.000 C16.045,17.000 16.998,16.263 16.998,15.300 L16.998,4.930 L-0.005,4.930 ZM4.709,14.450 L3.438,14.450 C3.226,14.450 3.014,14.280 3.014,13.997 L3.014,12.580 C3.014,12.353 3.173,12.127 3.438,12.127 L4.762,12.127 C4.974,12.127 5.186,12.297 5.186,12.580 L5.186,13.997 C5.133,14.280 4.974,14.450 4.709,14.450 ZM4.709,9.350 L3.438,9.350 C3.226,9.350 3.014,9.180 3.014,8.897 L3.014,7.480 C3.014,7.253 3.173,7.027 3.438,7.027 L4.762,7.027 C4.974,7.027 5.186,7.197 5.186,7.480 L5.186,8.897 C5.133,9.180 4.974,9.350 4.709,9.350 ZM8.947,14.450 L7.623,14.450 C7.411,14.450 7.199,14.280 7.199,13.997 L7.199,12.580 C7.199,12.353 7.358,12.127 7.623,12.127 L8.947,12.127 C9.159,12.127 9.371,12.297 9.371,12.580 L9.371,13.997 C9.371,14.280 9.212,14.450 8.947,14.450 ZM8.947,9.350 L7.623,9.350 C7.411,9.350 7.199,9.180 7.199,8.897 L7.199,7.480 C7.199,7.253 7.358,7.027 7.623,7.027 L8.947,7.027 C9.159,7.027 9.371,7.197 9.371,7.480 L9.371,8.897 C9.371,9.180 9.212,9.350 8.947,9.350 ZM13.184,14.450 L11.860,14.450 C11.648,14.450 11.436,14.280 11.436,13.997 L11.436,12.580 C11.436,12.353 11.595,12.127 11.860,12.127 L13.184,12.127 C13.396,12.127 13.608,12.297 13.608,12.580 L13.608,13.997 C13.608,14.280 13.449,14.450 13.184,14.450 ZM13.184,9.350 L11.860,9.350 C11.648,9.350 11.436,9.180 11.436,8.897 L11.436,7.480 C11.436,7.253 11.595,7.027 11.860,7.027 L13.184,7.027 C13.396,7.027 13.608,7.197 13.608,7.480 L13.608,8.897 C13.608,9.180 13.449,9.350 13.184,9.350 Z"></path>
                                                    </svg><?php echo get_the_date("M d Y"); ?>
                                                </span>
                                                <span>
                                                <?php 
                                                if($type == 1){
                                                        echo '<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 32 32" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m21.386 10c-1.055-4.9-3.305-8-5.386-8s-4.331 3.1-5.386 8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10 16a30.013 30.013 0 0 0 .267 4h11.466a30.013 30.013 0 0 0 .267-4 30.013 30.013 0 0 0 -.267-4h-11.466a30.013 30.013 0 0 0 -.267 4z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10.614 22c1.055 4.9 3.305 8 5.386 8s4.331-3.1 5.386-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m23.434 10h6.3a15.058 15.058 0 0 0 -10.449-8.626c1.897 1.669 3.385 4.755 4.149 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m30.453 12h-6.7a32.332 32.332 0 0 1 .247 4 32.332 32.332 0 0 1 -.248 4h6.7a14.9 14.9 0 0 0 0-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m19.285 30.626a15.058 15.058 0 0 0 10.451-8.626h-6.3c-.766 3.871-2.254 6.957-4.151 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8.566 22h-6.3a15.058 15.058 0 0 0 10.451 8.626c-1.899-1.669-3.387-4.755-4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m12.715 1.374a15.058 15.058 0 0 0 -10.451 8.626h6.3c.766-3.871 2.254-6.957 4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8 16a32.332 32.332 0 0 1 .248-4h-6.7a14.9 14.9 0 0 0 0 8h6.7a32.332 32.332 0 0 1 -.248-4z" fill="currentColor" data-original="#000000"></path></g></svg>';
                                                    } else {
                                                        echo '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><g xmlns="http://www.w3.org/2000/svg" id="e"><path d="m512 363.78c-1.2-66.8-9.09-134.35-22.03-202.53-10.54-47.37-48.46-89.56-109.05-92.65-44.73-1.84-53.38 23.64-104.06 23.15-13.88-.09-27.75-.09-41.63 0-50.69.49-59.36-24.99-104.07-23.15-60.6 3.09-99.7 45.17-109.09 92.65-12.95 68.18-20.84 135.72-22.03 202.52-.29 46.51 45.63 77.45 75.93 79.57 58.53 4.42 105.03-98.79 140.46-98.8 26.41.15 52.81.16 79.22 0 35.44 0 81.9 103.23 140.47 98.81 30.29-2.12 77.4-33.27 75.89-79.57zm-321.06-130.34h-27.3v27.3c0 11.52-9.34 20.86-20.86 20.86s-20.86-9.34-20.86-20.86v-27.3h-27.3c-11.52 0-20.86-9.34-20.86-20.86s9.34-20.86 20.86-20.86h27.3v-27.3c0-11.52 9.34-20.86 20.86-20.86s20.86 9.34 20.86 20.86v27.3h27.3c11.52 0 20.86 9.34 20.86 20.86s-9.34 20.86-20.86 20.86zm168.57 48.15c-16.33.44-29.91-12.46-30.35-28.78-.43-16.38 12.48-29.98 28.8-30.4 16.34-.41 29.94 12.48 30.36 28.82.41 16.34-12.48 29.94-28.81 30.36zm49.25-78.85c-16.33.46-29.94-12.45-30.39-28.78-.41-16.36 12.48-29.94 28.82-30.39 16.36-.43 29.94 12.48 30.38 28.82.43 16.33-12.49 29.94-28.81 30.35z" fill="currentColor" data-original="#000000"></path></g></g></svg>';
                                                    }
                                                ?>
                                                <?php echo esc_html($location); ?>
                                                </span>
                                                <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26px" height="16px">
                                                <path fill-rule="evenodd" fill="rgb(121, 121, 121)" d="M25.691,8.827 C24.330,10.510 19.472,16.000 13.476,16.000 C7.479,16.000 2.621,10.510 1.260,8.827 C0.872,8.331 0.872,7.641 1.260,7.172 C2.621,5.490 7.479,-0.000 13.476,-0.000 C19.472,-0.000 24.330,5.490 25.691,7.172 C26.079,7.669 26.079,8.359 25.691,8.827 ZM13.476,2.207 C10.255,2.207 7.646,4.800 7.646,8.000 C7.646,11.200 10.255,13.793 13.476,13.793 C16.696,13.793 19.306,11.200 19.306,8.000 C19.306,4.800 16.696,2.207 13.476,2.207 ZM13.476,12.000 C11.255,12.000 9.450,10.207 9.450,8.000 C9.450,5.793 11.255,4.000 13.476,4.000 C15.696,4.000 17.501,5.793 17.501,8.000 C17.501,10.207 15.696,12.000 13.476,12.000 ZM13.170,5.490 C11.949,5.490 10.949,6.483 10.921,7.724 L12.254,7.724 C12.254,7.227 12.670,6.814 13.170,6.814 L13.170,5.490 Z"></path>
                                                </svg><?php echo esc_html($total_views); ?></span>
                                        </div>
                                        <?php 
                                            $title1 = $title2 = $title3 = '';
                                            if($type == 1){
                                                $title1 = 'Payout';
                                                $title2 = 'Categories';                                                
                                            } else {
                                                $title1 = 'Worth';
                                                $title2 = 'Platform';
                                            }
                                            $NewDate = Date('M-d-Y', strtotime('+10 days'));
                                            $end_date = !empty($end_date) ? $end_date : $NewDate;
                                            
                                            // $end_date = !empty($end_date) : $end_date : 
                                            if(!empty($payout) || !empty($free_giveaway) || !empty($end_date)){
                                                echo '<div class="gw_payout_box">
                                                    <ul>';
                                                        if($type == 1){
                                                            if(!empty($payout)){
                                                                echo '<li>'.esc_html($title1).':<span>'.esc_html($payout).'</span></li>';
                                                            }
                                                        }
                                                        if(!empty($cate_array)){
                                                            echo '<li>'.esc_html($title2).':<span>'.esc_html(implode(",  ",$cate_array)).'</span></li>';
                                                        }
                                                        if(!empty($end_date)){
                                                            echo '<li>'.esc_html('End Date:').'<span>'.esc_html($end_date).'</span></li>';
                                                        }
                                                        echo '<li>'.esc_html('Description:').'<span>'.esc_html('Hello there, Peeps! Do you want to win').'</span></li>';
                                                    echo '</ul>
                                                    <div class="gw_description_text">
                                                        <p><span>'.esc_html(get_the_title()).'</span>'.esc_html('for free? Your dream is only a sweepstakes entry away. After all, who wouldn\'t want a brand new car or phone with no down payment or EMI? Enter some fun time sweepstakes contests with prizes and you could be the proud owner of one of them. So why incur the expense of purchasing one when you can win one for free? To be eligible to win, you must enter a sweepstakes contest, which only takes a minute to complete by entering your name, email address, or so and continuing with the contest. The sweepstakes draw ensures that the stunning prizes are yours.').'</p>
                                                    </div>
                                                </div>';
                                            } 
                                        ?>
                                        <div class="gw_social_icons">
                                            <ul>
                                                <li>Share:</li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://www.facebook.com/sharer/sharer.php?u='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social1.png'; ?>" alt="facebook">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Facebook');?></p></div>
                                                    </a>
                                                </li>
                                                <li>   
                                                    <a href="<?php echo esc_url('https://twitter.com/intent/tweet?url='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social2.png'; ?>" alt="twitter">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Twitter');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://www.linkedin.com/shareArticle?mini=true&url='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social4.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('linkedin');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://pinterest.com/pin/create/button/?url='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social5.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Pinterest');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://api.whatsapp.com/send?text='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social6.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Whatsapp');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://reddit.com/submit?text='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social7.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Reddit');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://www.reddit.com/submit?url='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social8.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Tumblr');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://wordpress.com/wp-admin/press-this.php?text='.get_permalink()); ?>">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/social9.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Wordpress');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://buffer.com/add?text='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/socia20.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Buffer');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('https://refind.com/?text='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/socia21.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Refind');?></p></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo esc_url('http://www.sitejot.com/loginform.php?text='.get_permalink()); ?>" target="_blank">
                                                        <img src="<?php echo GS_PLUGIN_URL.'/includes/assets/images/socia22.png'; ?>" alt="Icon">
                                                        <div class="gw_tooltip_show"><p><?php echo esc_html('Sitejot');?></p></div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="gw_giveaway_btn">
                                            <?php
                                                $link = !empty($affiliate_link) ? $affiliate_link : 'javascript:;';
                                                $id = !empty($affiliate_link) ? 'gw_newsletter_popup_none' : 'gw_newsletter_popup';
                                            ?>
                                            
                                            <a href="<?php echo $link; ?>" class="gw_btn" id="<?php echo esc_attr($id); ?>"><?php echo esc_html('Enter Giveaway Here', 'giveaway');?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                                $image_id = get_option('bottom_banner');
                                $banner_link = get_option('bottom_banner_link');
                                $image = wp_get_attachment_image_src( $image_id, 'full' );
                                if(!empty($image[0])){
                                    echo '<div class="col-xl-12">
                                        <div class="gw_product_single_add">
                                            <a href="'.esc_attr($banner_link).'" target="_blank"><img src="'.esc_url($image[0]).'" alt="product-img"></a>
                                        </div>
                                    </div>';
                                }
                            ?>
                            <?php 
                                $tags = wp_get_object_terms(get_the_ID(), 'gw_product_tags');
                                if(!empty($tags)){
                                    echo '<div class="col-xl-12">
                                        <div class="gw_tagged_btn">';
                                    foreach($tags as $tag){
                                        echo '<a href="javascript:;">'.esc_html($tag->name).'</a>';
                                    }
                                        echo '</div>
                                    </div>';
                                }
                            ?>
                            <!-- //realetd -->
                            <div class="col-xl-12" bis_skin_checked="1">
                                <div class="gw_heading_wrapper" bis_skin_checked="1">
                                    <h2><?php echo esc_html('Related Giveaways', 'giveaway');?></h2>
                                </div>
                            </div>
                            <div class="row">
                            <?php 
                                //Post Loop
                                $args = array(  
                                    'post_type' => 'giveaway_product',
                                    'post_status' => 'publish',
                                    'post__not_in' => array(get_the_ID()),
                                    'posts_per_page' => 3, 
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'gw_product_category',
                                            'field' => 'slug',
                                            'terms' => $cate_array
                                        ),
                                    ),
                                );
                                $loop = new WP_Query( $args ); 
                                while ( $loop->have_posts() ) : $loop->the_post(); 
                                    echo '<div class="col-lg-4 col-md-6 col-sm-6" bis_skin_checked="1">
                                        <div class="gw_article_box" bis_skin_checked="1">
                                            <div class="gw_news_img gw_animation_box" bis_skin_checked="1">
                                                <span class="gw_product_list_img gw_animation">';
                                                    $image_ids = get_post_meta( get_the_ID(), 'giveaway_product_img_link', true );
                                                    if ($image_ids){
                                                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()), 'full' );
                                                        echo '<a href="javascript:;"><img src="'.esc_url($image_ids).'" class="gw_animation_img" alt="product-img"></a>';
                                                    }                                                    
                                                echo '</span>
                                                <div class="gw_blog_btn" bis_skin_checked="1">
                                                    <a href="javascript:;">'.implode(",  ",$cate_array).'</a>
                                                </div>
                                            </div>
                                            <div class="gw_blog_content" bis_skin_checked="1">
                                                <span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                        <path fill-rule="evenodd" fill="rgb(121, 121, 121)" d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"></path>
                                                    </svg>'.get_the_date("Y-m-d").'
                                                </span>
                                                <a href="'.esc_url(get_permalink()).'"><h5>'.esc_html(get_the_title()).'</h5></a>
                                                <p>'.esc_html(substr(get_the_content(), 0, 300)).'</p>
                                            </div>
                                        </div>
                                    </div>';
                                endwhile;	
                                wp_reset_postdata(); 	
                            ?>
                            </div>
                        </div>	
                    </div>
                </div> 
                <div class="col-xl-3 col-lg-3 col-md-12">
                    <?php if ( is_active_sidebar( 'giveawaysites' ) ) : ?>
                        <div id="secondary" class="widget-area" role="complementary">
                            <?php dynamic_sidebar( 'giveawaysites' ); ?>
                        </div>
                    <?php endif; ?>
                </div>           
            </div>
        </div>
    </div>
<?php
get_footer();