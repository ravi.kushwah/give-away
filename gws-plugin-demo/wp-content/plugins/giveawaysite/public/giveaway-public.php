<?php
// Public Css
add_action( 'wp_enqueue_scripts', 'giveaway_public_assets');
function giveaway_public_assets() {
    wp_enqueue_style('bootstrap', GS_PLUGIN_URL . '/includes/assets/css/bootstrap.min.css', array(), '1.0.0', 'all');
	wp_enqueue_style('toastr.min', GS_PLUGIN_URL . '/includes/assets/css/toastr.min.css', array(), '1.0.0', 'all');
    wp_enqueue_style('public-custom-style', GS_PLUGIN_URL . '/includes/assets/css/public-custom-style.css', array(), '1.0.0', 'all');

    // js
    wp_enqueue_script( 'bootstrap', GS_PLUGIN_URL . '/includes/assets/js/bootstrap.bundle.min.js', array(), '1.0.0', true);
	wp_enqueue_script( 'toastr.min', GS_PLUGIN_URL . '/includes/assets/js/toastr.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script( 'public-custum-script', GS_PLUGIN_URL . '/includes/assets/js/public-custum-script.js', array(), '1.0.0', true);    

    //ajax
	wp_localize_script( 'public-custum-script', 'gw_ajaxurl', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}

// Thumb size Codex
if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'style-thumb', 200, 209, true ); //300 pixels wide (and unlimited height)
    add_image_size( 'homepage-thumb', 220, 180, true ); //(cropped)
}

//Shortcode
function giveaway_product_shortcode( $atts) {
	$default_atts = array(
	"title" => '',
	"style" => '',
	"post" => '',
	"btn_title" => '',
	"btn_link" => '',
	"pagination" => ''
	);
	$params = shortcode_atts( $default_atts, $atts );
	$post = !empty($params['post']) ? $params['post'] : -1;
	$bt_name = !empty($params['btn_title']) ? $params['btn_title'] : '';
	$bt_link = !empty($params['btn_link']) ? $params['btn_link'] : '#';

	//Post Loop
	$args = array(  
		'post_type' => 'giveaway_product',
		'post_status' => 'publish',
		'posts_per_page' => $post, 
	);
	
	$loop = new WP_Query( $args ); 
	echo '<div class="gw_shortcodes_wrapper"> <div class="gw_product_box_list">
		<div class="gw_heading_wrapper">
			<h2>'.esc_html($params['title']).'</h2>';
			if(!empty($bt_name) || !empty($bt_link)){
				echo '<a href="'.esc_url($bt_link).'">'.esc_html($bt_name).'</a>';
			}
		echo '</div>
		<div class="row ajax_load_data">';		
			while ( $loop->have_posts() ) : $loop->the_post(); 
				$location = get_post_meta( get_the_ID(), 'giveaway_product_location', true );
				if($params['style'] == 'style1'){
					echo '<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="gw_article_box gw_horizontal_style">
							<div class="gw_news_img gw_animation_box">
								<span class="gw_product_list_img gw_animation">';
									$image_id = get_post_meta( get_the_ID(), 'giveaway_product_img_link', true );
									if ($image_id){
										echo '<a href="'.esc_url(get_permalink()).'"><img src="'.esc_html($image_id).'" class="gw_animation_img" alt="product-img"></a>';
									}
									echo '</span>
							</div>
							<div class="gw_blog_content">
								<div class="gw_blog_btn">
									<a href="javascript:;">'.esc_html($location).'</a>
									<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
										<path fill-rule="evenodd" fill="rgb(121, 121, 121)" d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"></path>
									</svg>'.get_the_date("M d, Y").'
									</span>
								</div>
								<a href="'.esc_url(get_permalink()).'" class="gw_product_title">'.esc_html(get_the_title()).'</a>
							</div>
						</div>
					</div>';
				} else {
					echo '<div class="col-lg-4 col-md-6 col-sm-6">
						<div class="gw_article_box">
							<div class="gw_news_img gw_animation_box">
								<span class="gw_product_list_img gw_animation">';
									$image_id = get_post_meta( get_the_ID(), 'giveaway_product_img_link', true );
									if ($image_id){
										echo '<a href="'.esc_url(get_permalink()).'"><img src="'.esc_html($image_id).'" class="gw_animation_img" alt="product-img"></a>';
									}
								echo '</span>
								<div class="gw_blog_btn">
									<a href="javascript:;">'.esc_html($location).'</a>
								</div>
							</div>
							<div class="gw_blog_content">
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
								<path fill-rule="evenodd" fill="rgb(121, 121, 121)" d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"></path>
								</svg>'.get_the_date("M d, Y").'</span>
								<a href="'.esc_url(get_permalink()).'"  class="gw_product_title">'.esc_html(get_the_title()).'</a>
								<p>'.esc_html(substr(get_the_content(), 0, 300)).'</p>
							</div>
						</div>
					</div>';
				}
			endwhile;
			wp_reset_postdata(); 
		echo '</div>';
		if($params['pagination'] == 'true'){
			echo '<div class="gw_pagination">
				<ul class="active_page">
					<li>
						<a href="javascript:;" class="post_prew" data-min='.($post*$i).' data-post="'.$post.'" data-value="1" data-style="'.$params['style'].'">
							<svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
								<path class="cls-1" d="M120.193,2325.53l3.331-3.34a0.676,0.676,0,0,1,.953.96l-2.856,2.85,2.856,2.86a0.672,0.672,0,0,1,0,.95,0.68,0.68,0,0,1-.953,0l-3.331-3.33a0.672,0.672,0,0,1,0-.95h0Zm4-.21,2.647-2.78a0.656,0.656,0,0,1,.962,0,0.741,0.741,0,0,1,0,1.01l-2.166,2.28,2.166,2.28a0.754,0.754,0,0,1,0,1.02,0.656,0.656,0,0,1-.962,0l-2.647-2.79a0.754,0.754,0,0,1,0-1.02h0Zm0,0" transform="translate(-120 -2322)"/>
							</svg>
						</a>
					</li>';
					$pages= $loop->max_num_pages/$post;
					for($i=1;$i<$pages+1;$i++) {
					    
					    $post1 =  ((int)$post*$i)-(int)$post;
					    
						$class = ($i == 1) ? 'pagination_a active post_nave' : 'pagination_a post_nave';
						if( $i < 4 || $i > $pages - 3 ) {
							echo '<li class="changePage"><a class="'.esc_attr($class).'" href="javascript:;" data-min="'.$post1.'" data-post="'.$post.'" data-style="'.$params['style'].'" data-value="'.esc_html($i).'">'.esc_html($i).'</a></li>';
						} 	
						if( $i == (int)$pages - 4 ) {
							echo '<a href="javascript:;">...</a>';	
						}
					}
					echo '<li>
						<a href="javascript:;" class="post_next" data-min='.($post + $post).' data-post="'.$post.'" data-style="'.$params['style'].'" data-value="1">
							<svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="7.97" viewBox="0 0 7.969 7.97">
								<path class="cls-1" d="M407.432,2325.36l-3.318-3.32a0.671,0.671,0,0,0-.949.95l2.845,2.84-2.845,2.85a0.668,0.668,0,0,0,.949.94l3.318-3.32a0.662,0.662,0,0,0,0-.94h0Zm-3.99-.21-2.636-2.77a0.651,0.651,0,0,0-.958,0,0.743,0.743,0,0,0,0,1.01l2.157,2.27-2.157,2.27a0.743,0.743,0,0,0,0,1.01,0.651,0.651,0,0,0,.958,0l2.636-2.77a0.754,0.754,0,0,0,0-1.02h0Zm0,0" transform="translate(-399.656 -2321.84)"/>
							</svg>
						</a>
					</li>
					<input type="hidden" value="'.(int)$pages.'" id="numberOfPages">
					<input type="hidden" value="2" id="webid">
				</ul>
			</div>';
		}
		echo '<span class="loading" style="display: none;">
			<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-loader"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg> Loading..
		</span>	
	</div></div>';
	}
add_shortcode( 'giveaway_product', 'giveaway_product_shortcode' );

//Single page
function giveaway_product_template( $template ) {
    global $post;
    if ( 'giveaway_product' === $post->post_type) {
        return GS_PLUGIN_PATH . '/public/single-product.php';
    }
    return $template;
}
add_filter( 'single_template', 'giveaway_product_template' );

// Popup Function
function giveaway_function_popup_news_letter() {
    echo '<div class="modal fade" id="Giveaway" tabindex="-1" aria-labelledby="Giveaway" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="gw_login_main">
			   <!--<form method="post" action="https://giveawaysites.co.in/authenticate/getCustomerLogin"  id="login_form">-->
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				<div class="gw_login_auth">
					<h1>Send Your Request</h1>
					<div class="gw_login_form">
						<div class="login-content">
							<form action="" class="form">
								<div class="gw_input_main">
									<label>Name</label>
									<div class="gw_input">
										<input type="text" placeholder="Enter Your Name" class="input required" id="l_name" name="l_name">
										<img src="https://giveawaysites.co.in/assets/frontend/images/user.svg" alt="">
									</div>
									<label>Email Address</label>
									<div class="gw_input">
										<input type="text" placeholder="Enter Your Email" value="" id="news_emails" name="l_email" class="required" data-valid="email">
										<img src="https://giveawaysites.co.in/assets/frontend/images/msg.svg" alt="">
									</div>
								</div>
								<div class="gw_login_btn">
									<button type="button" class="gw_btn submitLeadForm" id="gw_news_posts">Submit</button> 
								</div>
							</form>
						</div>
					</div>
				</div>
			  <!--</form>-->
			</div>
		</div>
	</div>
  </div>';
}
add_action( 'wp_footer', 'giveaway_function_popup_news_letter' );


// Single Product Banner
add_action('wp_ajax_giveaway_newsletter_post' , 'giveaway_newsletter_post');
add_action('wp_ajax_nopriv_giveaway_newsletter_post','giveaway_newsletter_post');
function giveaway_newsletter_post(){
	$giveaway_site = get_option('web_siteurl');
	$n_name = !empty($_POST['news_name']) ? $_POST['news_name'] : 'WordPress Site';
	if(!empty($_POST['email']) && !empty($giveaway_site)){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://giveawaysites.co.in/Api/news_letter',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('news_name' => $n_name,'w_siteurl' => $giveaway_site,'news_email' => $_POST['email']),
			CURLOPT_HTTPHEADER => array(
				'Cookie: ci_session=85fc3082bd3953ef5274d37984fc974de3145dc8'
			),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$data = json_decode($response);
		if($data->status == 1){
			$data_text = array('status' => 'true', 'msg'=> 'You Have Subscribed Successfully');
		} else {
			$data_text = array('status' => 'False', 'msg'=> 'Something went wrong');
		}
	} else {
		$data_text = array('status' => 'False', 'msg'=> 'Something went wrong');
	}
	echo json_encode($data_text);
	die();
}

// Single Product Banner
add_action('wp_ajax_giveaway_product_pagination' , 'giveaway_product_pagination');
add_action('wp_ajax_nopriv_giveaway_product_pagination','giveaway_product_pagination');
function giveaway_product_pagination(){
	//Post Loop
	$args = array(  
		'post_type' => 'giveaway_product',
		'post_status' => 'publish',
		'posts_per_page' => $_POST['nop'], 
		'offset' => $_POST['offest'], 
	);
	$loop = new WP_Query( $args ); 
	while ( $loop->have_posts() ) : $loop->the_post();
		$location = get_post_meta( get_the_ID(), 'giveaway_product_location', true );
		if($_POST['style'] == 'style1'){
			echo '<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="gw_article_box gw_horizontal_style">
					<div class="gw_news_img gw_animation_box">
						<span class="gw_product_list_img gw_animation">';
							$image_id = get_post_meta( get_the_ID(), 'giveaway_product_img_link', true );
							if ($image_id){
								echo '<a href="'.esc_url(get_permalink()).'"><img src="'.esc_html($image_id).'" class="gw_animation_img" alt="product-img"></a>';
							}
							echo '</span>
					</div>
					<div class="gw_blog_content">
						<div class="gw_blog_btn">
							<a href="javascript:;">'.esc_html($location).'</a>
							<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
								<path fill-rule="evenodd" fill="rgb(121, 121, 121)" d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"></path>
							</svg>'.get_the_date("Y-m-d").'
							</span>
						</div>
						<a href="'.esc_url(get_permalink()).'" class="gw_product_title">'.esc_html(get_the_title()).'</a>
					</div>
				</div>
			</div>';
		} else {
			echo '<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="gw_article_box">
					<div class="gw_news_img gw_animation_box">
						<span class="gw_product_list_img gw_animation">';
							$image_id = get_post_meta( get_the_ID(), 'giveaway_product_img_link', true );
							if ($image_id){
								echo '<a href="'.esc_url(get_permalink()).'"><img src="'.esc_html($image_id).'" class="gw_animation_img" alt="product-img"></a>';
							}
						echo '</span>
						<div class="gw_blog_btn">
							<a href="javascript:;">'.esc_html($location).'</a>
						</div>
					</div>
					<div class="gw_blog_content">
						<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
						<path fill-rule="evenodd" fill="rgb(121, 121, 121)" d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"></path>
						</svg>'.get_the_date("Y-m-d").'</span>
						<a href="'.esc_url(get_permalink()).'"  class="gw_product_title">'.esc_html(get_the_title()).'</a>
						<p>'.esc_html(substr(get_the_content(), 0, 300)).'</p>
					</div>
				</div>
			</div>';
		}
	endwhile;
	wp_reset_postdata();
	die();	
}
