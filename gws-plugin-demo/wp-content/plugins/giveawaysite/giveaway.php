<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             1.0.0
 * @package           Giveaway
 *
 * @wordpress-plugin
 * Plugin Name:       GiveawaySites
 * Plugin URI:        #
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            #
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       giveaway
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GIVEAWAY_VERSION', '1.0.0' );

define('GS_PLUGIN_PATH', dirname(__FILE__));
define('GS_PLUGIN_URL', plugins_url('', __FILE__));

require plugin_dir_path( __FILE__ ) . 'admin/giveaway-admin.php';
require plugin_dir_path( __FILE__ ) . 'public/giveaway-public.php';