jQuery( document ).ready(function($) {
	'use strict';

    // Banner Link Update 
	$(document).on('click','#gw_newsletter_popup',function(e) {
        $("#Giveaway").modal("show");
    });

    // Banner Link Update 
	$(document).on('click','#gw_news_post',function(e) {
		var email = $('#news_email').val();
        var news_name = $('#news_name').val();
        if(email.length != 0){
            if (validateEmail(email) ) {
                $.ajax({
                    url: gw_ajaxurl.ajaxurl,
                    type: 'POST',
                    data : {'email' : email, 'news_name' : news_name, 'action' : 'giveaway_newsletter_post'}, 
                    success: function (response) {
                        $('#news_email').val('');
                        var result = JSON.parse(response);						
                        if(result.status == 'true'){
                            toastr["success"](result.msg);
                        } else {
                            toastr["error"](result.msg); 
                        }
                    }
                });
            } else {
                toastr["error"]('Invaild Email Address');
            }
        } else {
            toastr["error"]('Field Is Empty ');            
        }
	});

    // Banner Link Update 
	$(document).on('click','#gw_news_posts',function(e) {
		var email = $('#news_emails').val();
        var news_name = $('#l_name').val();
        
        if(email.length != 0){
            if (validateEmail(email) ) {
                $.ajax({
                    url: gw_ajaxurl.ajaxurl,
                    type: 'POST',
                    data : {'email' : email, 'news_name' : news_name, 'action' : 'giveaway_newsletter_post'}, 
                    success: function (response) {
                        $('#news_emails').val('');
                        var result = JSON.parse(response);						
                        if(result.status == 'true'){
                            toastr["success"](result.msg);
                            $("#Giveaway").modal("hide");
                        } else {
                            toastr["error"](result.msg); 
                        }
                    }
                });
            } else {
                toastr["error"]('Invaild Email Address');
            }
        } else {
            toastr["error"]('Field Is Empty ');            
        }
	});

    // Banner Link Update 
	$(document).on('click','.post_nave',function(e) {
        var active = $(this).parents('.gw_shortcodes_wrapper').find('.ajax_load_data').addClass('active_loop');
        $(this).parents('.gw_shortcodes_wrapper').find('.loading').addClass('active_loader');
        $('.active_loader').show();  
        $('.post_nave').removeClass('active');   
        $(this).addClass('active');      
        var offest = $(this).attr('data-min');
        var nop = $(this).attr('data-post');
        var number = $(this).attr('data-value');
        var style = $(this).attr('data-style');
        
        var numberOfPages = parseInt($(this).parent().parent().parent().find('#numberOfPages').val());
        $(this).parent().parent().addClass('active_page_nav');
        var curent = parseInt(number)+4;
        var datamin = parseInt(offest)-parseInt(nop);
        var datamax = parseInt(offest)+parseInt(nop);
        var numbers_plus = parseInt(number) + 1;
        var numbers_min = parseInt(number) - 1;
        $.ajax({
            url: gw_ajaxurl.ajaxurl,
            type: 'POST',
            data : {'offest' : offest, 'nop' : nop, 'style' : style, 'action' : 'giveaway_product_pagination'}, 
            success: function (response) {  
                var pagination = '';
                pagination +='<li><a href="javascript:;" class="post_prew" data-min="'+datamin+'" data-post="'+nop+'" data-value="'+numbers_min+'" data-style="'+style+'"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8"><path class="cls-1" d="M120.193,2325.53l3.331-3.34a0.676,0.676,0,0,1,.953.96l-2.856,2.85,2.856,2.86a0.672,0.672,0,0,1,0,.95,0.68,0.68,0,0,1-.953,0l-3.331-3.33a0.672,0.672,0,0,1,0-.95h0Zm4-.21,2.647-2.78a0.656,0.656,0,0,1,.962,0,0.741,0.741,0,0,1,0,1.01l-2.166,2.28,2.166,2.28a0.754,0.754,0,0,1,0,1.02,0.656,0.656,0,0,1-.962,0l-2.647-2.79a0.754,0.754,0,0,1,0-1.02h0Zm0,0" transform="translate(-120 -2322)"/></svg></a></li>';
                for(var i=number; i<numberOfPages+1;i++) {
                    var cls = (i == number) ? 'pagination_a active post_nave' : 'pagination_a post_nave' ;
                    if( i < 4 || i < curent || i > numberOfPages - 3 ){
                        pagination +='<li class="changePage"><a class="'+cls+'" href="javascript:;" data-min="'+parseInt((nop*i)-nop)+'" data-post="'+nop+'" data-style="'+style+'" data-value="'+i+'">'+i+'</a></li>';
                    }
                    if( i == numberOfPages - 4 ) { 
                        pagination += '<span class="changePage">...</span>';
                    }
                }
                pagination +=' <li><a href="javascript:;" class="post_next" data-min="'+datamax+'" data-post="'+nop+'" data-value="'+numbers_plus+'" data-style="'+style+'"><svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="7.97" viewBox="0 0 7.969 7.97"><path class="cls-1" d="M407.432,2325.36l-3.318-3.32a0.671,0.671,0,0,0-.949.95l2.845,2.84-2.845,2.85a0.668,0.668,0,0,0,.949.94l3.318-3.32a0.662,0.662,0,0,0,0-.94h0Zm-3.99-.21-2.636-2.77a0.651,0.651,0,0,0-.958,0,0.743,0.743,0,0,0,0,1.01l2.157,2.27-2.157,2.27a0.743,0.743,0,0,0,0,1.01,0.651,0.651,0,0,0,.958,0l2.636-2.77a0.754,0.754,0,0,0,0-1.02h0Zm0,0" transform="translate(-399.656 -2321.84)"/></svg></a></li>';
                pagination +='<input type="hidden" value="'+numberOfPages+'" id="numberOfPages">';
                $('.active_page_nav').html(pagination);
                $('.active_page').removeClass('active_page_nav');
                setTimeout(function () {                    
                    $('.active_loader').hide();
                    $('.loading').removeClass('active_loader');
                    $('.active_loop').html(response);
                    $('.ajax_load_data').removeClass('active_loop');
                }, 1000);
            }
        });
    });

    $(document).on('click','.post_prew',function(e) {        
        $(this).parents('.gw_shortcodes_wrapper').find('.loading').addClass('active_loader');
        $('.active_loader').show();  
        $(this).parent().parent().addClass('active_page_nav');
        var offest = $(this).attr('data-min');
        var nop = $(this).attr('data-post');
        var number = $(this).attr('data-value');
        var style = $(this).attr('data-style');
        var active = $(this).parents('.gw_shortcodes_wrapper').find('.ajax_load_data').addClass('active_loop');
        var numberOfPages = parseInt($(this).parent().parent().find('#numberOfPages').val());
        var curent = parseInt(number)+4;
        var datamin = parseInt(offest)-parseInt(nop);
        var datamax = parseInt(offest)+parseInt(nop);
        var numbers_min = parseInt(number) - 1;
        $.ajax({
            url: gw_ajaxurl.ajaxurl,
            type: 'POST',
            data : {'offest' : datamin, 'nop' : nop, 'style' : style, 'action' : 'giveaway_product_pagination'}, 
            success: function (response) { 
                var pagination = '';

                pagination +='<li><a href="javascript:;" class="post_prew" data-min="'+datamin+'" data-post="'+nop+'" data-value="'+numbers_min+'" data-style="'+style+'"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8"><path class="cls-1" d="M120.193,2325.53l3.331-3.34a0.676,0.676,0,0,1,.953.96l-2.856,2.85,2.856,2.86a0.672,0.672,0,0,1,0,.95,0.68,0.68,0,0,1-.953,0l-3.331-3.33a0.672,0.672,0,0,1,0-.95h0Zm4-.21,2.647-2.78a0.656,0.656,0,0,1,.962,0,0.741,0.741,0,0,1,0,1.01l-2.166,2.28,2.166,2.28a0.754,0.754,0,0,1,0,1.02,0.656,0.656,0,0,1-.962,0l-2.647-2.79a0.754,0.754,0,0,1,0-1.02h0Zm0,0" transform="translate(-120 -2322)"/></svg></a></li>';
                for(var i=parseInt(number)-1; i<numberOfPages+1;i++) {
                    var cls = (i == parseInt(number)) ? 'pagination_a active post_nave' : 'pagination_a post_nave' ;
                    if( i < 4 || i < curent || i > numberOfPages - 3 ){
                        pagination +='<li class="changePage"><a class="'+cls+'" href="javascript:;" data-min="'+parseInt((nop*i)-nop)+'" data-post="'+nop+'" data-style="'+style+'" data-value="'+i+'">'+i+'</a></li>';
                    }
                    if( i == numberOfPages - 4 ) { 
                        pagination += '<span class="changePage">...</span>';
                    }
                }
                pagination +=' <li><a href="javascript:;" class="post_next" data-min="'+datamax+'" data-post="'+nop+'" data-value="'+parseInt(number)+'" data-style="'+style+'"><svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="7.97" viewBox="0 0 7.969 7.97"><path class="cls-1" d="M407.432,2325.36l-3.318-3.32a0.671,0.671,0,0,0-.949.95l2.845,2.84-2.845,2.85a0.668,0.668,0,0,0,.949.94l3.318-3.32a0.662,0.662,0,0,0,0-.94h0Zm-3.99-.21-2.636-2.77a0.651,0.651,0,0,0-.958,0,0.743,0.743,0,0,0,0,1.01l2.157,2.27-2.157,2.27a0.743,0.743,0,0,0,0,1.01,0.651,0.651,0,0,0,.958,0l2.636-2.77a0.754,0.754,0,0,0,0-1.02h0Zm0,0" transform="translate(-399.656 -2321.84)"/></svg></a></li>';
                pagination +='<input type="hidden" value="'+numberOfPages+'" id="numberOfPages">';
                $('.active_page_nav').html(pagination);
                $('.active_page').removeClass('active_page_nav');
                setTimeout(function () {
                    $('.active_loader').hide();
                    $('.loading').removeClass('active_loader');
                    $('.active_loop').html(response);
                    $('.ajax_load_data').removeClass('active_loop');
                }, 1000);
            }
        });

    });

    $(document).on('click','.post_next',function(e) {
        $(this).parents('.gw_shortcodes_wrapper').find('.loading').addClass('active_loader');
        $('.active_loader').show();  
        $(this).parent().parent().addClass('active_page_nav');
        var offest = $(this).attr('data-min');
        var nop = $(this).attr('data-post');
        var number = $(this).attr('data-value');
        var style = $(this).attr('data-style');
        var active = $(this).parents('.gw_shortcodes_wrapper').find('.ajax_load_data').addClass('active_loop');
        var numberOfPages = parseInt($(this).parent().parent().find('#numberOfPages').val());
        var curent = parseInt(number)+4;
        var datamin = parseInt(offest)-parseInt(nop);
        var datamax = parseInt(offest)+parseInt(nop);
        var numbers_plus = parseInt(number) + 1;
        $.ajax({
            url: gw_ajaxurl.ajaxurl,
            type: 'POST',
            data : {'offest' : datamin, 'nop' : nop, 'style' : style, 'action' : 'giveaway_product_pagination'}, 
            success: function (response) { 
                var pagination = '';

                pagination +='<li><a href="javascript:;" class="post_prew" data-min="'+datamin+'" data-post="'+nop+'" data-value="'+parseInt(number)+'" data-style="'+style+'"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8"><path class="cls-1" d="M120.193,2325.53l3.331-3.34a0.676,0.676,0,0,1,.953.96l-2.856,2.85,2.856,2.86a0.672,0.672,0,0,1,0,.95,0.68,0.68,0,0,1-.953,0l-3.331-3.33a0.672,0.672,0,0,1,0-.95h0Zm4-.21,2.647-2.78a0.656,0.656,0,0,1,.962,0,0.741,0.741,0,0,1,0,1.01l-2.166,2.28,2.166,2.28a0.754,0.754,0,0,1,0,1.02,0.656,0.656,0,0,1-.962,0l-2.647-2.79a0.754,0.754,0,0,1,0-1.02h0Zm0,0" transform="translate(-120 -2322)"/></svg></a></li>';
                for(var i=parseInt(number); i<numberOfPages+1;i++) {
                    var cls = (i == parseInt(number)+1) ? 'pagination_a active post_nave' : 'pagination_a post_nave' ;
                    if( i < 4 || i < curent || i > numberOfPages - 3 ){
                        pagination +='<li class="changePage"><a class="'+cls+'" href="javascript:;" data-min="'+parseInt((nop*i)-nop)+'" data-post="'+nop+'" data-style="'+style+'" data-value="'+i+'">'+i+'</a></li>';
                    }
                    if( i == numberOfPages - 4 ) { 
                        pagination += '<span class="changePage">...</span>';
                    }
                }
                pagination +=' <li><a href="javascript:;" class="post_next" data-min="'+datamax+'" data-post="'+nop+'" data-value="'+numbers_plus+'" data-style="'+style+'"><svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="7.97" viewBox="0 0 7.969 7.97"><path class="cls-1" d="M407.432,2325.36l-3.318-3.32a0.671,0.671,0,0,0-.949.95l2.845,2.84-2.845,2.85a0.668,0.668,0,0,0,.949.94l3.318-3.32a0.662,0.662,0,0,0,0-.94h0Zm-3.99-.21-2.636-2.77a0.651,0.651,0,0,0-.958,0,0.743,0.743,0,0,0,0,1.01l2.157,2.27-2.157,2.27a0.743,0.743,0,0,0,0,1.01,0.651,0.651,0,0,0,.958,0l2.636-2.77a0.754,0.754,0,0,0,0-1.02h0Zm0,0" transform="translate(-399.656 -2321.84)"/></svg></a></li>';
                pagination +='<input type="hidden" value="'+numberOfPages+'" id="numberOfPages">';
                $('.active_page_nav').html(pagination);
                $('.active_page').removeClass('active_page_nav');
                setTimeout(function () {
                    $('.active_loader').hide();
                    $('.loading').removeClass('active_loader');
                    $('.active_loop').html(response);
                    $('.ajax_load_data').removeClass('active_loop');
                }, 1000);
            }
        });

    });

    function validateEmail(uemail) {
	    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	    if (filter.test(uemail)) {
	        return true;
	    }
	    else {
	        return false;
	    }
	}
});