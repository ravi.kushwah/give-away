jQuery( document ).ready(function($) {
	'use strict';

	// Single Product Banner
	$(document).on('click','.gw_top_ad_image',function(e) {
		var data_id = $(this).attr('data-id');
		var image_frame;
		if(image_frame){
			image_frame.open();
			return;
		}
		// Define image_frame as wp.media object
		image_frame = wp.media({
			title: 'Select Media',
			multiple : false,
			library : {
				type : 'image',
			}
		});
		image_frame.on('select', function(e){
			// This will return the selected image from the Media Uploader, the result is an object
			var uploaded_image = image_frame.state().get('selection').first();
			// We convert uploaded_image to a JSON object to make accessing it easier
			// Output to the console uploaded_image
			var image_url = uploaded_image.toJSON().url;
			var image_id = uploaded_image.toJSON().id;
			// Let's assign the url value to the input field
			//console.log();	
			if(data_id == 'ad_top'){
				$("#gw_top_ad_img_preview").attr('src', image_url);
				$("#gw_top_ad_img_id").val(image_id);				
			} else {
				$("#gw_top_bottom_img_preview").attr('src', image_url);	
				$("#gw_top_bottom_img_id").val(image_id);						
			}
		});   
		image_frame.open();
	});    


	// Setting Save
	$(document).on('click','#gw_save_settings',function(e) {
		var top_banner_img = $('#gw_top_ad_img_id').val();
		var top_banner_link = $('#gw_top_affilate_link').val();
		var bottom_banner_img = $('#gw_top_bottom_img_id').val();
		var bottom_banner_link = $('#gw_bottom_affilate_link').val();
		var color = $('#gw_color_piker').val();
		var gw_site_name = $('#gw_site_name').val();
		$.ajax({
			url: gw_ajaxurl.ajaxurl,
			type: 'POST',
			data : {'top_banner_img' : top_banner_img, 'top_banner_link': top_banner_link, 'bottom_banner_img': bottom_banner_img, 'bottom_banner_link': bottom_banner_link, 'color': color, 'gw_site_name' : gw_site_name, 'action' : 'giveaway_save_settings'}, 
			success: function (response) {	
				var result = JSON.parse(response);						
				if(result.status == 'true'){
					toastr["success"]('Settings Save Successfully');
				} else {
					toastr["error"]('Something Is Wrong');
				}
			}
		});
	});

	// Banner Link Update 
	$(document).on('click','#gw_api_import',function(e) {
		var apikey = $('#gw_api_link').val();
		if(apikey.length != 0){
			$(this).children('.gw_bt_text').addClass('active_btn');
			$('.active_btn').text('');
			$(this).children('.loading').addClass('active_loader');	
			$('.active_loader').show();			
			$.ajax({
				url: gw_ajaxurl.ajaxurl,
				type: 'POST',
				data : {'apikey' : apikey, 'action' : 'giveaway_api_add'}, 
				success: function (response) {
					$('.active_loader').hide();
					$('.loading').removeClass('active_loader');
					var result = JSON.parse(response);	
					if(result.status == 'true'){
						$('.active_btn').text('Connected');
						$('.gw_bt_text').removeClass('active_btn');
						toastr["success"](result.msg);
						setTimeout(function () {
							location.reload();
						}, 1000);
					} else {
						$('.active_btn').text('Connect');
						toastr["error"](result.msg);
						$('.gw_bt_text').removeClass('active_btn');
					}
				}
			});
		} else {
			toastr["error"]('Invaild Api Key Enter');
		}
	});

	// Product
	$(document).on('click','.gw_product_import ',function(e) {
		var type = $(this).attr('data-type');
		$(this).children('.gw_bt_text').addClass('active_btn');
		$('.active_btn').text('');		
    	$(this).children('.loading').addClass('active_loader');	
		$('.active_loader').show();
		$.ajax({
			url: gw_ajaxurl.ajaxurl,
			type: 'POST',
			data : {'type' : type, 'action' : 'giveaway_product_import'}, 
			success: function (response) {
				$('.gw_note').show().delay(3000).hide(100).fadeOut("slow");	
				$('.active_loader').hide();
				$('.loading').removeClass('active_loader');
				var result = JSON.parse(response);						
				if(result.status == 'true'){
					$('.active_btn').text('Imported');
					$('.gw_bt_text').removeClass('active_btn');
					$('.gw_note p').text(result.msg);
					$('.gw_product_import').removeClass('active_btn');
				} else {
					$('.active_btn').text('Import');
					$('.gw_bt_text').removeClass('active_btn');
					$('.gw_note p').text(result.msg);
					$('.gw_product_import').removeClass('active_btn');
				}
			}
		});
	});


	// Banner Link Update 
	$(document).on('click','.gw_close_btn ',function(e) {
		var img = $(this).attr('data-url');
		var data_id = $(this).attr('data-id');
		if( data_id == 'ad_top'){
			$('#gw_top_ad_img_preview').attr('src', img+'/includes/assets/images/banner.jpg');
			$('#gw_top_ad_img_id').val('');
		} else {
			$('#gw_top_bottom_img_preview').attr('src', img+'/includes/assets/images/banner.jpg');
			$('#gw_top_bottom_img_id').val('');
		}
	});

});