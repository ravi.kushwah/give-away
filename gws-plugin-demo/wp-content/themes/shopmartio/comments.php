<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shopmartio
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<div id="comments" class="comments-area">
    <?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title shp-lined-title">
			<?php
			$shopmartio_comment_count = get_comments_number();
			if ( '1' === $shopmartio_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'shopmartio' ),
					'<span>' . wp_kses_post( get_the_title() ) . '</span>'
				);
			} else {
				printf( 
					/* translators: 1: comment count number, 2: title. */
					esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $shopmartio_comment_count, 'comments title', 'shopmartio' ) ),
					number_format_i18n( $shopmartio_comment_count ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					'<span>' . wp_kses_post( get_the_title() ) . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
					'avatar_size' => 61,
				    'callback'   => 'shopmartio_comment_callback',
				)
			);
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'shopmartio' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().
    $args = array(
        'fields' => apply_filters(
            'comment_form_default_fields', array(
                'author' =>'<div class="row">
                             <div class="col-md-6 col-lg-6">
                			  <div class="input-wrap">
                			   <input type="text" id="author" name="author" value="'.esc_attr( $commenter['comment_author'] ).
		               '" placeholder="'.esc_attr__('Your Name','shopmartio').'" >
                		 	</div>
                		 </div>',
                'email' =>'<div class="col-md-6 col-lg-6">
                			<div class="input-wrap">
                			 <input type="text" id="email" name="email" value="'.esc_attr($commenter['comment_author_email']).
		              '" placeholder="'.esc_attr__('Your Email','shopmartio').'" >
                			</div>
                		  </div>
                		</div>'
            )
         ),
        'comment_field' =>'<div class="row">
                        <div class="col-md-12 col-lg-12">
            			  <div class="input-wrap">
            				<textarea placeholder="'.esc_attr__('Your Comment', 'shopmartio').'" id="comment" name="comment" ></textarea>
            			  </div>
            		    </div>
            		 </div>',
        'id_submit'	=> 'comment-submit',
    	'class_submit'	=> 'submit',
    	'name_submit'	=> 'submit',
    	);
	comment_form($args);
	?>

</div><!-- #comments -->
