<?php 
/**
 * Add shortcode js_composer
 */
add_action( 'vc_before_init', 'shopmartio_shortcodes_map' );
function shopmartio_shortcodes_map(){
	
	// image and icon path
	$icon_path = plugin_dir_url( dirname( __FILE__ ) ).'shortcodes/icons/'; 
	$product_cats = get_categories( array('taxonomy' => 'product_cat') );
	$cats['All'] = 'all';
	foreach( $product_cats as $terms ){
		$cats[$terms->name] = $terms->slug;
	}
	/**
 	 * WooCoommerce Product Slider
	 */
	vc_map( array(
      "name" => __( "Product Slider", "shopmartio" ),
      "base" => "shopmartion_product_sliders",
	  "category" => __('Shop Martio','shopmartio'), 
	  "icon"	 => $icon_path . 'postsldier.png',
      "params" => array(
			    array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Heading", "shopmartio"),
					"param_name" => "heading"
				  ),
				array(
					"type" => "textarea",
					"value" =>"", 
					"heading" => __("Description", "shopmartio"),
					"param_name" => "description"
				  ),
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Show Number Of Product", "shopmartio"),
					"param_name" => "number_product"
				  ),
		    ),
	    ) 
	);
	// Create multi dropdown param type
    vc_add_shortcode_param( 'dropdown_multi', 'dropdown_multi_settings_field' );
    function dropdown_multi_settings_field( $param, $value ) {
       $param_line = '';
       $param_line .= '<select multiple name="'. esc_attr( $param['param_name'] ).'" class="wpb_vc_param_value wpb-input wpb-select '. esc_attr( $param['param_name'] ).' '. esc_attr($param['type']).'">';
       foreach ( $param['value'] as $text_val => $val ) {
           if ( is_numeric($text_val) && (is_string($val) || is_numeric($val)) ) {
                        $text_val = $val;
                    }
                    $text_val = __($text_val, "shopmartio");
                    $selected = '';
    
                    if(!is_array($value)) {
                        $param_value_arr = explode(',',$value);
                    } else {
                        $param_value_arr = $value;
                    }
    
                    if ($value!=='' && in_array($val, $param_value_arr)) {
                        $selected = ' selected="selected"';
                    }
                    $param_line .= '<option class="'.$val.'" value="'.$val.'"'.$selected.'>'.$text_val.'</option>';
                }
       $param_line .= '</select>';
    
       return  $param_line;
    }

	/**
 	 * WooCoommerce Product Filter
	 */
	vc_map( array(
      "name" => __( "Product Filter", "shopmartio" ),
      "base" => "shopmartion_product_filter",
	  "category" => __('Shop Martio','shopmartio'), 
	  "icon"	 => $icon_path . 'categorybanner.png',
      "params" => array(
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Heading", "shopmartio"),
					"param_name" => "heading"
				  ),
				array(
					"type" => "textarea",
					"value" =>"", 
					"heading" => __("Description", "shopmartio"),
					"param_name" => "description"
				  ),
				array(
				    "type" => "textfield",
					"value" =>"", 
					"heading" => __("Show Number Of Product", "shopmartio"),
					"param_name" => "shownumberp"
				    ),
    			array(
                   "type" => "dropdown_multi",
                   "heading" => __("Select Categories",'shopmartio'),
                   "param_name" => "product_cat",
                   'holder' => 'div',
                   "value" => $cats,
                ),
                array(
				    "type" => "textfield",
					"value" =>"", 
					"heading" => __("NUmber Of Load More Product", "shopmartio"),
					"param_name" => "loadermore"
				) 
		    ),
	    ) 
	);
	
	
	/**
 	 * Subscribe Newsletter
	 */
	vc_map( array(
      "name" => __( "Newsletter", "shopmartio" ),
      "base" => "shopmartion_newsletter", 
	  "category" => __('Shop Martio','shopmartio'), 
	  "icon"	 => $icon_path . 'List.png',
      "params" => array(
                array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Heading", "shopmartio"),
					"param_name" => "heading"
				   ),
				array(
					"type" => "textarea",
					"value" =>"", 
					"heading" => __("Descreption", "shopmartio"),
					"param_name" => "newsletter_descreption"
				    ),
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Mailchimp Api Key", "shopmartio"),
					"param_name" => "api_key"
				   ),
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Mailchimp List ID", "shopmartio"),
					"param_name" => "list_id"
				    ),
				array(
                    "type"        => "attach_images",
                    "heading"     => esc_html__( "Add You Newsletter Images", "shopmartio" ),
                    "description" => esc_html__( "Add You Newsletter Images", "shopmartio" ),
                    "param_name"  => "newsletter_image",
                    "value"       => "",
                    ),
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Facebook Page url", "shopmartio"),
					"param_name" => "facebook"
			       ),
			    array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Twitter Page url", "shopmartio"),
					"param_name" => "twitter"
			      ),
			    array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Google Plus Page url", "shopmartio"),
					"param_name" => "googleplus"
			      ),
			    array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Whatsapp Page url", "shopmartio"),
					"param_name" => "whatsapp"
			       ),
			    ),
			
	       ) 
	    );
	
	/**
 	 * Advertisement Banner
	 */
	vc_map( array(
	  "name" => __( "Advertisement Banner", "shopmartio" ),
	  "base" => "shopmartion_advertisement_banner",  
	  "category" => __('Shop Martio','shopmartio'), 
	  "icon"	 => $icon_path . 'Image.png',
	  "params" => array(
	            array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Enter Left Url", "shopmartio"),
					"param_name" => "left_page_url"
				   ),
	            array(
                    "type"        => "attach_images",
                    "heading"     => esc_html__( "Left Image", "shopmartio" ),
                    "description" => esc_html__( "Add Left Image", "shopmartio" ),
                    "param_name"  => "left_image",
                    "value"       => "",
                   ),
                array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Enter Center Url", "shopmartio"),
					"param_name" => "center_page_url"
				   ),   
                array(
                    "type"        => "attach_images",
                    "heading"     => esc_html__( "Left Image", "shopmartio" ),
                    "description" => esc_html__( "Add Left Image", "shopmartio" ),
                    "param_name"  => "center_image",
                    "value"       => "",
                   ),
                array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Enter Right Url", "shopmartio"),
					"param_name" => "right_page_url"
				   ),   
                array(
                    "type"        => "attach_images",
                    "heading"     => esc_html__( "Left Image", "shopmartio" ),
                    "description" => esc_html__( "Add Left Image", "shopmartio" ),
                    "param_name"  => "right_image",
                    "value"       => "",
                   ),
			
			),
		)  
	); 
	
	/**
	 * Advertisement Banner Style 2
	 */  
	 vc_map( array(
	  "name" => __( "Advertisement Banner Style 2", "shopmartio" ),
	  "base" => "shopmartion_advertisement_banner_style2",  
	  "category" => __('Shop Martio','shopmartio'), 
	  "icon"	 => $icon_path . 'Image.png',
	  "params" => array(
	            array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Enter Left Url", "shopmartio"),
					"param_name" => "left_page_url"
				   ),
	            array(
                    "type"        => "attach_images",
                    "heading"     => esc_html__( "Left Image", "shopmartio" ),
                    "description" => esc_html__( "Add Left Image", "shopmartio" ),
                    "param_name"  => "left_image",
                    "value"       => "",
                   ),
                array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Enter Right Url", "shopmartio"),
					"param_name" => "right_page_url"
				   ),   
                array(
                    "type"        => "attach_images",
                    "heading"     => esc_html__( "Right Image", "shopmartio" ),
                    "description" => esc_html__( "Add Right Image", "shopmartio" ),
                    "param_name"  => "right_image",
                    "value"       => "",
                   ), 
            ),
		)  
	); 
	
	/**
 	 * Conatct Us Information
	 */  
	vc_map( array(
	  "name" => __( "Conatct Information", "shopmartio" ),
	  "base" => "shopmartion_conatctus_information",  
	  "as_parent" => array('only' => 'shopmartion_conatctus_information_add'),
	  "content_element" => true,
	  "show_settings_on_create" => false,
       "is_container" => true,
	  "category" => __('Shop Martio','shopmartio'), 
	  "icon"	 => $icon_path . 'contactinfo.png',
	  "params" => array(
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Information Heading", "shopmartio"),
					"param_name" => "in_heading"
				  ),
				  
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("From Heading", "shopmartio"),
					"param_name" => "from_heading"
				  ),
				  
				array(
					"type" => "textarea",
					"value" =>"", 
					"heading" => __("From Description", "shopmartio"),
					"param_name" => "from_description"
				  ),
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Contact Forms7 Title", "shopmartio"),
					"param_name" => "from7_shortcode_title"
				  ),  
				array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Contact Forms7 Shortcode ID", "shopmartio"),
					"param_name" => "from7_shortcode_id"
				  ),
				    
			),
		"js_view" => 'VcColumnView'
		)  
	); 
	
	vc_map( array(
          "name" => __("Information Add", "shopmartio"),
          "base" => "shopmartion_conatctus_information_add",
          "content_element" => true,
          "as_child" => array('only' => 'shopmartion_conatctus_information'), 
          "params" => array(
          
            array(
                  "type" => "textfield",
                  "heading" => __("Heading", "shopmartio"),
                  "param_name" => "sub_heading",
                  "description" => __("", "shopmartio")
                ),
            array(
                  "type" => "textarea",
                  "heading" => __("Description", "shopmartio"),
                  "param_name" => "sub_description",
                  "description" => __("", "shopmartio")
                )
            )
        ) );
	
	if(class_exists( 'WPBakeryShortCodesContainer' ) ) {
       class WPBakeryShortCode_shopmartion_conatctus_information extends WPBakeryShortCodesContainer {
      }
    }
    if (class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_shopmartion_conatctus_information_add extends WPBakeryShortCode {
      }
    }
    
    /** 
     * Heading Title
     */
     
     vc_map( array(
      "name" => __( "Shopmartio Title", "shopmartio" ),
      "base" => "shopmartion_title",
	  "category" => __('Shop Martio','shopmartio'), 
	  "icon"	 => $icon_path . 'postsldier.png',
      "params" => array(
			    array(
					"type" => "textfield",
					"value" =>"", 
					"heading" => __("Heading", "shopmartio"),
					"param_name" => "heading"
				  ),
				array(
					"type" => "textarea",
					"value" =>"", 
					"heading" => __("Description", "shopmartio"),
					"param_name" => "description"
				  ),
		    ),
	    ) 
	);
    
	  
}