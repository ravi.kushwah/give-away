<?php 
/**
 * Shopmartio Core Meta Option
 */
add_action( 'add_meta_boxes', 'shopmartiocore_custom_meta_boxes', 10, 2 );
function shopmartiocore_custom_meta_boxes( $post_type, $post ) {
    add_meta_box(
        'shopmartio-meta-box',
        __( 'Product Extra Setting','shopmartiocore'),
        'shopmartiocore_meta_box',
        array('product'), //post types here
        'normal',
        'high'
    );
}


function shopmartiocore_meta_box($post) {
    $product_image_url = get_post_meta($post->ID, 'shopmartion_product_image', true);
    $shp_new_product = get_post_meta($post->ID, 'shopmartion_new_product', true);
    $sale_time_title = get_post_meta($post->ID, 'shopmartion_sale_time_title', true);
    $sale_datetime = get_post_meta($post->ID, 'shopmartion_sale_datetime', true);
    $shp_checked = '';
    if($shp_new_product == true):
      $shp_checked = esc_html__('checked','shopmartiocore');
    endif;
    ?>
    <table>
        <tr>
        <td>
        <label for="shopmartion_product_image"><?php echo esc_html__('Product Hover Image','shopmartiocore');?></label>
        <input type="text" name="shopmartion_product_image" id="shopmartion_product_image" class="" value="<?php echo esc_url($product_image_url); ?>" style="width:500px;" /></td>
        <td><a href="javascript:void(null)" class="shp_upload_image_button button button-secondary"><?php _e('Upload Image','shopmartiocore'); ?></a></td>
        </tr>
        <tr>
        <td> 
         <label for="shp_new_product"><?php echo esc_html__('Add New Product Tag','shopmartiocore');?></label>
         <input type="checkbox" id="shp_new_product" name="shp_new_product" value="new" <?php echo esc_attr($shp_checked); ?> >
        </td>
        </tr>
       
        <tr>
        <td> 
         <label for="shp_sale_time_title"><?php echo esc_html__('Sale Time Title','shopmartiocore');?></label>
         <input type="text" id="shp_sale_time_title" name="shp_sale_time_title" value="<?php echo esc_attr($sale_time_title); ?>">
        </td>
        </tr>
        <tr>
        <td> 
         <label for="shp_sale_datetime"><?php echo esc_html__('Add New Product Tag','shopmartiocore');?></label>
         <input type="text" id="shp_sale_datetime" name="shp_sale_datetime" value="<?php echo esc_attr($sale_datetime); ?>" >
         <span><?php echo esc_html__('Example:sept 2, 2021 11:30:00','shopmartiocore'); ?></span>
        </td>
        </tr>
    </table>
    <script>
    jQuery(document).ready( function($){
  	"use strict";  
  	
  	$('body').on('click', '.shp_upload_image_button', function(e){
     e.preventDefault();

    var button = $(this),
    aw_uploader = wp.media({
        title: 'Custom image',
        library : {
            type : 'image'
        },
        button: {
            text: 'Use this image'
        },
        multiple: false
    }).on('select', function() {
        var attachment = aw_uploader.state().get('selection').first().toJSON();
        $('#shopmartion_product_image').val(attachment.url);
    })
    .open();
      });
  });
</script>
<?php
}

add_action('save_post', 'shopmartiocore_save_postdata');
function shopmartiocore_save_postdata($post_id)
{
    if (array_key_exists('shopmartion_product_image', $_POST)) {
        update_post_meta(
            $post_id,
            'shopmartion_product_image',
            $_POST['shopmartion_product_image']
        );
    }
    
    if(array_key_exists('shp_new_product', $_POST)) {
        update_post_meta(
            $post_id,
            'shopmartion_new_product',
            $_POST['shp_new_product']
        );
    }
    
    if(array_key_exists('shp_sale_time_title', $_POST)) {
        update_post_meta(
            $post_id,
            'shopmartion_sale_time_title',
            $_POST['shp_sale_time_title']
        );
    }
    
    if(array_key_exists('shp_sale_datetime', $_POST)) {
        update_post_meta(
            $post_id,
            'shopmartion_sale_datetime',
            $_POST['shp_sale_datetime']
        );
    }
}
