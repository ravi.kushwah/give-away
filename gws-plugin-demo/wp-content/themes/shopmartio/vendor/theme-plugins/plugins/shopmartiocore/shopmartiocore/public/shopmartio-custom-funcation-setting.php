<?php 
/**
 * Shopmartion top header WooCommerce function
 */
if(!function_exists('shopmartion_top_header_woocommerce')){
    function shopmartion_top_header_woocommerce(){
    
    $theme_option = '';
	if(function_exists('shopmartio_theme_option_settings')):
	  $theme_option = shopmartio_theme_option_settings();
	endif;   
	$notification_message = '';
	if(!empty($theme_option['notification_message'])):
	    $notification_message = $theme_option['notification_message'];
	endif;
	?>
	<div class="shp-header-top-wrapper">
	    <div class="container-fluid">
	        <div class="shp-header-top-row">
        	    <?php 
        	    if(!empty($notification_message)):
        	    ?>
        		<div class="shp-header-offer-msg">
        			<svg xmlns:xlink="http://www.w3.org/1999/xlink"><path fill-rule="evenodd" d="M18.284,7.677 L17.530,7.677 C17.365,7.677 17.215,7.610 17.107,7.501 C16.967,8.262 16.389,8.879 15.631,9.062 L15.631,13.252 C15.631,13.639 15.379,13.967 15.007,14.067 C14.631,14.169 14.249,14.004 14.058,13.677 C13.162,12.134 11.751,11.080 9.862,10.541 C8.422,10.133 7.243,10.188 7.235,10.193 L7.213,10.194 L7.102,10.189 L7.835,13.207 C7.936,13.627 7.842,14.062 7.577,14.403 C7.308,14.743 6.908,14.938 6.476,14.938 L5.821,14.938 C5.174,14.938 4.615,14.498 4.462,13.869 L3.560,10.154 C2.050,9.921 0.932,8.631 0.932,7.079 C0.932,5.363 2.325,3.968 4.038,3.968 L7.234,3.966 C7.359,3.974 8.504,4.002 9.862,3.617 C11.751,3.079 13.163,2.024 14.058,0.482 C14.252,0.148 14.631,-0.010 15.007,0.091 C15.379,0.191 15.631,0.519 15.631,0.907 L15.631,5.097 C16.389,5.279 16.967,5.896 17.107,6.657 C17.215,6.548 17.365,6.482 17.530,6.482 L18.284,6.482 C18.614,6.482 18.881,6.751 18.881,7.079 C18.881,7.409 18.614,7.677 18.284,7.677 ZM6.603,8.743 L4.743,8.743 C4.414,8.743 4.146,8.475 4.146,8.145 C4.146,7.815 4.414,7.548 4.743,7.548 L6.603,7.548 L6.603,5.163 L4.038,5.163 C2.983,5.163 2.126,6.023 2.126,7.079 C2.126,8.131 2.980,8.991 4.030,8.996 L6.344,8.995 L6.603,8.996 L6.603,8.743 ZM6.614,13.241 L6.544,13.241 C6.215,13.241 5.947,12.973 5.947,12.644 C5.947,12.388 6.108,12.170 6.334,12.085 L6.281,11.864 L6.154,11.864 C5.824,11.864 5.557,11.596 5.557,11.266 C5.557,10.991 5.743,10.759 5.996,10.690 L5.875,10.191 L4.798,10.191 L5.623,13.586 C5.645,13.679 5.727,13.743 5.821,13.743 L6.476,13.743 C6.540,13.743 6.597,13.715 6.637,13.665 C6.676,13.615 6.691,13.550 6.674,13.489 L6.614,13.241 ZM14.437,2.035 C13.392,3.335 11.930,4.274 10.165,4.772 C9.191,5.047 8.332,5.133 7.797,5.157 L7.797,9.002 C8.333,9.027 9.193,9.112 10.164,9.386 C11.930,9.884 13.391,10.824 14.437,12.123 L14.437,2.035 ZM15.946,7.016 C15.946,6.764 15.824,6.534 15.631,6.391 L15.631,7.768 C15.824,7.625 15.946,7.394 15.946,7.142 L15.946,7.016 ZM3.890,8.377 C3.878,8.408 3.860,8.442 3.837,8.476 C3.813,8.512 3.789,8.542 3.763,8.568 C3.736,8.595 3.707,8.619 3.675,8.640 C3.636,8.666 3.605,8.682 3.571,8.696 C3.539,8.711 3.498,8.723 3.455,8.731 C3.424,8.739 3.384,8.743 3.340,8.743 C3.298,8.743 3.256,8.739 3.217,8.730 C3.186,8.724 3.147,8.713 3.109,8.696 C3.074,8.681 3.044,8.665 3.014,8.646 C2.969,8.615 2.938,8.590 2.912,8.562 C2.893,8.545 2.865,8.511 2.841,8.472 C2.822,8.446 2.804,8.409 2.792,8.381 C2.772,8.331 2.761,8.295 2.754,8.258 C2.747,8.221 2.743,8.184 2.743,8.145 C2.743,8.108 2.747,8.070 2.754,8.033 C2.761,7.995 2.772,7.959 2.786,7.924 C2.807,7.875 2.824,7.842 2.846,7.810 C2.869,7.776 2.895,7.744 2.924,7.717 C2.939,7.700 2.972,7.674 3.009,7.648 C3.038,7.629 3.071,7.612 3.105,7.597 C3.155,7.577 3.191,7.566 3.228,7.559 C3.301,7.543 3.381,7.544 3.458,7.560 C3.492,7.567 3.530,7.578 3.565,7.592 C3.607,7.610 3.641,7.628 3.675,7.651 C3.701,7.666 3.733,7.693 3.763,7.723 C3.787,7.747 3.810,7.776 3.832,7.805 C3.860,7.850 3.878,7.883 3.892,7.916 C3.906,7.952 3.916,7.985 3.925,8.022 C3.934,8.070 3.937,8.108 3.937,8.145 C3.937,8.184 3.934,8.221 3.927,8.258 C3.916,8.306 3.906,8.341 3.890,8.377 ZM3.026,8.015 L3.026,8.015 L3.026,8.015 C3.026,8.015 3.026,8.015 3.026,8.015 ZM17.388,5.189 C17.279,5.289 17.136,5.346 16.986,5.346 C16.818,5.346 16.657,5.274 16.544,5.150 C16.437,5.033 16.382,4.879 16.390,4.719 C16.397,4.560 16.466,4.412 16.584,4.305 L17.566,3.410 C17.811,3.187 18.188,3.206 18.410,3.450 C18.517,3.567 18.572,3.720 18.564,3.880 C18.557,4.039 18.488,4.185 18.371,4.293 L17.388,5.189 ZM16.958,8.814 C17.115,8.797 17.270,8.862 17.388,8.969 L18.370,9.865 C18.488,9.973 18.557,10.120 18.564,10.279 C18.572,10.438 18.517,10.592 18.409,10.710 C18.297,10.833 18.136,10.905 17.969,10.905 C17.820,10.905 17.677,10.850 17.567,10.750 L16.583,9.852 C16.466,9.746 16.397,9.599 16.390,9.439 C16.382,9.279 16.437,9.125 16.545,9.007 C16.651,8.891 16.799,8.821 16.958,8.814 Z"/></svg>
        			<span class="free-ship">
        			<?php echo esc_html($notification_message); ?></span>
        		</div> 
        		<?php endif; ?>
        		<div class="shp-header-drop-downs">
        			<div class="shp-header-info">
        			   <?php dynamic_sidebar('shp-top-header');  ?>
        			</div> 
        		</div>  
    		</div> 
	    </div>  
	</div>  
	<?php	
	}
}
/**
 * Shopmartion WooCommerce MinCart Search Function
 */
if(!function_exists('shopmartion_woocommerce_mincart_search')){
    function shopmartion_woocommerce_mincart_search(){
        $theme_option = '';
    	if(function_exists('shopmartio_theme_option_settings')):
    	  $theme_option = shopmartio_theme_option_settings();
    	endif;  
    	$search_header_switch = '';
    	if(!empty($theme_option['search_header_switch'])):
    	    $search_header_switch = $theme_option['search_header_switch'];
    	endif;
    	$mincart_header_switch = '';
    	if(!empty($theme_option['mincart_header_switch'])):
    	    $mincart_header_switch = $theme_option['mincart_header_switch'];
    	endif;
	?>   
	<div class="shp-head-icon">
		<div class="shp-search-cart ">
		     <?php 
		    if($search_header_switch == true):
		    ?>
		    <span class="shp-search-btn">
			   <svg class="shp-search-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" viewBox="0 0 15 16.031">
				  <path d="M12.490,7.014 C12.467,7.837 12.244,8.933 11.679,9.915 C11.478,10.263 11.539,10.450 11.794,10.713 C12.735,11.684 13.658,12.675 14.576,13.671 C15.305,14.463 15.040,15.650 14.075,15.948 C13.545,16.111 13.121,15.892 12.750,15.492 C11.839,14.512 10.915,13.545 10.010,12.558 C9.794,12.321 9.642,12.257 9.335,12.436 C7.170,13.702 4.989,13.682 2.905,12.279 C0.866,10.906 -0.084,8.842 0.001,6.278 C0.104,3.170 2.407,0.526 5.349,0.071 C9.133,-0.514 12.508,2.615 12.490,7.014 ZM10.617,6.642 C10.611,4.097 8.643,1.991 6.270,1.989 C3.829,1.988 1.865,4.078 1.865,6.678 C1.865,9.235 3.848,11.336 6.257,11.330 C8.657,11.324 10.623,9.210 10.617,6.642 Z"/>
			   </svg>
			 </span>
			<?php 
			endif;
			if($mincart_header_switch == true):
			     if(class_exists("Woocommerce")):
			?>
			<!-- Cart Button -->
			<span class="shp-cart-btn">
			   <span class="shp-cart-quntity">
			       <span class="shp-cart-icon">
					  <svg xmlns:xlink="http://www.w3.org/1999/xlink"><path fill-rule="evenodd" d="M18.692,2.761 C18.200,1.488 17.025,0.666 15.700,0.666 L15.512,0.666 L15.512,0.666 C15.512,0.298 15.223,-0.001 14.867,-0.001 L7.133,-0.001 C6.777,-0.001 6.488,0.298 6.488,0.666 L6.488,0.666 L6.300,0.666 C4.974,0.666 3.800,1.488 3.308,2.761 L1.239,7.999 L-0.000,7.999 L-0.000,9.333 L0.927,9.333 C1.223,9.333 1.480,9.540 1.553,9.838 L3.685,18.485 C3.901,19.376 4.672,19.999 5.561,19.999 L16.439,19.999 C17.328,19.999 18.142,19.376 18.358,18.484 L20.447,9.838 C20.520,9.540 20.776,9.333 21.073,9.333 L22.000,9.333 L22.000,7.999 L20.761,7.999 L18.692,2.761 ZM7.777,17.333 L6.488,17.333 L6.488,10.666 L7.777,10.666 L7.777,17.333 ZM11.645,17.333 L10.355,17.333 L10.355,10.666 L11.645,10.666 L11.645,17.333 ZM15.512,17.333 L14.223,17.333 L14.223,10.666 L15.512,10.666 L15.512,17.333 ZM4.505,3.256 C4.800,2.493 5.505,1.999 6.300,1.999 L6.488,1.999 L6.488,1.999 C6.488,2.368 6.777,2.666 7.133,2.666 L14.867,2.666 C15.223,2.666 15.512,2.368 15.512,1.999 L15.512,1.999 L15.700,1.999 C16.495,1.999 17.200,2.493 17.495,3.256 L19.372,7.999 L2.628,7.999 L4.505,3.256 Z"></path></svg>
				    </span>
				  <span class="shp-cart-count">
					<?php echo WC()->cart->get_cart_contents_count(); ?>
				  </span>
			   </span>
			   <span class="shp-cart-total">
				<?php
				 echo WC()->cart->get_cart_subtotal(); 
				?> 
			   </span>
			</span>
			<?php 
			  endif;
			endif;
			?>
		</div>
	</div> 
	<?php 
    if($search_header_switch == true):
    ?>
	<!-- Search Box -->
    <div class="shp-search-form">
        <div class="shp-search-form-inner">
            <form method="get" action="<?php echo esc_url(home_url( '/' )); ?>">
            <a href="javascript:void(0);" class="close-search">
              <svg viewBox="0 0 413.348 413.348" xmlns="http://www.w3.org/2000/svg"><path d="m413.348 24.354-24.354-24.354-182.32 182.32-182.32-182.32-24.354 24.354 182.32 182.32-182.32 182.32 24.354 24.354 182.32-182.32 182.32 182.32 24.354-24.354-182.32-182.32z"/></svg>
            </a>
            <div class="search-bar-inner">
              <input type="text" name="s" id="s" placeholder="<?php echo esc_attr_x( 'Search here', 'shopmartion') ?>" />
              <button class="shp-header-search">
                <svg class="shp-search-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" viewBox="0 0 15 16.031">
                  <path d="M12.490,7.014 C12.467,7.837 12.244,8.933 11.679,9.915 C11.478,10.263 11.539,10.450 11.794,10.713 C12.735,11.684 13.658,12.675 14.576,13.671 C15.305,14.463 15.040,15.650 14.075,15.948 C13.545,16.111 13.121,15.892 12.750,15.492 C11.839,14.512 10.915,13.545 10.010,12.558 C9.794,12.321 9.642,12.257 9.335,12.436 C7.170,13.702 4.989,13.682 2.905,12.279 C0.866,10.906 -0.084,8.842 0.001,6.278 C0.104,3.170 2.407,0.526 5.349,0.071 C9.133,-0.514 12.508,2.615 12.490,7.014 ZM10.617,6.642 C10.611,4.097 8.643,1.991 6.270,1.989 C3.829,1.988 1.865,4.078 1.865,6.678 C1.865,9.235 3.848,11.336 6.257,11.330 C8.657,11.324 10.623,9.210 10.617,6.642 Z"/>
                </svg>
              </button>
            </div>
           </form>
        </div>
    </div>
    <?php 
	endif;
	if($mincart_header_switch == true):
	?>
    <!-- Mincart box Box -->
    <div class="shp-header-cart-box">
		<div class="shp-header-cart-inner">
			<div class="shp-cart-head">
				<h4>
				 <?php 
    			 $contents_count = '';
    			 if(class_exists('WooCommerce')):
    			   $contents_count = WC()->cart->get_cart_contents_count();
    			 endif;
    			 printf(esc_html__( 'Shopping Cart ( %s )', 'shopmartion' ),$contents_count);
				 ?>
				</h4>
				<p><?php printf( esc_html__( 'Your Have %s Item In Your Cart', 'shopmartion' ), $contents_count); ?></p>
				<a href="javascript:void(0);" class="shp-close-cart">
				 <?php esc_html_e('close','shopmartion'); ?>
				</a>
			</div>
			<div class="shp-cart-products">
			  <?php 
			  if(class_exists('WooCommerce')):
				woocommerce_mini_cart();
			  endif;
			  ?>
			</div>
		</div>
	</div>
	<?php 
	endif;
    }	
}

/**
 * Shopmartion Newsletter 
 */
if(!function_exists('shopmartion_defult_newsletter')){
    function shopmartion_defult_newsletter(){
        $theme_option = '';
        if(function_exists('shopmartio_theme_option_settings')):
          $theme_option = shopmartio_theme_option_settings();
        endif;
        $newsletter_heading = '';
    	if(!empty($theme_option['newsletter_heading'])):
    	    $newsletter_heading = $theme_option['newsletter_heading'];
    	endif;
    	
    	$newsletter_desc = '';
    	if(!empty($theme_option['newsletter_desc'])):
    	    $newsletter_desc = $theme_option['newsletter_desc'];
    	endif;
    	
    	$api_key = '';
    	if(!empty($theme_option['newsletter_api_key'])):
    	  $api_key = $theme_option['newsletter_api_key'];
    	endif;
    	
    	$list_id = '';
    	if(!empty($theme_option['newsletter_list_id'])):
    	    $list_id = $theme_option['newsletter_list_id'];
    	endif;
    	
    	$background_image = '';
    	if(!empty($theme_option['newsletter_background_image']['url'])):
    	    $background_image = 'background-image: url('.$theme_option['newsletter_background_image']['url'].') !important;';
    	endif;
    	
    	$newsletter_gallery = '';
    	if(!empty($theme_option['newsletter_gallery'])):
    	    $newsletter_gallery = $theme_option['newsletter_gallery'];
    	endif;
    	$facebook = '';
    	if(!empty($theme_option['shopmartio_facebook'])):
    	    $facebook = $theme_option['shopmartio_facebook'];
    	endif;
    	$twitter = '';
    	if(!empty($theme_option['shopmartio_twitter'])):
    	    $twitter = $theme_option['shopmartio_twitter'];
    	endif;
    	$googleplus = '';
    	if(!empty($theme_option['shopmartio_googleplus'])):
    	    $googleplus = $theme_option['shopmartio_googleplus'];
    	endif;
        $whatsapp = '';
    	if(!empty($theme_option['shopmartio_whatsapp'])):
    	    $whatsapp = $theme_option['shopmartio_whatsapp'];
    	endif;
    	
    echo '<section class="shp-newsletter-defult shp-newsletter-wrapper2" style="'.esc_attr($background_image).'">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="shp-newsletter-wrap text-center">
                            <span class="shp-newsletter-icon">
                                <svg xmlns:xlink="http://www.w3.org/1999/xlink" width="50px" height="50px"><path fill-rule="evenodd" fill="rgb(96, 186, 190)" d="M47.551,47.888 C47.538,47.901 47.527,47.915 47.514,47.928 C47.483,47.959 47.450,47.987 47.417,48.012 C46.042,49.247 44.225,50.000 42.236,50.000 L40.918,50.000 C40.379,50.000 39.941,49.562 39.941,49.023 C39.941,48.484 40.379,48.046 40.918,48.046 L42.236,48.046 C43.380,48.046 44.446,47.713 45.345,47.141 L29.913,31.708 C27.204,28.999 22.796,28.999 20.087,31.708 L8.438,43.357 L21.484,43.357 C22.023,43.357 22.461,43.794 22.461,44.333 C22.461,44.873 22.023,45.310 21.484,45.310 L6.485,45.310 L4.654,47.141 C5.554,47.713 6.620,48.046 7.764,48.046 L32.220,48.046 C32.759,48.046 33.196,48.484 33.196,49.023 C33.196,49.562 32.759,50.000 32.220,50.000 L7.764,50.000 C3.483,50.000 -0.000,46.517 -0.000,42.236 L-0.000,17.183 C-0.000,16.924 0.103,16.676 0.286,16.493 L7.227,9.552 L7.227,3.906 C7.227,1.752 8.979,-0.000 11.133,-0.000 L38.867,-0.000 C41.021,-0.000 42.773,1.752 42.773,3.906 L42.773,9.601 L49.711,16.490 C49.896,16.673 50.000,16.923 50.000,17.183 L50.000,42.236 C50.000,44.462 49.057,46.471 47.551,47.888 ZM7.227,12.314 L2.358,17.183 L7.227,22.052 L7.227,12.314 ZM1.953,19.541 L1.953,42.236 C1.953,43.592 2.421,44.840 3.203,45.830 L15.723,33.310 L1.953,19.541 ZM40.820,3.906 C40.820,2.829 39.944,1.953 38.867,1.953 L11.133,1.953 C10.056,1.953 9.180,2.829 9.180,3.906 L9.180,24.005 L17.104,31.929 L18.706,30.327 C22.177,26.856 27.823,26.856 31.294,30.327 L32.896,31.929 L40.820,24.005 L40.820,3.906 ZM42.773,12.353 L42.773,22.052 L47.640,17.185 L42.773,12.353 ZM48.047,19.541 L34.277,33.310 L46.797,45.830 C47.579,44.840 48.047,43.592 48.047,42.236 L48.047,19.541 ZM14.798,24.023 C14.798,23.484 15.235,23.046 15.774,23.046 L34.226,23.046 C34.765,23.046 35.202,23.484 35.202,24.023 C35.202,24.562 34.765,24.999 34.226,24.999 L15.774,24.999 C15.235,24.999 14.798,24.562 14.798,24.023 ZM34.226,17.968 L15.774,17.968 C15.235,17.968 14.798,17.531 14.798,16.992 C14.798,16.453 15.235,16.015 15.774,16.015 L34.226,16.015 C34.765,16.015 35.202,16.453 35.202,16.992 C35.202,17.531 34.765,17.968 34.226,17.968 ZM30.110,10.937 C29.853,10.937 29.602,10.833 29.420,10.651 C29.238,10.469 29.134,10.217 29.134,9.960 C29.134,9.704 29.238,9.452 29.420,9.270 C29.602,9.088 29.853,8.984 30.110,8.984 C30.367,8.984 30.619,9.088 30.802,9.270 C30.983,9.452 31.087,9.704 31.087,9.960 C31.087,10.217 30.983,10.469 30.802,10.651 C30.619,10.833 30.367,10.937 30.110,10.937 ZM25.586,10.937 L15.774,10.937 C15.235,10.937 14.798,10.500 14.798,9.960 C14.798,9.422 15.235,8.984 15.774,8.984 L25.586,8.984 C26.125,8.984 26.562,9.422 26.562,9.960 C26.562,10.500 26.125,10.937 25.586,10.937 ZM36.573,48.046 L36.586,48.046 C37.125,48.046 37.562,48.484 37.562,49.023 C37.562,49.562 37.125,50.000 36.586,50.000 L36.573,50.000 C36.034,50.000 35.597,49.562 35.597,49.023 C35.597,48.484 36.034,48.046 36.573,48.046 Z"/></svg>
                            </span>';
                            if(!empty($newsletter_heading)):
                               echo '<h2 class="shp-newsletter-title">'.esc_html($newsletter_heading).'</h2>';
                            endif;
                            if(!empty($newsletter_desc)):
                               echo '<div><p class="shp-newsletter-des">'.wp_kses($newsletter_desc,true).'</p></div>';
                            endif;
                            echo'<div class="shp-newsletter-box">
                                  <input type="text" id="shm_newsletter" name="shm_newsletter" placeholder="'.esc_attr__('Enter Your Email Here...','shopmartio').'">
                                  <a href="javascript:void(0);" data-apikey="'.esc_attr($api_key).'" data-listid="'.esc_attr($list_id).'" id="shmid_newsletter" class="shp-btn newsletter-btn">'.esc_html__('Subscribe Now','shopmartio').'</a>
                                </div>
                                <div class="shm_messages"></div>';
                            if(!empty($facebook) || !empty($twitter) || !empty($googleplus) || !empty($whatsapp)):
                                echo '<ul class="shp-social-icons">';
                                    if(!empty($facebook)):
                                        echo '<li>
                                               <a href="'.esc_url($facebook).'">
                                                <svg xmlns:xlink="http://www.w3.org/1999/xlink" width="8px" height="16px"><path fill-rule="evenodd" d="M7.700,0.001 L5.781,-0.002 C3.625,-0.002 2.231,1.544 2.231,3.936 L2.231,5.752 L0.301,5.752 C0.134,5.752 -0.001,5.897 -0.001,6.078 L-0.001,8.709 C-0.001,8.889 0.134,9.035 0.301,9.035 L2.231,9.035 L2.231,15.673 C2.231,15.852 2.366,15.999 2.533,15.999 L5.050,15.999 C5.217,15.999 5.352,15.852 5.352,15.673 L5.352,9.035 L7.608,9.035 C7.775,9.035 7.910,8.889 7.910,8.709 L7.911,6.078 C7.911,5.992 7.879,5.909 7.823,5.847 C7.766,5.786 7.690,5.752 7.609,5.752 L5.352,5.752 L5.352,4.212 C5.352,3.473 5.515,3.098 6.406,3.098 L7.700,3.096 C7.866,3.096 8.001,2.950 8.001,2.770 L8.001,0.328 C8.001,0.147 7.866,0.002 7.700,0.001 L7.700,0.001 Z"/></svg>
                                               </a>
                                            </li>';
                                    endif;   
                                    if($twitter):
                                          echo '<li>
                                                 <a href="'.esc_url($twitter).'">
                                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19px" height="16px"><path fill-rule="evenodd" d="M19.000,1.893 C18.293,2.214 17.540,2.427 16.755,2.531 C17.563,2.030 18.179,1.246 18.469,0.299 C17.716,0.765 16.885,1.093 15.999,1.277 C15.284,0.487 14.265,-0.001 13.154,-0.001 C10.997,-0.001 9.261,1.813 9.261,4.037 C9.261,4.357 9.287,4.665 9.351,4.957 C6.113,4.794 3.248,3.185 1.323,0.734 C0.987,1.339 0.789,2.030 0.789,2.775 C0.789,4.174 1.484,5.413 2.520,6.131 C1.894,6.118 1.280,5.930 0.760,5.633 C0.760,5.646 0.760,5.662 0.760,5.677 C0.760,7.640 2.110,9.269 3.880,9.645 C3.563,9.734 3.218,9.777 2.859,9.777 C2.610,9.777 2.358,9.763 2.122,9.709 C2.626,11.307 4.059,12.482 5.762,12.521 C4.436,13.595 2.754,14.243 0.932,14.243 C0.613,14.243 0.306,14.228 -0.000,14.187 C1.725,15.340 3.770,15.999 5.975,15.999 C13.143,15.999 17.062,9.845 17.062,4.511 C17.062,4.332 17.056,4.160 17.047,3.989 C17.821,3.421 18.470,2.710 19.000,1.893 Z"/></svg>
                                                 </a>
                                               </li>';
                                    endif;    
                                    if(!empty($googleplus)):
                                       echo '<li>
                                                <a href="'.esc_url($googleplus).'">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="16px"><path fill-rule="evenodd" d="M12.981,7.743 L12.981,9.842 L11.988,9.842 L11.988,7.743 L9.979,7.743 L9.979,6.701 L11.988,6.701 L11.988,4.615 L12.981,4.615 L12.981,6.701 L15.000,6.701 L15.000,7.743 L12.981,7.743 ZM7.490,0.849 C7.951,1.244 8.913,2.079 8.913,3.666 C8.913,5.209 8.071,5.941 7.228,6.629 C6.967,6.900 6.666,7.192 6.666,7.652 C6.666,8.109 6.967,8.360 7.187,8.549 L7.911,9.133 C8.795,9.904 9.597,10.613 9.597,12.055 C9.597,14.017 7.769,15.998 4.316,15.998 C1.404,15.998 -0.000,14.559 -0.000,13.014 C-0.000,12.263 0.361,11.200 1.545,10.468 C2.790,9.676 4.477,9.572 5.381,9.508 C5.099,9.132 4.778,8.736 4.778,8.089 C4.778,7.735 4.879,7.527 4.979,7.276 C4.757,7.297 4.538,7.317 4.337,7.317 C2.209,7.317 1.004,5.668 1.004,4.040 C1.004,3.082 1.425,2.016 2.289,1.244 C3.433,0.264 4.998,-0.002 6.084,-0.002 L10.039,-0.002 L8.733,0.849 L7.490,0.849 ZM3.493,10.428 C3.052,10.594 1.767,11.095 1.767,12.576 C1.767,14.059 3.153,15.123 5.300,15.123 C7.228,15.123 8.253,14.161 8.252,12.870 C8.252,11.802 7.590,11.240 6.063,10.114 C5.903,10.093 5.802,10.093 5.602,10.093 C5.421,10.093 4.337,10.134 3.493,10.428 ZM4.619,0.786 C4.095,0.786 3.534,1.057 3.212,1.475 C2.871,1.914 2.771,2.475 2.771,3.019 C2.771,4.417 3.553,6.733 5.281,6.733 C5.783,6.733 6.323,6.483 6.645,6.149 C7.108,5.667 7.147,5.002 7.147,4.625 C7.147,3.123 6.284,0.786 4.619,0.786 Z"/></svg>
                                                </a>
                                            </li>';
                                    endif;       
                                    if(!empty($whatsapp)):
                                        echo '<li>
                                               <a href="'.esc_url($whatsapp).'">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="16px"><path fill-rule="evenodd" d="M12.814,2.324 C11.404,0.826 9.529,0.000 7.531,-0.001 C3.414,-0.001 0.064,3.556 0.062,7.927 C0.062,9.324 0.406,10.688 1.059,11.890 L-0.000,15.999 L3.959,14.897 C5.050,15.528 6.278,15.861 7.528,15.861 L7.531,15.861 C11.648,15.861 14.998,12.305 15.000,7.933 C15.001,5.815 14.224,3.822 12.814,2.324 L12.814,2.324 ZM7.531,14.523 L7.529,14.523 C6.415,14.522 5.322,14.204 4.369,13.604 L4.143,13.461 L1.793,14.115 L2.420,11.683 L2.272,11.434 C1.651,10.385 1.323,9.173 1.323,7.927 C1.325,4.294 4.110,1.337 7.534,1.337 C9.192,1.338 10.750,2.025 11.922,3.271 C13.094,4.517 13.739,6.172 13.739,7.933 C13.737,11.566 10.953,14.523 7.531,14.523 L7.531,14.523 ZM10.936,9.587 C10.750,9.488 9.832,9.008 9.661,8.943 C9.490,8.876 9.365,8.843 9.241,9.042 C9.117,9.240 8.759,9.686 8.650,9.818 C8.541,9.950 8.433,9.967 8.246,9.868 C8.059,9.769 7.458,9.559 6.745,8.885 C6.190,8.359 5.816,7.710 5.707,7.513 C5.598,7.314 5.706,7.217 5.789,7.108 C5.991,6.842 6.193,6.563 6.255,6.430 C6.318,6.298 6.287,6.183 6.240,6.084 C6.193,5.984 5.820,5.009 5.664,4.612 C5.513,4.226 5.359,4.278 5.245,4.272 C5.136,4.266 5.011,4.265 4.887,4.265 C4.763,4.265 4.560,4.315 4.389,4.513 C4.218,4.712 3.736,5.191 3.736,6.166 C3.736,7.141 4.405,8.083 4.498,8.216 C4.591,8.347 5.814,10.349 7.686,11.206 C8.131,11.411 8.479,11.532 8.750,11.624 C9.197,11.774 9.604,11.753 9.925,11.703 C10.284,11.646 11.029,11.223 11.185,10.760 C11.341,10.297 11.341,9.901 11.294,9.818 C11.247,9.736 11.123,9.686 10.936,9.587 L10.936,9.587 Z"/></svg>
                                               </a>
                                             </li>';
                                    endif;       
                                   echo '</ul>';
                            endif;
                        echo'</div>
                          </div>
                      </div>
                   </div>';
                  
            if(!empty($newsletter_gallery)):
                $images_id = explode(',', $newsletter_gallery);
                echo '<div class="shp-newletter-moving-wrap">
                    <ul class="shp-newletter-moving-list">';
					foreach( $images_id as $imgid ):
					   $src = wp_get_attachment_image_src($imgid ,'full');
					   echo '<li><img src="'.esc_url($src[0]).'" alt="'.esc_attr__('image','shopmartio').'" class="img-fluid"></li>';
					endforeach;
                echo  '</ul>        
                 </div>';
            endif;
        echo '</section>';
    
    }
}
 
/** 
 * Shopmartion Woocommerce Register form start
 */
add_action( 'woocommerce_register_form_start', 'shopmartion_wooc_extra_register_fields' );
function shopmartion_wooc_extra_register_fields() {?>
   <p class="form-row form-row-wide">
   <label for="reg_billing_phone"><?php _e( 'Phone', 'shopmartion' ); ?></label>
   <input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
   </p>
   <p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e( 'First name', 'shopmartion' ); ?><span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
   </p>
   <p class="form-row form-row-last">
   <label for="reg_billing_last_name"><?php _e( 'Last name', 'shopmartion' ); ?><span class="required">*</span></label>
   <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
   </p>
   <div class="clear"></div>
<?php
}

/**
* Shopmartion register fields Validating.
*/
add_action( 'woocommerce_register_post', 'shopmartion_wooc_validate_extra_register_fields', 10, 3 );
function shopmartion_wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
      if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
             $validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'shopmartion' ) );
      }
      if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
             $validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'shopmartion' ) );
      }
    return $validation_errors;
}

/**
* Shopmartion Below code save extra fields.
*/
add_action('woocommerce_created_customer', 'shopmartion_wooc_save_extra_register_fields');
function shopmartion_wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_phone'] ) ) {
                 // Phone input filed which is used in WooCommerce
                 update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
          }
      if ( isset( $_POST['billing_first_name'] ) ) {
             //First name field which is by default
             update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
             // First name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
      }
      if ( isset( $_POST['billing_last_name'] ) ) {
             // Last name field which is by default
             update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
             // Last name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
      }
}

/**
 * Shopmartion WooCommerce Count  
 */ 
if(!function_exists('shopmartion_sell_countdowen')):
    function shopmartion_sell_countdowen($postid=false){
        
     $sale_time_title = get_post_meta($postid, 'shopmartion_sale_time_title', true);
     $sale_datetime = get_post_meta($postid, 'shopmartion_sale_datetime', true);  
     if(!empty($sale_datetime) && !empty($sale_time_title)):
       echo '<p class="shp-price-counter">'.esc_html($sale_time_title).' <span id="shp_timer" data-datetime="'.esc_attr($sale_datetime).'"></span></p>';
     endif;
     
    }
endif;