jQuery(document).ready( function($){
    "use strict";
    
    /**
     * Mailchimp jQuery
     */ 
	$('#shmid_newsletter').on('click',function(e) {
        e.preventDefault();
	    var subc_email = $("#shm_newsletter").val();
        var apikey = $(this).attr('data-apikey');
        var listid = $(this).attr('data-listid');
		if (!validateEmail(subc_email))  
        {  
         $(".shm_messages").html("<span style='color:red;'>Please make sure you enter a valid email address.</span>");
        }else{
		    jQuery.ajax({ 
			    type : "post",
                url : frontadminajax.ajax_url,
                data : {'action': "shopmartio_send_newsletter", apikey : apikey,listid:listid,subc_email:subc_email}, 
				success: function(response) {
				   
                        if(response=="200" ){
                           $(".shm_messages").html('<span style="color:green;">You have successfully subscribed to our mailing list.</span>');
						} else if(response=="204"){
                            $(".shm_messages").html('<span style="color:red;">Your Email Alreday Subscribed List</span>');  
						}else{
						  $(".shm_messages").html('<span style="color:red;">Please Check Email Address</span>');  
						}
					}
                });
	        }
	 });
	 
	 /**
	  * Widget MailChimp
	  */ 
	 $('#ss_newslatter').on('click',function(e) {
        e.preventDefault();
	    var subc_email = $("#ns_email").val();
        var apikey = $('#ns_apikey').val();
        var listid = $('#ns_list').val();
		if (!validateEmail(subc_email))  
        {  
         $(".ss_messages").html("<span style='color:red;'>Please make sure you enter a valid email address.</span>");
        }else{
		    jQuery.ajax({ 
			    type : "post",
                url : frontadminajax.ajax_url,
                data : {'action': "shopmartio_send_newsletter", apikey : apikey,listid:listid,subc_email:subc_email}, 
				success: function(response) {
				   
                        if(response=="200" ){
                           $(".ss_messages").html('<span style="color:green;">You have successfully subscribed to our mailing list.</span>');
						} else if(response=="204"){
                            $(".ss_messages").html('<span style="color:red;">Your Email Alreday Subscribed List</span>');  
						}else{
						  $(".ss_messages").html('<span style="color:red;">Please Check Email Address</span>');  
						}
					}
                });
	        }
	 });
	  
   /**
	* email checker
	*/ 
    function validateEmail(uemail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	    if (filter.test(uemail)) {
	        return true;
	    }
	    else {
	        return false;
	    }
    }
    
    
    /**
     * WooCommerce Product Filter Ajax
     */ 
    $('.product_cat_filter').on('click',function(e) {
        e.preventDefault();
        $(".shp-filter-menus>ul>li ").find('a').removeClass("gallery-active");
        
        $(this).addClass('gallery-active');
        $('.ajax-loader').show();
        var showp = $('.shp-all').attr('data-show');
	    var product_cat = $(this).attr('data-filter');
	     
        jQuery.ajax({ 
		    type : "post",
            url : frontadminajax.ajax_url,
            data : {'action': "shopmartion_product_filter_catogeri_wise", product_cat : product_cat,showp:showp}, 
            
			success: function(response) {
			     $('#shp-porduct-load').html(response);
			     $('.ajax-loader').hide();
			     
				}
			});
	});
	 
	/**
     * WooCommerce Product Load More    
     */ 
    var product_loadmore = 0; 
    $('.product_load_more').on('click',function(e) {
        e.preventDefault();
        $('.ajax-loader').show();
        var product_cat = $('.gallery-active').attr('data-filter');
        var product_load = $(this).attr('data-loadmore');
	    var product_showp = $(this).attr('data-show');
        var load_more = parseFloat(product_load)+parseFloat(product_showp);
        load_more +=product_loadmore;
        product_loadmore++;
        jQuery.ajax({ 
		    type : "post",
            url : frontadminajax.ajax_url,
            data : {'action': "shopmartion_product_load_more", product_cat : product_cat,load_more:load_more}, 
			success: function(response) {
			       $('#shp-porduct-load').html(response);  
			       $('.ajax-loader').hide();
				}
            }); 
	        
	 });
	 
	/**
	 * wishlist
	 */ 
	 
	$('.add_to_wishlist').on('click', function() {
       toastr.success('Product has been added to your wishlist');
    });
    
    setInterval(function(){ 
        var datetimes = $('#shp_timer').attr('data-datetime');
        if(datetimes){
        var countdowen =shopmartion_updateTimer(datetimes);
        }
     }, 1000);
});

/**
 * Shopmartion WooCommerce Count dowen
 */ 
function shopmartion_updateTimer(datetime) {
    future = Date.parse(datetime);
    now = new Date();
    diff = future - now;

    days = Math.floor(diff / (1000 * 60 * 60 * 24));
    hours = Math.floor(diff / (1000 * 60 * 60));
    mins = Math.floor(diff / (1000 * 60));
    secs = Math.floor(diff / 1000);

    d = days;
    h = hours - days * 24;
    m = mins - hours * 60;
    s = secs - mins * 60;
  if(Math.sign(d) > 0 ){
    document.getElementById("shp_timer")
        .innerHTML =
        '<span>' + h + '<span>h:</span></span>' +
        '<span>' + m + '<span>m:</span></span>' +
        '<span>' + s + '<span>s</span></span>';
  }else{
    document.getElementById("timer")
        .innerHTML = '<h1>Seling Time Out</h1>';
       
  }
}