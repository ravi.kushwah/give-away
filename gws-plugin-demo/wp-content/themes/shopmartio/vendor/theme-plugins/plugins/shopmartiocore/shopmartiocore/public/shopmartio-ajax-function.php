<?php 
/**
 * Shopmartio Newsletter 
 */
add_action('wp_ajax_shopmartio_send_newsletter', 'shopmartio_send_newsletter');
add_action('wp_ajax_nopriv_shopmartio_send_newsletter', 'shopmartio_send_newsletter'); 
function shopmartio_send_newsletter(){ 
    require_once 'mailchimp/MailChimp.php';
	$api_key = '';
	if(!empty($_POST['apikey'])):
	   $api_key = $_POST['apikey'];
	endif;
	$list_id = '';
	if(!empty($_POST['listid'])):
       $list_id = $_POST['listid']; 
	endif;
	$subc_email = '';
	if(!empty($_POST['subc_email'])):
        $subc_email = $_POST['subc_email'];
	endif;
    $mailchimpob = new MailChimp_Subscribed($api_key,$list_id,$subc_email);
    wp_die(); 
}

/**
 * WooCommerce Product Filter Catogeri Wise
 */  
add_action('wp_ajax_shopmartion_product_filter_catogeri_wise','shopmartion_product_filter_catogeri_wise');
add_action('wp_ajax_nopriv_shopmartion_product_filter_catogeri_wise','shopmartion_product_filter_catogeri_wise');
function shopmartion_product_filter_catogeri_wise(){
    
    $cat_slug = '';
    if(isset($_POST['product_cat'])):
      $cat_slug = $_POST['product_cat'];  
    endif;
    $showp = '';
    if(isset($_POST['showp'])):
      $showp = $_POST['showp'];  
    endif;
     
    if($cat_slug == 'all'):
        
        $args = array(
    	    'post_type' =>'product',
    	    'post_status' => 'publish',
    	    'posts_per_page' => $showp
            );    
    else:
        $args = array(
    	    'post_type' =>'product',
    	    'post_status' => 'publish',
    	    'tax_query' => array(
                    array(
                     'taxonomy'  => 'product_cat',
                     'field'     => 'slug',
                     'terms'     => $cat_slug
                    )
                )
            );  
    endif;
	$product_query = new WP_Query($args);
	    if($product_query->have_posts() ) :
            while($product_query->have_posts()): $product_query->the_post();
              if(class_exists('WooCommerce')):
                $product = wc_get_product(get_the_ID());
    			if(has_post_thumbnail(get_the_ID())):
    				$attachment_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full');
    			endif;
    			echo '<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">'.shopmartio_content_products().'</div>';
    			endif;
            endwhile;
            wp_reset_query();
        endif;
    wp_die();    
}

/**
 * WooCommerce Product Load More Ajax
 */ 
add_action('wp_ajax_shopmartion_product_load_more','shopmartion_product_load_more');
add_action('wp_ajax_nopriv_shopmartion_product_load_more','shopmartion_product_load_more');
function shopmartion_product_load_more(){
    $product_cat = '';
    if(isset($_POST['product_cat'])):
      $product_cat = $_POST['product_cat'];  
    endif;
    $load_more = '';
    if(isset($_POST['load_more'])):
      $load_more = $_POST['load_more'];  
    endif;
   if($product_cat == 'all'):
        $args = array(
    	    'post_type' =>'product',
    	    'post_status' => 'publish',
    	    'posts_per_page' => $load_more
            );    
    else:
        $args = array(
    	    'post_type' =>'product',
    	    'post_status' => 'publish',
    	    'posts_per_page' => $load_more,
    	    'tax_query' => array(
                    array(
                     'taxonomy'  => 'product_cat',
                     'field'     => 'slug',
                     'terms'     => $product_cat
                    )
                )
            );  
    endif;
	$product_query = new WP_Query($args);
	if($product_query->have_posts() ) :
	    
        while($product_query->have_posts()): $product_query->the_post();
          if(class_exists('WooCommerce')):
            $product = wc_get_product(get_the_ID());
			if(has_post_thumbnail(get_the_ID())):
			    $attachment_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full');
			endif;
			  
			echo '<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12">'.shopmartio_content_products().'</div>';
			 
			endif;
        endwhile;
        wp_reset_query();
    endif;
    $args = $num = '';
    $args = array( 'post_type'=>'product','posts_per_page' => -1 );
    $num = count(get_posts( $args ));
    if($load_more >= $num ) 
    { ?>
	<style scoped> 
	.product_load_more{display:none;}
	</style>
    <?php
    }
 wp_die();   
}
 
   
/**
 * woocommerce add to cart Ajax Function 
 */
add_action('wp_ajax_shopmartion_woocommerce_ajax_add_to_cart', 'shopmartion_woocommerce_ajax_add_to_cart'); 
add_action('wp_ajax_nopriv_shopmartion_woocommerce_ajax_add_to_cart', 'shopmartion_woocommerce_ajax_add_to_cart');
function shopmartion_woocommerce_ajax_add_to_cart() {
     
   $product_id = '';
   if(!empty($_POST['product_id'])){
      $product_id = $_POST['product_id'];
   }
   global $woocommerce;
   if($woocommerce->cart->add_to_cart( $product_id,1)){
       
      printf('<div class="md_add_cart_message"><a href="%s" class="button">%s</a> %s</div>', get_permalink(woocommerce_get_page_id('cart')), esc_html__('View Cart', 'shopmartion'), esc_html__('Product successfully added to your cart.', '') );
        
   }else{
       echo esc_html__('Some Problem!','shopmartion'); 
   }
   
wp_die();   
}
 