<?php
/**
 * Shopmartio Widgets 
 * version 1.0.0
 * Register and load the widget
 */
 
function shopmartion_load_widget() {    
    register_widget('Shopmartio_widget_textlogo');
    register_widget('Shopmartio_widget_menu'); 
    register_widget('Shopmartio_widget_newsletter'); 
    register_widget('Shopmartio_widget_recentpost');
    register_widget('Shopmartio_widget_image');
    register_widget('Shopmartio_contact_information');
    register_widget('shopmartion_user_profile_widget');
}
add_action('widgets_init', 'shopmartion_load_widget');

/**
 * Shopmartion User Profile Widget
 */ 
class shopmartion_user_profile_widget extends WP_Widget{
    function  __construct() {
    	parent::__construct(
        'shopmartion_user_profile_widget', // Base ID of your widget
            esc_html__('Shopmartio User Profile', 'shopmartio'),  // Widget name will appear in UI
            array( 'description' => esc_html__( 'Shopmartio User Profile', 'shopmartio' ), ) // Widget description
            );
            
    }
    public function widget( $args, $instance ) {
        $page_title = '';
        if(isset( $instance['page_title'] ) ):
           $page_title = $instance['page_title'];
        endif;
        $page_url = '';
        if(isset( $instance['page_url'] ) ):
           $page_url = $instance['page_url'];
        endif;
        $logout_url = wp_logout_url(home_url());
        echo $args['before_widget'];
        ?>
        <div class="shp_user_profile">
          <?php if(is_user_logged_in()): ?>
             <a href="<?php echo esc_url($logout_url); ?>"><?php echo esc_html__('Logout','shopmartio'); ?></a>
          <?php else: ?>
             <a href="<?php echo esc_url($page_url); ?>"><?php echo esc_html($page_title); ?></a>
          <?php endif; ?>
        </div>
        <?php
        echo $args['after_widget'];
    }
    public function form( $instance ) {
    
    $page_title = '';
    if(isset( $instance['page_title'] ) ):
       $page_title = $instance['page_title'];
    endif;
    $page_url = '';
    if(isset( $instance['page_url'] ) ):
       $page_url = $instance['page_url'];
    endif;
    ?>
    <p>
    <label for="<?php echo esc_attr($this->get_field_id('page_title')); ?>">
      <?php esc_html_e('Page Title :','shopmartio'); ?>
    </label>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('page_title')); ?>" name="<?php echo esc_attr($this->get_field_name('page_title')); ?>" type="text" value="<?php echo esc_attr($page_title); ?>" />
      
    <label for="<?php echo esc_attr($this->get_field_id('page_url')); ?>">
     <?php esc_html_e('Page Url :','shopmartio'); ?>
    </label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('page_url')); ?>" name="<?php echo esc_attr($this->get_field_name('page_url')); ?>" type="text" value="<?php echo esc_attr($page_url); ?>" />
    </p>
    <?php    
    }
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['page_title'] = ( ! empty( $new_instance['page_title'] ) ) ? strip_tags( $new_instance['page_title'] ) : '';
        $instance['page_url'] = ( ! empty( $new_instance['page_url'] ) ) ? strip_tags( $new_instance['page_url'] ) : '';
        return $instance;
    }
    
}
/**
 * Text Summary and Logo Widget
 **/ 
class Shopmartio_widget_textlogo extends WP_Widget {
    function  __construct() {
    	parent::__construct(
        'Shopmartio_widget_textlogo', // Base ID of your widget
            esc_html__('Shopmartio Text Summary & Logo', 'shopmartio'),  // Widget name will appear in UI
            array( 'description' => esc_html__( 'Shopmartio Text summary and logo', 'shopmartio' ), ) // Widget description
            );
    }

    public function widget( $args, $instance ) {
        
        $main_site_logo = '';
        if ( isset( $instance[ 'main_site_logo' ] ) ) {
            $main_site_logo = $instance[ 'main_site_logo' ];
        }
        $widget_desc_data = '';
        if ( isset( $instance[ 'widget_desc_data' ] ) ) {
            $widget_desc_data = $instance[ 'widget_desc_data' ];
        }
        $social_title = '';
        if ( isset( $instance[ 'social_title' ] ) ) {
            $social_title = $instance[ 'social_title' ];
        }
        $facebook_url = '';
        if ( isset( $instance[ 'facebook_url' ] ) ) {
            $facebook_url = $instance[ 'facebook_url' ];
        }
        $twitter_url = '';
        if ( isset( $instance[ 'twitter_url' ] ) ) {
            $twitter_url = $instance[ 'twitter_url' ];
        }
         $googleplus_url = '';
        if ( isset( $instance[ 'googleplus_url' ] ) ) {
            $googleplus_url = $instance[ 'googleplus_url' ];
        }
        $youtube_url = '';
        if ( isset( $instance[ 'youtube_url' ] ) ) {
            $youtube_url = $instance[ 'youtube_url' ];
        }
        $payment_title = '';
        if(isset( $instance['payment_title'] ) ) {
           $payment_title = $instance['payment_title'];
        }
        $payment_url = '';
        if(isset( $instance['payment_url'] ) ) {
           $payment_url = $instance['payment_url'];
        }
        ?>
        <div class="shp-footer-logo-text">
            <?php if(!empty($main_site_logo)): ?>
                <div class="shp-footer-logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>">
                        <img src="<?php echo esc_url($main_site_logo); ?>" alt="<?php esc_attr_e('logo','shopmartio'); ?>" />
                    </a>                    
                </div>
            <?php else: ?>
                <div class="shp-footer-logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-logo.svg" alt="<?php esc_attr_e('logo','shopmartio'); ?>" />
                    </a>
                </div>
            <?php endif; ?>
            <?php if(!empty($widget_desc_data)): ?>
                <div class="shp-footer-text">
                    <p><?php echo esc_html($widget_desc_data); ?></p>
                </div>  
            <?php endif; ?>                      
            <div class="shp-footer-social">
                <?php if(!empty($social_title)): ?>
                   <?php echo $args['before_title'].esc_html($social_title).$args['after_title']; ?>
                <?php endif; ?>
                
                <ul class="shp-social-icons">
                    <?php if(!empty($facebook_url)): ?>
                        <li>
                            <a href="<?php echo esc_url($facebook_url);?>" target ="<?php esc_attr_e('_blank','shopmartio'); ?>">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                    <?php endif; 
                         if(!empty($twitter_url)): ?>
                        <li>
                            <a href="<?php echo esc_url($twitter_url);?>" target ="<?php esc_attr_e('_blank','shopmartio'); ?>">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    <?php endif; 
                          if(!empty($googleplus_url)): ?>
                        <li>
                            <a href="<?php echo esc_url($googleplus_url);?>" target ="<?php esc_attr_e('_blank','shopmartio'); ?>">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                    <?php endif;
                    if(!empty($youtube_url)): ?>
                        <li>
                            <a href="<?php echo esc_url($youtube_url);?>" target ="<?php esc_attr_e('_blank','shopmartio'); ?>">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
                <?php 
                echo $args['before_title'].esc_html($payment_title).$args['after_title'];
                if(!empty($payment_url)):
                ?>
                <div class="shp-foot-payment">
                    <img src="<?php echo esc_url($payment_url); ?>" alt="<?php echo esc_attr__('payment','shopmartio'); ?>" class="img-fluid">
                </div>
                <?php endif; ?>
            </div>
        </div> 
    <?php
    }    
    public function form( $instance ) {
        $main_site_logo = '';
        if ( isset( $instance[ 'main_site_logo' ] ) ) {
            $main_site_logo = $instance[ 'main_site_logo' ];
        }
        $widget_desc_data = '';
        if ( isset( $instance[ 'widget_desc_data' ] ) ) {
            $widget_desc_data = $instance[ 'widget_desc_data' ];
        }
        $social_title = '';
        if ( isset( $instance[ 'social_title' ] ) ) {
            $social_title = $instance[ 'social_title' ];
        }
        $facebook_url = '';
        if ( isset( $instance[ 'facebook_url' ] ) ) {
            $facebook_url = $instance[ 'facebook_url' ];
        }
        $twitter_url = '';
        if ( isset( $instance[ 'twitter_url' ] ) ) {
            $twitter_url = $instance[ 'twitter_url' ];
        }
         $googleplus_url = '';
        if ( isset( $instance[ 'googleplus_url' ] ) ) {
            $googleplus_url = $instance[ 'googleplus_url' ];
        }
        $youtube_url = '';
        if ( isset( $instance[ 'youtube_url' ] ) ) {
            $youtube_url = $instance[ 'youtube_url' ];
        }
        
        $payment_title = '';
        if(isset( $instance['payment_title'] ) ) {
           $payment_title = $instance['payment_title'];
        }
        $payment_url = '';
        if(isset( $instance['payment_url'] ) ) {
           $payment_url = $instance['payment_url'];
        }
        ?>  
            <p class="img-prev">
                <?php 
                if (isset($main_site_logo)) { 
                    echo '<img src="'.esc_url($main_site_logo).'" class="window_image_url" with="100" height="100"/>';
                } 
                ?>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('main_site_logo'); ?>">
                    <?php esc_html_e('upload Image', 'shopmartio') ?>
                </label> 
                <input  id="<?php echo $this->get_field_id('main_site_logo'); ?>" type="hidden" class="upload_logo_image_text" name="<?php echo $this->get_field_name('main_site_logo'); ?>" value="<?php if (isset($main_site_logo)) echo esc_attr($main_site_logo); ?>" width="50"/>
                <input data-title="<?php esc_attr_e('Image','shopmartio'); ?>" data-btntext="<?php esc_attr_e('Select it','shopmartio'); ?>" class="button upload_logo_image_button" type="button" value="<?php esc_attr_e('Upload','shopmartio') ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id( 'widget_desc_data' )); ?>">
                    <?php esc_html_e( 'Text Content :','shopmartio'); ?>
                </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'widget_desc_data' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'widget_desc_data' )); ?>" type="text" value="<?php echo esc_attr($widget_desc_data); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id( 'social_title' )); ?>">
                    <?php esc_html_e( 'Social Title :','shopmartio'); ?>
                </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'social_title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'social_title' )); ?>" type="text" value="<?php echo esc_attr($social_title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id( 'facebook_url' )); ?>">
                    <?php esc_html_e( 'Facebook URL :','shopmartio'); ?>
                </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'facebook_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'facebook_url' )); ?>" type="text" value="<?php echo esc_attr($facebook_url); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id( 'twitter_url' )); ?>">
                    <?php esc_html_e( 'Twitter URL :','shopmartio'); ?>
                </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'twitter_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter_url' )); ?>" type="text" value="<?php echo esc_attr($twitter_url); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id( 'googleplus_url' )); ?>">
                    <?php esc_html_e( 'Google Plus URL :','shopmartio'); ?>
                </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'googleplus_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'googleplus_url' )); ?>" type="text" value="<?php echo esc_attr($googleplus_url); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id( 'youtube_url' )); ?>">
                    <?php esc_html_e( 'Youtube URL :','shopmartio'); ?>
                </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'youtube_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'youtube_url' )); ?>" type="text" value="<?php echo esc_attr($youtube_url); ?>" />
            </p>
            <p>
            <label for="<?php echo esc_attr($this->get_field_id('payment_title')); ?>">
             <?php esc_html_e('Payment Title :','shopmartio'); ?>
            </label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('payment_title')); ?>" name="<?php echo esc_attr($this->get_field_name('payment_title')); ?>" type="text" value="<?php echo esc_attr($payment_title); ?>" />
            </p>
            <p>
            <label for="<?php echo esc_attr($this->get_field_id('payment_url')); ?>">
                <?php esc_html_e( 'Payment Image URL :','shopmartio'); ?>
            </label>
            <input class="upload_payment_image_text" id="<?php echo esc_attr($this->get_field_id('payment_url')); ?>" name="<?php echo esc_attr($this->get_field_name('payment_url')); ?>" type="text" value="<?php echo esc_attr($payment_url); ?>" />
            <input data-title="<?php esc_attr_e('Image','shopmartio'); ?>" data-btntext="<?php esc_attr_e('Select it','shopmartio'); ?>" class="button upload_payment_image_button" type="button" value="<?php esc_attr_e('Upload','shopmartio') ?>" />
            </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['main_site_logo'] = ( ! empty( $new_instance['main_site_logo'] ) ) ? strip_tags( $new_instance['main_site_logo'] ) : '';
        $instance['widget_desc_data'] = ( ! empty( $new_instance['widget_desc_data'] ) ) ? strip_tags( $new_instance['widget_desc_data'] ) : '';
        $instance['social_title'] = ( ! empty( $new_instance['social_title'] ) ) ? strip_tags( $new_instance['social_title'] ) : '';
        $instance['facebook_url'] = ( ! empty( $new_instance['facebook_url'] ) ) ? strip_tags( $new_instance['facebook_url'] ) : '';
        $instance['twitter_url'] = ( ! empty( $new_instance['twitter_url'] ) ) ? strip_tags( $new_instance['twitter_url'] ) : '';
        $instance['googleplus_url'] = ( ! empty( $new_instance['googleplus_url'] ) ) ? strip_tags( $new_instance['googleplus_url'] ) : '';
        $instance['youtube_url'] = ( ! empty( $new_instance['youtube_url'] ) ) ? strip_tags( $new_instance['youtube_url'] ) : '';
        $instance['payment_title'] = ( ! empty( $new_instance['payment_title'] ) ) ? strip_tags( $new_instance['payment_title'] ) : '';
        $instance['payment_url'] = ( ! empty( $new_instance['payment_url'] ) ) ? strip_tags( $new_instance['payment_url'] ) : '';
        return $instance;
    }
}

/**
 * Shopmartio Widget Menu
 **/ 

class Shopmartio_widget_menu extends WP_Widget {
    
    function  __construct() {
	parent::__construct(
		'Shopmartio_widget_menu', // Base ID of your widget
		 esc_html__('shopmartio Menu Option', 'shopmartio'),  // Widget name will appear in UI
		   array( 'description' => esc_html__( 'shopmartio Menu Option', 'shopmartio' ), ) // Widget description
		 );
    } 

    public function widget( $args, $instance ) {
        $app_widget_title = '';
        if ( isset( $instance[ 'app_widget_title' ] ) ) {
            $app_widget_title = $instance[ 'app_widget_title' ];
        }
        $nav_menu = ! empty( $instance['nav_menu'] ) ? wp_get_nav_menu_object( $instance['nav_menu'] ) : false;
        if (!$nav_menu):
        return;
        endif; 
        ?>
        <div class="shp-nav-menus">
            <?php if(!empty($app_widget_title)): ?>
            <?php echo $args['before_title'].esc_html($app_widget_title).$args['after_title']; ?>
            <?php endif; ?>
            <?php wp_nav_menu( array( 'menu' => $nav_menu) ); ?>
        </div>
    <?php
    }
    
    public function form( $instance ) {
        $app_widget_title = '';
        if ( isset( $instance[ 'app_widget_title' ]) ) {
            $app_widget_title = $instance[ 'app_widget_title' ];
        }
        $nav_menu = '';
        if ( isset( $instance[ 'nav_menu' ] ) ) {
          $nav_menu = $instance[ 'nav_menu' ];
        }
        $menus = get_terms('nav_menu', array( 'hide_empty' => false ) );
            if ( !$menus ):
               echo '<p>'. sprintf( esc_html__('No menus have been created yet.<a href="%s">Create some</a>.'), admin_url('nav-menus.php') ) .'</p>';
            return;
	    endif;
        ?>
        <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'app_widget_title' )); ?>">
            <?php esc_html_e( 'Title:','shopmartio'); ?>
        </label> 
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'app_widget_title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'app_widget_title' )); ?>" type="text" value="<?php echo esc_attr( $app_widget_title ); ?>" />
        </p>
        <p class="img-prev">
          <?php if (isset($gplay_image_url)) { 
                       echo '<img src="'.esc_url($gplay_image_url).'" class="window_image_url">';
                       
                       } ?>
        </p>
        <p>
        <label for="<?php echo $this->get_field_id('nav_menu'); ?>"><?php esc_html_e('Select Menu:','shopmartio'); ?></label>
        <select id="<?php echo $this->get_field_id('nav_menu'); ?>" name="<?php echo $this->get_field_name('nav_menu'); ?>">
        <?php
            foreach ( $menus as $menu ):
                $selected = $nav_menu == $menu->term_id ? ' selected="selected"' : '';
                echo '<option'.esc_attr($selected).' value="'.esc_attr($menu->term_id).'">'.esc_html($menu->name).'</option>';
            endforeach;  
        ?>
        </select>
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['app_widget_title'] = ( ! empty( $new_instance['app_widget_title'] ) ) ? strip_tags( $new_instance['app_widget_title'] ) : '';
        $instance['nav_menu'] = ( ! empty( $new_instance['nav_menu'] ) ) ? strip_tags( $new_instance['nav_menu'] ) : '';
        return $instance;
    }
}

/**
 * Newsletter Widget
 **/ 
class Shopmartio_widget_newsletter extends WP_Widget {
    function  __construct() {
    	parent::__construct(
        'shopmartio_widget_newsletter', // Base ID of your widget
            esc_html__('shopmartio Newsletter Widget', 'shopmartio'),  // Widget name will appear in UI
            array( 'description' => esc_html__( 'Newsletter Widget', 'shopmartio' ), ) // Widget description
            );
    }

    public function widget( $args, $instance ) {
        $widget_newsletter_title = '';
        if ( isset( $instance[ 'widget_newsletter_title' ] ) ) {
            $widget_newsletter_title = $instance[ 'widget_newsletter_title' ];
        }
        $widget_desc_newsletter = '';
        if ( isset( $instance[ 'widget_desc_newsletter' ] ) ) {
            $widget_desc_newsletter = $instance[ 'widget_desc_newsletter' ];
        }
        $widget_apikey_newsletter = '';
        if (isset($instance['widget_apikey_newsletter'])) {
            $widget_apikey_newsletter = $instance['widget_apikey_newsletter'];
        }
        $widget_listid_newsletter = '';
        if (isset($instance['widget_listid_newsletter'])) {
            $widget_listid_newsletter = $instance['widget_listid_newsletter'];
        }
        ?>
        <div class="shp-newsletter-widget">
            <?php if(!empty($widget_newsletter_title)): ?>
                <div class="shp-footer-text">
                    <h4><?php echo esc_html($widget_newsletter_title); ?></h4>
                </div>  
            <?php endif; ?>  
            <div class="shp-footer-newsletter">
            <?php if(!empty($widget_desc_newsletter)): ?>
                <p><?php echo esc_html($widget_desc_newsletter); ?></p>
            <?php endif; ?>
            <div class="shp-newsletter-wrap">
                <input type="hidden" id="ns_apikey" name="ns_apikey" value="<?php echo esc_attr($widget_apikey_newsletter); ?>">
				<input type="hidden" id="ns_list" name="ns_list"  value="<?php echo esc_attr($widget_listid_newsletter); ?>">
                <input type="text" id="ns_email" name="ns_email" placeholder="<?php esc_attr_e('Enter Your Email Here','shopmartio'); ?>">
                <button type="button" id="ss_newslatter">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" viewBox="0 0 20 20">
                        <path d="M17.704,16.835 C17.677,17.026 17.569,17.195 17.411,17.296 C17.312,17.358 17.199,17.391 17.086,17.391 C17.018,17.391 16.949,17.378 16.884,17.356 L8.149,14.240 L16.738,3.440 L5.638,13.345 L0.425,11.486 C0.190,11.401 0.024,11.179 0.004,10.918 C-0.014,10.658 0.117,10.411 0.339,10.291 L19.088,0.072 C19.297,-0.041 19.549,-0.022 19.740,0.119 C19.932,0.262 20.030,0.504 19.996,0.748 L17.704,16.835 ZM8.423,19.733 C8.303,19.904 8.114,19.999 7.919,19.999 C7.855,19.999 7.789,19.989 7.725,19.967 C7.468,19.879 7.294,19.629 7.294,19.347 L7.294,15.314 L10.683,16.523 L8.423,19.733 Z"></path>
                    </svg>
                </button>
              </div>   
              <div class="ss_messages"></div>
            </div>
        </div>
       <?php

    }     
    public function form( $instance ) {
        $widget_newsletter_title = '';
        if (isset($instance['widget_newsletter_title'])) {
            $widget_newsletter_title = $instance['widget_newsletter_title'];
        }
        $widget_desc_newsletter = '';
        if (isset($instance['widget_desc_newsletter'])) {
            $widget_desc_newsletter = $instance['widget_desc_newsletter'];
        }
        $widget_apikey_newsletter = '';
        if (isset($instance['widget_apikey_newsletter'])) {
            $widget_apikey_newsletter = $instance['widget_apikey_newsletter'];
        }
        $widget_listid_newsletter = '';
        if (isset($instance['widget_listid_newsletter'])) {
            $widget_listid_newsletter = $instance['widget_listid_newsletter'];
        }
        ?>   
        <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'widget_newsletter_title' )); ?>">
            <?php esc_html_e( 'Title :','shopmartio'); ?>
        </label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'widget_newsletter_title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'widget_newsletter_title' )); ?>" type="text" value="<?php echo esc_attr($widget_newsletter_title); ?>" />
        </p>
        <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'widget_desc_newsletter' )); ?>">
            <?php esc_html_e( 'Description :','shopmartio'); ?>
        </label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'widget_desc_newsletter' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'widget_desc_newsletter' )); ?>" type="text" value="<?php echo esc_attr($widget_desc_newsletter); ?>" />
        </p>
        <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'widget_apikey_newsletter' )); ?>">
                <?php esc_html_e( 'Api key :','shopmartio'); ?>
        </label>  
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'widget_apikey_newsletter' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'widget_apikey_newsletter' )); ?>" type="text" value="<?php echo esc_attr($widget_apikey_newsletter); ?>" />    
        </p>  
        <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'widget_listid_newsletter' )); ?>">
           <?php esc_html_e('List Id:','shopmartio'); ?>
        </label>  
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'widget_listid_newsletter')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_listid_newsletter')); ?>" type="text" value="<?php echo esc_attr($widget_listid_newsletter); ?>" />    
        </p>   
        <?php
    }
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['widget_newsletter_title'] = ( ! empty( $new_instance['widget_newsletter_title'] ) ) ? strip_tags( $new_instance['widget_newsletter_title'] ) : '';
        
        $instance['widget_desc_newsletter'] = ( ! empty( $new_instance['widget_desc_newsletter'] ) ) ? strip_tags( $new_instance['widget_desc_newsletter'] ) : '';
        
        $instance['widget_apikey_newsletter'] = ( ! empty( $new_instance['widget_apikey_newsletter'] ) ) ? strip_tags( $new_instance['widget_apikey_newsletter'] ) : '';
         
        $instance['widget_listid_newsletter'] = ( ! empty( $new_instance['widget_listid_newsletter'] ) ) ? strip_tags( $new_instance['widget_listid_newsletter'] ) : '';
        
       return $instance;
    } 
}

/**
 * Shopmartio Recent Post
 **/ 
class Shopmartio_widget_recentpost extends WP_Widget {
     
    function  __construct() {
        parent::__construct(
    	    'shopmartio_widget_recentpost', // Base ID of your widget
    		esc_html__('shopmartio Recent Post', 'shopmartio'),  // Widget name will appear in UI
    		array( 'description' => esc_html__( 'shopmartio Recent Post', 'shopmartio' ), ) // Widget description
		   );
    } 
	
	public function widget( $args, $instance ) {
	
		$app_widget_title = '';
		if ( isset( $instance[ 'app_widget_title' ] ) ) {
			$app_widget_title = $instance[ 'app_widget_title' ];
		} 
		
		$app_widget_number = '';
		if ( isset( $instance[ 'app_widget_number' ] ) ) {
		   $app_widget_number = $instance[ 'app_widget_number' ];
		}   
    ?>
    <section class="sr-recent-post-widgets widget">
		<h2 class="widget-title"><?php echo esc_html($app_widget_title); ?></h2>
        <ul>
            <?php
            $args = array(
                'post_type' =>'post',
                'posts_per_page' => $app_widget_number,
                'post_status' =>'publish',
                );
            $shopmartio_query = new WP_Query($args);
            if($shopmartio_query->have_posts()) :
                while($shopmartio_query->have_posts()) : $shopmartio_query->the_post();
                if(has_post_thumbnail(get_the_ID())):
                    $attachment_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full');
                endif;	
            ?>
            <li>
                <?php if(!empty($attachment_url)): ?>
                <img src="<?php echo esc_url($attachment_url); ?>" alt="<?php esc_attr_e('image','shopmartio'); ?>">
                <?php endif; ?>
                <a href="<?php echo esc_url(get_the_permalink(get_the_ID())); ?>">
                    <p class="shp-widget-post-title mb-0">
					<?php
					echo substr(get_the_title(), 0, 40); 
					?>
					</p>
                </a>
            </li>
            <?php
            endwhile;
            endif;
            ?>
        </ul>
    </section>
	<?php
    }
    public function form( $instance ) {
		
        $app_widget_title = '';
        if(isset( $instance[ 'app_widget_title' ] ) ) {
            $app_widget_title = $instance[ 'app_widget_title' ];
        } 
		
        $app_widget_number = '';
        if(isset( $instance[ 'app_widget_number' ] ) ) {
           $app_widget_number = $instance[ 'app_widget_number' ];
        }  
    ?> 
    <p>
    <label for="<?php echo esc_attr($this->get_field_id( 'app_widget_title' )); ?>">
    <?php esc_html_e( 'Title:','shopmartio'); ?></label> 
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'app_widget_title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'app_widget_title' )); ?>" type="text" value="<?php echo esc_attr( $app_widget_title ); ?>" />
    </p>
    <p>
    <label for="<?php echo esc_attr($this->get_field_id( 'app_widget_number' )); ?>">
    <?php esc_html_e( 'Show No.of Post :','shopmartio'); ?></label> 
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'app_widget_number' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'app_widget_number' )); ?>" type="text" value="<?php echo esc_attr( $app_widget_number ); ?>" />
    </p>
    <?php
    }
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['app_widget_title'] = ( ! empty( $new_instance['app_widget_title'] ) ) ? strip_tags( $new_instance['app_widget_title'] ) : '';
        $instance['app_widget_number'] = ( ! empty( $new_instance['app_widget_number'] ) ) ? strip_tags( $new_instance['app_widget_number'] ) : '';
        return $instance;  
    }
}



/**
 * Shopmartio Custom Image Widget
**/ 
class Shopmartio_widget_image extends WP_Widget {
    function  __construct() {
    	parent::__construct(
        'Shopmartio_widget_image', // Base ID of your widget
            esc_html__('shopmartio Image', 'shopmartio'),  // Widget name will appear in UI
            array( 'description' => esc_html__( 'shopmartio Image', 'shopmartio' ), ) // Widget description
            );
    }

    public function widget( $args, $instance ) {
        $widgets_custom_img = '';
        if ( isset( $instance[ 'widgets_custom_img' ] ) ) {
            $widgets_custom_img = $instance[ 'widgets_custom_img' ];
        }
        $widgets_page_url = '';
        if(isset($instance['widgets_page_url'])) {
            $widgets_page_url = $instance['widgets_page_url'];
        }    
        echo $args['before_widget'];
        ?>
        <div class="shp-blog-widgetbox hover-animation text-center shp-cads-inner">
             <?php if(!empty($widgets_custom_img)): ?>
             <a href="<?php echo esc_url($widgets_page_url); ?>">
               <img src="<?php echo esc_url($widgets_custom_img); ?>" alt="<?php esc_attr_e('shopmartio WordPress Theme','shopmartio'); ?>"  class="img-fluid" />
             </a>   
            <?php endif; ?>
        </div> 
    <?php
        echo $args['after_widget'];
    }    
    public function form( $instance ) {
        $widgets_custom_img = '';
        if(isset($instance['widgets_custom_img'])) {
            $widgets_custom_img = $instance['widgets_custom_img'];
        }      
        $widgets_page_url = '';
        if(isset($instance['widgets_page_url'])) {
            $widgets_page_url = $instance['widgets_page_url'];
        }       
        ?>  
        <p class="img-prev">
        <?php 
          if (isset($widgets_custom_img)) { 
            echo '<img src="'.esc_url($widgets_custom_img).'" class="window_image_url" with="100" height="100"/>';
          } 
        ?>
        </p>
        <p>
        <label for="<?php echo $this->get_field_id('widgets_custom_img'); ?>">
            <?php esc_html_e('upload Image', 'shopmartio') ?>
        </label> 
        <input  id="<?php echo $this->get_field_id('widgets_custom_img'); ?>" type="text" class="upload_logo_image_text" name="<?php echo $this->get_field_name('widgets_custom_img'); ?>" value="<?php if (isset($widgets_custom_img)) echo esc_attr($widgets_custom_img); ?>"/>
        <input data-title="<?php esc_attr_e('Image','shopmartio'); ?>" data-btntext="<?php esc_attr_e('Select it','shopmartio'); ?>" class="button upload_logo_image_button" type="button" value="<?php esc_attr_e('Upload','shopmartio') ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id('widgets_page_url'); ?>">
            <?php esc_html_e('Enter Page Url', 'shopmartio') ?>
        </label> 
        <input  id="<?php echo $this->get_field_id('widgets_page_url'); ?>" type="text" class="widgets_page_url" name="<?php echo $this->get_field_name('widgets_page_url'); ?>" value="<?php if (isset($widgets_page_url)) echo esc_attr($widgets_page_url); ?>"/>
        </p>
        <?php
    }
    
    public function update( $new_instance, $old_instance ) {
        $instance = array();
		$instance['widgets_custom_img'] = ( ! empty( $new_instance['widgets_custom_img'] ) ) ? strip_tags( $new_instance['widgets_custom_img'] ) : '';
		$instance['widgets_page_url'] = ( ! empty( $new_instance['widgets_page_url'] ) ) ? strip_tags( $new_instance['widgets_page_url'] ) : '';
        return $instance;
    }
}  

/**
 * Shopmartio Contact Information Widget
 */ 
class Shopmartio_contact_information extends WP_Widget {
    
    function  __construct() {
    	parent::__construct(
        'Shopmartio_contact_information', // Base ID of your widget
            esc_html__('shopmartio Contact Information', 'shopmartio'),  // Widget name will appear in UI
            array( 'description' => esc_html__( 'shopmartio Contact Information', 'shopmartio' ), ) // Widget description
            );
    }
    
    public function widget( $args, $instance ) {
       
    $shp_info_title = $shp_phone_number = $shp_address = $shp_phone_number = '';
	$shp_email_address = '';
	if(isset($instance['shp_contect_info']) || isset($instance['shp_phone_number']) || isset($instance['shp_address']) || isset($instance['shp_phone_number']) || isset($instance['shp_email_address'])):
		$shp_info_title = $instance['shp_contect_info'];
		$shp_phone_number = $instance['shp_phone_number'];
		$shp_address = $instance['shp_address'];
		$shp_phone_number = $instance['shp_phone_number'];
		$shp_email_address = $instance['shp_email_address']; 
    endif;		
    echo $args['before_widget'];
    ?>
    <div class="shp-widget-footer-content">
	   <?php 
	   if(!empty($shp_info_title)):
	      echo $args['before_title'].esc_html($shp_info_title).$args['after_title'];
	   endif;
	   ?>
		<div class="shp-info-content">
		    <?php if(!empty($shp_address)): ?>
			 <div class="shp-contact-detail">
				<span><i class="fas fa-map-marker-alt"></i></span>
			    <p><?php echo esc_html($shp_address);?></p>
			 </div>
			<?php 
			endif;
			if(!empty($shp_phone_number)): ?>
    			<div class="shp-contact-detail">
    			  <span><i class="fas fa-phone-volume"></i></span>
    			  <p><?php echo esc_html($shp_phone_number); ?></p>
    			</div>
			<?php 
			endif;
			if(!empty($shp_email_address)):
			?>
    		  <div class="shp-contact-detail">
    			<span><i class="far fa-envelope"></i></span>
    			<p><a href="mailto:<?php echo esc_url($shp_email_address);?>"><?php echo esc_html($shp_email_address);?></a></p>
    		   </div>
			<?php 
			endif;
			?>
		</div>
	</div> 
    <?php
    echo $args['after_widget'];
    }
    public function form( $instance ) {
        
        $shp_info_title = $shp_phone_number = $shp_address = $shp_phone_number = $shp_contect_info = '';
    	$shp_email_address = '';
    	if(isset($instance['shp_contect_info']) || isset($instance['shp_phone_number']) || isset($instance['shp_address']) || isset($instance['shp_phone_number']) || isset($instance['shp_email_address'])):
    		$shp_info_title = $instance['shp_contect_info'];
    		$shp_phone_number = $instance['shp_phone_number'];
    		$shp_address = $instance['shp_address'];
    		$shp_phone_number = $instance['shp_phone_number'];
    		$shp_email_address = $instance['shp_email_address']; 
        endif;	
    ?>
    <p>
	  <label for="<?php echo $this->get_field_id( 'shp_contect_info' ); ?>"><?php echo esc_html__(' Contact Info Title','shopmartio'); ?></label> 
	  <input class="widefat" id="<?php echo $this->get_field_id('shp_contect_info'); ?>" name="<?php echo $this->get_field_name('shp_contect_info'); ?>" type="text" value="<?php echo esc_attr($shp_contect_info); ?>">
	</p> 
	<p>
	  <textarea class="widefat" rows="6" cols="10" id="<?php echo $this->get_field_id('shp_address'); ?>" name="<?php echo $this->get_field_name('shp_address'); ?>"><?php if (isset($shp_address)) echo esc_html($shp_address); ?></textarea>
	 </p> 
	<p>
	  <label for="<?php echo $this->get_field_id('shp_phone_number'); ?>"><?php echo esc_html__('Phone Number','shopmartio'); ?></label> 
	  <input class="widefat" id="<?php echo $this->get_field_id('shp_phone_number'); ?>" name="<?php echo $this->get_field_name('shp_phone_number'); ?>" type="text" value="<?php echo esc_attr($shp_phone_number); ?>">
	</p> 
	<p>
	  <label for="<?php echo $this->get_field_id('shp_email_address'); ?>"><?php echo esc_html__('Email Address','shopmartio'); ?></label> 
	  <input class="widefat" id="<?php echo $this->get_field_id('shp_email_address'); ?>" name=" <?php echo $this->get_field_name('shp_email_address'); ?>" type="text" value="<?php echo esc_attr($shp_email_address); ?>">
	</p>     
    <?php 
    }
    public function update( $new_instance, $old_instance ) {
        
      $instance = array();
	  
	  $instance['shp_contect_info'] = (!empty($new_instance['shp_contect_info']))?strip_tags($new_instance['shp_contect_info']):'';

	  $instance['shp_address'] = (!empty($new_instance['shp_address']))?strip_tags($new_instance['shp_address']):'';

	  $instance['shp_phone_number'] = (!empty($new_instance['shp_phone_number']))?strip_tags($new_instance['shp_phone_number']):'';

	  $instance['shp_email_address'] = (!empty($new_instance['shp_email_address']))?strip_tags($new_instance['shp_email_address']):'';
	  
	  return $instance;
        
    }
    

} 