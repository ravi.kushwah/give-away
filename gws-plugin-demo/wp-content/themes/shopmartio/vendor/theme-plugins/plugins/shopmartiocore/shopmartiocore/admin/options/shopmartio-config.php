<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
	   
	// This is your option name where all the Redux data is stored.
    $opt_name = "shopmartio_options";
    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */ 

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Shopmartio Options', 'shopmartio' ),
        'page_title'           => __( 'Shopmartio Options', 'shopmartio' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,   // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-art',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => 99,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => 'dashicons-screenoptions',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => false,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,
		
		'show_options_object' => false,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => '#7791ea',
            'icon_size'     => 'large',
            'tip_style'     => array(
                'color'   => 'dark',
                'shadow'  => true,
                'rounded' => true,
                'style'   => 'bootstrap',
            ),
            'tip_position'  => array(
                'my' => 'top right',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'shopmartio' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'shopmartio' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'shopmartio' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'shopmartio' )
        )
    );
    //Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'shopmartio' );
    Redux::setHelpSidebar( $opt_name, $content );


// -> START Basic Fields

Redux::setSection( $opt_name, array(
    'id' => 'wbc_importer_section',
    'title'  => esc_html__( 'Demo Importer', 'shopmartio' ),
    'icon'   => 'el el-download-alt',
	'desc'  => __('', 'shopmartio' ),
    'fields' => array(
            array(
                'id'   => 'wbc_demo_importer',
                'type' => 'wbc_importer'
                )
        )
    )
);

Redux::setSection( $opt_name, array(
	'title' => __( 'Theme Layout', 'shopmartio' ),
	'id'    => 'theme_layout',
	'desc'  => __( 'General settings of the site', 'shopmartio' ),
	'icon'  => 'el el-brush',
	'fields'  => array(
	       array(
			   'id'  => 'theme_layout_on',
			   'type' => 'switch',
			   'title'  => __('Theme Layout Enable/Disable', 'shopmartio'),
			   'default' => false
 	          ),
	      ) 
    ) ); 
 
Redux::setSection( $opt_name, array(
	'title' => __( 'Header', 'shopmartio' ),
	'id'    => 'general',
	'desc'  => __( 'General settings of the site', 'shopmartio' ),
	'icon'  => 'el el-credit-card'
) );

/* Top Header Setting start */
Redux::setSection( $opt_name, array(
	'title' => __( 'Top Header', 'shopmartio' ),
	'desc' => __( '', 'shopmartio' ),
	'id'  => 'top-header',
	'subsection' => true,
	'fields'  => array(
	    array(
			'id'       => 'top_header',
			'type'     => 'switch',
			'title'    => __('Enable/Disable', 'shopmartio'),
			'default'  => false
 	       ),	 
 	    array(
		    'id'  => 'notification_message',
		    'type'  => 'text',
		    'title'    => __( 'Notification Message', 'shopmartio' ),
		    'default'  => '',
	       ),
	    array(
			'id'       => 'menu_swither',
			'type'     => 'switch',
		    'title' => __( 'Top Menu Enable/Disable', 'shopmartio' ), 
			'default'  => false
 	       ),
	)
		
/* Topbar Setting ends */
) );

Redux::setSection( $opt_name, array(
	'title' => __( 'Main Menu', 'shopmartio' ),
	'desc'  => __( '', 'shopmartio' ),
	'id'    => 'general-header',
	'subsection' => true,
	'fields'  => array(
		array(
		   'id' => 'section-navigatoin-start',
		   'type' => 'section',
		   'title' => __('Main Settings', 'shopmartio'),
		   'indent' => true 
	      ),
		array(
			'id'       => 'site_logo',
			'type'     => 'media',
			'url'      => true,
			'title'    => __( 'Site Logo', 'shopmartio' ),
			'compiler' => 'true',
		    ),
		array(
			'id'       => 'mobile_site_logo',
			'type'     => 'media',
			'url'      => true,
			'title'    => __( 'Site Mobile Logo ', 'shopmartio' ),
			'compiler' => 'true',
		   ),
		array(
			'id'       => 'search_header_switch',
			'type'     => 'switch',
			'title'    => __('Search Enable/Disable', 'shopmartio'),
			'default'  => false
 	       ),
 	    array(
			'id'       => 'mincart_header_switch',
			'type'     => 'switch',
			'title'    => __('Mincart Enable/Disable', 'shopmartio'),
			'default'  => false
 	       ),	 
 	    array(
			'id'       => 'mega_menu_switch',
			'type'     => 'switch',
			'title'    => __('Mega Menu Enable/Disable', 'shopmartio'),
			'default'  => false
 	       ),
	   ) 
) );

Redux::setSection( $opt_name, array(
    'title'     => __( 'Page Title', 'shopmartio' ),
    'id'        => 'general-page-title',
    'subsection' => true,
	'desc'  => __( '', 'shopmartio' ),
    'fields' => array(
			array(
			  'id' => 'shopmartio_global_breadcrumb',
			  'type' => 'switch',
			  'title' => 'Breadcrumb ON/OFF',
			  'default' => false
		    ),
		)
    ) );

Redux::setSection( $opt_name, array(
	'title' => __( 'Sidebar', 'shopmartio' ),
	'id'    => 'general-sidebar',
	'desc'  => __( '', 'shopmartio' ),
	'icon'          => 'dashicons dashicons-align-left',
	'fields'     => array(
		array(
			'id'       => 'sidebar_postion',
            'type'     => 'image_select',
            'title'    => __( 'Sidebar Position', 'shopmartio' ),
            'desc'     => __( 'Select sidebar position as you want to show on page.', 'shopmartio' ),
            
            'options'  => array(
                'full' => array(
                    'alt' => 'No sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                ),
                'left' => array(
                    'alt' => 'Left Sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                ),
                'right' => array(
                    'alt' => 'Right Sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                ),
				'both' => array(
                    'alt' => 'Both Side Sidebars',
                    'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                ),
            ),
            'default'  => 'right'
		),
	)
) );

Redux::setSection( $opt_name, array(
	'title'      => __( 'Footer', 'shopmartio' ),
	'id'         => 'general-footer',
	'icon'  => 'el el-credit-card',
	'desc'  => __( '', 'shopmartio' ),
	'fields'     => array(
		array(
			'id'       => 'footer_text',
			'type'     => 'text',
			'title'    => __( 'Copyright Text', 'shopmartio' ),
			'default'  => '',
		   ),
		array(
			'id'       => 'footer_menu_switcher',
			'type'     => 'switch',
			'title'    => __('Footer Menu Enable/Disable','shopmartio'),
			'default'  => false
		  ),  
        array(
			'id'       => 'footer_bottomtotop_switcher',
			'type'     => 'switch',
			'title'    => __('Bottom To Top Button Enable/Disable','shopmartio'),
			'default'  => false
		  ),  		  
	    )
    ) );

Redux::setSection( $opt_name, array(
	'title' => __( 'Appearance', 'shopmartio' ),
	'id'    => 'shopmartio-appearance',
	'desc'  => __( '', 'shopmartio' ),
	'icon'  => 'dashicons dashicons-welcome-view-site'
) );
 
Redux::setSection( $opt_name, array( 
		'title' => __( 'General Settings', 'shopmartio' ),
		'id'    => 'shopmartio-general-appearance',
		'subsection' => true,
		'desc'  => __( '', 'shopmartio' ),
		'fields'     => array(
			          array(
        				'id'  => 'section-theme-loader-start',
        				'type'  => 'section',
        				'title'  => __( 'Page Loader', 'shopmartio' ),
        				'indent' => true,
        			    ),
        			   array(
        				'id'       => 'page_loader_switch',
        				'type'     => 'switch',
        				'title'    => __( 'Page Loader', 'shopmartio' ),
        				'default'  => false
        			    ),
        		      array(
        				'id'       => 'page_loader',
        				'type'     => 'media', 
        				'url'      => true,
        				'title'    => __('Upload Loader Image', 'redux-framework-demo'),
        				'desc'     => __('Loader will work on front page of the site.', 'shopmartio'),
        		    	),
	        	  )
            ) );

Redux::setSection( $opt_name, array(
	'title' => __( 'WooCommerce Styles', 'shopmartio' ),
	'id'    => 'woo_colors',
	'desc'  => __( '', 'shopmartio' ),
	'icon'  => 'el el-shopping-cart',
	'fields'     => array(
		array(
		   'id' => 'section-woo-start',
		   'type' => 'section',
		   'title' => __('Main Settings', 'shopmartio'),
		   'indent' => true 
	     ),
		array(
			'id'       => 'woo_sidebar_postion',
            'type'     => 'image_select',
            'title'    => __( 'Woocommerce Sidebar Position', 'shopmartio' ),
            'desc'     => __( 'Select sidebar position as you want to show on products realted page.', 'shopmartio' ),
            'options'  => array(
                'full' => array(
                    'alt' => 'No sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                ),
                'left' => array(
                    'alt' => 'Left Sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                ),
                'right' => array(
                    'alt' => 'Right Sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                ),
				'both' => array(
                    'alt' => 'Both Side Sidebars',
                    'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                ),
            ),
            'default'  => 'right'
		),
		
		array(
			'id'       => 'woo_single_sidebar_postion',
            'type'     => 'image_select',
            'title'    => __( 'Woocommerce Single Products Sidebar Position', 'shopmartio' ),
            'desc'     => __( 'Select Single Products sidebar position as you want to show on products realted page.', 'shopmartio' ),
            'options'  => array(
                'full' => array(
                    'alt' => 'No sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                ),
                'left' => array(
                    'alt' => 'Left Sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                ),
                'right' => array(
                    'alt' => 'Right Sidebar',
                    'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                ),
				'both' => array(
                    'alt' => 'Both Side Sidebars',
                    'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                ),
            ),
            'default'  => 'right'
		),
		
		array(
			'id'       => 'related_product_title',
			'type'     => 'text',
			'title'    => __( 'Enter Related Product Title', 'shopmartio' ),
			'default'    => '',
			),
		array(
			'id'       => 'related_product_description',
			'type'     => 'textarea',
			'title'    => __( 'Enter Related Product Description', 'shopmartio' ),
			'default'    => '',
			),		
	    )
   ) );
 
Redux::setSection( $opt_name, array(
	'title' => __( 'News Newsletter Setting', 'shopmartio' ),
	'id'    => 'newsletter_setting',
	'desc'  => __( '', 'shopmartio' ),
	'icon'  => 'el el-envelope',
	'fields'  => array(
	    array(
			'id'       => 'newsletter_on_switch',
			'type'     => 'switch',
			'title'    => __('News Newsletter Switch', 'shopmartio' ),
			'default'  => false
		    ),
	    array(
			'id'       => 'newsletter_heading',
			'type'     => 'text',
			'title'    => __( 'Enter Title', 'shopmartio' ),
			'default'    => '',
			),
		array(
			'id'       => 'newsletter_desc',
			'type'     => 'textarea',
			'title'    => __( 'Enter Description', 'shopmartio' ),
			'default'    => '',
			),
		array(
			'id'       => 'newsletter_background_image',
			'type'     => 'media', 
			'url'      => true,
			'title'    => __('News Letter', 'shopmartio'),
			'desc'     => __('', 'shopmartio'),
			),
		array(
            'id'       => 'newsletter_gallery',
            'type'     => 'gallery',
            'title'    => __( 'Add/Edit Gallery', 'shopmartio'),
            'subtitle' => __( '', 'shopmartio'),
            'desc'     => __( '', 'shopmartio'),
        ),
	
	    )
    ) ); 
 
Redux::setSection( $opt_name, array(
	'title' => __( '404 Page Setting', 'shopmartio' ),
	'id'    => '404_colors',
	'desc'  => __( '', 'shopmartio' ),
	'icon'  => 'el el-shopping-cart',
	'fields'     => array(
	    
	    array(
			'id'       => 'err_title',
			'type'     => 'text',
			'title'    => __( 'Enter Title', 'shopmartio' ),
			'default'    => '',
			),
		array(
			'id'       => 'error_404_desc',
			'type'     => 'textarea',
			'title'    => __( 'Enter Description', 'shopmartio' ),
			'default'    => '',
			),
		array(
			'id'       => 'error_404_image',
			'type'     => 'media', 
			'url'      => true,
			'title'    => __('Uploa 404  Image', 'shopmartio'),
			'desc'     => __('', 'shopmartio'),
			),
	
	  )
 ) );
 
Redux::setSection( $opt_name, array(
	'title'      => __( 'Social', 'shopmartio' ),
	'desc'       => __( '', 'shopmartio' ),
	'id'         => 'general-social',
	'icon'  => 'dashicons dashicons-twitter',
	'fields'     => array(
		array(
		   'id' => 'section-socials-start',
		   'type' => 'section',
		   'title' => __('Add Social Page URL', 'shopmartio'),
		   'indent' => true 
		   ),
		array(
			'id'       => 'shopmartio_facebook',
			'type'     => 'text',
			'title'    => __( 'Facebook', 'shopmartio' ),
			'default'    => 'https://www.facebook.com/',
			),
		array(
			'id'       => 'shopmartio_twitter',
			'type'     => 'text',
			'title'    => __( 'Twitter', 'shopmartio' ),
			'default'    => 'https://twitter.com/',
		   ),
		array(
			'id'       => 'shopmartio_dribble',
			'type'     => 'text',
			'title'    => __( 'Dribbble', 'shopmartio' ),
		   ),
		array(
			'id'       => 'shopmartio_instagram',
			'type'     => 'text',
			'title'    => __( 'Instagram', 'shopmartio' ),
			'default'    => 'https://www.instagram.com/',
		   ),
		array(
			'id'       => 'shopmartio_linkedIn',
			'type'     => 'text',
			'title'    => __( 'LinkedIn', 'shopmartio' ),
		   ),
		array(
			'id'       => 'shopmartio_flickr',
			'type'     => 'text',
			'title'    => __( 'Flickr', 'shopmartio' )
		   ),
		array(
			'id'       => 'shopmartio_googleplus',
			'type'     => 'text',
			'title'    => __( 'Google +', 'shopmartio' )
		   ),
		array(
			'id'       => 'shopmartio_skype',
			'type'     => 'text',
			'title'    => __( 'Skype', 'shopmartio' )
		   ),
		array(
			'id'       => 'shopmartio_youtube',
			'type'     => 'text',
			'title'    => __( 'Youtube', 'shopmartio' )
		   ),
		array(
			'id'       => 'shopmartio_whatsapp',
			'type'     => 'text',
			'title'    => __( 'Whatsapp', 'shopmartio' )
		   ),
		array(
			'id'       => 'shopmartio_soundcloud',
			'type'     => 'text',
			'title'    => __( 'SoundCloud', 'shopmartio' )
		  ),
		array(
			'id'       => 'shopmartio_tumblr',
			'type'     => 'text',
			'title'    => __( 'Tumblr', 'shopmartio' )
		  ),
		array(
			'id'       => 'shopmartio_pinterst',
			'type'     => 'text',
			'title'    => __( 'Pinterst', 'shopmartio' ),
			'subtitle' => __( '', 'shopmartio' ),
			'desc' => __( '', 'shopmartio' ),
		   ),
		array(
			'id'       => 'shopmartio_vimeo',
			'type'     => 'text',
			'title'    => __( 'Vimeo', 'shopmartio' )
		   ),
		
	   )
) );
 
Redux::setSection( $opt_name, array(
		'title' => __( 'Google Analytics/AdSense', 'shopmartio' ),
		'id'    => 'shopmartio-google_analytics',
		'icon'  => 'dashicons dashicons-chart-area',
		'desc'  => __( '', 'shopmartio' ), 
		'fields'     => array(
			array(
				'id'       => 'analytics_script',
				'type'     => 'textarea',
				'title'    => 'Add Script',
				'desc'  => __('Enter Google Analytics/AdSense script here.After adding it, you can add your add script on any place of the page using WP editor or Text Widget.', 'shopmartio'),
			)
		)
) );

Redux::setSection( $opt_name, array(
		'title' => __( 'Custom Code', 'shopmartio' ),
		'id'    => 'shopmartio-custom-code',
		'icon'  => 'dashicons dashicons-editor-code',
		'desc'  => __( '', 'shopmartio' ),
		'fields'     => array(
			array(
                'id'       => 'shopmartio_custom_css',
                'type'     => 'ace_editor',
                'title'    => __( 'CSS Code', 'shopmartio' ),
                'subtitle' => __( 'Paste your CSS code here.', 'shopmartio' ),
                'mode'     => 'css',
                'theme'    => 'monokai',
                'default'  => "#yourIdORClass{\n   margin: 0 auto;\n}"
            ),
            array(
                'id'       => 'shopmartio_custom_js',
                'type'     => 'ace_editor',
                'title'    => __( 'JS Code', 'shopmartio' ),
                'subtitle' => __( 'Paste your JS code here.', 'shopmartio' ),
                'mode'     => 'javascript',
                'theme'    => 'chrome',
                'default'  => "jQuery(document).ready(function(){\n\n});"
            ),
		)
) );

/*
 * <--- END SECTIONS
 */