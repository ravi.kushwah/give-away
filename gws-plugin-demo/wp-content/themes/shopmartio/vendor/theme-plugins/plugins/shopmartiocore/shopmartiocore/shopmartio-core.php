<?php 
/**
 * Plugin Name: Shop Martio Theme Plugin
 * Plugin URI:  http://kamleshyadav.com/wp/shopmartio/
 * Description: Clean and well-designed, Shopmartio is an efficient Multi-Vendor Marketplace WordPress eCommerce Theme. Developed by the dedicated team of Kamlesh Yadav, this amazing multi-vendor e-commerce WordPress theme enables you to create powerful e-commerce websites across all niches and markets. Best WordPress theme for affiliate marketing, Shopmartio allows you to explore your creativity and build a unique website for your online store.This theme will go excellently well with the multi-category store, fashion store, grocery store, cosmetics store, books shop, medical store, toys store, kids fashion store, gifts shop, apparel store, mobile accessory shop, fashion accessories store, etc.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            kamaleshyadav
 * Author URI:        https://themeforest.net/user/kamleshyadav
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       shopmartio
 * Domain Path:       /languages
 */ 

global $shopmartio_plug_version;

$shopmartio_plug_version = '1.0.0';

remove_action( 'wp_head', 'rest_output_link_wp_head'); 

/**
 * Admin Enqueue Scripts
 **/
add_action( 'admin_enqueue_scripts', 'shopmartio_widget_upload_script' );
function shopmartio_widget_upload_script($hook) {
    if ( 'widgets.php' != $hook ) {
        return;
    }
    
    wp_enqueue_style('shopmartiocore-admin-style', plugin_dir_url( __FILE__ )  . 'assets/css/shopmartiocore-admin-style.css', array(), '1', 'all' );
    
    wp_enqueue_media();
    wp_enqueue_script('shopmartio-admin-scripts', plugin_dir_url( __FILE__ ) . 'assets/js/shopmartio-admin-scripts.js', array('jquery') );
    wp_localize_script('shopmartio-admin-scripts', 'frontadminajax', array('ajax_url' => admin_url('admin-ajax.php'))); 
}

/**
 * plugin script enqueue
**/
add_action( 'wp_enqueue_scripts', 'shopmartio_pluginscript_enqueue' );

function shopmartio_pluginscript_enqueue(){
    
  //style  
  wp_enqueue_style('toastr-style', plugin_dir_url( __FILE__ )  . 'assets/css/toastr.min.css', array(), '1', 'all' );
  
  // script
  wp_enqueue_script( 'toastr', plugin_dir_url( __FILE__ ) . 'assets/js/toastr.min.js', array('jquery'), '20151215', true );
  
  wp_enqueue_script('shopmartio-custom-ajax-script', plugin_dir_url( __FILE__ ) . 'assets/js/shopmartio-custom-ajax-script.js', array('jquery'), '20151215', true );
  
  wp_localize_script('shopmartio-custom-ajax-script', 'frontadminajax', array('ajax_url' => admin_url( 'admin-ajax.php' )) );
   
}  

/**
 * Redux Framework
 */   
if(class_exists( 'ReduxFramework' ) ) {
  require_once 'admin/options/shopmartio-config.php';
  require_once 'admin/options/custom-extensions/loader.php';
}   
  
/** 
 * WPBakery Page Builder Add Shortcode
 */ 
require_once 'admin/shortcodes/shopmartio-shortcodes-map.php'; 
require_once 'public/shortcodes/shopmartio-shortcodes-view.php'; 
/** 
 * Shopmartio ajax function
**/
require_once 'public/shopmartio-ajax-function.php';

/**
 * Shopmartio custom meta option
 */ 
 
require_once 'admin/meta/shopmartio-meta-custom-post.php';

/** 
 * ShopMartio Widgets
 */ 
require_once 'admin/widgets/shopmartio-widgets.php';  

/**
 * Shopmartio Custom Function Settings 
 */ 
require_once 'public/shopmartio-custom-funcation-setting.php'; 


//add class on body
add_filter( 'body_class','shopmartio_core_body_classes' );
function shopmartio_core_body_classes( $classes ) {
    $theme_option = '';
    if(function_exists('shopmartio_theme_option_settings')):
      $theme_option = shopmartio_theme_option_settings();
    endif;
    
     $demoimport = '';
    if(!empty($theme_option['theme_layout_on'])){
        $demoimport = $theme_option['theme_layout_on'];
    }
	if ($demoimport == true) {
        $classes[] = 'shp_theme_layout';

    }else{
        $classes[] = '';
    }
  
   return $classes;

}

/**
 * Theme demo impoter
 */
if ( !function_exists( 'shopmartio_wbc_extended' ) ) {
	function shopmartio_wbc_extended( $demo_active_import , $demo_directory_path ) {

		reset( $demo_active_import );
		$current_key = key( $demo_active_import );

		/************************************************************************
		* Import slider(s) for the current demo being imported
		*************************************************************************/

		if ( class_exists( 'RevSlider' ) ) {

			//If it's demo3 or demo5
			$wbc_sliders_array = array(
				'shopmartio-demo' => 'shopmortioslider.zip', //Set slider zip name
			);

			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_sliders_array ) ) {
				$wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];

				if ( file_exists( $demo_directory_path.$wbc_slider_import ) ) {
					$slider = new RevSlider();
					$slider->importSliderFromPost( true, true, $demo_directory_path.$wbc_slider_import );
				}
			}
		}

		/************************************************************************
		* Setting Menus
		*************************************************************************/

		// If it's demo1 - demo6
		$wbc_menu_array = array( 'shopmartio-demo');

		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {
		  
			$header_menu = get_term_by('name', 'Main_Menu', 'nav_menu' );
			$top_menu = get_term_by( 'name', 'Top_header_Menu', 'nav_menu' );
			$fot_menu = get_term_by( 'name', 'Footer_Menu', 'nav_menu' );
           
			if ( isset( $top_menu->term_id ) ) {
				set_theme_mod( 'nav_menu_locations', array(
						'shopmartio-header-menu' => $header_menu->term_id,
						'shopmartio-top-header-menu' => $top_menu->term_id,
						'shopmartio-footer-menu' => $fot_menu->term_id,
					)
				); 
			} 

		} 

		/************************************************************************
		* Set HomePage
		*************************************************************************/

		// array of demos/homepages to check/select from
		$wbc_home_pages = array(
			'shopmartio-demo' => 'Home', 
		);
  
		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
			$page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
			if ( isset( $page->ID ) ) {
				update_option( 'page_on_front', $page->ID );
				update_option( 'show_on_front', 'page' );
			}
		}

	}
add_action( 'wbc_importer_after_content_import', 'shopmartio_wbc_extended', 10, 2 );
} 