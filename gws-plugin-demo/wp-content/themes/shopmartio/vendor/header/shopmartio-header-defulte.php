<?php
$theme_option = '';
if(function_exists('shopmartio_theme_option_settings')):
  $theme_option = shopmartio_theme_option_settings();
endif;
$site_logo = '';
if(!empty($theme_option['site_logo']['url'])):
 $site_logo = $theme_option['site_logo']['url'];	
endif;

$mobile_site_logo = '';
if(!empty($theme_option['mobile_site_logo']['url'])):
 $mobile_site_logo = $theme_option['mobile_site_logo']['url'];	
endif;

$megamenu = '';
if(!empty($theme_option['mega_menu_switch'])):
 $megamenu = $theme_option['mega_menu_switch'];	
endif; 
if(class_exists('Mega_Menu') && $megamenu==true):
?>
<!-- With Work Meng Menu -->
<div class="shp-nav-header-default-style shp-mega-menus">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-3 mega-menu-mobile-logo">
                <div class="shp-logo shp-desktop-logo">
					<?php if(!empty($site_logo)): ?>
        				<a href="<?php echo esc_url(home_url('/')); ?>">
        				    <img src="<?php echo esc_url($site_logo); ?>" alt="<?php echo esc_attr__('logo','shopmartio'); ?>" />
        				</a> 
        		    <?php else: ?>
        			    <a class="shp-logo-title" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo get_bloginfo( 'name' ); ?></a>
        			<?php endif; ?> 
				</div>
				<div class="shp-logo shp-mobile-logo">
					<?php if(!empty($mobile_site_logo)): ?>
        				<a href="<?php echo esc_url(home_url('/')); ?>">
        				    <img src="<?php echo esc_url($mobile_site_logo); ?>" alt="<?php echo esc_attr__('logo','shopmartio'); ?>" />
        				</a> 
        		    <?php else: ?>
        			    <a class="shp-logo-title" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo substr(get_bloginfo( 'name' ),0,2); ?></a>
        			<?php endif; ?> 
				</div>
			</div> 
            <div class="col-xl-8 col-lg-7 col-md-6 col-sm-3 col-5 text-center mobile-mega-menu">
                 <div class="shp-main-nav-wrapper">
                	<div class="shp-nav-default-style">
                	<?php
                		wp_nav_menu(array(
                			  'theme_location' => 'shopmartio-header-menu',
                			  'menu_id'        => 'shopmartio-menu',
                			  'menu_class'=> 'menu', 
                			  'fallback_cb'=> 'shopmartio_menu_editor'
                			) 
                		);   
                	 ?> 
                	  <span class="shp-btn shp-top-menu">
        			    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><circle cx="7.97" cy="8.04" r="6.86"/><circle cx="32.03" cy="8.04" r="6.86"/><circle cx="7.97" cy="31.96" r="6.86"/><circle cx="32.03" cy="31.96" r="6.86"/></svg>
        			</span>
                	 
                	</div> 
                </div>
           </div>
           <div class="col-xl-2 col-lg-2 col-md-3 col-sm-5 col-4 text-right mobile-icon">
                <div class="shp-mega-menu-mobile">
                   	<?php 
                	if(function_exists('shopmartion_woocommerce_mincart_search')):
                	  shopmartion_woocommerce_mincart_search();
                	endif;
                	?>
            	</div>
           </div> 
       </div> 
   </div> 
</div> 
<?php else: ?> 
<div class="shp-nav-header-default-style shp-default-nav">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-3">
                <div class="shp-logo shp-desktop-logo">
					<?php if(!empty($site_logo)): ?>
        				<a href="<?php echo esc_url(home_url('/')); ?>">
        				    <img src="<?php echo esc_url($site_logo); ?>" alt="<?php echo esc_attr__('logo','shopmartio'); ?>" />
        				</a> 
        		    <?php else: ?>
        			    <a class="shp-logo-title" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo get_bloginfo( 'name' ); ?></a>
        			<?php endif; ?> 
				</div>
				<div class="shp-logo shp-mobile-logo">
					<?php if(!empty($mobile_site_logo)): ?>
        				<a href="<?php echo esc_url(home_url('/')); ?>">
        				    <img src="<?php echo esc_url($mobile_site_logo); ?>" alt="<?php echo esc_attr__('logo','shopmartio'); ?>" />
        				</a> 
        		    <?php else: ?>
        			    <a class="shp-logo-title" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo substr(get_bloginfo( 'name' ),0,2); ?></a>
        			<?php endif; ?> 
				</div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-6 col-9 text-right">
                 <div class="shp-main-nav-wrapper">
                	<div class="shp-nav-default-style">
                	<?php
                		wp_nav_menu(array(
                			  'theme_location' => 'shopmartio-header-menu',
                			  'menu_id'        => 'shopmartio-menu',
                			  'menu_class'=> 'menu', 
                			  'fallback_cb'=> 'shopmartio_menu_editor'
                			) 
                		);   
                	 ?> 
                	</div>  
                	<?php if(!class_exists('Mega_Menu') || $megamenu!=true): ?>
                        	<div class="nav-toggle-btn">
                               <span></span>
                               <span></span>
                               <span></span>
                            </div>
                    <?php endif; 
                    $top_header = '';
                    if(!empty($theme_option['top_header'])):
                     $top_header = $theme_option['top_header'];	
                    endif; 
                    if($top_header == true):
                    ?>
                    <span class="shp-btn shp-top-menu mr-3">
        			    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><circle cx="7.97" cy="8.04" r="6.86"/><circle cx="32.03" cy="8.04" r="6.86"/><circle cx="7.97" cy="31.96" r="6.86"/><circle cx="32.03" cy="31.96" r="6.86"/></svg>
        			</span>
        			<?php 
        			endif;
                	if(function_exists('shopmartion_woocommerce_mincart_search')):
                	  shopmartion_woocommerce_mincart_search();
                	endif;
                	?>
                </div>
           </div>
       </div> 
   </div> 
</div>   
<?php endif; ?>