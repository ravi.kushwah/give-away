<?php 
$theme_option = '';
if(function_exists('shopmartio_theme_option_settings')):
  $theme_option = shopmartio_theme_option_settings();
endif;
$newsletter_on_switch = '';
if(!empty($theme_option['newsletter_on_switch'])):
$newsletter_on_switch = $theme_option['newsletter_on_switch'];   
endif;
if($newsletter_on_switch == true):
    if(function_exists('shopmartion_defult_newsletter')):
       shopmartion_defult_newsletter();
    endif;
endif;
$footer_cop_class = '';
if( is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4') ):
?>
<footer class="shp-footer-wrapper default-footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
			    <div class="shp-footer-widgets">
				  <?php dynamic_sidebar('footer-1');  ?>
                </div>
			</div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
			    <div class="shp-footer-widgets">
				  <?php dynamic_sidebar('footer-2');  ?>
                </div>
			</div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
		    	<div class="shp-footer-widgets">
			      <?php dynamic_sidebar('footer-3');  ?>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="shp-footer-widgets">
			      <?php dynamic_sidebar('footer-4');  ?>
                </div>
            </div>
        </div> 
    </div>
</footer>
<?php 
else:
 $footer_cop_class = esc_html__("mt-0","shopmartio");
endif;
?>
<!-- Bottom Footer start -->
<div class="shp-copyright-wrapper default-copyright-area  <?php echo esc_attr($footer_cop_class); ?>">
    <!-- GO To Top -->
	<?php 
	$bottomtotop_switcher = '';
	if(!empty($theme_option['footer_bottomtotop_switcher'])):
	  $bottomtotop_switcher = $theme_option['footer_bottomtotop_switcher'];
	endif;
	if($bottomtotop_switcher == true): ?>
    <a href="javascript:void(0);" id="scroll">
        <svg xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="24px"><path fill-rule="evenodd" fill="rgb(255, 255, 255)" d="M6.638,0.361 C6.638,0.361 6.637,0.361 6.637,0.362 L0.357,6.757 C-0.114,7.237 -0.112,8.012 0.361,8.489 C0.834,8.966 1.599,8.964 2.069,8.485 L6.285,4.191 L6.285,22.770 C6.285,23.446 6.826,23.994 7.493,23.994 C8.160,23.994 8.701,23.446 8.701,22.770 L8.701,4.191 L12.917,8.485 C13.387,8.964 14.152,8.966 14.625,8.489 C15.098,8.012 15.099,7.237 14.629,6.757 L8.349,0.362 C8.349,0.361 8.348,0.361 8.348,0.361 C7.876,-0.119 7.109,-0.117 6.638,0.361 L6.638,0.361 Z"/></svg></a>
	<?php endif; ?>        
    <div class="container">            
        <div class="row">    
        <?php 
		$menu_switcher = '';
		if(!empty($theme_option['footer_menu_switcher'])):
		  $menu_switcher = $theme_option['footer_menu_switcher'];
		endif;
		if($menu_switcher == true):
		?>
		<div class="col-lg-6 col-md-6 col-sm-12 col-12">
			<p class="shp-footer-text text-center text-lg-left">
				<?php
				if(!empty($theme_option['footer_text'])):
				   echo esc_html($theme_option['footer_text']);
				else:
					/* translators: 1: Theme name, 2: Theme author. */
					printf( esc_html__( 'Theme: %1$s by %2$s.', 'shopmartio' ), 'shopmartio', '<a href="https://themeforest.net/user/kamleshyadav">kamleshyadav</a>' );
				endif
				?>
			</p>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-12">
		<?php
		 wp_nav_menu(array(
			'theme_location' => 'shopmartio-footer-menu',
			'menu_id'        => 'shopmartio-menu',
			'menu_class'=> 'shp-bfooter-list', 
			'fallback_cb'=> 'shopmartio_menu_editor'
			)
		  );   
		?>  
		</div> 
        </div>
		<?php 
		else:
		?>
		<p class="shp-footer-text text-center text-lg-left">
			<?php
			if(!empty($theme_option['footer_text'])):
			   echo esc_html($theme_option['footer_text']);
			else:
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'shopmartio' ), 'shopmartio', '<a href="'.esc_url('https://themeforest.net/user/kamleshyadav').'">kamleshyadav</a>' );
			endif
			?>
		</p>
		<?php
		endif;
		?>
	</div>
</div>  