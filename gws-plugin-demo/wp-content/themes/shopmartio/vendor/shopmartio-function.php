<?php
/**
 * shopmartio functions and definitions
 * @package shopmartio
 */
 
/** 
 * Shomartion Theme Core features
 */
require get_template_directory() . '/vendor/lib/shopmartio-core-lib.php';  
  
/** 
 * class tgm plugin activation file require 
 */
require get_template_directory() . '/vendor/theme-plugins/shopmartio-plugin-activate-config.php';  

/**
 * Aq Resizer file require 
 */
require get_template_directory() . '/vendor/lib/shopmartio-aq-resizer.php'; 

/**
 * Comments customization file require 
 */    
require get_template_directory() . '/vendor/lib/shopmartio-comment.php';       
/**
 * Shopmartio woocommerce
 */  
if(class_exists( 'woocommerce' )):
  require get_template_directory() . '/woocommerce/shopmartio-woocommerce.php'; 
endif;
remove_action('shutdown', 'wp_ob_end_flush_all', 1);

/**
 * Shomartion Theme Option Settings
 */
 
if(!function_exists('shopmartio_theme_option_settings')):
    function shopmartio_theme_option_settings(){
	  global $shopmartio_options;	
	  return $shopmartio_options;
	}
endif;
 
/**
 * Shopmartio Menu Editor Funcation
 */
if(!function_exists('shopmartio_menu_editor')):
 
	function shopmartio_menu_editor($args){
       
	   if(!current_user_can('manage_options')){
           return;
        }
        /* see wp-includes/nav-menu-template.php for available arguments */
        extract( $args );
         $link = $link_before
             . '<a href="' .admin_url( 'nav-menus.php' ) . '">'.$before.esc_html__('Add a menu','shopmartio').$after.'</a>'
            . $link_after;
        /* We have a list */
        if ( FALSE !== stripos( $items_wrap, '<ul' )

			or FALSE !== stripos( $items_wrap, '<ol' )

		) {
           $link = "<li>$link</li>";
        }
        $output = sprintf( $items_wrap, $menu_id, $menu_class, $link );
        if ( ! empty ( $container ) ){
           $output  = "<$container class='$container_class' id='$container_id'>$output</$container>";
        }
        if ( $echo ){
           echo "$output";
        }
        return $output;
	} 
endif;
  
/**
 * shopmartio code for category count number breakt remove
 */
if(!function_exists('shopmartio_categories_postcount_filter')){
    
    function shopmartio_categories_postcount_filter ($variable) {
    	$variable = str_replace('(', '<span class="shp-post-counting"> ', $variable);
    	$variable = str_replace(')', ' </span>', $variable);
    	return $variable;
     }
    add_filter('wp_list_categories','shopmartio_categories_postcount_filter');
 
}
 
/**
 * shopmartio code for archive count number breakt remove
 */
if(!function_exists('shopmartio_style_the_archive_count')){
    function shopmartio_style_the_archive_count($links) {
        $links = str_replace('</a>&nbsp;(', '</a> <span class="shp-post-counting">', $links);
        $links = str_replace(')', '</span>', $links);
        return $links;
    }
   add_filter('get_archives_link', 'shopmartio_style_the_archive_count');
}

/** 
 * Date Custome function
 */
if(!function_exists('shopmartio_date_custome')){ 
    function shopmartio_date_custome(){
        echo '<span>'.get_the_date( 'd' ).'</span>'.get_the_date( 'M' );
        
    }
}