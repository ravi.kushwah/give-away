<?php 
/**
 * shopmartio core Features class
 * VERSION:1.0.0
 * @package shopmartio
 */
Class Shopmartion_setting {
	
	/**
     * shopmartio Top Header Settings
     */
	public function shopmartio_top_header_settings(){
		
		$theme_option = '';
		if(function_exists('shopmartio_theme_option_settings')):
		  $theme_option = shopmartio_theme_option_settings();
		endif;
		
		$header_top_style = esc_html__('default','shopmartio');
	    if(!empty($theme_option['top_header'])):
		   $top_header = $theme_option['top_header'];
		else:
		   $top_header = false;   
		endif;
		
		if($top_header == true):
    		Switch($header_top_style):
    		    case 'default':
    			   get_template_part( 'vendor/header/shopmartio-top-header', 'defult' );
    			break;
    			default:
    			   get_template_part( 'vendor/header/shopmartio-top-header', 'defult' );
    		endswitch;
    	endif; 
	}  
	 
	/**
	 * Shopmartion Header Settings.
	 */
	public function shopmartion_header_settings(){
		$theme_option = '';
		if(function_exists('shopmartio_theme_option_settings')):
		  $theme_option = shopmartio_theme_option_settings();
		endif;
		
		$header_style = 'default';
		switch ($header_style):
			case 'default':
			   get_template_part( 'vendor/header/shopmartio-header', 'defulte' );
			break;
			default:
			   get_template_part( 'vendor/header/shopmartio-header', 'defulte' );
		endswitch;
	} 
	
	/**
     * Shopmartio Theme Loader Settings
     */ 
    public function shopmartio_themeloader_setting(){
        $theme_option = '';
		if(function_exists('shopmartio_theme_option_settings')):
		  $theme_option = shopmartio_theme_option_settings();
		endif;
        $loader_switch = '';
        if(!empty($theme_option['page_loader_switch'])):
           $loader_switch = $theme_option['page_loader_switch'];
        else:
           $loader_switch = esc_html__('on','shopmartio');  
        endif;
        $loader_image = '';
        if(!empty($themeoption_data['loader_image']['url'])):
          $loader_image = $themeoption_data['loader_image']['url'];
        endif; 
        if($loader_switch == true):
          if(!empty($loader_image)):
        ?>
        <div class="preloader">
          <div class="preloader-inner">
            <img src="<?php echo esc_url($loader_image); ?>" alt="<?php esc_attr_e('loading','shopmartio'); ?>" />
          </div>
        </div>
    <?php else: ?>    
        <div class="preloader_wrapper preloader_active preloader_open">
            <div class="preloader_holder">
                <div class="preloader d-flex justify-content-center align-items-center h-100">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    <?php
    endif;
    endif;
    }	
      
	/**
	 * shopmartio Breadcrumbs Settings
	 */
	public function shopmartio_breadcrumbs_settings(){
		
		$theme_option = '';
		if(function_exists('shopmartio_theme_option_settings')):
		  $theme_option = shopmartio_theme_option_settings();
		endif;
		$page_breadcrumbs_settings = get_post_meta(get_the_ID(),'shopmartio_page_breadcrumb',true);
		
	    if(!empty($page_breadcrumbs_settings)):
		  $page_breadcrumbs_settings = get_post_meta(get_the_ID(),'shopmartio_page_breadcrumb',true);
	    endif;
	
		if($page_breadcrumbs_settings == true):
		   
		   $page_breadcrumbs= get_post_meta(get_the_ID(),'shopmartio_breadcrumb_on',true);
		   if(!empty($page_breadcrumbs)):
		     $page_breadcrumbs = get_post_meta(get_the_ID(),'shopmartio_breadcrumb_on',true);
		   endif;
		   if($page_breadcrumbs == true):
		     get_template_part('vendor/lib/shopmartio', 'breadcrumbs');
		   endif;
		else:
		    $global_breadcrumbs= '';
			if(!empty($theme_option['shopmartio_global_breadcrumb'])):
				$global_breadcrumbs = $theme_option['shopmartio_global_breadcrumb'];
			else:
			   $global_breadcrumbs = true;
			endif;
			if($global_breadcrumbs == true):
				 get_template_part('vendor/lib/shopmartio', 'breadcrumbs');
			endif;
		endif;
	} 
	
	/**
	 *Shopmartio Footer Settings
	 */
	public function shopmartio_footer_settings(){
		
		$theme_option = '';
		if(function_exists('shopmartio_theme_option_settings')):
		  $theme_option = shopmartio_theme_option_settings();
		endif;
		
		$footer_style = 'default';
	    switch($footer_style):
			case 'default':
			   get_template_part( 'vendor/footers/shopmartio-footer', 'defult' );
			break; 
			default:
			   get_template_part( 'vendor/footers/shopmartio-footer', 'defult' );
		endswitch;  
			 
	}
	
	/**
	 * Shopmartio 404 Page
	 */ 
	public function shopmartio_404_setting($pageid){
	    
	    $theme_option = '';
		if(function_exists('shopmartio_theme_option_settings')):
		  $theme_option = shopmartio_theme_option_settings();
		endif;
        
        $error_404_title = '';
        if(!empty($theme_option['err_title'])):
           $error_404_title = $theme_option['err_title'];
        else:
           $error_404_title = esc_html__('','shopmartio');
        endif;

        $error_404_desc = '';
        if(!empty($theme_option['error_404_desc'])):
          $error_404_desc = $theme_option['error_404_desc'];
        else:
          $error_404_desc = esc_html__("It looks like nothing was found at this location.","shopmartio");
        endif; 
        $error_image_four_zero_four = '';
        if(!empty($theme_option['error_404_image']['url'])):
          $error_image_four_zero_four = $theme_option['error_404_image']['url'];
        else:
          $error_image_four_zero_four = get_template_directory_uri().'/assets/images/404.png';
        endif;
    ?>
	<div class="shp-error-page-wrapper"> 
    	<div class="container">
	  	 <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="shp-error-info">
                <div class="shp-error-img">
                    <?php 
                    if(!empty($error_image_four_zero_four)): ?>
                    <img class="parallax" src="<?php echo esc_url($error_image_four_zero_four); ?>" alt="<?php esc_attr_e('error 404','shopmartio'); ?>" title="<?php esc_attr_e('404','shopmartio'); ?>"/>
                    <?php 
                    endif; 
                    ?>
                </div>
                <?php
                  if(!empty($error_404_title)):
                    echo '<h1>' . esc_html($error_404_title) . '</h1>';
                    endif; 
                    if(!empty($error_404_desc)):
                      echo '<h4>' . wp_kses($error_404_desc,true) . '</h4>';
                    endif;
                  ?>
                  <div class="shp-error-btn">
                    <a href="<?php echo esc_url(home_url('/')); ?>" class="shp-btn"><?php esc_html_e('Back to Home','shopmartio'); ?></a>
                  </div> 
              </div>  
            </div> 
          </div>
	 	</div>
   	</div> 
	<?php
	}
} 