<?php
$theme_option = '';
if(function_exists('shopmartio_theme_option_settings')):
  $theme_option = shopmartio_theme_option_settings();
endif;
?>
<div class="shp-breadcrumb-wrapper text-center" <?php if(!empty($themeoption_data['bgimge']['attachment_id'])): echo esc_html($breadimg); endif; ?>>
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="shp-breadcrumb-container">
					<?php 
					echo '<h1>';
						if(is_home() && have_posts()) {
						  esc_html_e('Blog','shopmartio');
						}
						if(is_page()){
							while(have_posts()) { the_post();
							   the_title();
							}
						}
						if( class_exists("woocommerce") && is_product() && is_single()){
							esc_html_e('Product Details','shopmartio');
						}
						elseif(is_single()){ 
							  the_title();
						}
						if(class_exists( 'WooCommerce' ) ){
							if(is_shop()){
								esc_html_e('Shop','shopmartio');
							}
							else{
								if(is_archive()){
									the_archive_title();
								}
							}
						}
						else{
							if(is_archive()){
							  the_archive_title();
							}
						}
						if(is_search()){
							printf( esc_html__( 'Search Results for: %s', 'shopmartio' ), '<span>'.get_search_query().'</span>' );
						}
						if(is_404()){
							esc_html_e('404 Not Found','shopmartio');
						}
					echo '</h1>';
					?> 
					<ul> 
					<?php
					if (is_category() || is_tag()) {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						echo '<li>'. get_the_archive_title() .'</li>';
					}
					# Daily archive
					elseif (is_day()) {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						echo '<li>'. get_the_time('F jS, Y') .'</li>';
					}
					# Monthly archive
					elseif (is_month()) {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						echo '<li>'. get_the_time('F, Y') .'</li>';
					}
					# Yearly archive
					elseif (is_year()) {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						echo '<li>'. get_the_time('Y') .'</li>';
					}
					# Author archive
					elseif (is_author()) {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						echo '<li>'.is_author().'<li>';
					}
					# Paged archive
					elseif (is_paged()) {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
					}
					elseif( class_exists("woocommerce") && is_product() && is_single()){
						global $post;
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						if(class_exists("woocommerce") && is_product() && is_single()){
							echo '<li>'. esc_html('Shop','shopmartio') .'</li>';
						}
						echo "<li>". get_the_title() ."</li>";   
					}
					# Single post
					elseif (is_single()) {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
							echo '<li>'. esc_html('Blog','shopmartio') .'</li>';
						echo "<li>". get_the_title() ."</li>";
					}							
					# Single page

					elseif (!is_front_page() && is_home()) {
						global $post;
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						echo '<li>'. esc_html('Blog','shopmartio') .'</li>';   
					}	

					elseif (is_page()) {
						global $post;
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						
						echo "<li>". get_the_title() ."</li>";   
					}
					elseif( class_exists("woocommerce") && is_shop()){
						global $post;
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__('Home','shopmartio').'</a></li>';
						echo '<li>'. esc_html('Shop','shopmartio') .'</li>'; 
					}
					else {
						echo '<li><a href="'.esc_url(home_url()).'">'.esc_html__("Home", "shopmartio").'</a></li>';
					} ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div> 