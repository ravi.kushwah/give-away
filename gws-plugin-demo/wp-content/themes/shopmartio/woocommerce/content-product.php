<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$theme_option = '';
if(function_exists('shopmartio_theme_option_settings')):
  $theme_option = shopmartio_theme_option_settings();
endif;
$sidebar_position = '';
if(!empty($theme_option['woo_sidebar_postion'])):
  $sidebar_position = $theme_option['woo_sidebar_postion'];
else:
  $sidebar_position = esc_html__('right','shopmartio');
endif;
if(! is_active_sidebar('wc-right-sidebar') || ! is_active_sidebar('wc-left-sidebar')):
   $sidebar_position = esc_html__('full','shopmartio');     
endif;
if($sidebar_position == 'full'):
  $contenr_class = 'col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12';
else:
  $contenr_class = 'col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12'; 
endif;
$hover_product_full_image = get_post_meta(get_the_ID(),'shopmartion_product_image',true);
$hover_product_image = shopmartio_aq_resize($hover_product_full_image,255,300,true);
$shp_new_product = get_post_meta(get_the_ID(), 'shopmartion_new_product', true);
$newaddclass = '';
if($shp_new_product == 'new'):
  $newaddclass = 'shp-new-product';
endif;  
?>
<div class="<?php echo esc_attr($contenr_class); ?>">
    <div class="shp-product-thumb">
        <div class="shp-product-image <?php echo esc_attr($newaddclass); ?>"> 
        <?php if($shp_new_product == 'new'): ?>
          <span class="shp-new-prod-tag"><?php echo esc_html__('New','shopmartio'); ?></span>
        <?php
        endif;
    	/**
    	 * Hook: woocommerce_before_shop_loop_item.
    	 *
    	 * @hooked woocommerce_template_loop_product_link_open - 10
    	 */
    	do_action( 'woocommerce_before_shop_loop_item' );
        
    	/**
    	 * Hook: woocommerce_before_shop_loop_item_title.
    	 *
    	 * @hooked woocommerce_show_product_loop_sale_flash - 10
    	 * @hooked woocommerce_template_loop_product_thumbnail - 10
    	 */
    	do_action( 'woocommerce_before_shop_loop_item_title' );
    	if(!empty($hover_product_image)):
    	?>
    	<div class="shp-overlay-img">
          <img src="<?php echo esc_url($hover_product_image); ?>" alt="<?php echo esc_attr__('product-img','shopmartio'); ?>" class="img-fluid">
        </div>
        <?php 
        endif;
        ?>
        <div class="shp-product-hover">
			<ul>
			 <li>
			  <?php if($product = wc_get_product(get_the_ID())): ?>    
			   <a href="<?php echo esc_url($product->add_to_cart_url()); ?>" class="shp-product-icon toltiped" title="<?php echo esc_attr__('Cart','shopmartio'); ?>" data-productid="<?php echo esc_attr(get_the_ID()); ?>">
				    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" viewBox="0 0 16 16">
							<path d="M15.875,2.855 C15.069,4.479 14.253,6.098 13.442,7.719 C13.263,8.076 13.055,8.423 12.912,8.794 C12.673,9.419 12.247,9.635 11.586,9.625 C9.642,9.594 7.698,9.612 5.753,9.615 C5.228,9.616 4.916,9.818 4.826,10.197 C4.698,10.737 5.048,11.188 5.634,11.195 C6.792,11.208 7.950,11.200 9.108,11.200 C10.552,11.201 11.996,11.195 13.440,11.203 C14.163,11.208 14.574,11.712 14.343,12.297 C14.193,12.677 13.884,12.812 13.489,12.811 C11.945,12.807 10.401,12.808 8.857,12.808 C7.799,12.808 6.741,12.809 5.683,12.807 C4.528,12.805 3.590,12.131 3.289,11.091 C2.978,10.014 3.399,8.956 4.392,8.324 C4.440,8.294 4.486,8.262 4.576,8.202 C4.071,6.518 3.573,4.855 3.074,3.193 C2.943,2.756 2.801,2.322 2.686,1.881 C2.629,1.662 2.522,1.582 2.297,1.588 C1.812,1.603 1.325,1.604 0.839,1.592 C0.311,1.579 -0.011,1.255 0.000,0.773 C0.011,0.314 0.329,0.015 0.847,0.007 C1.590,-0.003 2.334,-0.003 3.077,0.006 C3.619,0.012 3.838,0.186 4.004,0.712 C4.091,0.984 4.168,1.258 4.267,1.591 C4.494,1.591 4.733,1.592 4.972,1.591 C8.174,1.590 11.377,1.589 14.579,1.588 C14.765,1.588 14.951,1.582 15.136,1.594 C15.869,1.642 16.201,2.200 15.875,2.855 ZM4.796,3.217 C4.794,3.263 4.785,3.294 4.792,3.319 C5.233,4.806 5.668,6.295 6.131,7.776 C6.165,7.885 6.403,7.994 6.547,7.996 C7.761,8.014 8.984,7.921 10.188,8.032 C11.216,8.127 11.828,7.815 12.104,6.826 C12.149,6.664 12.253,6.519 12.329,6.366 C12.843,5.336 13.356,4.305 13.898,3.217 C10.816,3.217 7.806,3.217 4.796,3.217 ZM7.196,14.422 C7.636,14.422 7.992,14.768 7.996,15.199 C8.000,15.650 7.634,16.007 7.175,16.001 C6.737,15.994 6.391,15.639 6.397,15.202 C6.403,14.762 6.752,14.422 7.196,14.422 ZM11.999,14.419 C12.448,14.419 12.812,14.789 12.801,15.231 C12.791,15.657 12.434,16.001 12.003,16.001 C11.548,16.001 11.188,15.636 11.199,15.190 C11.210,14.758 11.562,14.418 11.999,14.419 Z"></path>
						</svg>
					  </a>
					<?php endif; ?>
				</li>
				<li> 
				<a href="?add_to_wishlist=<?php echo esc_attr(get_the_ID()); ?>" class="shp-product-icon toltiped add_to_wishlist single_add_to_wishlist" title="<?php echo esc_attr('Wishlist','shopmartio'); ?>" data-product-id="<?php echo esc_attr(get_the_ID()); ?>"  data-original-product-id="<?php echo esc_attr(get_the_ID()); ?>"  data-title="<?php esc_attr__('Add to wishlist','shopmartio'); ?>">
				     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 16"><path class="cls-1" d="M849.5,4969a0.764,0.764,0,0,1-.385-0.11c-6.3-3.8-9.115-7.01-9.115-10.42a5.229,5.229,0,0,1,5.121-5.47,4.968,4.968,0,0,1,4.007,2.03,5.779,5.779,0,0,1,.372.52,5.779,5.779,0,0,1,.372-0.52,4.968,4.968,0,0,1,4.007-2.03,5.228,5.228,0,0,1,5.121,5.47c0,3.41-2.811,6.62-9.116,10.42A0.758,0.758,0,0,1,849.5,4969Zm-4.379-14.52a3.751,3.751,0,0,0-3.637,3.99,6.433,6.433,0,0,0,1.961,4.25,27.8,27.8,0,0,0,6.055,4.68,27.8,27.8,0,0,0,6.055-4.68,6.433,6.433,0,0,0,1.961-4.25,3.749,3.749,0,0,0-3.637-3.99,3.506,3.506,0,0,0-2.837,1.45,5.7,5.7,0,0,0-.834,1.51,0.743,0.743,0,0,1-1.417,0,5.589,5.589,0,0,0-.86-1.54A3.5,3.5,0,0,0,845.121,4954.48Z" transform="translate(-840 -4953)"/>
                        </svg>
					</a>
			    </li>
				<li>
				<a href="javascript:void(0)" class="shp-product-icon toltiped yith-wcqv-button" data-product_id="<?php echo esc_attr(get_the_ID()); ?>" title="<?php echo esc_attr__('View','shopmartio'); ?>">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" viewBox="0 0 15 16.03">
							<path d="M12.495,7.008 C12.472,7.833 12.249,8.928 11.684,9.910 C11.483,10.258 11.544,10.445 11.799,10.709 C12.740,11.679 13.663,12.670 14.581,13.666 C15.310,14.458 15.045,15.645 14.080,15.942 C13.550,16.106 13.126,15.887 12.755,15.488 C11.844,14.507 10.920,13.539 10.015,12.552 C9.799,12.317 9.647,12.252 9.340,12.431 C7.175,13.697 4.994,13.677 2.910,12.274 C0.871,10.901 -0.079,8.837 0.006,6.273 C0.109,3.165 2.412,0.521 5.354,0.066 C9.138,-0.519 12.513,2.610 12.495,7.008 ZM10.622,6.637 C10.616,4.092 8.648,1.986 6.275,1.985 C3.834,1.983 1.870,4.073 1.870,6.672 C1.870,9.230 3.853,11.330 6.262,11.325 C8.662,11.319 10.628,9.205 10.622,6.637 Z"></path>
						</svg>
					</a>
				</li>
			</ul>
		 </div>
       </div>  
   	   <div class="shp-product-info">
	    <?php 
		/**
		 * Hook: woocommerce_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_product_title - 10
		 */
		do_action('woocommerce_shop_loop_item_title');
        /**
		 * Hook: woocommerce_after_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */
		do_action('woocommerce_after_shop_loop_item_title');
		?>
		 <p class="procategory-des"><?php echo substr(get_the_content(),0,100); ?></p>
		<?php
		/**
		 * Hook: woocommerce_after_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_close - 5
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		 do_action('woocommerce_after_shop_loop_item');
		 ?>
		</div>
	</div>
</div>