<?php
/**
 * shopmartio equipment woocommerce support
 */
add_action( 'after_setup_theme', 'shopmartio_add_woocommerce_support' );
function shopmartio_add_woocommerce_support() {
    add_theme_support('woocommerce'); 
    add_theme_support('wc-product-gallery-zoom' );
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
}   
/**
 * woocommerce remove breadcrumb
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0); 

/**
 *woocommerce remove product anchor tag
 */
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5); 

/**
 * woocommerce product title
 */ 
remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_title', 5 );
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);  
add_action("woocommerce_shop_loop_item_title",'shopmartio_woocommerce_template_loop_product_title'); 
function shopmartio_woocommerce_template_loop_product_title() { 
    echo '<h2><a href="'.esc_url(get_the_permalink(get_the_ID())).'" class="shp-single-title">'.get_the_title().'</a></h2>'; 
} 

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter('loop_shop_per_page', 'shopmartio_loop_shop_per_page', 20 );
function shopmartio_loop_shop_per_page( $cols ) {
   $cols = 9;
   return $cols;
}

/**
 * Filter WooCommerce Search Field
 */
add_filter('get_product_search_form' , 'shopmartio_custom_product_searchform' );
function shopmartio_custom_product_searchform( $form ) {
    $form = '<form class="woocommerce-product-search" role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '">
            <input type="text" value="' . get_search_query() . '" name="s" id="ws" placeholder="' . esc_attr__( 'Search', 'shopmartio' ) . '" />
            <button type="submit" class="search-submit sr-btn">
	        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        	 viewBox="0 0 512.005 512.005" xml:space="preserve">
            <g>
            	<g>
        		<path d="M505.749,475.587l-145.6-145.6c28.203-34.837,45.184-79.104,45.184-127.317c0-111.744-90.923-202.667-202.667-202.667
        			S0,90.925,0,202.669s90.923,202.667,202.667,202.667c48.213,0,92.48-16.981,127.317-45.184l145.6,145.6
        			c4.16,4.16,9.621,6.251,15.083,6.251s10.923-2.091,15.083-6.251C514.091,497.411,514.091,483.928,505.749,475.587z
        			 M202.667,362.669c-88.235,0-160-71.765-160-160s71.765-160,160-160s160,71.765,160,160S290.901,362.669,202.667,362.669z"/>
        	</g>
        </g>
        </svg>
	 </button>
   <input type="hidden" name="post_type" value="'.esc_attr__('product','shopmartio').'" />
</form>';

return $form;
}
/**
 * add to cart remove from shop page
 */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10); 

/**
 * remove woocommerce catalog ordering
 */ 
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Remove "Description" Heading Title @ WooCommerce Single Product Tabs
 */
add_filter( 'woocommerce_product_description_heading', '__return_null');

/**
 * Remove "Reviews"  Title @ WooCommerce Single Product Tabs
 */
add_filter('woocommerce_product_reviews_heading', '__return_empty_string');
/**
 * WooCommerce Register widget area.
 */
add_action( 'widgets_init', 'shopmartio_woocommerce_widgets_init' );
function shopmartio_woocommerce_widgets_init() {
	
	register_sidebar(
		array(
			'name'          => esc_html__( 'WooCommerce Right Sidebar', 'shopmartio' ),
			'id'            => 'wc-right-sidebar',
			'description'   => esc_html__( 'Add Right widgets here.', 'shopmartio' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'WooCommerce Left Sidebar', 'shopmartio' ),
			'id'            => 'wc-left-sidebar',
			'description'   => esc_html__( 'Add Right widgets here.', 'shopmartio' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

/**
 * WooCommerc Product Function
 */ 
 if(!function_exists('shopmartio_content_products')){
  function shopmartio_content_products(){
    global $product;
    $rating_count = $product->get_rating_count();
    $review_count = $product->get_review_count();
    $average = $product->get_average_rating();
    $result = '';
    if(has_post_thumbnail(get_the_ID())):
      $attachment_full_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full');
      $attachment_url = shopmartio_aq_resize($attachment_full_url,255,300,true);
	endif;
    $hover_product_full_image = get_post_meta(get_the_ID(),'shopmartion_product_image',true);
    $hover_product_image = shopmartio_aq_resize($hover_product_full_image,255,300,true);
    $shp_new_product = get_post_meta(get_the_ID(), 'shopmartion_new_product', true);
    $newaddclass = '';
    if($shp_new_product == 'new'):
      $newaddclass = 'shp-new-product';
    endif;  
    $result .= '<div class="shp-product-thumb">
                <div class="shp-product-image '.esc_attr($newaddclass).'">';
    if($shp_new_product == 'new'):
    $result .= '<span class="shp-new-prod-tag">'.esc_html__('New','shopmartio').'</span>';
    endif;
    if($product->get_sale_price()):
     $result .= '<span class="onsale">'.esc_html__('Sale!','shopmartio').'</span>';
    endif;
    $result .= '<a href="'.esc_url(get_the_permalink()).'">
    				      <img src="'.esc_url($attachment_url).'" alt="'.esc_attr__('product-img','shopmartio').'" class="img-fluid attachment-woocommerce_thumbnail size-woocommerce_thumbnail">
                        </a>';
                if(!empty($hover_product_image)):
                    $result .= '<div class="shp-overlay-img">
                                <img src="'.esc_url($hover_product_image).'" alt="'.esc_attr__('product-img','shopmartio').'" class="img-fluid">
                               </div>';  
                endif; 
    $result .= '</div> 
		        <div class="shp-product-info">
                 <div class="shp-product-hover">
                 <ul>';
                if($product = wc_get_product(get_the_ID())):   		
                     $result .= '<li><a href="javascript:void(0)" class="shp-product-icon toltiped add_to_cart" title="'.esc_attr__('Cart','shopmartio').'" data-productid="'.esc_attr(get_the_ID()).'"> 
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 16">
                          <path d="M906.022,4936.83h-3.536a2.262,2.262,0,0,1-2.262-1.69,2.461,2.461,0,0,1,.97-2.75,0.329,0.329,0,0,0,.149-0.44c-0.569-1.99-1.13-3.99-1.68-5.98a0.361,0.361,0,0,0-.417-0.34c-0.428.02-.857,0.02-1.285,0.01a0.785,0.785,0,0,1-.835-0.83,0.757,0.757,0,0,1,.835-0.77c0.67-.01,1.34-0.01,2.009-0.01a0.832,0.832,0,0,1,.929.75c0.243,0.86.243,0.86,1.091,0.86,3,0,6-.01,9-0.01,0.133,0,.268,0,0.4.01a0.794,0.794,0,0,1,.629,1.21c-0.959,2.06-1.933,4.1-2.886,6.17a0.95,0.95,0,0,1-.954.62q-2.712-.015-5.424,0a3.87,3.87,0,0,0-.482.01,0.754,0.754,0,0,0-.646.75,0.769,0.769,0,0,0,.626.81,2.565,2.565,0,0,0,.48.03h6.831c0.714,0,1.066.27,1.059,0.8s-0.356.79-1.067,0.79h-3.536Zm-4.434-9.59c0.437,1.57.855,3.09,1.3,4.59a0.455,0.455,0,0,0,.371.19c1.434,0.02,2.867,0,4.3.02a0.494,0.494,0,0,0,.519-0.34c0.611-1.33,1.24-2.66,1.861-3.99,0.067-.14.126-0.29,0.2-0.47h-8.552Zm6.786,11.2a0.8,0.8,0,0,1-.017,1.59,0.763,0.763,0,0,1-.723-0.81A0.752,0.752,0,0,1,908.374,4938.44Zm-3.753.79a0.78,0.78,0,0,1-.73.8,0.8,0.8,0,0,1-.013-1.59A0.768,0.768,0,0,1,904.621,4939.23Z" transform="translate(-897.125 -4924.03)"/>
                        </svg>
                      </a>
                     </li>';
                endif; 
        $result .= '<li>
                    <a href="?add_to_wishlist='.esc_attr(get_the_ID()).'" class="shp-product-icon toltiped add_to_wishlist single_add_to_wishlist" title="'.esc_attr('Wishlist','shopmartio').'"  
						   data-product-id="'.esc_attr(get_the_ID()).'"  data-original-product-id="'.esc_attr(get_the_ID()).'"  data-title="'.esc_attr__('Add to wishlist','shopmartio').'">
					    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 14.03">
                           <path d="M910.692,4876.99a4.155,4.155,0,0,0-3.132-1.42,3.861,3.861,0,0,0-2.473.89,5.444,5.444,0,0,0-.681.67,5.065,5.065,0,0,0-.681-0.67,3.856,3.856,0,0,0-2.472-.89,4.155,4.155,0,0,0-3.132,1.42,5.177,5.177,0,0,0-1.229,3.44,6.051,6.051,0,0,0,1.5,3.88,31.438,31.438,0,0,0,3.6,3.52c0.464,0.42,1.039.93,1.622,1.46a1.177,1.177,0,0,0,1.592,0c0.586-.53,1.162-1.05,1.626-1.46a30.789,30.789,0,0,0,3.592-3.52,6.052,6.052,0,0,0,1.5-3.88A5.177,5.177,0,0,0,910.692,4876.99Zm-9.439.21a2.318,2.318,0,0,1,1.507.55,3.779,3.779,0,0,1,.876,1.06,0.885,0.885,0,0,0,1.541,0,3.727,3.727,0,0,1,.876-1.06,2.314,2.314,0,0,1,1.507-.55,2.619,2.619,0,0,1,1.972.89,3.522,3.522,0,0,1,.816,2.34,4.421,4.421,0,0,1-1.141,2.84,30.611,30.611,0,0,1-3.4,3.32l-0.028.03c-0.375.33-.862,0.77-1.371,1.22-0.49-.44-0.96-0.86-1.4-1.25a29.663,29.663,0,0,1-3.4-3.32,4.423,4.423,0,0,1-1.142-2.84,3.523,3.523,0,0,1,.817-2.34A2.617,2.617,0,0,1,901.253,4877.2Z" transform="translate(-896.906 -4875.56)"/>
                        </svg>
					</a>
                  </li>
                  <li>
                    <a href="javascript:void(0)" class="shp-product-icon toltiped yith-wcqv-button" data-product_id="'.esc_attr(get_the_ID()).'" title="'.esc_attr__('View','shopmartio').'">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.969 14.96">
                          <path d="M911.1,5035.96c-0.421-.46-0.851-0.92-1.28-1.37l-2.168-2.31a6.424,6.424,0,0,0-1.2-8.45,5.4,5.4,0,0,0-7.4.55,6.435,6.435,0,0,0-.565,7.93,5.653,5.653,0,0,0,3.684,2.36,5.485,5.485,0,0,0,4.257-1.07l0.07,0.08c0.154,0.19.312,0.38,0.478,0.55,0.918,0.99,1.837,1.98,2.767,2.96a1.35,1.35,0,0,0,.608.38,1.151,1.151,0,0,0,.182.02,0.838,0.838,0,0,0,.729-0.5A0.95,0.95,0,0,0,911.1,5035.96Zm-5.3-10.21a4.377,4.377,0,0,1-.012,5.89,3.753,3.753,0,0,1-2.748,1.21h-0.007a3.745,3.745,0,0,1-2.761-1.24,4.225,4.225,0,0,1-1.128-2.93,4.015,4.015,0,0,1,3.9-4.15h0A3.794,3.794,0,0,1,905.8,5025.75Z" transform="translate(-897.406 -5022.63)"/>
                        </svg>
					</a>
                  </li>
                </ul>
            </div>
            <h2 class="shp-product-title">
                <a href="'.esc_url(get_permalink()).'">'.
                   get_the_title()
               .'</a>
            </h2>';
            if($rating_count >= 0 ) :
              $result .= wc_get_rating_html($average, $rating_count);
            endif;
        $result .= '<span class="price">';
                if($product->get_price_html()):
                  $result .= $product->get_price_html(); 
                endif;
    $result .= '</span>	
        </div>  
    </div>';
    return $result;
    }
 }
/**
 * woocommerce inline style remove
 */
 add_action( 'wp_print_styles', function(){
	wp_style_add_data( 'woocommerce-inline', 'after', '' );
});