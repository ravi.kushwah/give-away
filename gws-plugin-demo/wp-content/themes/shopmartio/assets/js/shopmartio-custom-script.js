/*--------------------- Copyright (c) 2021 -----------------------
[Master Javascript]
Project: Shopmartio
Version: 1.0.0
Assigned to: Theme Forest
-------------------------------------------------------------------*/
(function($) {
    "use strict";
    /*-----------------------------------------------------
    	Function  Start
    -----------------------------------------------------*/
    var Shopmartio = {
        initialised: false,
        version: 1.0,
        mobile: false,
        collapseScreenSize: 1199,
        init: function() {
            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }
            /*-----------------------------------------------------
            	Function Calling
			-----------------------------------------------------*/
            this.loader();
            this.headerCart();
            this.searchBox();
            this.fixClasses();
            this.topButton();
            this.focusText();
            this.toggleMenu();
            this.hoverTooltip();
            this.popupVideo();
            this.relaterProduct();
            this.Product();
            this.niceSelectType();
            this.gridListView();
        },
        /*-----------------------------------------------------
            	Function Start
		-----------------------------------------------------*/
        /*-----------------------------------------------------
            	Fix Loader JS
		-----------------------------------------------------*/
        loader: function() {
            jQuery(window).on('load', function() {
                
                $(".preloader").fadeOut();
                $(".preloader-inner").delay(500).fadeOut("slow");
            });


            $(window).on('load', function() {
                $(".preloader_wrapper").removeClass('preloader_active');
            });
            jQuery(window).on('load', function() {
                setTimeout(function() {
                    jQuery('.preloader_open').addClass('loaded');
                }, 100);
            });
        },
        
        /*-----------------------------------------------------
            	Fix Header Cart JS
		-----------------------------------------------------*/
        headerCart: function() {
            $(".shp-cart-btn").on('click', function() {
                $("body").toggleClass('open-cart');
            });
            $("body, .shp-close-cart").on('click', function() {
                $("body").removeClass('open-cart');
            });
            $('.shp-header-cart-box, .shp-cart-btn').click(function(event) {
                event.stopPropagation();
            });
        },

        /*-----------------------------------------------------
            	Fix Classes JS
		-----------------------------------------------------*/
        fixClasses: function() {
            $('.wc-forward, .wp-block-button__link, .woocommerce div.product form.cart .button, .woocommerce-Button.button, .yith-wcaf.yith-wcaf-settings.woocommerce input[type="submit"], .yith-wcaf.yith-wcaf-link-generator input[type="submit"]').addClass('shp-btn');
            $('.wp-block-search__button, .search-form .search-submit, .form-submit .submit, form.post-password-form input[type="submit"], .widget.woocommerce.widget_price_filter button.button, .woocommerce-product-search button').addClass('shp-btn');
            if ($(window).width() >= Shopmartio.collapseScreenSize) {
                $('.shp-right select, .currency-switcher').addClass('select-type');
            }
            $(".dashboard-title, .yith-wcaf-share h4.yith-wcaf-share-title").addClass('shp-lined-title');
        },

        /*-----------------------------------------------------
        	Fix GoToTopButton
        -----------------------------------------------------*/
        topButton: function() {
            var scrollTop = $("#scroll");
            $(window).on('scroll', function() {
                if ($(this).scrollTop() < 500) {
                    scrollTop.removeClass("active");
                } else {
                    scrollTop.addClass("active");
                }
            });
            $('#scroll').click(function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 2000);
                return false;
            });
        },

        /*-----------------------------------------------------
            	Fix Mobille Menu JS
		-----------------------------------------------------*/
        toggleMenu: function() {

            /* Header Style One */
            $(".nav-toggle-btn").on('click', function() {
                $(".shp-nav-default-style, .shp-main-nav-wrapper").toggleClass('open-nav');
            });
            $("body").on('click', function() {
                $(".shp-nav-default-style, .shp-main-nav-wrapper").removeClass('open-nav');
            });
            $(".shp-nav-default-style, .nav-toggle-btn, .menu-item-has-children > a").on('click', function(e) {
                e.stopPropagation();
            });

            if ($(window).width() <= Shopmartio.collapseScreenSize) {
                $(".shp-nav-default-style ul > li:has(ul)").addClass('has_sub_menu');
                $(".shp-nav-default-style ul > li:has(ul) > a").on('click', function(e) {
                    e.preventDefault();
                    $(this).parent('.shp-nav-default-style ul li').children('ul.sub-menu').slideToggle();
                    $(this).parent('.shp-nav-default-style ul li').toggleClass("open");
                });

                /* Top Heaader */
                $(".shp-top-menu").on('click', function() {
                    $(".shp-header-info").toggleClass('open-nav');
                });
                $("body").on('click', function() {
                    $(".shp-header-info").removeClass('open-nav');
                });
                $(".shp-header-info, .shp-top-menu").on('click', function(e) {
                    e.stopPropagation();
                });
                $(".shp-header-info ul > li:has(ul)").addClass('has_sub_menu');
                $(".shp-header-info ul > li:has(ul) > a").on('click', function(e) {
                    e.preventDefault();
                    $(this).parent('.shp-header-info ul li').children('ul.sub-menu').slideToggle();
                    $(this).parent('.shp-header-info ul li').toggleClass("open");
                });



            }
        },
        /*-----------------------------------------------------
        	Fix Search Bar
        -----------------------------------------------------*/
        searchBox: function() {
            $('.shp-search-btn').on("click", function() {
                $('.shp-search-form').addClass('show-search');
            });
            $('.close-search').on("click", function() {
                $('.shp-search-form').removeClass('show-search');
            });
            $('.shp-search-form').on("click", function() {
                $('.shp-search-form').removeClass('show-search');
            });
            $(".shp-search-form-inner").on('click', function(e) {
                e.stopPropagation();
            });

        },


        /*-----------------------------------------------------
        	 Grid List View
        -----------------------------------------------------*/
        gridListView: function() {
            $('.shp-view-list-grid li').on('click', function() {
                if ($(this).hasClass('Category-grid-btn')) {
                    $('.shp-products-container').removeClass('list').addClass('grid');
                    $(this).addClass('active').siblings().removeClass('active');
                } else if ($(this).hasClass('Category-list-btn')) {
                    $('.shp-products-container').removeClass('grid').addClass('list');
                    $(this).addClass('active').siblings().removeClass('active');
                }
                $(".shp-products-container").append(`
                <div class="section-loader">
                    <div class="section-loader-inner">
                        <img src="https://kamleshyadav.com/wp/shopmartio/wp-content/themes/shopmartio/assets/images/ajax-loader.gif" alt="loader">
                    </div>
                </div>`);
                setTimeout(function() {
                    $('.section-loader').remove();
                }, 1000);
            });

        },

        /*-----------------------------------------------------
        	Fix  On focus Placeholder
        -----------------------------------------------------*/
        focusText: function() {
            var place = '';
            $('input,textarea').focus(function() {
                place = $(this).attr('placeholder');
                $(this).attr('placeholder', '');
            }).blur(function() {
                $(this).attr('placeholder', place);
            });
        },

        /*-----------------------------------------------------
        	Fix Select Field
        -----------------------------------------------------*/
        niceSelectType: function() {
            if ($('.select-type').length > 0) {
                $('.select-type').niceSelect();
            }
        },

        /*-----------------------------------------------------
        	Fix Tooltip Js 
        -----------------------------------------------------*/
        hoverTooltip: function() {
            $('.toltiped').tooltip();
        },

        /*-----------------------------------------------------
				Fix Video Popup
		----------------------------------------------------*/
        popupVideo: function() {
            if ($(".popup-youtube").length > 0) {
                $(".popup-youtube").magnificPopup({
                    type: "iframe",
                });
            }
        },

        /*-----------------------------------------------------
			Fix Related Product Slider
		----------------------------------------------------*/
        relaterProduct: function() {
            var productCount = $('.shp-related-product-swiper').attr('data-product');
            if ($('.shp-related-product-swiper').length > 0) {
                var swiper = new Swiper('.shp-related-product-swiper', {
                    slidesPerView: productCount,
                    spaceBetween: 30,
                    loop: false,
                    autoplay: false,
                    speed: 1500,
                    breakpoints: {
                        0: {
                            slidesPerView: 1,
                            spaceBetween: 0
                        },
                        480: {
                            slidesPerView: 1,
                            spaceBetween: 0
                        },
                        767: {
                            slidesPerView: 2,
                            spaceBetween: 15
                        },
                        991: {
                            slidesPerView: 2,
                            spaceBetween: 30
                        },
                        1200: {
                            slidesPerView: productCount,
                            spaceBetween: 30
                        }
                    }

                });

            }

        },

        Product: function() {
            if ($('.product-slider').length > 0) {
                var swiper = new Swiper('.product-slider', {
                    slidesPerView: 4,
                    spaceBetween: 30,
                    loop: true,
                    autoplay: true,
                    speed: 1500,
                    breakpoints: {
                        0: {
                            slidesPerView: 1,
                            spaceBetween: 0
                        },
                        480: {
                            slidesPerView: 1,
                            spaceBetween: 0
                        },
                        767: {
                            slidesPerView: 2,
                            spaceBetween: 15
                        },
                        991: {
                            slidesPerView: 2,
                            spaceBetween: 30
                        },
                        1200: {
                            slidesPerView: 4,
                            spaceBetween: 30
                        }
                    },
                     navigation: {
                        nextEl: '.shp-product-nav .shp-swiper-next',
                        prevEl: '.shp-product-nav .shp-swiper-prev',
                    },

                });

            }

        },


    };

    Shopmartio.init();

    /**
     * Cart Quantity Count
     */
    if (!String.prototype.getDecimals) {
        String.prototype.getDecimals = function() {
            var num = this,
                match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            if (!match) {
                return 0;
            }
            return Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
        }
    }
    // Quantity "plus" and "minus" buttons
    $(document.body).on('click', '.plus, .minus', function() {
        var $qty = $(this).closest('.quantity').find('.qty'),
            currentVal = parseFloat($qty.val()),
            max = parseFloat($qty.attr('max')),
            min = parseFloat($qty.attr('min')),
            step = $qty.attr('step');

        // Format values
        if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
        if (max === '' || max === 'NaN') max = '';
        if (min === '' || min === 'NaN') min = 0;
        if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;

        // Change the value
        if ($(this).is('.plus')) {
            if (max && (currentVal >= max)) {
                $qty.val(max);
            } else {
                $qty.val((currentVal + parseFloat(step)).toFixed(step.getDecimals()));
            }
        } else {
            if (min && (currentVal <= min)) {
                $qty.val(min);
            } else if (currentVal > 0) {
                $qty.val((currentVal - parseFloat(step)).toFixed(step.getDecimals()));
            }
        }

        // Trigger change event
        $qty.trigger('change');
    });
    
    $('.add_to_cart').on('click', function() {

        var product_id = $(this).attr('data-productid');
        var formdata = 'product_id=' + product_id;
        formdata += '&action=shopmartion_woocommerce_ajax_add_to_cart';
        $.ajax({
            type: 'post',
            url: frontadminajax.ajax_url,
            data: formdata,
            success: function(response) {

                toastr.success('Product has been added to your cart');
                location.reload();
            }

        });

        $('.add_to_wishlist').on('click', function() {
            toastr.success('Product has been added to your wishlist');
        });

    });

})(jQuery);