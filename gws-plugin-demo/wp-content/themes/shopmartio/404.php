<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package shopmartio
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
    <?php
    /**
     * shopmartio 404 content
     */
	$shopmartio_setting = ''; 
	if(class_exists('Shopmartion_setting')):
	   $shopmartio_setting = new Shopmartion_setting();
	   $shopmartio_setting->shopmartio_404_setting(get_the_ID());
	endif;  
	?>
    </main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();