<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shopmartio
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="header-main-wrapper">
	<?php 
	if(class_exists('Shopmartion_setting')):
	     $theme_setting_option = new Shopmartion_setting();
	     
	     $theme_setting_option->shopmartio_themeloader_setting();
	     ?>
	     <header class="shp-header-default-style"> 
	     <?php 
	    //Top Header Settings 	 
		 $theme_setting_option->shopmartio_top_header_settings();
		//Header Settings
	     $theme_setting_option->shopmartion_header_settings();
	     ?>
	     </header> 
	     <?php
		// Breadcrumbs Settings
		 $theme_setting_option->shopmartio_breadcrumbs_settings();
		 
	endif;  
	?> 
</div>  