<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shopmartio
 */
get_header();

$theme_option = '';
if(function_exists('shopmartio_theme_option_settings')):
  $theme_option = shopmartio_theme_option_settings();
endif;
$sidebar_position = '';
if(!empty($theme_option['sidebar_postion'])):
  $sidebar_position = $theme_option['sidebar_postion'];
else:
  $sidebar_position = esc_html__('right','shopmartio');
endif;
if(! is_active_sidebar('left-sidebar') || ! is_active_sidebar('right-sidebar')):
   $sidebar_position = esc_html__('full','shopmartio');     
endif;
if($sidebar_position == 'both'):
	$side_col = 'col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12';
	$main_col = 'col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12';
elseif($sidebar_position == 'full'):
	$side_col = '';
	$main_col = 'col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12';
else:
	$side_col = 'col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12';
	$main_col = 'col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12';
endif; 
?> 
<section class="shp-main-post-wrapper">
    <div class="container">
        <div class="row">
		    <?php 
			if($sidebar_position=='left' || $sidebar_position=='both'):
			?>
            <div class="<?php echo esc_attr($side_col); ?>">
                <div class="shp-sidebar-wrapper">
                <?php 
				if(is_active_sidebar('left-sidebar')):
				  dynamic_sidebar( 'left-sidebar' );
				endif;
				?>
                </div>                      
            </div>   
			<?php endif; ?> 
			<div class="<?php echo esc_attr($main_col); ?>">
			  <div class="shp-blog-columns">           
                <?php
				if ( have_posts() ) :
				
                    /* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						* Include the Post-Type-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Type name) and that will be used instead.
						*/
						get_template_part( 'template-parts/content', get_post_type() );

					endwhile;
 
					 shopmartio_blog_pagination();

				else :

					 get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
                </div>     
            </div>   
            <?php 
			if($sidebar_position=='right' || $sidebar_position=='both'):
			?>
            <div class="<?php echo esc_attr($side_col); ?>">
                <div class="shp-sidebar-wrapper">
                <?php 
				if(is_active_sidebar('right-sidebar')):
				 dynamic_sidebar( 'right-sidebar' );
				endif;
				?> 
                </div>                      
            </div>  
			<?php endif; ?>			
		</div> 
    </div>
</section> 
<?php
get_footer();