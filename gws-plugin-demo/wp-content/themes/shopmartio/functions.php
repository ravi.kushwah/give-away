<?php
/**
 * shopmartio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package shopmartio
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}
if ( ! function_exists( 'shopmartio_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function shopmartio_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on shopmartio, use a find and replace
		 * to change 'shopmartio' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'shopmartio', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
          
		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
			'shopmartio-header-menu' => esc_html__('Header Menu', 'shopmartio' ),
			'shopmartio-top-header-menu' => esc_html__('Top Header Menu', 'shopmartio' ),
			'shopmartio-footer-menu' => esc_html__( 'Footer Menu', 'shopmartio' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'shopmartio_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
		add_theme_support( "custom-header", array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			));
		add_theme_support( "custom-background", array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			));	
		/**
		 * Add support for editor styles.
		 */ 
        add_theme_support( 'editor-styles' );
        add_theme_support( 'dark-editor-style' );
        add_theme_support( 'align-wide' );
        get_the_tag_list();
         
        /**
         * Enqueue editor styles.
         */
		add_editor_style('assets/css/style-editor.css');
	}
endif;
add_action( 'after_setup_theme', 'shopmartio_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function shopmartio_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'shopmartio_content_width', 640 );
}
add_action( 'after_setup_theme', 'shopmartio_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function shopmartio_widgets_init() {
	
	register_sidebar(
		array(
			'name'          => esc_html__( 'Right Sidebar', 'shopmartio' ),
			'id'            => 'right-sidebar',
			'description'   => esc_html__( 'Add Right widgets here.', 'shopmartio' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
    register_sidebar(
		array(
			'name'          => esc_html__('Left Sidebar', 'shopmartio' ),
			'id'            => 'left-sidebar',
			'description'   => esc_html__( 'Add Left widgets here.', 'shopmartio' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	); 
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'shopmartio' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add Footer Col One Widgets Here.', 'shopmartio' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'shopmartio' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add Footer Col Two Widgets Here.', 'shopmartio' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'shopmartio' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add Footer Col Three Widgets Here.', 'shopmartio' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'shopmartio' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add Footer Col Four Widgets Here.', 'shopmartio' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar(
		array(
		'name' => esc_html__('Shopmartio Top Header', 'shopmartio' ),
		'id'   => 'shp-top-header',
		'description'   => esc_html__( 'Add Shopmartio Top Header Setting.', 'shopmartio' ),
		'before_widget' => '<div class="shp-right">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
		)
	);
}
add_action( 'widgets_init', 'shopmartio_widgets_init' );

/**
 * Enqueue scripts and styles.
 */ 
function shopmartio_scripts() {
    
    // Styles File Enqueue
    if (!is_user_logged_in()) {
        wp_dequeue_style('dashicons');
        wp_deregister_style('dashicons');
    }
    wp_enqueue_style( 'shopmartio-style', get_stylesheet_uri(), array(), _S_VERSION );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '1', 'all' );
    wp_enqueue_style( 'nice-select', get_template_directory_uri() . '/assets/css/nice-select.css', array(), '1', 'all' );
    wp_enqueue_style( 'font', get_template_directory_uri() . '/assets/css/font.css', array(), '1', 'all' );
    wp_enqueue_style( 'swiper-bundle', get_template_directory_uri() . '/assets/css/swiper-bundle.min.css', array(), '1', 'all' );
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/assets/css/fontawesome.min.css', array(), '1', 'all' );
    wp_enqueue_style( 'shopmartio-custom-style', get_template_directory_uri() . '/assets/css/shopmartio-custom-style.css', array(), '1', 'all');
    
	// Script File Enqueue  
	wp_enqueue_script( 'bootstrap-bundle', get_template_directory_uri().'/assets/js/bootstrap.bundle.min.js',array('jquery'), '20151215', true);
	wp_enqueue_script( 'SmoothScroll', get_template_directory_uri().'/assets/js/SmoothScroll.min.js',array('jquery'), '20151215', true);
    wp_enqueue_script( 'jquery-nice-select', get_template_directory_uri().'/assets/js/jquery.nice-select.min.js',array('jquery'), '20151215', true);
    wp_enqueue_script( 'swiper-bundle', get_template_directory_uri().'/assets/js/swiper-bundle.min.js',array('jquery'), '20151215', true);
    wp_enqueue_script( 'isotope-pkgd', get_template_directory_uri().'/assets/js/isotope.pkgd.min.js',array('jquery'), '20151215', true);
	wp_enqueue_script( 'shopmartio-custom-script', get_template_directory_uri().'/assets/js/shopmartio-custom-script.js',array('jquery'), '20151215', true);
    wp_enqueue_script( 'shopmartio-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), _S_VERSION, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'shopmartio_scripts' );
 
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';
 
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
    
/**
 * Shopmartio Custom Functions
 */
require get_template_directory() . '/vendor/shopmartio-function.php';   