<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shopmartio
 */
?> 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="shp-details-wrapper">
        <?php if(the_post_thumbnail()): ?>
		<div class="shp-single-imgae-wrap">
			<div class="shp-single-imgae">
			   <?php shopmartio_post_thumbnail(); ?>
			</div>	
		</div>	
		<?php endif; ?>
		<!-- entry-content -->
		<div class="entry-content shp-single-data">
    		<?php
    		the_content();
    	    wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'shopmartio' ).'<span class="page-number">',
						'after'  => '</span></div>',
					) );
    		?>
    	</div><!-- .entry-content -->
    	<?php if ( get_edit_post_link() ) : ?>
			<div class="edit-btn-read-more-wrap">
        		<footer class="entry-footer">
        			<?php
        			edit_post_link(
        				sprintf(
        					wp_kses(
        						/* translators: %s: Name of current post. Only visible to screen readers */
        						__( 'Edit <span class="screen-reader-text">%s</span>', 'shopmartio' ),
        						array(
        							'span' => array(
        								'class' => array(),
        							),
        						)
        					),
        					wp_kses_post( get_the_title() )
        				),
        				'<span class="edit-link">',
        				'</span>'
        			);
        			?>
        		</footer><!-- .entry-footer -->
        		</div>
    	    <?php endif; ?>
			<!-- .entry-footer -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->