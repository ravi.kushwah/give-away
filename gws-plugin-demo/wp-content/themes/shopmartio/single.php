<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package shopmartio
 */

get_header();

$theme_option = '';
if(function_exists('shopmartio_theme_option_settings')):
  $theme_option = shopmartio_theme_option_settings();
endif;
$page_sidebar_option = get_post_meta(get_the_ID(),'shopmartio_page_sidebar',true);
if($page_sidebar_option == true):
    $sidebar_position = get_post_meta(get_the_ID(),'shopmartio_sidebar_postion',true);
	if(!empty($sidebar_position)):
	   $sidebar_position = get_post_meta(get_the_ID(),'shopmartio_sidebar_postion',true);
	else:
	  $sidebar_position = esc_html__('right','shopmartio');
	endif;
	if(! is_active_sidebar('left-sidebar') || ! is_active_sidebar('right-sidebar')):
      $sidebar_position = esc_html__('full','shopmartio');     
    endif;
	if($sidebar_position == 'both'):
    	$side_col = 'col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12';
    	$main_col = 'col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12';
    elseif($sidebar_position == 'full'):
    	$side_col = '';
    	$main_col = 'col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12';
    else:
    	$side_col = 'col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12';
    	$main_col = 'col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12';
    endif; 
else:
	$sidebar_position = '';
	if(!empty($theme_option['sidebar_postion'])):
	  $sidebar_position = $theme_option['sidebar_postion'];
	else:
	  $sidebar_position = esc_html__('right','shopmartio');
	endif;
	if(! is_active_sidebar('left-sidebar') || ! is_active_sidebar('right-sidebar')):
       $sidebar_position = esc_html__('full','shopmartio');     
    endif;
	if($sidebar_position == 'both'):
    	$side_col = 'col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12';
    	$main_col = 'col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12';
    elseif($sidebar_position == 'full'):
    	$side_col = '';
    	$main_col = 'col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12';
    else:
    	$side_col = 'col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12';
    	$main_col = 'col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12';
    endif; 
endif; 
?>
<main id="primary" class="site-main">
<section class="shp-single-blog">
     <div class="container">
        <div class="row">
		    <?php 
			if($sidebar_position=='left' || $sidebar_position=='both'):
			?>
            <div class="<?php echo esc_attr($side_col); ?>">
                <div class="shp-sidebar-wrapper shp-left-sidebar">
                <?php 
				if(is_active_sidebar('left-sidebar')):
				  dynamic_sidebar( 'left-sidebar' );
				endif;
				?>
                </div>                      
            </div>   
			<?php endif; ?> 
            <div class="<?php echo esc_attr($main_col); ?>">
                <div class="shp-blog-columns">     
                <?php
				while ( have_posts() ) : the_post();
					
                     get_template_part( 'template-parts/content', 'single');

                ?>
				<div class="shp-blog-single-nave">
				    <div class="shp-prenext-post mb-80">
						<div class="row">
        				<?php
        				$prevPost = get_previous_post(true);
        				$prevthumbnail = '';
                        if(!empty($prevPost->ID)):
                         $prevthumbnail = get_the_post_thumbnail($prevPost->ID, array(100,100) );
                        endif;
        				$nextPost = get_next_post(true);
        				$nextthumbnail = '';
        				if(!empty($nextPost->ID)):
        				  $nextthumbnail = get_the_post_thumbnail($nextPost->ID, array(100,100) );
        				endif;
				        the_post_navigation(
        						array(
    							'prev_text' => '<div class="col-md-12">
    							                    <div class="shp-prenext-post">
                                                        <div class="shp-prenext-img">'.
                                                        $prevthumbnail
                                                        .'</div>
                                                        <div class="shp-prenext-data">
                                                            <p class="shp-prenext-lbl">'.esc_html__('Previous Post', 'shopmartio' ).'</p>
                                                            <span class="nav-title">%title</span>
                                                        </div>
                                                    </div>
                                                </div>',
    							'next_text' => '<div class="col-md-12">
    							                  <div class="shp-prenext-post next">
                                                    <div class="shp-prenext-data">
                                                        <p class="shp-prenext-lbl">'.esc_html__('Next Post', 'shopmartio' ).'</p>
                                                        <span class="nav-title">%title</span>
                                                    </div>
                                                    <div class="shp-prenext-img">'.
                                                    $nextthumbnail  
                                                    .'</div>
                                                 </div>
    							                </div>',
        							               
        						)
        					);
        				?> 
				   </div>
				  </div>
				</div>
                <?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
		       ?>
               </div>     
            </div>    
			<?php 
			 if($sidebar_position=='right' || $sidebar_position=='both'):
			?>
            <div class="<?php echo esc_attr($side_col); ?>">
                <div class="shp-sidebar-wrapper shp-right-sidebar">
                <?php 
				if(is_active_sidebar('right-sidebar')):
				 dynamic_sidebar( 'right-sidebar' );
				endif;
				?> 
                </div>                      
            </div>  
			<?php endif; ?>			
		</div>
    </div>
  </section> 
</main><!-- #main -->
<?php
get_footer();