<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shopmartio
 */
 
if(class_exists('Shopmartion_setting')):
	$theme_setting_option = new Shopmartion_setting();
	$theme_setting_option->shopmartio_footer_settings();
endif; 
?>
<?php wp_footer(); ?>
</body>
</html> 