<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_cpanel_domain {
    
    public $cPanelUserName = C_USERNAME, 
    $cPanelPassword = C_PASSWORD, 
    $siteDomain = 'plrsitebuilder.co.in', 
    $cpanelURL = 'https://plrsitebuilder.co.in:2083/',

    $cPanelQueryUrl = '', //not need to provide this value
    $cPanelToken = ''; //not need to provide this value

    

    function __construct() {
        $this->cPanelToken = self::createSession();
        $this->cPanelQueryUrl = $this->cpanelURL . $this->cPanelToken ."/json-api/cpanel?cpanel_jsonapi_version=2&";
    }
    

       

    /**
     * 
     * 
     * 
     */
    function checkDomainExist($params){
        try{
			extract($params);
			/*
			$params = [
				"domainName" : "example.com", //required
				"type" : domain / subDomain, //required
			]
			*/
			
			if($type == 'subDomain'){
			    $que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=listsubdomains&cpanel_jsonapi_module=SubDomain";
			}else{
			    $que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=listaddondomains&cpanel_jsonapi_module=AddonDomain";
			}
			
			$resp =  $this->request( $que );
			if(isset($resp['cpanelresult']) && isset($resp['cpanelresult']['data']) && is_array($resp['cpanelresult']['data'])){
			    $isExist = false;
			    foreach($resp['cpanelresult']['data'] as $domainData){
			        if($domainData['domain'] == $domainName){
			            $isExist = true;
			            break;
			        }
			    }
			    
			    return [
                    "status" => true,
                    "isExist" => $isExist
                ];
			}else{
			    throw new Exception("Unable to process this request, please try again later.");
			}

		}catch(Exception $e) {
            return [
                "status" => false,
                "message" => $e->getMessage()
            ];
        }

    }
    

    /**
     * 
     * 
     * 
     */

	

    function addAddonDomain($params){
        try{
			extract($params);
			
// 			echo"<pre>";print_r($this->siteDomain);die;
			/*
			$params = [
				"domainName" : "example.com", //required
				"subDomainName" : "example.SITE_DOMAIN", //required
				"dir" : "", // optional
			]
			*/
			
// 			$subDomainName = $subDomainName.'.'.$this->siteDomain;
			
			$que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=addaddondomain&cpanel_jsonapi_module=AddonDomain&newDomain=".$domainName."&subdomain=$subDomainName&dir=$dir";
			return  $this->request(
				$que
			);
		}catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }

    }

    /***
     * 
     * 
     * 
     */

    
    function removeAddonDomain($params){
		try{
			extract($params);

			/*
			$params = [
				"domainName" : "example.com", //required
				"subDomainName" : "example.SITE_DOMAIN", //required
				"dir" : "", // optional
			]
			*/
			
			
			return $this->request(
				$this->cPanelQueryUrl . "cpanel_jsonapi_func=deladdondomain&cpanel_jsonapi_module=AddonDomain&domain=$domainName&subdomain=$subDomainName.".$this->siteDomain
			);
		}catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}

        

    }


    /**
     * 
     * 
     * 
     * 
     */
    
    function addSubDomain($params){
        
        try {
			extract($params);

			/*
			$params = [
				"subDomainName" : "example.SITE_DOMAIN", //required
				"dir" : "", // optional
			]
			*/ 
			
            $dir = isset($dir) ? $dir : $subDomainName . "." . $this->siteDomain;

    
            return $this->request(
               $this->cPanelQueryUrl . "cpanel_jsonapi_func=addsubdomain&cpanel_jsonapi_module=SubDomain&domain=$subDomainName&rootdomain=" . $this->siteDomain . "&dir=$dir" 
            );
        }catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }


    /**
     * 
     * 
     * 
     * 
     */


    
    function removeSubDomain($params){
		try{
			extract($params);

			/*
			$params = [
				"subDomainName" : "example.SITE_DOMAIN", //required
				"dir" : "", // optional
			]
			*/
			
			$dir = isset($dir) ? $dir : $subDomainName . "." . $this->siteDomain;
	
			// $removeSubDomainQuery = $this->cPanelQueryUrl . "cpanel_jsonapi_func=delsubdomain&cpanel_jsonapi_module=SubDomain&domain=".$subDomainName.'.'.$this->siteDomain;
			$removeSubDomainQuery = $this->cPanelQueryUrl . "cpanel_jsonapi_func=delsubdomain&cpanel_jsonapi_module=SubDomain&domain=".$subDomainName;
	
			// $removeDirQuery = in_array($dir , ['public_html' , '/public_html' , 'public_html/' , '/public_html/']) ?
			// 	''
			// : 
			// 	$this->cPanelQueryUrl . "cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=unlink&sourcefiles=$dir";
	
			// return $this->request($removeSubDomainQuery , $removeDirQuery);
			return $this->request($removeSubDomainQuery);
		}catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
        
    }

    private function request($query, $dir = false){
        // echo $query;echo"<br>";
        // echo $this->cPanelUserName;echo"<br>";
        // echo $this->cPanelPassword;echo"<br>";
		try{
		
			$curl = curl_init();                                // Create Curl Object
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);       // Allow self-signed certs
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);       // Allow certs that do not match the hostname
			curl_setopt($curl, CURLOPT_HEADER,0);               // Do not include header in output
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);       // Return contents of transfer on curl_exec
			$header[0] = "Authorization: Basic " . base64_encode($this->cPanelUserName.":".$this->cPanelPassword) . "\n\r";
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);    // set the username and password
			curl_setopt($curl, CURLOPT_URL, $query);            // execute the query
			$result = curl_exec($curl);
			if ($result == false) {
				error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
			}
			curl_close($curl);
	
			if ($dir) {
				self::request($dir);
			}
			
			print_r($result);
	
			return $result != '' ? json_decode($result , true) : [];
		}catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}

    }


    
    private function createSession() {
        try {
            
            $url = $this->cpanelURL . "login";
            $cookies = "/path/to/storage/for/cookies.txt";
// echo"<pre>";print_r($url);die;
            // Create new curl handle
            $ch=curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookies); // Save cookies to
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=" . $this->cPanelUserName . "&pass=". $this->cPanelPassword);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(["user" => $this->cPanelUserName , "pass" => $this->cPanelPassword]));
            curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // Execute the curl handle and fetch info then close streams.
            $f = curl_exec($ch);
            $h = curl_getinfo($ch);
            curl_close($ch);

            // If we had no issues then try to fetch the cpsess
            if ($f == true and strpos($h['url'],"cpsess")){
                // Get the cpsess part of the url
                $pattern="/.*?(\/cpsess.*?)\/.*?/is";
                $preg_res=preg_match($pattern,$h['url'],$cpsess);
            }

            // If we have a session then return it otherwise return empty string
            $token =  (isset($cpsess[1])) ? $cpsess[1] : "";
// echo $token;echo"<br>";
            if($token == ''){
                throw new Exception("Unable to create session, please check cPanel login details.");
            }else{
                return $token;
            }
            
        }catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }
    

}
				