<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>VIP Exclusive Bonuses</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group plr_bonus_list">
                            <ul>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/omg-manga-graphic-novel-comic-reader.zip">Novel and Comic Site Builder</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/wiktionary-dictionary-wikipedia-api-based-php-dictionary-script.zip">Dictionary GiveAway </a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/podcasty-podcast-hosting-and-management-app.zip">Podcast Hosting Software</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/nfulab-influencer-hiring-platform.zip">Hiring Website Builder</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/tasky-complete-task-management-solution.zip">Task Management Software </a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/riverr-freelance-services-marketplace.zip">GiveAway Freelancer MarketPlace</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/digitalsell-digital-product-and-subscription-selling-platform-saas.zip">Digital Seller Platform</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Special WhiteLabel Software</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group plr_bonus_list">
                            <ul>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/XDesignert-designer-tool-php-version.zip">xDesigner Whitelabel</a></li>
                                <li><a href="https://drive.google.com/file/d/1GZdXzuDNo_5BYcevRPkhyKIjLY0K2qhL/view?usp=sharing">Article Rewriter (WhiteLabel Rights)</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/emarketplace-premium-digital-content-marketplace.zip">Marketplace  (WhiteLabel Rights)</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/blogify-content-management-system.zip ">Content Management System (WhiteLabel Rights)</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/tmail-multi-domain-temporary-email-system.zip">Marke Temporary Email System (WhiteLabel Rights) </a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/sitespy-complete-visitor-seo-analytics.zip">Spy - The Most Complete Visitor Analytics & SEO Tools (WhiteLabel Rights)</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/jobfinder-job-search-engine-affiliate-script.zip">Job Finder (WhiteLabel Rights)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Semi-Exclusive Bonuses</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group plr_bonus_list">
                            <ul>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/jobpilot-job-portal-laravel-script.zip">PLR Jobs Site Builder</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/gamers-arena-digital-online-game-store-game-top-up-voucher-gamer-id-selling-tools.zip">Gaming Store Builder</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/automatic-links.zip">AutoMated Links</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/billar-invoice-management-system.zip">Invoice Management System</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/charito-crowdfunding-platform.zip">Charity Software</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/sumowebtools-online-web-tools-script.zip">PLR Web Tools</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/webflix-movies-tv-series-live-tv-channels-subscription.zip">PLR Movies PlatForm</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/epubfan-online-epub-maker-composer-manager-lite.zip">ePubBoks Site Builder</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/affiliation-affiliate-link-sharing-platform.zip">Affiliate Link Sharing Platform</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>General Bonuses</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group plr_bonus_list">
                            <ul>
                                <li><a href="https://www.dropbox.com/s/f7u3kb9p0ngncrb/Theme-portal-marketplace-sell-digital-products-themes-plugins-scripts-multi-vendor.zip?dl=1">multi-vendor eCommerce marketplace</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/livepreview-responsive-digital-product-demo-bar.zip">Digital Product </a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/automatic-links.zip">AutoMated Links</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/zigaform-php-form-builder-contact-survey.zip">Form Builder - Contact & Survey</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/whatsapp-chat-universal-platform-wordpress-php-html-standalone-software.zip">WhatsApp Chat For WordPress </a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/halfdata-admin-panel.zip">Admin Panel</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/wordpress-plugin-maker-freelancer-version.zip">Development Tool</a></li>
                                <li><a href="https://s3.ap-south-1.amazonaws.com/softstrem.com/propanel-wordpress-theme-options-panel.zip">ProPanel </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       