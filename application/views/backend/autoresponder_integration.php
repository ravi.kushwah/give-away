<?//php echo"<pre>";print_r($autoresponderData);die;?>
<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active">AutoResponder Integration</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>AutoResponder Integration</h4>
                    </div>
                    <div class="card-body">
                        <div class="choose-product">
                            <ul>
                                <?php
                                            
                                $a_array = array( 
                                    array('GetResponse' ,'getresponse.png'),
                                    array('Mailchimp' ,'mailchimp.png'),
                                    array('ActiveCampaign' ,'activecampaign.png'),
                                    array('Sendiio' ,'sendiio.png'),
                                    array('Mailvio' ,'mailvio.png'),
                                    array('Sendlane' ,'sendlane.png'),
                                    array('SendFox' ,'sendfox.png'),
                                );
                                for($i=0; $i < count($a_array); $i++){
                                    echo '<li class="" data-name="'.$a_array[$i][0].'">
                                            <label for="'.strtolower($a_array[$i][0]).'" class="rdo">
                                                <div>
                                                    <img src="'.base_url('assets/backend/images/'.$a_array[$i][1]).'" alt="'.$a_array[$i][0].'">
                                                </div>
                                                <span>'.$a_array[$i][0].'</span>
                                                <input id="'.strtolower($a_array[$i][0]).'" type="checkbox" name="autoresponder[]"  value="'.$a_array[$i][0].'" '.(in_array($a_array[$i][0], $connect_autoresponder) ? 'checked' : '').'>
                                                <img src="'.base_url('assets/backend/images/check_icon.png').'" class="checkbox_img">
                                                <span class="checkmark"></span>
                                            </label>
                                        </li>';
                                        }
                                ?>
                                <li class=" CustomHTML"  data-name="CustomHTML">
                                   <a href="#CustomHTML" data-toggle="modal" data-target="#CustomHTML">
                                        <label for="customhtml" class="rdo">
                                            <div bis_skin_checked="1">
                                                <img src="<?=base_url()?>/assets/backend/images/customhtml.png" alt="CustomHTML">
                                            </div>
                                            <span>CustomHTML</span>
                                            <input id="customhtml" type="checkbox" name="" value="SendFox" <?=  empty($autoresponder) ? 'checked' : '' ?>>
                                            <img src="<?=base_url()?>/assets/backend/images/check_icon.png" class="checkbox_img">
                                            <span class="checkmark"></span>
                                        </label>
                                   </a>
                                </li>
                             
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>