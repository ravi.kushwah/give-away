<div class="ad-footer-btm">
            <p>Copyright <?= date('Y') ?> &copy; <?= SITENAME ?> All Rights Reserved.</p>
        </div>
    </div>
</div>
</div>
	<div class="modal fade" id="CustomHTML" tabindex="-1" aria-labelledby="CustomHTML" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content ap_modal_content">
                    <div class="modal-header ap_modal_header">
                        <h4 class="model_title">Connect Auto-Responder</h4>
                        <button type="button" class="close close_autoresponder" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="ap_label">You have to configure custom HTML form in manage section of the website</label>
                            <span class="acy_info"></span>
                        </div>
                        <div class="modal-footer">
                            <!--<button type="button" class="btn ad-btn autoResponder_connect" data-responder="GetResponse">Submit</button>-->
                        </div>
                    </div>
              </div>
            </div>
        </div>
 <!-- Model -->
  <div class="modal fade" id="integration_popup" tabindex="-1" role="dialog" aria-hidden="true">
  <!--<div id="integration_popup" class="modal fade ap_modal" tabindex="-1" role="dialog" aria-labelledby="form_modal" aria-hidden="true">-->
    <div class="modal-dialog modal-dialog-centered ap_modal_dialog ap_modal_dialog-centered" role="document">
        <div class="modal-content ap_modal_content">
            <!-- Modal header -->
            
            <!--<a href="javascript:void(0);" class="ap_close close close_autoresponder" data-dismiss="modal" aria-label="Close">-->
            <!--    <span aria-hidden="true"><img src="<?=base_url('assets/backend/images/close_icon.png');?>" alt="Close Icon"></span>-->
            <!--</a>-->
            <div class="modal-header ap_modal_header">
                <h4 class="model_title">Connect Auto-Responder</h4>
                <button type="button" class="close close_autoresponder" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- <span class="model_close close_autoresponder" data-dismiss="modal">×</span> -->
            </div>

            <div class="modal-body text-left">
                <form class="d-none" action="" id="GetResponse">
                <div class="acy_modelinner automation_model">
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">API Key</label>
                            <input type="text" value="" placeholder="Enter API Key" class="api_key form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ad-btn autoResponder_connect" data-responder="GetResponse">Submit</button>
                        <p class="tb_info"><a href="https://support.getresponse.com/faq/where-i-find-api-key" target="_blank">How do I find this information?</a></p>
                    </div>
                </div>
            </form> 
            <form class="d-none" action="" id="Mailchimp">
                <div class="acy_modelinner automation_model">
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">API Key</label>
                            <input type="text" value="" placeholder="Enter API Key" class="api_key form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ad-btn autoResponder_connect" data-responder="Mailchimp">Submit</button>
                        <p class="acy_info"><a href="http://admin.mailchimp.com/account/api" target="_blank">How do I find this
                            information?</a></p>
                    </div>                    
                </div>
            </form>
            <form class="d-none" action="" id="Sendlane">
                <div class="acy_modelinner automation_model">
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">Your API URL</label>
                            <input type="text" value="" placeholder="yourapi.sendlane.com" class="user_url form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">API Key</label>
                            <input type="text" value="" placeholder="Enter API Key" class="api_key form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">HASH Key</label>
                            <input type="text" value="" placeholder="Enter HASH Key" class="hash_key form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ad-btn autoResponder_connect" data-responder="Sendlane">Submit</button>
                        <p class="acy_info"><a href="https://help.sendlane.com/article/71-how-to-find-your-api-key-api-hash-key-and-subdomain" target="_blank">How do I find this information?</a></p>
                    </div> 
                </div>
            </form>
            <form class="d-none" action="" id="ActiveCampaign">
                <div class="acy_modelinner automation_model">
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">API URL</label>
                            <input type="text" value="" placeholder="Enter API URL" class="api_url form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">API Key</label>
                            <input type="text" value="" placeholder="Enter API Key" class="api_key form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ad-btn autoResponder_connect" data-responder="ActiveCampaign">Submit</button>
                        <p class="acy_info"><a href="http://www.activecampaign.com/help/using-the-api/" target="_blank">How do I find this information?</a></p>
                    </div>
                </div>
            </form>                
            <form class="d-none" action="" id="Sendiio">
                <div class="acy_modelinner automation_model">
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">API Token</label>
                            <input type="text" value="" placeholder="Enter API Token" class="api_token form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">Secret</label>
                            <input type="text" value="" placeholder="Enter Secret" class="api_secret form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ad-btn autoResponder_connect" data-responder="Sendiio">Submit</button>
                        <p class="acy_info"><a href="https://sendiio.com/auth/login" target="_blank">How do I find this information?</a></p>
                    </div>
                </div>
            </form>               
            <form class="d-none" action="" id="Mailvio">
                <div class="acy_modelinner automation_model">
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">API Key</label>
                
                            <input type="text" value="" placeholder="Enter API Key" class="api_key form-control ap_input">
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ad-btn autoResponder_connect" data-responder="Mailvio">Submit</button>
                        <p class="acy_info"><a href="http://help.meetvio.com/articles/48642-how-to-connect-mailvio" target="_blank">How do I find this information?</a></p>
                    </div>
                </div>                   
            </form>
            <form class="d-none" action="" id="SendFox">
                <div class="acy_modelinner automation_model">
                    <div class="model_filde_wrap">
                        <div class="form-group">
                            <label class="ap_label">Access Token</label>
                            <textarea placeholder="Enter Access Token" class="form-control tb_textarea access_token"></textarea>
                            <span class="acy_info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ad-btn autoResponder_connect" data-responder="SendFox">Submit</button>
                        <p class="tb_info" ><a href="https://sendfox.com/login" target="_blank">How do I find this information?</a></p>
                    </div>
                </div> 
            </form>
            </div>
        </div>
    </div>
</div>
  <!-- Model -->
  
  <!-- Confirm Popup Model -->
<div class="modal fade" id="confirmPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content plr_delete_modal">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <img src="<?= base_url() ?>assets/backend/images/question-mark.svg" alt="" class="img">
            <h4>Are you sure?</h4>
            <p>You want to disconnect this.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn squer-btn btn-secondary mt-2 mr-2" data-dismiss="modal">Cancel</button>
          <button type="button" id="confirmPopupBtn" class="btn btn-danger squer-btn mt-2 mr-2">Yes, Delete</button>
        </div>
      </div>
    </div>
</div>
      <!-- Confirm Model -->
  
	<!-- Delete Model -->
<div class="modal fade" id="deleteWebsite" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content plr_delete_modal">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <img src="<?= base_url() ?>assets/backend/images/delete.png" alt="" class="img">
            <h4>Are you sure, you want to delete this website?</h4>
            <p>You can't recover it back and we will be deleting all the products and analytics regarding this website.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn squer-btn btn-secondary mt-2 mr-2" data-dismiss="modal">Cancel</button>
          <button type="button" id="deleteWebsiteBtn" class="btn btn-danger squer-btn mt-2 mr-2">Yes, Delete</button>
        </div>
      </div>
    </div>
  </div>
  
    <div class="modal fade" id="socialicon" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="add_customer_title">Share To Social Site</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="traf_share_wrapper text-left">
                    <ul class="socialul" data-idtext="" id="4">
                        <li class="twitter">
                            <a href="javascript:;" data-url="https://twitter.com/intent/tweet?text=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/twitter.svg" alt="">
                                </span> 
                                twitter</a>
                        </li>
                        <li class="pinterest">
                            <a href="javascript:;" data-url="https://pinterest.com/pin/create/button/?description=COM-BLAST-QUES&amp;media=COM-BLAST-IMAGE&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/pinterest.svg" alt="">
                                </span> 
                                pinterest</a>
                        </li>
                        <li class="whatsapp">
                            <a href="javascript:;" data-url="https://web.whatsapp.com/send?text=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/whatsapp.svg" alt="">
                                </span> 
                                whatsapp</a>
                        </li>
                        <li class="linkedin">
                            <a href="javascript:;" data-url="https://www.linkedin.com/shareArticle?title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/linkedin.svg" alt="">
                                </span> 
                                linkedin                                    </a>
                        </li>
                        <li class="reddit">
                            <a href="javascript:;" data-url="https://reddit.com/submit?title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/reddit.svg" alt="">
                                </span> 
                                reddit</a>
                        </li>
                        <li class="tumblr">
                            <a href="javascript:;" data-url="https://www.tumblr.com/share?t=COM-BLAST-QUES&amp;u=COM-BLAST-URL&amp;v=3" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/tumblr.svg" alt="">
                                </span> 
                                tumblr</a>
                        </li>
                        <li class="buffer">
                            <a href="javascript:;" data-url="https://buffer.com/add?text=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/buffer.svg" alt="">
                                </span> 
                                buffer</a>
                        </li>
                        <li class="digg">
                            <a href="javascript:;" data-url="https://digg.com/submit?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/digg.svg" alt="">
                                </span> 
                                digg</a>
                        </li>
                        <li class="flipboard"> 
                            <a href="javascript:;" data-url="https://share.flipboard.com/bookmarklet/popout?ext=COM-BLAST-QUES&amp;title=COM-BLAST-QUES&amp;url=COM-BLAST-URL&amp;utm_campaign=widgets&amp;v=2" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/flipboard.svg" alt="">
                                </span> 
                                flipboard</a>
                        </li>
                        <li class="meneame">
                            <a href="javascript:;" data-url="https://meneame.net/submit.php?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/meneame.svg" alt="">
                                </span> 
                                meneame</a>
                        </li>
                        <li class="facebook">
                            <a href="javascript:;" data-url="https://www.facebook.com/sharer.php?t=COM-BLAST-QUES&amp;u=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/facebook.svg" alt="">
                                </span> 
                                facebook</a>
                        </li>
                        <li class="blogger">
                            <a href="javascript:;" data-url="https://www.blogger.com/blog-this.g?n=COM-BLAST-QUES&amp;t=COM-BLAST-ANS&amp;u=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/blogger.svg" alt="">
                                </span> 
                                blogger</a>
                        </li>
                        <li class="evernote">
                            <a href="javascript:;" data-url="https://www.evernote.com/clip.action?title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/evernote.svg" alt="">
                                </span> 
                                evernote</a>
                        </li>
                        <li class="instapaper">
                            <a href="javascript:;" data-url="http://www.instapaper.com/edit?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;description=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/instapaper.svg" alt="">
                                </span> 
                                instapaper</a>
                        </li>
                        <li class="pocket">
                            <a href="javascript:;" data-url="https://getpocket.com/edit?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/pocket.svg" alt="">
                                </span> 
                                pocket</a>
                        </li>
                        <li class="telegram">
                            <a href="javascript:;" data-url="https://t.me/share/url?url=COM-BLAST-URL&amp;text=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/telegram.svg" alt="">
                                </span> 
                                telegram</a>
                        </li>
                        <li class="wordpress">
                            <a href="javascript:;" data-url="https://wordpress.com/wp-admin/press-this.php?u=COM-BLAST-URL&amp;t=COM-BLAST-QUES&amp;s=COM-BLAST-ANS&amp;i=COM-BLAST-IMAGE" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/wordpress.svg" alt="">
                                </span> 
                                wordpress</a>
                        </li>
                        <li class="stumbleupon">
                            <a href="javascript:;" data-url="http://www.stumbleupon.com/submit?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/stumbleupon.svg" alt="">
                                </span> 
                                stumbleupon</a>
                        </li>
                        <li class="mix">
                            <a href="javascript:;" data-url="https://mix.com/mixit?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/mix.svg" alt="">
                                </span> 
                                mix</a>
                        </li>
                        <li class="bibsonomy">
                            <a href="javascript:;" data-url="https://www.bibsonomy.org/ShowBookmarkEntry?jump=yes&amp;url=COM-BLAST-URL&amp;description=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/bibsonomy.svg" alt="">
                                </span> 
                                bibsonomy</a>
                        </li>
                        <li class="care2">
                            <a href="javascript:;" data-url="https://www.care2.com/news/news_post.html?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;v=1.3" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/care2.svg" alt="">
                                </span> 
                                care2</a>
                        </li>
                        <li class="blogmarks">
                            <a href="javascript:;" data-url="http://blogmarks.net/my/new.php?mini=1&amp;simple=1&amp;title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/blogmarks.svg" alt="">
                                </span> 
                                blogmarks</a>
                        </li>
                        <li class="livejournal">
                            <a href="javascript:;" data-url="https://www.livejournal.com/update.bml?event=COM-BLAST-URL&amp;subject=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/livejournal.svg" alt="">
                                </span> 
                                livejournal</a>
                        </li>
                        <li class="folkd">
                            <a href="javascript:;" data-url="http://www.folkd.com/page/social-bookmarking.html?addurl=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/folkd.svg" alt="">
                                </span> 
                                folkd</a>
                        </li>
                        <li class="myspace">
                            <a href="javascript:;" data-url="https://myspace.com/Modules/PostTo/Pages/?u=COM-BLAST-URL&amp;t=COM-BLAST-QUES&amp;c=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/myspace.svg" alt="">
                                </span> 
                                myspace</a>
                        </li>
                        <li class="plurk">
                            <a href="javascript:;" data-url="https://www.plurk.com/m?qualifier=shares&amp;status=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/plurk.svg" alt="">
                                </span> 
                                plurk</a>
                        </li>
                        <li class="symbaloo">
                            <a href="javascript:;" data-url="https://www.symbaloo.com/go/add/?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/symbaloo.svg" alt="">
                                </span> 
                                symbaloo</a>
                        </li>
                        <li class="sitejot">
                            <a href="javascript:;" data-url="http://www.sitejot.com/loginform.php?iSiteAdd=COM-BLAST-URL&amp;iSiteDes=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/sitejot.svg" alt="">
                                </span> 
                                sitejot</a>
                        </li>
                        <li class="vk">
                            <a href="javascript:;" data-url="https://vk.com/share.php?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/vk.svg" alt="">
                                </span> 
                                vk</a>
                        </li>
                        <li class="wechat">
                            <a href="javascript:;" data-url="https://api.qrserver.com/v1/create-qr-code/?size=154x154&amp;data=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/wechat.svg" alt="">
                                </span> 
                                wechat</a>
                        </li>
                        <li class="yahoomail">
                            <a href="javascript:;" data-url="http://compose.mail.yahoo.com/?to=&amp;subject=COM-BLAST-QUES&amp;body=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/yahoomail.svg" alt="">
                                </span> 
                                yahoomail</a>
                        </li>
                        <li class="skype">
                            <a href="javascript:;" data-url="https://web.skype.com/share?url=COM-BLAST-URL&amp;text=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/skype.svg" alt="">
                                </span> 
                                skype</a>
                        </li>
                        <li class="surfingbird">
                            <a href="javascript:;" data-url="http://surfingbird.ru/share?url=COM-BLAST-URL&amp;description=COM-BLAST-ANS&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/surfingbird.svg" alt="">
                                </span> 
                                surfingbird</a>
                        </li>
                        <li class="renren">
                            <a href="javascript:;" data-url="http://widget.renren.com/dialog/share?resourceUrl=COM-BLAST-URL&amp;srcUrl=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;description=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/renren.svg" alt="">
                                </span> 
                                renren</a>
                        </li>
                        <li class="refind">
                            <a href="javascript:;" data-url="https://refind.com/?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/refind.svg" alt="">
                                </span> 
                                refind</a>
                        </li>
                        <li class="qzone">
                            <a href="javascript:;" data-url="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/qzone.svg" alt="">
                                </span> 
                                qzone</a>
                        </li>
                        <li class="line">
                            <a href="javascript:;" data-url="https://lineit.line.me/share/ui?url=COM-BLAST-URL&amp;text=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/line.svg" alt="">
                                </span> 
                                line</a>
                        </li>
                        <li class="hackernews">
                            <a href="javascript:;" data-url="https://news.ycombinator.com/submitlink?u=COM-BLAST-URL&amp;t=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/hackernews.svg" alt="">
                                </span> 
                                hackernews</a>
                        </li>
                        <li class="gmail">
                            <a href="javascript:;" data-url="https://mail.google.com/mail/u/0/?view=cm&amp;to&amp;su=COM-BLAST-QUES&amp;body=COM-BLAST-URL&amp;bcc&amp;cc&amp;fs=1&amp;tf=1" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/gmail.svg" alt="">
                                </span> 
                                gmail</a>
                        </li>
                        <li class="gBookmarks">
                            <a href="javascript:;" data-url="https://www.google.com/bookmarks/mark?op=edit&amp;bkmk=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;annotation=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/gBookmarks.svg" alt="">
                                </span> 
                                gBookmarks</a>
                        </li>
                        <li class="douban">
                            <a href="javascript:;" data-url="http://www.douban.com/recommend/?title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/douban.svg" alt="">
                                </span> 
                                douban</a>
                        </li>
                        <li class="diaspora">
                            <a href="javascript:;" data-url="https://share.diasporafoundation.org/?title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/diaspora.svg" alt="">
                                </span> 
                                diaspora</a>
                        </li>
                        <li class="mailru">
                            <a href="javascript:;" data-url="https://connect.mail.ru/share?share_url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/mailru.svg" alt="">
                                </span> 
                                mailru</a>
                        </li>
                        <li class="xing">
                            <a href="javascript:;" data-url="https://www.xing.com/app/user?op=share&amp;title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/xing.svg" alt="">
                                </span> 
                                xing</a>
                        </li>
                        <li class="odnoklassniki">
                            <a href="javascript:;" data-url="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&amp;st.shareUrl=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/odnoklassniki.svg" alt="">
                                </span> 
                                odnoklassniki</a>
                        </li>
                        <li class="weibo">
                            <a href="javascript:;" data-url="http://service.weibo.com/share/share.php?title=COM-BLAST-QUES&amp;url=COM-BLAST-URL&amp;pic=" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/weibo.svg" alt="">
                                </span> 
                                weibo</a>
                        </li>
                        <li class="slashdot">
                            <a href="javascript:;" data-url="https://slashdot.org/submission?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/slashdot.svg" alt="">
                                </span> 
                                slashdot</a>
                        </li>
                        <li class="yoolink">
                            <a href="javascript:;" data-url="http://auth.yoolink.to/authenticate/login?service=yoolink.to&amp;landing_path=/addorshare?url_value=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/yoolink.svg" alt="">
                                </span> 
                                yoolink</a>
                        </li>
                        <li class="yummly">
                            <a href="javascript:;" data-url="http://www.yummly.com/urb/verify?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;yumtype=button" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/yummly.svg" alt="">
                                </span> 
                                yummly </a>
                        </li>
                        <li class="viadeo">
                            <a href="javascript:;" data-url="https://www.viadeo.com/shareit/share/?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/viadeo.svg" alt="">
                                </span> 
                                viadeo</a>
                        </li>
                        <li class="bookmarksFr">
                            <a href="javascript:;" data-url="https://www.bookmarks.fr/Connexion/?action=add&amp;address=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/bookmarksFr.svg" alt="">
                                </span> 
                                bookmarksFr</a>
                        </li>
                        <li class="citeulike">
                            <a href="javascript:;" data-url="http://www.citeulike.org/posturl?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/citeulike.svg" alt="">
                                </span> 
                                citeulike</a>
                        </li>
                        <li class="box">
                            <a href="javascript:;" data-url="https://www.box.net/api/1.0/import?url=COM-BLAST-URL&amp;description=COM-BLAST-QUES&amp;import_as=link" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/box.svg" alt="">
                                </span> 
                                box</a>
                        </li>
                        <li class="bitty">
                            <a href="javascript:;" data-url="http://www.bitty.com/manual/?contenttype=&amp;contentvalue=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/bitty.svg" alt="">
                                </span> 
                                bitty</a>
                        </li>
                        <li class="known">
                            <a href="javascript:;" data-url="https://withknown.com/share/?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/known.svg" alt="">
                                </span> 
                                known</a>
                        </li>
                        <li class="kik">
                            <a href="javascript:;" data-url="https://www.kik.com/send/article/?app_name=Share&amp;text=COM-BLAST-ANS&amp;title=COM-BLAST-QUES&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/kik.svg" alt="">
                                </span> 
                                kik</a>
                        </li>
                        <li class="hatena">
                            <a href="javascript:;" data-url="https://b.hatena.ne.jp/entry/panel/?url=COM-BLAST-URL&amp;btitle=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/hatena.svg" alt="">
                                </span> 
                                hatena</a>
                        </li>
                        <li class="gClassroom">
                            <a href="javascript:;" data-url="https://classroom.google.com/u/0/share?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/gClassroom.svg" alt="">
                                </span> 
                                gClassroom</a>
                        </li>
                       <li class="fark">
                            <a href="javascript:;" data-url="https://www.fark.com/submit?new_url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/fark.svg" alt="">
                                </span> 
                                fark</a>
                        </li>
                        <li class="draugiem">
                            <a href="javascript:;" data-url="https://www.draugiem.lv/say/ext/add.php?link=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;login=0" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/draugiem.svg" alt="">
                                </span> 
                                draugiem</a>
                        </li>
                       <li class="diigo">
                            <a href="javascript:;" data-url="https://www.diigo.com/post?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;desc=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/diigo.svg" alt="">
                                </span> 
                                diigo</a>
                        </li>
                        <li class="kakao">
                            <a href="javascript:;" data-url="https://story.kakao.com/share?url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/kakao.svg" alt="">
                                </span> 
                                kakao</a>
                        </li>
                        <li class="netvouz">
                            <a href="javascript:;" data-url="https://netvouz.com/action/submitBookmark?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;popup=no&amp;description=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/netvouz.svg" alt="">
                                </span> 
                                netvouz</a>
                        </li>
                        <li class="outlook">
                            <a href="javascript:;" data-url="https://outlook.live.com/mail/deeplink/compose?path=%2Fmail%2Finbox&amp;subject=COM-BLAST-QUES&amp;body=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/outlook.svg" alt="">
                                </span> 
                                outlook</a>
                        </li>
                        <li class="mewe">
                            <a href="javascript:;" data-url="https://mewe.com/share?link=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/mewe.svg" alt="">
                                </span> 
                                mewe</a>
                        </li>
                        <li class="pinboard">
                            <a href="javascript:;" data-url="https://pinboard.in/popup_login/?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;later=&amp;description=COM-BLAST-ANS&amp;next=same" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/pinboard.svg" alt="">
                                </span> 
                                pinboard</a>
                        </li>
                        <li class="papaly">
                            <a href="javascript:;" data-url="https://papaly.com/api/share.html?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/papaly.svg" alt="">
                                </span> 
                                papaly</a>
                        </li>
                        <li class="rediff">
                            <a href="javascript:;" data-url="https://share.rediff.com/bookmark/addbookmark?bookmarkurl=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/rediff.svg" alt="">
                                </span> 
                                rediff</a>
                        </li>
                        <li class="protopage">
                            <a href="javascript:;" data-url="https://www.protopage.com/add-button-site?url=COM-BLAST-URL&amp;label=&amp;type=page" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/protopage.svg" alt="">
                                </span> 
                                protopage</a>
                        </li>
                        <li class="svejo">
                            <a href="javascript:;" data-url="https://svejo.net/story/submit_by_url?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES&amp;summary=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/svejo.svg" alt="">
                                </span> 
                                svejo </a>
                        </li>
                        <li class="tuenti">
                            <a href="javascript:;" data-url="https://www.tuenti.com/share?p=b5dd6602&amp;url=COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/tuenti.svg" alt="">
                                </span> 
                                tuenti</a>
                        </li>
                        <li class="typepad">
                            <a href="javascript:;" data-url="https://www.typepad.com/secure/services/signin?to=/services/quickpost/post?v=2&amp;qp_show=ac&amp;qp_href=COM-BLAST-URL&amp;qp_title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/typepad.svg" alt="">
                                </span> 
                                typepad  </a>
                        </li>
                        <li class="trello">
                            <a href="javascript:;" data-url="https://trello.com/add-card?mode=popup&amp;url=COM-BLAST-URL&amp;name=COM-BLAST-QUES&amp;desc=COM-BLAST-ANS" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/trello.svg" alt="">
                                </span> 
                                trello </a>
                        </li>
                        <li class="stocktwits">
                            <a href="javascript:;" data-url="https://api.stocktwits.com/widgets/share?body=COM-BLAST-QUES%20COM-BLAST-ANS%20COM-BLAST-URL" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/stocktwits.svg" alt="">
                                </span> 
                                stocktwits</a>
                        </li>
                        <li class="wanelo">
                            <a href="javascript:;" data-url="https://wanelo.co/p/save?url=COM-BLAST-URL&amp;title=COM-BLAST-QUES" onclick="sharethelink(this)">
                                <span class="traf_shareIcon">
                                    <img src="<?= base_url() ?>assets/backend/images/social/wanelo.svg" alt="">
                                </span> 
                                wanelo</a>
                        </li>
                        
                    </ul>
                </div>
                <!--<div class="modal-footer">-->
                <!--<input type="hidden" value="" id="c_id">-->
                <!--  <button type="button" class="btn ad-btn" id="submitCustomerData">Submit</button>-->
                <!--</div>-->
            </div>
        </div>
    </div>
      <!-- Delete Model -->

    <?php if(isset($customers)) { ?>
    <div class="modal fade" id="add_customer" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="add_customer_title">Add New Categories</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Name</label>
                    <input type="text" placeholder="Enter Name" class="form-control custAddCls" id="c_name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Email</label>
                    <input type="text" placeholder="Enter Email" class="form-control custAddCls" id="c_email">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Password</label>
                    <input type="text" placeholder="Enter Password" class="form-control custAddCls" id="c_password">
                    <p>Don't want to change, leave it empty.</p>
                </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" value="" id="c_id">
              <button type="button" class="btn ad-btn" id="submitCustomerData">Submit</button>
            </div>
          </div>
        </div>
    </div>
    <?php } ?>


    <?php if(isset($agency_users)) { ?>
    <div class="modal fade" id="add_agencyuser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="add_agencyuser_title">Add New Categories</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Name</label>
                    <input type="text" placeholder="Enter Name" class="form-control agencyuserAddCls" id="u_name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Email</label>
                    <input type="text" placeholder="Enter Email" class="form-control agencyuserAddCls" id="u_email">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Password</label>
                    <input type="text" placeholder="Enter Password" class="form-control agencyuserAddCls" id="u_password">
                    <p>Don't want to change, leave it empty.</p>
                </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" value="" id="u_id">
              <button type="button" class="btn ad-btn" id="submitAgencyUserData">Submit</button>
            </div>
          </div>
        </div>
    </div>
    <?php } ?>


    <?php if(isset($custom_products)) { ?>
    <div class="modal fade" id="add_custom_products" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="add_products_title">Add New Give Aways </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">General GiveAways Name</label>
                    <input type="text" placeholder="Enter General GiveAways Name" class="form-control productsAddCls" id="g_offer_name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">General GiveAways URL Name</label>
                    <input type="text" placeholder="Enter General GiveAways URL Name" class="form-control productsAddCls" id="slug">
                    <p>Don't add space or special characters, instead of space use dash(-)</p>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Payout ( Offer Prize )</label>
                    <input type="number" placeholder="Enter Payout ( Offer Prize )" class="form-control productsAddCls" id="g_payout">
                </div>
               <div class="form-group">
                    <label class="col-form-label">Image Link</label>
                    <input type="text" placeholder="Enter Image Link" class="form-control productsAddCls" id="g_priview">
                    <p>Use full URL for the Giveaways Priview image, using https:// or http://</p>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Categories</label>
                    <select class="form-control" id="g_categories" >
                        <?php if(!empty($web_categories)){
                            foreach($web_categories as $soloCate){
                                echo '<option value="'.$soloCate['cate_id'].'">'.$soloCate['cate_name'].'</option>';
                            }
                        } else {
                            echo '<option value="0">No Category Found</option>';
                        }   ?>
                    </select>
                </div>
                 <div class="form-group">
                    <label class="col-form-label">Country Name </label>
                    <input type="text" placeholder="Enter Country" class="form-control productsAddCls" id="g_countries">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Redirect Offer URL</label>
                    <input type="text" placeholder="Enter Redirect Offer UR" class="form-control productsAddCls" id="g_offer_url">
                    <p>Use full URL for the Offer , using https:// or http://</p>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Redirect SignUp URL </label>
                    <input type="text" placeholder="Enter Redirect SignUp URL" class="form-control productsAddCls" id="g_sign_up_url">
                    <p>Use full URL for the SignUp , using https:// or http://</p>
                </div>
            </div>
            <div class="modal-footer">
             <input type="hidden" value="<?php echo $this->uri->segment(3);?>" id="webid">
              <button type="button" class="btn ad-btn  submitProductsData" p_id="0" id="p_id">Submit</button>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="deleteCustomProduct" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content plr_delete_modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <img src="<?= base_url() ?>assets/backend/images/delete.png" alt="" class="img">
                <h4>Are you sure, you want to delete this product?</h4>
                <p>You can't recover it back and we will be deleting all the download records associated with this product.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn squer-btn btn-secondary mt-2 mr-2" data-dismiss="modal">Cancel</button>
                <button type="button" id="deleteCustomProductBtn" class="btn btn-danger squer-btn mt-2 mr-2">Yes, Delete</button>
            </div>
        </div>
        </div>
    </div>
    <?php } ?>


    <?php if(isset($categories)) { ?>
    <div class="modal fade" id="add_categories" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="add_categories_title">Add New Categories</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Category Name</label>
                    <input type="text" placeholder="Category Name" class="form-control cateAddCls" id="cate_name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Category URL</label>
                    <input type="text" placeholder="Category URL" class="form-control cateAddCls" id="cate_slug">
                    <p>Don't add space or special characters, instead of space use dash(-)</p>
                </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" value="" id="cate_id">
              <button type="button" class="btn ad-btn" id="submitCateData">Submit</button>
            </div>
          </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteCategory" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content plr_delete_modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <img src="<?= base_url() ?>assets/backend/images/delete.png" alt="" class="img">
                <h4>Are you sure, you want to delete this category?</h4>
                <p>You can't recover it back and we will be deleting all the products associated with this category.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn squer-btn btn-secondary mt-2 mr-2" data-dismiss="modal">Cancel</button>
                <button type="button" id="deleteCategoryBtn" class="btn btn-danger squer-btn mt-2 mr-2">Yes, Delete</button>
            </div>
        </div>
        </div>
    </div>
    

      
    
<?php } ?>

      <!--===Notification model===-->
		<div class="plr_notification">
			<div class="plr_close_icon">
				<span class='close'>×</span>
			</div>
			<div class="plr_success_flex">
				<div class="plr_happy_img">
					<img src=""/>
				</div>
				<div class="plr_yeah">
					<h5></h5>
					<p></p>
				</div>
			</div>
		</div>
		<!--===Notification model===-->
    <!-- Script Start -->
    <script>window.baseurl = "<?= base_url() ?>"</script>
	<script src="<?= base_url() ?>assets/common/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/datatables.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/dataTables.responsive.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/spectrum.min.js"></script>
	<!-- Page Specific -->
<?php if($this->uri->segment(2) == 'autoresponder_integration'){?>
    <script src="<?= base_url() ?>assets/backend/js/ar_settings.js?<?= date('his') ?>"></script>
<?php }?>
    <!-- Custom Script -->
    <script src="<?= base_url() ?>assets/backend/js/custom.js?<?= date('his') ?>"></script>
</body>

</html>