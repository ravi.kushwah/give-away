<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="col xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active">Profile</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Products view Start -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Profile Settings</h4>
                    </div>
                    <div class="card-body">
                        <div class="plr_profile_title">   
                            <div class="plr_profileimg">
                            <?= $this->session->userdata['initials'] ?>
                            </div>
                            <div class="media-body">
                                <h5 class="mb-1"><?= $this->session->userdata['firstname'] ?></h5>
                                <p><?= $this->session->userdata['email'] ?></p>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Name</label>
                                        <input type="text" class="form-control" placeholder="Enter name" value="<?= $userList[0]['u_name'] ?>" id="u_name">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label class="form-label">New Password</label>
                                        <input type="password" class="form-control" placeholder="Enter password" id="pwd">
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="form-footer">
                                        <?php if($this->session->userdata['id'] == 4) { ?>
                                        <button class="btn btn-primary squer-btn">Disabled for JV Access</button>
                                        <?php } else { ?>
                                        <button class="btn btn-primary squer-btn" onclick="saveProfile()">Save Details</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>