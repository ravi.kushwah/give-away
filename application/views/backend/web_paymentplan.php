<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card" id="planHTML">
                    <div class="card-header">
                        <h4><?= $pageName ?>  <?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?></h4>
                        <div class="plr_genrate_btn">
                            <a href="javascript:;" class="ad-btn" onclick="addNewPlan()">Add New Plan</a>
                        </div>
                    </div>
                    <?php 
                    if( $webDetail[0]['w_plansettings'] == '' ) { ?>
                    
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Plan Name</label>
                                    <input class="form-control planSettings_1" type="text" placeholder="Plan Name" id="planname_1">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Plan Amount ( in USD )</label>
                                    <input class="form-control planSettings_1" type="text" placeholder="Plan Amount" id="amount_1">
                                    <p>If you want to give free access, put 0 as the amount.</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Duration</label>
                                    <select class="form-control planSettings_1" id="duration_1">
                                        <option value="One Time">One Time</option>
                                        <option value="Month">Month</option>
                                        <option value="Year">Year</option>
                                    <select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Number of Downloads</label>
                                    <input class="form-control planSettings_1" type="text" placeholder="Number of Downloads" id="numberofdownloads_1" value="">
                                    <p>If you want to give unlimited , put 0 as the count.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } else { 
                        $planArr = json_decode($webDetail[0]['w_plansettings'],true);
                        foreach($planArr as $key=>$soloPlan) {
                        $planData = json_decode($soloPlan,true);
                        $counter = explode('_',$key)[1];
                    ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Plan Name</label>
                                    <input class="form-control planSettings_<?= $counter ?>" type="text" placeholder="Plan Name" id="planname_<?= $counter ?>" value="<?= $planData['planname_'.$counter]?>">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Plan Amount ( in USD )</label>
                                    <input class="form-control planSettings_<?= $counter ?>" type="text" placeholder="Plan Amount" id="amount_<?= $counter ?>" value="<?= $planData['amount_'.$counter]?>">
                                    <p>If you want to give free access, put 0 as the amount.</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Duration</label>
                                    <select class="form-control planSettings_<?= $counter ?>" id="duration_<?= $counter ?>">
                                        <option value="One Time" <?= $planData['duration_'.$counter] == 'One Time' ? 'selected' : '' ?>>One Time</option>
                                        <option value="Month" <?= $planData['duration_'.$counter] == 'Month' ? 'selected' : '' ?>>Month</option>
                                        <option value="Year" <?= $planData['duration_'.$counter] == 'Year' ? 'selected' : '' ?>>Year</option>
                                    <select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group plr_require">
                                    <label class="col-form-label">Number of Downloads</label>
                                    <input class="form-control planSettings_<?= $counter ?>" type="text" placeholder="Number of Downloads" id="numberofdownloads_<?= $counter ?>" value="<?= $planData['numberofdownloads_'.$counter]?>">
                                    <p>If you want to give unlimited , put 0 as the count.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } } ?>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="plr_genrate_btn">
                    <input type="hidden" value="<?= !empty($webDetail) ? $webDetail[0]['w_id'] : '' ?>" id="w_id">
                    <input type="hidden" value="<?= $webDetail[0]['w_plansettings'] == '' ? 1 : count($planArr) ?>" id="planCounter">
                    <a href="javascript:;" class="ad-btn" onclick="savePlanSettings()">SAVE</a>
                </div>
            </div>
        </div>


        <div id="planHTML_div" style="display:none;">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group plr_require">
                            <label class="col-form-label">Plan Name</label>
                            <input class="form-control planSettings_ID" type="text" placeholder="Plan Name" id="planname_ID">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group plr_require">
                            <label class="col-form-label">Plan Amount ( in USD )</label>
                            <input class="form-control planSettings_ID" type="text" placeholder="Plan Amount" id="amount_ID">
                            <p>If you want to give free access, put 0 as the amount.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group plr_require">
                            <label class="col-form-label">Duration</label>
                            <select class="form-control planSettings_ID" id="duration_ID">
                                <option value="One Time">One Time</option>
                                <option value="Month">Month</option>
                                <option value="Year">Year</option>
                            <select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group plr_require">
                            <label class="col-form-label">Number of Downloads</label>
                            <input class="form-control planSettings_ID" type="text" placeholder="Number of Downloads" id="numberofdownloads_ID" value="">
                            <p>If you want to give unlimited , put 0 as the count.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>