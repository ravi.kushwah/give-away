<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <!--<div class="row">-->
        <!--    <div class="col-xl-12">-->
        <!--        <div class="card">-->
        <!--            <div class="card-header">-->
        <!--                <h4> Give Away Sites  Wordpress Plugin </h4><br>-->
        <!--                <h5>Please download the Wordpress plugin from here   </h5><br>-->
        <!--                <a href="https://www.dropbox.com/s/5ykntjvvggzh2ys/giveawaysite.zip?dl=1" class="ad-btn">Download</a>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Give Away Sites  Wordpress Plugin</h4>
                    </div>
                    <div class="card-body">
                        <h5>Please download the Wordpress plugin from here</h5><br>
                        <a href="https://plrsitebuilder-products.s3.amazonaws.com/giveawaysite.zip" class="ad-btn">Download</a>
                    </div>
                </div>
            </div>
        </div>