<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4> <?= $pageName ?></h4>
                       <!--<p style="color: red;font-weight: bold;">We are only showing 500 coupons out of total coupons.</p>-->
                    </div>
                  
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Website Name</th>
                                         <th>Giveaway Link</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($leads)){
                                        $sno=0;
                                        foreach($leads as $soloProd) {
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                             echo '<td>'.$soloProd['l_name'].'</td>';
                                              echo '<td>'.$soloProd['l_email'].'</td>';
                                             echo '<td><a href="'.base_url().'ga/'.$soloProd['w_siteurl'].'"target="_blank">'.$soloProd['w_siteurl'].'</a></td>';
                                              echo '<td><a href="'.$soloProd['giveaway_link'].'" target="_blank">'.$soloProd['giveaway_name'].'</a></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>