<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4>Pre-Loaded <?= $pageName ?>  
                        <!--<?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?>-->
                        </h4>
                         <a href="<?= base_url() ?>home/tutorials" class="ad-btn">How to earn</a>
                    </div>
                    <input type="hidden" value="<?= $webDetail[0]['w_id']?>" id="webid">
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Network</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($products)){
                                        $sno=0;
                                        foreach($products as $soloProd) {
                                            $cid= $soloProd['g_categories'];
                                            $catData = $this->DBfile->get_data('categories' , array('cate_id'=>$cid));
                                            $u = $siteUrl.'/'.$soloProd['slug'];
                                            $d = base_url().'home/download_product/'.$soloProd['g_offer_name'];
					                        $cateName = (isset($catData[0]['cate_name']) && !empty($catData[0]['cate_name'])? $catData[0]['cate_name'] : '');
					                       if(strpos($soloProd['g_network'], "https://") !== false || strpos($soloProd['g_network'], "http://") !== false){
					                            $network = '<a href="'.$soloProd['g_network'].'"target="_blank">Grab Your Affiliate Link</a>';
					                        }else{
					                            $network = '<a href="https://www.google.com/search?q='.trim(str_replace(' ','+',$soloProd['g_network'])).'" target="_blank">Sign Up on '.trim($soloProd['g_network']).'</a>';
					                        }
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.mb_strimwidth($soloProd['g_offer_name'], 0, 30, "...").'</td>';
                                            echo '<td>'.$cateName.'</td>';
                                            echo '<td>'.$network.'</td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                    <!--<a href="'.$d.'"><i class="fa fa-download"></i><div class="plr_tooltip_show"><p>Download</p></div></a>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>