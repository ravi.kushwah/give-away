<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <form class="separate-form" id="setupUploadForm">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Basic Settings  <?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?></h4>
                    </div>
                    <div class="card-body">
                            <div class="row">
                                
                                <div class="col-xl-12">
                                    <div class="form-group row">
                                        <label class="col-md-3">Tell us, on which GiveAway you want to create your site</label>
                                        <div class="col-md-9 plr_check_lable_box">
                                            <div class="checkbox">
                                                <input type="radio"  id="rememberme" class="w_type" name="w_type" value="5" <?php  echo (!empty($webDetail[0]['w_type']) && $webDetail[0]['w_type']=='5') ? 'checked':'';?>>
                                                <label for="rememberme">Games GiveAways</label>
                                            </div>
                                            <div class="checkbox">
                                                <input type="radio" id="rememberme2" class="w_type" name="w_type" value="1"<?php echo (!empty($webDetail[0]['w_type']) && $webDetail[0]['w_type']=='1')?'checked':'';?>>
                                                <label for="rememberme2">General GiveAways</label>
                                            </div>
                                             <div class="checkbox">
                                                <input type="radio" id="rememberme3" class="w_type" name="w_type" value="2"<?php echo (!empty($webDetail[0]['w_type']) && $webDetail[0]['w_type']=='2')?'checked':'';?>>
                                                <label for="rememberme3">GiveAway Videos</label>
                                            </div>
                                             <div class="checkbox">
                                                <input type="radio" id="rememberme4" class="w_type" name="w_type" value="3"<?php echo (!empty($webDetail[0]['w_type']) && $webDetail[0]['w_type']=='3')?'checked':'';?>>
                                                <label for="rememberme4">Contests</label>
                                            </div>
                                            <div class="checkbox">
                                                <input type="radio" id="rememberme5" class="w_type" name="w_type" value="4"<?php echo (!empty($webDetail[0]['w_type']) && $webDetail[0]['w_type']=='4')?'checked':'';?>>
                                                <label for="rememberme5">Gift Cards</label>
                                            </div>
                                            <div class="checkbox">
                                                <input type="radio" id="rememberme6" class="w_type" name="w_type" value="6"<?php echo (!empty($webDetail[0]['w_type']) && $webDetail[0]['w_type']=='6')?'checked':'';?>>
                                                <label for="rememberme6">Coupons</label>
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="form-group row plr_require">
                                        <label class="col-md-12">URL</label>
                                        <label class="col-md-3 spacing_manage"><?= base_url() ?>ga/</label>
                                        <div class="col-md-9 pl-0 plr_site_urls">
                                            <input class="form-control" type="text" placeholder="Enter URL" name="w_siteurl" value="<?= !empty($webDetail) ? $webDetail[0]['w_siteurl'] : '' ?>">
                                            <span class="check_site_url">Check</span>
                                            <span class="plr_url_text">
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <?php $userDetail =  $this->DBfile->get_data('usertbl',array('u_id'=>$this->session->userdata['id']));
                                if( $userDetail[0]['is_oto4'] == 1 ) { ?>
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Choose a Niche / Category to create your Niche Based Website</label>
                                        <select class="select2 form-control" name="w_cateid">
                                            <option value="0">All</option>
                                            <?php foreach($cateDetail as $soloCate) { 
                                                echo '<option value="'.$soloCate['cate_id'].'">'.$soloCate['cate_name'].'</option>';
                                            } ?>
                                        </select>
                                        <p>Note : Only applicable for General GiveAways.</p>
                                    </div>
                                </div>
                                <?php }
                                ?>

                                <div class="col-lg-12">
                                    <div class="form-group plr_require">
                                        <label class="col-form-label">Title</label>
                                        <input class="form-control" type="text" placeholder="Enter Title" name="w_title" value="<?= !empty($webDetail) ? $webDetail[0]['w_title'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Keywords</label>
                                        <input class="form-control" type="text" placeholder="Enter keywords" name="w_keywords" value="<?= !empty($webDetail) ? $webDetail[0]['w_keywords'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Description</label>
                                        <textarea class="form-control" placeholder="Enter your Description" name="w_description"><?= !empty($webDetail) ? $webDetail[0]['w_description'] : '' ?></textarea>
                                    </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Monetize</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label class="col-form-label">Enter your affiliate link where you want to send traffic</label>
                                    <input class="form-control" type="text" placeholder="Enter your affiliate" name="while_claiming_the_offer" value="<?= !empty($webDetail) ? $webDetail[0]['while_claiming_the_offer'] : '' ?>">
                                    <p>Note : If this remains empty then Enter GiveAway will show popup to grab email leads.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Change Logo & Favicon</h4>
                    </div>
                    <div class="card-body">
                            <div class="row">
                                <div class="col-xl-6 col-lg-12">
                                    <div class="plr_upload_wrapper">
                                        <label class="col-form-label">Upload Logo</label>
                                        <div class="plr_upload_box">
                                            <div class="plr_upload_icon">
                                                <input class="form-control" type="file" name="w_logourl">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="59" height="50" viewBox="0 0 59 50">
                                                    <path d="M47.177,41.803 L45.403,41.802 C44.557,41.802 43.868,41.120 43.868,40.281 C43.868,39.875 44.028,39.493 44.318,39.206 C44.608,38.919 44.993,38.761 45.403,38.761 L47.177,38.761 C49.537,38.761 51.750,37.842 53.410,36.172 C55.070,34.504 55.964,32.297 55.928,29.960 C55.855,25.254 51.876,21.427 47.057,21.427 L44.577,21.427 L44.577,18.293 C44.577,9.921 37.766,3.079 29.393,3.041 L29.322,3.041 C21.472,3.041 14.948,8.865 14.148,16.587 L14.018,17.847 L12.743,17.948 C7.320,18.377 3.071,22.941 3.071,28.337 C3.071,34.077 7.786,38.753 13.582,38.761 L13.713,38.761 C14.561,38.761 15.250,39.443 15.250,40.281 C15.250,41.120 14.561,41.802 13.715,41.802 L13.598,41.803 C6.099,41.803 -0.000,35.762 -0.000,28.337 C-0.000,24.931 1.288,21.679 3.625,19.183 C5.537,17.142 7.991,15.756 10.722,15.174 L11.253,15.061 L11.361,14.534 C12.138,10.729 14.091,7.332 17.007,4.709 C20.384,1.672 24.758,-0.000 29.323,-0.000 C39.157,-0.000 47.375,7.918 47.642,17.651 L47.662,18.373 L48.387,18.446 C54.437,19.057 59.000,24.064 59.000,30.094 C59.000,36.550 53.696,41.803 47.177,41.803 ZM41.151,31.226 L40.696,31.440 C39.207,32.141 37.425,31.837 36.261,30.684 L31.095,25.568 L31.095,48.479 C31.095,49.318 30.406,50.000 29.559,50.000 C28.712,50.000 28.024,49.318 28.024,48.479 L28.024,25.568 L22.858,30.684 C22.121,31.413 21.142,31.815 20.100,31.815 C19.510,31.815 18.945,31.689 18.420,31.441 L17.967,31.226 L29.559,19.746 L41.151,31.226 Z" class="cls-1"></path>
                                                </svg>
                                            </div>
                                            <div class="plr_upload_detail">
                                                <h6>Supports: JPG, JPEG, PNG</h6>
                                                <p>Recommended Size - 167x60 px</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12">
                                    <div class="plr_upload_wrapper">
                                        <label class="col-form-label">Logo Preview</label>
                                        <div class="plr_upload_box plr_upload_center">
                                            <span class="plr_close_btn DeleteSiteLogo" type="logo" data-id="<?= isset($webDetail[0]['w_id'])?$webDetail[0]['w_id']:''; ?>">×</span>
                                            <?php if(!empty($webDetail) && $webDetail[0]['w_logourl'] != '') { ?>
                                                <img src="<?= base_url() ?>assets/webupload/<?= $webDetail[0]['w_logourl'] ?>" class="RemoveLogo"alt="Logo Preview">
                                            <?php } else { ?>
                                                <img src="<?= base_url() ?>assets/backend/images/logo.png" class="RemoveLogo" alt="Default Logo">
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="plr_or">
                                        <h4>Or</h4>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Logos Text</label>
                                        <input class="form-control" type="text" placeholder="Enter logo text" name="w_logotext" value="<?= !empty($webDetail) ? $webDetail[0]['w_logotext'] : '' ?>">
                                    </div>
                                </div>                                
                                <div class="col-xl-6 col-lg-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Choose Font</label>
                                        <select class="select2 form-control" name="w_logofont">
                                            <option value="Lato" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Lato' ? 'selected' : '' : '' ?>>Lato</option>
                                            <option value="Lobster" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Lobster' ? 'selected' : '' : '' ?>>Lobster</option>
                                            <option value="Montserrat" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Montserrat' ? 'selected' : '' : '' ?>>Montserrat</option>
                                            <option value="Oswald" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Oswald' ? 'selected' : '' : '' ?>>Oswald</option>
                                            <option value="Pacifico" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Pacifico' ? 'selected' : '' : '' ?>>Pacifico</option>
                                            <option value="Raleway" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Raleway' ? 'selected' : '' : '' ?>>Raleway</option>
                                            <option value="Roboto" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Roboto' ? 'selected' : '' : '' ?>>Roboto</option>
                                            <option value="Maven Pro" <?= !empty($webDetail) ? $webDetail[0]['w_logofont'] == 'Maven Pro' ? 'selected' : '' : '' ?>>Maven Pro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Choose Color</label>
                                        <input type="text" value="<?= !empty($webDetail) ? $webDetail[0]['w_logocolor'] : '#5886ff' ?>" name="w_logocolor" id='colorpicker'/>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12">
                                    <div class="plr_upload_wrapper">
                                        <label class="col-form-label">Upload Favicon</label>
                                        <div class="plr_upload_box">
                                            <div class="plr_upload_icon">
                                                <input class="form-control" type="file" name="w_faviconurl">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="59" height="50" viewBox="0 0 59 50">
                                                    <path d="M47.177,41.803 L45.403,41.802 C44.557,41.802 43.868,41.120 43.868,40.281 C43.868,39.875 44.028,39.493 44.318,39.206 C44.608,38.919 44.993,38.761 45.403,38.761 L47.177,38.761 C49.537,38.761 51.750,37.842 53.410,36.172 C55.070,34.504 55.964,32.297 55.928,29.960 C55.855,25.254 51.876,21.427 47.057,21.427 L44.577,21.427 L44.577,18.293 C44.577,9.921 37.766,3.079 29.393,3.041 L29.322,3.041 C21.472,3.041 14.948,8.865 14.148,16.587 L14.018,17.847 L12.743,17.948 C7.320,18.377 3.071,22.941 3.071,28.337 C3.071,34.077 7.786,38.753 13.582,38.761 L13.713,38.761 C14.561,38.761 15.250,39.443 15.250,40.281 C15.250,41.120 14.561,41.802 13.715,41.802 L13.598,41.803 C6.099,41.803 -0.000,35.762 -0.000,28.337 C-0.000,24.931 1.288,21.679 3.625,19.183 C5.537,17.142 7.991,15.756 10.722,15.174 L11.253,15.061 L11.361,14.534 C12.138,10.729 14.091,7.332 17.007,4.709 C20.384,1.672 24.758,-0.000 29.323,-0.000 C39.157,-0.000 47.375,7.918 47.642,17.651 L47.662,18.373 L48.387,18.446 C54.437,19.057 59.000,24.064 59.000,30.094 C59.000,36.550 53.696,41.803 47.177,41.803 ZM41.151,31.226 L40.696,31.440 C39.207,32.141 37.425,31.837 36.261,30.684 L31.095,25.568 L31.095,48.479 C31.095,49.318 30.406,50.000 29.559,50.000 C28.712,50.000 28.024,49.318 28.024,48.479 L28.024,25.568 L22.858,30.684 C22.121,31.413 21.142,31.815 20.100,31.815 C19.510,31.815 18.945,31.689 18.420,31.441 L17.967,31.226 L29.559,19.746 L41.151,31.226 Z" class="cls-1"></path>
                                                </svg>
                                            </div>
                                            <div class="plr_upload_detail">
                                                <h6>Supports: JPG, JPEG, PNG</h6>
                                                <p>Recommended Size - 60x60 px</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12">
                                    <div class="plr_upload_wrapper">
                                        <label class="col-form-label">Favicon Preview</label>
                                        <div class="plr_upload_box plr_upload_fav">
                                             <span class="plr_close_btn DeleteSiteLogo" type="favicon" data-id="<?= isset($webDetail[0]['w_id'])?$webDetail[0]['w_id']:''; ?>">×</span>
                                            <?php if(!empty($webDetail) && $webDetail[0]['w_faviconurl'] != '') { ?>
                                                <img src="<?= base_url() ?>assets/webupload/<?= $webDetail[0]['w_faviconurl'] ?>" class="RemovefavIcon" alt="Favicon Preview">
                                            <?php } else { ?>
                                                <img src="<?= base_url() ?>assets/backend/images/favicon.png" class="RemovefavIcon" alt="Default Favicon">
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Change Theme Color</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="cl_sidebar_input">
                                    <div class="cl_chart_content">
                                        <div class="ed_gradient_list linear_gradiant_color ">
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#28284d" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#28284d' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #28284d;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#4da1f7" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#4da1f7' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #4da1f7;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#ff7640" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#ff7640' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #ff7640;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#f03063" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#f03063' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #f03063;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#ca6419" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#ca6419' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #ca6419;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#9474c9" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#9474c9' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #9474c9;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#0dc6b9" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#0dc6b9' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #0dc6b9;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#73c5e6" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#73c5e6' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #73c5e6;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#edba56" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#edba56' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #edba56;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#f55a51" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#f55a51' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #f55a51;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#0072ff" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#0072ff' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #0072ff;"></div>
                                            </label>
                                            <label for="" class="layout_label">
                                                <input type="radio" class="select_box_radio" name="w_themecolor" value="#86bd47" <?= !empty($webDetail) ? $webDetail[0]['w_themecolor']== '#86bd47' ? 'checked' : '' : '' ?>>
                                                <span class="ao_selected_radio"></span>
                                                <div class="ed_grad_color" style="background: #86bd47;"></div>
                                            </label>                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Sidebar Social Setting</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-form-label"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</label>
                                    <input class="form-control" type="text" placeholder="Enter Facebook Link" name="w_fblink" value="<?= !empty($webDetail) ? $webDetail[0]['w_fblink'] : '' ?>">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-form-label"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</label>
                                    <input class="form-control" type="text" placeholder="Enter Twitter Link" name="w_twitterlink" value="<?= !empty($webDetail) ? $webDetail[0]['w_twitterlink'] : '' ?>">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-form-label"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</label>
                                    <input class="form-control" type="text" placeholder="Enter Instagram Link" name="w_instalink" value="<?= !empty($webDetail) ? $webDetail[0]['w_instalink'] : '' ?>">
                                </div>
                            </div>
                           
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-form-label"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin</label>
                                    <input class="form-control" type="text" placeholder="Enter Linkedin Link" name="w_linkedinlink" value="<?= !empty($webDetail) ? $webDetail[0]['w_linkedinlink'] : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Footer Setting</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="form-group">
                                    <label class="col-form-label">Footer Text </label>
                                    <textarea class="form-control" placeholder="Enter Footer Text" name="w_footertext"><?= !empty($webDetail) ? $webDetail[0]['w_footertext'] : '' ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="plr_genrate_btn">
                    <input type="hidden" value="<?= !empty($webDetail) ? $webDetail[0]['w_id'] : '' ?>" name="w_id">
                    <a href="javascript:;" class="ad-btn" onclick="saveBasicSiteOption()">Generate</a>
                </div>
            </div>
        </div>
        </form>