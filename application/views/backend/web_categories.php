<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4><?= $pageName ?>  <?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?></h4>
                        
                        <?php $userDetail =  $this->DBfile->get_data('usertbl',array('u_id'=>$this->session->userdata['id']));
                        if( $userDetail[0]['is_oto2'] == 1 ) { ?>
                        <a class="ad-btn cateEdit" id="0">Add New</a>
                        <?php } ?>
                    </div>
                    <input type="hidden" value="<?= $webDetail[0]['w_id']?>" id="webid">
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Products</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($categories)){
                                        $sno=0;
                                        $w_blockcategory = $webDetail[0]['w_blockcategory'] == '' ? array() : explode(',',$webDetail[0]['w_blockcategory']);

                                        foreach($categories as $soloCate) {
                                            $prodCount = $this->DBfile->getCount('giveaway',array('g_categories'=>$soloCate['cate_id']));
                                            $checked = in_array($soloCate['cate_id'],$w_blockcategory) ? '' : 'checked';
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$soloCate['cate_name'].'</td>';
                                            echo '<td>'.$prodCount[0]['totCount'].'</td>';
                                            echo '<td><label class="switch"><input type="checkbox" class="switch-inpt cateCls" '.$checked.' id="'.$soloCate['cate_id'].'"><span class="slider round"></span></label></td>';
                                            echo '<td><div class="plr_action_icon">';
                                            echo '<a href="'.base_url('ga/'.$webDetail[0]['w_siteurl'].'/'.$soloCate['cate_slug']).'" target="_blank"><i class="fa fa-eye"></i><div class="plr_tooltip_show"><p>View</p></div></a>';
                                            echo '</div></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    if(!empty($web_categories)){
                                        foreach($web_categories as $soloCate) {
                                            $prodCount = $this->DBfile->getCount('web_products',array('p_categoryid'=>$soloCate['cate_id']));
                                            $checked = $soloCate['cate_status'] == 0 ? '' : 'checked';
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$soloCate['cate_name'].'</td>';
                                            echo '<td>'.$prodCount[0]['totCount'].'</td>';
                                            echo '<td><label class="switch"><input type="checkbox" class="switch-inpt customcateCls" '.$checked.' id="'.$soloCate['cate_id'].'"><span class="slider round"></span></label></td>';
                                            echo '<td><div class="plr_action_icon">';
                                            echo '<a href="'.base_url('plr/'.$webDetail[0]['w_siteurl'].'/'.$soloCate['cate_slug']).'" target="_blank"><i class="fa fa-eye"></i><div class="plr_tooltip_show"><p>View</p></div></a>';
                                            echo '<a href="javascript:;" class="cateEdit" id="'.$soloCate['cate_id'].'"><i class="fa fa-pencil"></i><div class="plr_tooltip_show"><p>Edit</p></div></a>';
                                            echo '<a href="javascript:;" class="deleteCategory" data-cateid="'.$soloCate['cate_id'].'"><i class="fa fa-trash"></i><div class="plr_tooltip_show"><p>Delete</p></div></a>';
                                            echo '</div></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>