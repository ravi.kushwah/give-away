<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
          <form class="separate-form" id="adsBannerSave">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Ads Setting</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6 col-lg-12">
                                <div class="form-group">
                                    <label class="col-form-label">Home Page Banner Ad</label>
                                    <input class="form-control" type="text" placeholder="Ad Image URL" name="home_page_add" value="<?= isset($adsSetting[0]) ? $adsSetting[0]['home_page_add'] : '' ?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                                <div class="form-group">
                                    <label class="col-form-label">Banner URL</label>
                                    <input class="form-control" type="text" placeholder="Banner ad redirecting URL" name="home_page_link" value="<?=isset($adsSetting[0])?$adsSetting[0]['home_page_link']: ''?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                               <div class="form-group">
                                    <label class="col-form-label">Home Page Sidebar Ad</label>
                                    <input class="form-control" type="text" placeholder="Ad Image URL" name="home_page_sidebar_ad" value="<?=isset($adsSetting[0])?$adsSetting[0]['home_page_sidebar_ad']: ''?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                               <div class="form-group">
                                    <label class="col-form-label">Sidebar URL</label>
                                    <input class="form-control" type="text" placeholder="Sidebar ad redirecting URL" name="home_page_sidebar_ink" value="<?=isset($adsSetting[0])?$adsSetting[0]['home_page_sidebar_ink']: ''?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                               <div class="form-group">
                                    <label class="col-form-label">Single Page Top Ad</label>
                                    <input class="form-control" type="text" placeholder="Ad Image URL" name="single_page_top_ad" value="<?=isset($adsSetting[0])?$adsSetting[0]['single_page_top_ad']: ''?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                               <div class="form-group">
                                    <label class="col-form-label">Top URL</label>
                                    <input class="form-control" type="text" placeholder="Top ad redirecting URL" name="single_page_top_link" value="<?=isset($adsSetting[0])?$adsSetting[0]['single_page_top_link']: ''?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                               <div class="form-group">
                                    <label class="col-form-label">Single Page Bottom Ad</label>
                                    <input class="form-control" type="text" placeholder="Ad Image URL" name="single_page_bottom_ad" value="<?=isset($adsSetting[0])?$adsSetting[0]['single_page_bottom_ad']: ''?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                               <div class="form-group">
                                    <label class="col-form-label">Bottom URL</label>
                                    <input class="form-control" type="text" placeholder="Bottom ad redirecting URL" name="single_page_bottom_link" value="<?=isset($adsSetting[0])?$adsSetting[0]['single_page_bottom_link']: ''?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="plr_genrate_btn">
                    <input type="hidden" value="<?=isset($adsSetting[0])?$adsSetting[0]['a_id']: '0'?>" name="a_id">
                    <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="web_id">
                    <a href="javascript:;" class="ad-btn" onclick="saveads()">Update</a>
                </div>
            </div>
        </div>
        </form>

       