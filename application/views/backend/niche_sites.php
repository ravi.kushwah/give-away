<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4>Niche Websites</h4>
                    </div>
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Generate Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($websiteList)){
                                        $sno=0;
                                        foreach($websiteList as $soloWeb) {
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$soloWeb['w_title'].'</td>';
                                            echo '<td>'.date_format(date_create($soloWeb['w_createddate']),"jS F, Y").'</td>';
                                            echo '<td><div class="plr_action_icon">';
                                            echo '<a href="'.base_url('home/add_new_website/'.$soloWeb['w_id']).'"><i class="fa fa-cogs"></i><div class="plr_tooltip_show"><p>Manage</p></div></a>';
                                            echo '<a href="'.base_url('plr/'.$soloWeb['w_siteurl']).'" target="_blank"><i class="fa fa-eye"></i><div class="plr_tooltip_show"><p>View</p></div></a>';
                                            echo '<a href="javascript:void(0); " class="copy_button" data-tobecopied="'.base_url('plr/'.$soloWeb['w_siteurl']).'"><i class="fa fa-clone"></i><div class="plr_tooltip_show"><p>Copy URL</p></div></a>';
                                            echo '</div></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>