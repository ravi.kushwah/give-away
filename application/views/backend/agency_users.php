<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4><?= $pageName ?></h4>
                        <a class="ad-btn agencyuserClsEdit" id="0">Add New</a>
                    </div>
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($agency_users)){
                                        $sno=0;
                                        foreach($agency_users as $soloAgency) {
                                            $checked = $soloAgency['u_status'] == 2 ? '' : 'checked';
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$soloAgency['u_name'].'</td>';
                                            echo '<td>'.$soloAgency['u_email'].'</td>';
                                            echo '<td>'.date_format(date_create($soloAgency['u_purchaseddate']),"jS F, Y").'</td>';
                                            echo '<td><label class="switch"><input type="checkbox" class="switch-inpt agencyuserCls" '.$checked.' id="'.$soloAgency['u_id'].'"><span class="slider round"></span></label></td>';
                                            echo '<td><div class="plr_action_icon">';
                                            echo '<a href="javascript:;" class="agencyuserClsEdit" id="'.$soloAgency['u_id'].'"><i class="fa fa-pencil"></i><div class="plr_tooltip_show"><p>Edit</p></div></a>';
                                            echo '</div></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>