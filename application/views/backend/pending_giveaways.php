<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4>Pre-Loaded <?= $pageName ?> 
                        <!--<?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?>-->
                        </h4>
                       
                    </div>
                    <input type="hidden" value="<?= $webDetail[0]['w_id']?>" id="webid">
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Give Aways Name </th>
                                        <th>Country</th>
                                        <th>Status</th>
                                        <th>Categories</th>
                                        <th>End Date</th>
                                        <th>Description</th>
                                        <th>Give URL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($products)){
                                        $sno=0;
                                        foreach($products as $soloProd) {
                                            $checked = $soloProd['status']=='0' ? '' :'checked' ;
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$soloProd['name'] .'</td>';
                                            echo '<td>'.$soloProd['email'].'</td>';
                                            echo '<td>'.$soloProd['give_away_url'].'</td>';
                                            echo '<td>'.$soloProd['country'].'</td>';
                                            echo '<td><label class="switch"><input type="checkbox" class="switch-inpt submitGiveaways" '.$checked.'  id="'.$soloProd['s_id'].'"><span class="slider round"></span></label></td>';
                                            echo '<td>'.$soloProd['give_away_categories'].'</td>';
                                            echo '<td>'.date("M d, Y", strtotime($soloProd['end_date'])).'</td>';
                                            echo '<td>'.$soloProd['prize_description'].'</td>';
                                            echo '<td>'.$soloProd['give_away_url'].'</td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                    <!--<a href="'.$d.'"><i class="fa fa-download"></i><div class="plr_tooltip_show"><p>Download</p></div></a>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>