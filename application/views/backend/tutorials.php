<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4><?= $pageName ?></h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                             <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-form-label">Give Site Pro Site Setup</label>
                                <!--<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://www.loom.com/embed/3fedb44053de4450a1ffc53c178d1ebd" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>-->
                                    <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/793569197?h=4f41ce9350&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                                </div>
                                
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-form-label">Give Site Pro WP Plugin Demo</label>
                                    <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/754300189?h=bba44e211a&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!--<div class="col-lg-6">-->
                            <!--    <div class="form-group">-->
                            <!--        <label class="col-form-label">Give Site Pro - Part 1</label>-->
                            <!--        <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://www.loom.com/embed/ad18cdef82aa4e7ab09d33309d7c6d3b" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            
                            <!--<div class="col-lg-6">-->
                            <!--    <div class="form-group">-->
                            <!--        <label class="col-form-label">Give Site Pro - Part 2</label>-->
                            <!--        <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://www.loom.com/embed/678d51c9ffa340599fecbfdd88befbc3" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>

                    </div>
                </div>
            </div>
        </div>