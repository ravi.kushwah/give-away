<div class="page-wrapper">
    <div class="main-content">
        <form class="separate-form" id="setupUploadForm">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Enter you Affiliate ID to monetize coupons and deals with your own Affiliate Link</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Alibaba <a href="https://alibaba.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="alibaba.com" name="alibaba" value="<?= isset($affiliate[0]['alibaba']) ? $affiliate[0]['alibaba'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Aliexpress <a href="https://aliexpress.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="aliexpress.com" name="aliexpress" value="<?= isset($affiliate[0]['aliexpress']) ? $affiliate[0]['aliexpress'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Amazon <a href="https://amazon.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="amazon.com" name="amazon_com" value="<?= isset($affiliate[0]['amazon_com']) ? $affiliate[0]['amazon_com'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Amazon <a href="https://amazon.in" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="amazon.in" name="amazon_in" value="<?= isset($affiliate[0]['amazon_in']) ? $affiliate[0]['amazon_in'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Cafago <a href="https://cafago.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="cafago.com " name="cafago" value="<?= isset($affiliate[0]['cafago']) ? $affiliate[0]['cafago'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Discoveryplus <a href="https://discoveryplus.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="discoveryplus.com" name="discoveryplus" value="<?= isset($affiliate[0]['discoveryplus']) ? $affiliate[0]['discoveryplus'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Flipkart <a href="https://flipkart.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="flipkart.com" name="flipkart" value="<?= isset($affiliate[0]['flipkart']) ? $affiliate[0]['flipkart'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Geekbuying <a href="https://geekbuying.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="geekbuying.com" name="geekbuying" value="<?= isset($affiliate[0]['geekbuying']) ? $affiliate[0]['geekbuying'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Macys <a href="https://macys.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="macys.com" name="macys" value="<?= isset($affiliate[0]['macys']) ? $affiliate[0]['macys'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Paramountplus <a href="https://paramountplus.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="paramountplus.com" name="paramountplus" value="<?= isset($affiliate[0]['paramountplus']) ? $affiliate[0]['paramountplus'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Pitchground <a href="https://pitchground.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="pitchground.com" name="pitchground" value="<?= isset($affiliate[0]['pitchground']) ? $affiliate[0]['pitchground'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Ticketnetwork <a href="https://ticketnetwork.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="ticketnetwork.com" name="ticketnetwork" value="<?= isset($affiliate[0]['ticketnetwork']) ? $affiliate[0]['ticketnetwork'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Tomtop <a href="https://tomtop.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="tomtop.com" name="tomtop" value="<?= isset($affiliate[0]['tomtop']) ? $affiliate[0]['tomtop'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Vapor <a href="https://vapor.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="vapor.com" name="vapor" value="<?= isset($affiliate[0]['vapor']) ? $affiliate[0]['vapor'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Vetsupply <a href="https://vetsupply.com.au" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="vetsupply.com.au" name="vetsupply" value="<?= isset($affiliate[0]['vetsupply']) ? $affiliate[0]['vetsupply'] : '' ?>">
                                    </div>
                                </div>
                                
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Abebooks <a href="https://abebooks.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="abebooks.com" name="abebooks" value="<?= isset($affiliate[0]['abebooks']) ? $affiliate[0]['abebooks'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Casetify <a href="https://casetify.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="casetify.com" name="casetify" value="<?= isset($affiliate[0]['casetify']) ? $affiliate[0]['casetify'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Coinsmart <a href="https://coinsmart.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="coinsmart.com" name="coinsmart" value="<?= isset($affiliate[0]['coinsmart']) ? $affiliate[0]['coinsmart'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>dhgate <a href="https://dhgate.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="dhgate.com" name="dhgate" value="<?= isset($affiliate[0]['dhgate']) ? $affiliate[0]['dhgate'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Examedge <a href="https://examedge.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="examedge.com" name="examedge" value="<?= isset($affiliate[0]['examedge']) ? $affiliate[0]['examedge'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Globalhealing <a href="https://globalhealing.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="globalhealing.com" name="globalhealing" value="<?= isset($affiliate[0]['globalhealing']) ? $affiliate[0]['globalhealing'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>gshopper <a href="https://gshopper.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="gshopper.com" name="gshopper" value="<?= isset($affiliate[0]['gshopper']) ? $affiliate[0]['gshopper'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Hekka <a href="https://hekka.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="hekka.com" name="hekka" value="<?= isset($affiliate[0]['hekka']) ? $affiliate[0]['hekka'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Istockphoto <a href="https://istockphoto.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="istockphoto.com" name="istockphoto" value="<?= isset($affiliate[0]['istockphoto']) ? $affiliate[0]['istockphoto'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Magix <a href="https://magix.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="magix.com" name="magix" value="<?= isset($affiliate[0]['magix']) ? $affiliate[0]['magix'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>pariscityvision <a href="https://pariscityvision.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="pariscityvision.com" name="pariscityvision" value="<?= isset($affiliate[0]['pariscityvision']) ? $affiliate[0]['pariscityvision'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Razer <a href="https://razer.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="razer.com" name="razer" value="<?= isset($affiliate[0]['razer']) ? $affiliate[0]['razer'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Remitly <a href="https://remitly.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="remitly.com" name="remitly" value="<?= isset($affiliate[0]['remitly']) ? $affiliate[0]['remitly'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Shutterstock <a href="https://shutterstock.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="shutterstock.com" name="shutterstock" value="<?= isset($affiliate[0]['shutterstock']) ? $affiliate[0]['shutterstock'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Springer <a href="https://springer.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="springer.com" name="springer" value="<?= isset($affiliate[0]['springer']) ? $affiliate[0]['springer'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Tenorshare <a href="https://tenorshare.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="tenorshare.com" name="tenorshare" value="<?= isset($affiliate[0]['tenorshare']) ? $affiliate[0]['tenorshare'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Ticketliquidator <a href="https://ticketliquidator.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="ticketliquidator.com" name="ticketliquidator" value="<?= isset($affiliate[0]['ticketliquidator']) ? $affiliate[0]['ticketliquidator'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Tigerdirect <a href="https://tigerdirect.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="tigerdirect.com" name="tigerdirect" value="<?= isset($affiliate[0]['tigerdirect']) ? $affiliate[0]['tigerdirect'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Vistaprint <a href="https://vistaprint.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="vistaprint.com" name="vistaprint" value="<?= isset($affiliate[0]['vistaprint']) ? $affiliate[0]['vistaprint'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Appsumo <a href="https://appsumo.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="appsumo.com" name="appsumo" value="<?= isset($affiliate[0]['appsumo']) ? $affiliate[0]['appsumo'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Cheapoair <a href="https://cheapoair.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="cheapoair.com" name="cheapoair" value="<?= isset($affiliate[0]['cheapoair']) ? $affiliate[0]['cheapoair'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Chicme <a href="https://chicme.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="chicme.com" name="chicme" value="<?= isset($affiliate[0]['chicme']) ? $affiliate[0]['chicme'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Corsair <a href="https://corsair.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="corsair.com" name="corsair" value="<?= isset($affiliate[0]['corsair']) ? $affiliate[0]['corsair'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Dell <a href="https://dell.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="dell.com" name="dell" value="<?= isset($affiliate[0]['dell']) ? $affiliate[0]['dell'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Elfsight <a href="https://elfsight.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="elfsight.com" name="elfsight" value="<?= isset($affiliate[0]['elfsight']) ? $affiliate[0]['elfsight'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Englishonlinev <a href="https://englishonline.britishcouncil.org" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="englishonline.britishcouncil.org" name="englishonlinev" value="<?= isset($affiliate[0]['englishonlinev']) ? $affiliate[0]['englishonlinev'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Gearbest <a href="https://f1store.formula1.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="f1store.formula1.com" name="gearbest" value="<?= isset($affiliate[0]['gearbest']) ? $affiliate[0]['gearbest'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Gearbest <a href="https://gearbest.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="gearbest.com" name="gearbest" value="<?= isset($affiliate[0]['gearbest']) ? $affiliate[0]['gearbest'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Higherstandards <a href="https://higherstandards.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="higherstandards.com" name="higherstandards" value="<?= isset($affiliate[0]['higherstandards']) ? $affiliate[0]['higherstandards'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Kitbag <a href="https://kitbag.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="kitbag.com" name="kitbag" value="<?= isset($affiliate[0]['kitbag']) ? $affiliate[0]['kitbag'] : '' ?>">
                                    </div>
                                </div>
                                
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Lenovo <a href="https://lenovo.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="lenovo.com" name="lenovo" value="<?= isset($affiliate[0]['lenovo']) ? $affiliate[0]['lenovo'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Lightinthebox <a href="https://lightinthebox.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="lightinthebox.com" name="lightinthebox" value="<?= isset($affiliate[0]['lightinthebox']) ? $affiliate[0]['lightinthebox'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Nba <a href="https://nba.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="nba.com" name="nba" value="<?= isset($affiliate[0]['nba']) ? $affiliate[0]['nba'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Newchic <a href="https://newchic.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="newchic.com" name="newchic" value="<?= isset($affiliate[0]['newchic']) ? $affiliate[0]['newchic'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Nflshop <a href="https://nflshop.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="nflshop.com" name="nflshop" value="<?= isset($affiliate[0]['nflshop']) ? $affiliate[0]['nflshop'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Omio <a href="https://omio.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="omio.com" name="omio" value="<?= isset($affiliate[0]['omio']) ? $affiliate[0]['omio'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Oneplus <a href="https://oneplus.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="oneplus.com" name="oneplus" value="<?= isset($affiliate[0]['oneplus']) ? $affiliate[0]['oneplus'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Onetravel <a href="https://onetravel.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="onetravel.com" name="onetravel" value="<?= isset($affiliate[0]['onetravel']) ? $affiliate[0]['onetravel'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Philo <a href="https://philo.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="philo.com" name="philo" value="<?= isset($affiliate[0]['philo']) ? $affiliate[0]['philo'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Photowall <a href="https://photowall.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="photowall.com" name="photowall" value="<?= isset($affiliate[0]['photowall']) ? $affiliate[0]['photowall'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Sage <a href="https://sage.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="sage.com" name="sage" value="<?= isset($affiliate[0]['sage']) ? $affiliate[0]['sage'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Samueljohnston <a href="https://samueljohnston.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="samueljohnston.com" name="samueljohnston" value="<?= isset($affiliate[0]['samueljohnston']) ? $affiliate[0]['samueljohnston'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Untilgone <a href="https://untilgone.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="untilgone.com" name="untilgone" value="<?= isset($affiliate[0]['untilgone']) ? $affiliate[0]['untilgone'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Vapesourcing <a href="https://vapesourcing.uk" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="vapesourcing.uk" name="vapesourcing" value="<?= isset($affiliate[0]['vapesourcing']) ? $affiliate[0]['vapesourcing'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Walgreens <a href="https://www.walgreens.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="walgreens.com" name="walgreens" value="<?= isset($affiliate[0]['walgreens']) ? $affiliate[0]['walgreens'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Wondershare <a href="https://wondershare.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="wondershare.com" name="wondershare" value="<?= isset($affiliate[0]['wondershare']) ? $affiliate[0]['wondershare'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Zaful <a href="https://zaful.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="zaful.com" name="zaful" value="<?= isset($affiliate[0]['zaful']) ? $affiliate[0]['zaful'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="ao_label_list">
                                            <span>Zulily <a href="https://zulily.com" target="_blank"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
                                        </div>
                                        <input class="form-control" type="text" placeholder="zulily.com" name="zulily" value="<?= isset($affiliate[0]['zulily']) ? $affiliate[0]['zulily'] : '' ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <div class="plr_genrate_btn">
                        <input type="hidden" value="<?= isset($affiliate[0]['id']) ? $affiliate[0]['id'] : '0' ?>" name="id">
                        <a href="javascript:;" class="ad-btn" onclick="add_affiliate()">Submit</a>
                    </div>
                </div>
            </div>   
        </form>    