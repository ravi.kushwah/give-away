<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row"hidden>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>When customer Sign Up</h4>
                    </div>
                    <?php $signArray = $webDetail[0]['w_signuplist'] != '' ? json_decode($webDetail[0]['w_signuplist'],true) : array() ; 
                    ?>
                    <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group plr_require">
                                        <label class="col-form-label">Choose from connected ARs</label>
                                        <input type="hidden" value="<?= !empty($signArray) ? $signArray['listid'] : 0 ?>">
                                        <select class="form-control arCls getArList" data-artype="signup" data-listid="signup" id="w_signuplist">
                                            <?php echo '<option value="0">Select One</option>';
                                            if(!empty($getAr)) {
                                                foreach($getAr as $k=>$v){
                                                    $sel = !empty($signArray) ? $signArray['ar'] == $k ? 'selected' : '' : '';
                                                    echo '<option value="'.$k.'" '.$sel.'>'.$k.'</option>';
                                                }
                                            }
                                            
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group plr_require">
                                        <label class="col-form-label">Choose a List</label>
                                        <select class="form-control" id="signup">
                                            <option value="">Choose One</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>When customer Enter Giveaways</h4>
                    </div>
                    <?php $buyArray = $webDetail[0]['w_buyplanlist'] != '' ? json_decode($webDetail[0]['w_buyplanlist'],true) : array() ; ?>
                    <div class="card-body">
                            <div class="row customHTMLGiveaways">
                               <?php
                                $responder = isset($autoresponderData[0]['value']) ? $autoresponderData[0]['value'] : '';
                               if(!empty(json_decode($responder))){
                                $hide = '';
                               }else{
                                    $hide = 'hidden';
                                   ?><div class="col-lg-6">
                                        <div class="form-group plr_require">
                                            <label class="col-form-label">Choose from connected ARs</label>
                                        <select class="form-control arCls " data-artype="" data-listid="" id="">
                                            <option value="0">CustomHTML</option>
                                        </select>
                                        </div>
                                    </div><?php
                               }
                               ?>
                                <div class="col-lg-6" <?=$hide?>>
                                    <div class="form-group plr_require">
                                        <label class="col-form-label">Choose from connected ARs</label>
                                        <input type="hidden"  value="<?= !empty($buyArray) && isset($buyArray['listid']) ? $buyArray['listid'] : 0 ?>">
                                        <select class="form-control arCls getArList" data-artype="planbuy" data-listid="planbuy" id="w_buyplanlist">
                                            <?php echo '<option value="0" >Select One</option>';
                                            if(!empty($getAr)) {
                                                foreach($getAr as $k=>$v){
                                                    $sel = !empty($buyArray) ? $buyArray['ar'] == $k ? 'selected' : '' : '';
                                                    echo '<option value="'.$k.'" '.$sel.'>'.$k.'</option>';
                                                }
                                            }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                   <?php
                                    if(empty(json_decode($responder))){
                                        ?>
                                         <textarea placeholder="Enter Custom HTML" class="form-control " id="customplan"> <?php echo $webDetail[0]['customHTMLgiveaways'] ; ?></textarea>
                                        <?php
                                    }else{
                                         ?>
                                          <div class="form-group plr_require planbuy">
                                                <label class="col-form-label">Choose a List</label>
                                                <select class="form-control" id="planbuy">
                                                    <option value="">Choose One</option>
                                                </select>
                                            </div>
                                         <?php
                                    }
                                   ?>
                                   
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>From the newsletter box</h4>
                    </div>
                    <?php $newsArray = $webDetail[0]['w_newsletterlist'] != '' ? json_decode($webDetail[0]['w_newsletterlist'],true) : array() ; ?>
                    <div class="card-body">
                            <div class="row">
                              <?php 
                                $responder = isset($autoresponderData[0]['value']) ? $autoresponderData[0]['value'] : '';
                              if(!empty(json_decode($responder))){
                                 $hideN = '';
                              }else{
                                $hideN = 'hidden';
                                  ?>
                                 <div class="col-lg-6">
                                    <div class="form-group plr_require">
                                        <label class="col-form-label">Choose from connected ARs</label>
                                    <select class="form-control arCls " data-artype="" data-listid="" id="">
                                        <option value="0">CustomHTML</option>
                                    </select>
                                    </div>
                                </div>
                                  <?php
                              }
                                
                                ?>
                                 <div class="col-lg-6"<?=$hideN?>>
                                    <div class="form-group plr_require">
                                        <label class="col-form-label">Choose from connected ARs</label>
                                        <input type="hidden"  value="<?= !empty($newsArray) && isset($newsArray['listid']) ? $newsArray['listid'] : 0 ?>">
                                        <select class="form-control arCls getArList" data-artype="newsletter" data-listid="planbuy" id="w_newsletterlist">
                                            <?php echo '<option value="0">Select One</option>';
                                            if(!empty($getAr)) {
                                                foreach($getAr as $k=>$v){
                                                    $sel = !empty($newsArray) ? $newsArray['ar'] == $k ? 'selected' : '' : '';
                                                    echo '<option value="'.$k.'" '.$sel.'>'.$k.'</option>';
                                                }
                                            }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6"><?php  
                                    if(empty(json_decode($responder))){
                                        
                                        ?>
                                          <textarea placeholder="Enter Custom HTML" class="form-control  customNewsletter" id="customNewsletter"><?php echo $webDetail[0]['customHTMLnews'] ; ?></textarea>
                                        <?php
                                    }else{
                                        ?>
                                         <div class="form-group plr_require newsletter">
                                            <label class="col-form-label">Choose a List</label>
                                            <select class="form-control" id="newsletter">
                                                <option value="">Choose One</option>
                                            </select>
                                        </div>
                                        <?php 
                                    }
                                   ?>
                                   
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="plr_genrate_btn">
                    <input type="hidden" value="<?= !empty($webDetail) ? $webDetail[0]['w_id'] : '' ?>" id="w_id">
                    <a href="javascript:;" class="ad-btn" onclick="saveLeadSettings()">Save</a>
                </div>
            </div>
        </div>