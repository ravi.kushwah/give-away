<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4><?= $pageName ?>  <?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?></h4>
                    </div>
                    <input type="hidden" value="<?= $webDetail[0]['w_id']?>" id="webid">
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer Name</th>
                                        <th>Customer Email</th>
                                        <th>Plan Name</th>
                                        <th>Plan Cost</th>
                                        <th>Amount Received</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($transactions)){
                                        $sno=0;
                                        $planArr = json_decode($webDetail[0]['w_plansettings'],true);
		
                                        foreach($transactions as $soloTransactions) {

                                            $planData = json_decode($planArr['plan_'.$soloTransactions['trans_planid']],true);
                                            $planAmount = $planData['amount_'.$soloTransactions['trans_planid']];
                                            $planName = $planData['planname_'.$soloTransactions['trans_planid']];
                                            
                                            $soloCust = $this->DBfile->get_data('customers',array('c_id'=>$soloTransactions['trans_customerid']),'c_name,c_email');
                                            
                                            $cname = !empty($soloCust) ? $soloCust[0]['c_name'] : '-';
                                            $cemail = !empty($soloCust) ? $soloCust[0]['c_email'] : '-';

                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$cname.'</td>';
                                            echo '<td>'.$cemail.'</td>';
                                            echo '<td>'.$planName.'</td>';
                                            echo '<td>'.$planAmount.'</td>';
                                            echo '<td>'.$soloTransactions['trans_mcgross'].'</td>';
                                            echo '<td>'.$soloTransactions['trans_status'].'</td>';
                                            echo '<td>'.date_format(date_create($soloTransactions['trans_date']),"jS F, Y").'</td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>