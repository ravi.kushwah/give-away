<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4>Custom <?= $pageName ?>  <?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?></h4>
                        <div class="plr_customproedit">
                            <a href="javascript:;" class="ad-btn custProdEdit" id="0">Add Custom GiveAways</a>
                            <a href="<?= base_url() ?>home/web_products/<?= $webDetail[0]['w_id']?>" class="ad-btn">Pre-Loaded GiveAways</a>
                        </div>
                    </div>
                    <input type="hidden" value="<?= $webDetail[0]['w_id']?>" id="webid">
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Category</th> 
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($custom_products)){
                                        $sno=0;
                                        foreach($custom_products as $soloProd) {
                                            $u = $siteUrl.'/'.$soloProd['slug'].'/web-giveAway/'.$soloProd['g_categories'];
                                            $d = base_url().'home/download_product/'.$soloProd['slug'];
					                        $cateName = ($this->DBfile->get_data('web_categories' , array('cate_id'=>$soloProd['g_categories'])))[0]['cate_name'];
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$soloProd['g_offer_name'].'</td>';
                                            echo '<td>'.$cateName.'</td>';
                                            echo '<td><div class="plr_action_icon">';
                                            echo '<a href="'.$u.'" target="_blank"><i class="fa fa-eye"></i><div class="plr_tooltip_show"><p>View</p></div></a>';
                                            echo '<a href="javascript:;" class="custProdEdit" id="'.$soloProd['g_id'].'"><i class="fa fa-pencil"></i><div class="plr_tooltip_show"><p>Edit</p></div></a>';
                                            echo '<a href="javascript:;" class="deleteCustomProduct" data-custprodid="'.$soloProd['g_id'].'"><i class="fa fa-trash"></i><div class="plr_tooltip_show"><p>Delete</p></div></a>';
                                            echo '</div></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                    <!--<a href="'.$d.'"><i class="fa fa-download"></i><div class="plr_tooltip_show"><p>Download</p></div></a>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>