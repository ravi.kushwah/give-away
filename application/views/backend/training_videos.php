<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard Start -->
        
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4>Pre-Loaded <?= $pageName ?> 
                        <!--<?= $siteUrl != '' ? '<a href="'.$siteUrl.'" target="_blank" style="font-size: 15px;font-style: italic;margin-left: 20px;color: #4b88ff;">View Site</a>' : '' ?>-->
                        </h4>
                    </div>
                    <input type="hidden" value="<?= $webDetail[0]['w_id']?>" id="webid">
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>URL Name</th>
                                         <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($training_videos)){
                                        $sno=0;
                                        foreach($training_videos as $soloProd) {
                                            $d = base_url().'home/download_product/'.$soloProd['v_urlname'];
                                            $sno++;
                                            echo '<tr>';
                                            echo '<td>'.$sno.'</td>';
                                            echo '<td>'.$soloProd['v_name'].'</td>';
                                            echo '<td>'.$soloProd['v_urlname'].'</td>';
                                             echo '<td><div class="plr_action_icon">';
                                            echo '<a href="'.$d.'"><i class="fa fa-download"></i><div class="plr_tooltip_show"><p>Download</p></div></a>';
                                            echo '</div></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                    <!--<a href="'.$d.'"><i class="fa fa-download"></i><div class="plr_tooltip_show"><p>Download</p></div></a>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>