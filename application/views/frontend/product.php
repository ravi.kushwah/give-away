<div class="plr_breadcrumbs_section">
        <div class="container">
            <div class="plr_breadcrumbs_box">
                <h2><?= $soloProdDetail[0]['p_name'] ?></h2>
                <span><a href="<?= $this->session->userdata['web_url'] ?>">Home</a></span>
            </div>
        </div>
    </div>
</div>
<!--===Header Section End===-->

<!--===Product Single Start===-->
<div class="plr_policy_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 align-self-center">
                <div class="plr_pro_single_img">
                    <img src="<?= $soloProdDetail[0]['p_imagelink'] ?>" alt="<?= $soloProdDetail[0]['p_name'] ?>">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 align-self-center">
                <div class="plr_pro_single_detail">
                    <h4><?= $soloProdDetail[0]['p_name'] ?></h4>
                    <span><?= $cateName ?></span>
                    <p><?= $soloProdDetail[0]['p_description'] == '' ? 'Private Label Rights product,'.$soloProdDetail[0]['p_name'] : $soloProdDetail[0]['p_description'] ?></p>
                    <?php if( isset($this->session->userdata['c_loggedin']) ) { ?>
                        <a href="<?= base_url() ?>home/access_download/<?= $websiteData['w_id'] ?>/<?= $this->session->userdata['c_id']?>/<?= $soloProdDetail[0]['p_urlname'] ?>" class="plr_btn">Download Now</a>
                    <?php } else { ?>
                        <a href="<?= count($checkURL) == 2 ? base_url('plr/'.$websiteData['w_siteurl'].'/pricing') : $this->session->userdata['web_url'].'/pages/pricing' ?>" class="plr_btn">Buy Now</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--===Product Single End===-->

<!--===Tab Section Start===-->
<div class="plr_tab_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="plr_template_box">
                    <div class="plr_tem_tab_buttom">
                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Description</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">License Terms</button>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="plr_tem_description">
                                <p><?= $soloProdDetail[0]['p_description'] == '' ? 'Private Label Rights product,'.$soloProdDetail[0]['p_name'] : $soloProdDetail[0]['p_description'] ?></p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="plr_tem_additional">
                                <div class="plr_box_previews">
                                    <ul>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>
                                        Can be packaged.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>You Can Use This Product Yourself.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can be sold as it is.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can pass on the Resell Rights privilege to your customers.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can Add This Product to a Membership Site or Bundled Within a Product Package as a Bonus.</li>
                                    </ul>
                                </div>
                                <div class="plr_box_previews">
                                    <ul>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can be used to create audio/webinar/video products.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can Give Away The Product (NOT The Source Code Files) To Your Subscribers, Members or Customers as a Bonus or Gift.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can change sales page and/or graphics.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can be added to paid membership sites.</li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.031" height="11.97" viewBox="0 0 11.031 11.97">
                                          <path class="cls-1" d="M444.853,1107.31l-4.586-4.98a0.866,0.866,0,0,0-1.311,0,1.066,1.066,0,0,0,0,1.42l3.931,4.27-3.931,4.27a1.066,1.066,0,0,0,0,1.42,0.881,0.881,0,0,0,1.311,0l4.586-4.98A1.064,1.064,0,0,0,444.853,1107.31Zm-5.515-.3-3.641-4.17a0.853,0.853,0,0,0-1.324,0,1.176,1.176,0,0,0,0,1.51l2.979,3.41-2.979,3.41a1.189,1.189,0,0,0,0,1.52,0.853,0.853,0,0,0,1.324,0l3.641-4.17A1.176,1.176,0,0,0,439.338,1107.01Z" transform="translate(-434.094 -1102.03)"/>
                                        </svg>Can sell master resale rights.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--===Tab Section End===-->