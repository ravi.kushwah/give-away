<!DOCTYPE html>
<!-- Project: PLR Site Builder-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Begin Head -->
  <head>
    <!--=== Required meta tags ===-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=== custom css ===-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Outfit:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=<?= $websiteData['w_logofont'] ?>:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
	<link rel="shortcut icon" type="image/ico" href="<?= base_url() ?>assets/frontend/images/favicon.png"/>
	<?php $colorArr = $websiteData['w_themecolor'] != '' ? explode(',',$websiteData['w_themecolor']) : array('#f582cb','#fa709a') ?>
	<style>
	:root {
    --text-color: #797979;
    --background: #eff5fc;
    --theme-color:<?= $colorArr[0] ?>;
    --white-color: #ffffff;
    --dark-color: #242323;
    --pink-color1: <?= $colorArr[0] ?>;
}
	</style>
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/style.css"/>
	<!--=== custom css ===-->
    <?php if($websiteData['w_faviconurl'] != '') { ?>
        <link rel="shortcut icon" type="image/ico" href="<?= base_url() ?>assets/webupload/<?= $websiteData['w_faviconurl'] ?>"/>
    <?php } else { ?>
        <link rel="shortcut icon" type="image/ico" href="<?= base_url() ?>assets/frontend/images/favicon.png"/>
    <?php } ?>
    
    <meta name="description" content="<?= $websiteData['w_description'] ?>">
    <meta name="keywords" content="<?= $websiteData['w_keywords'] ?>">
    <title><?= $websiteData['w_title'] ?><?= isset($pageName) ? ' | '.$pageName : '' ?></title>
  </head>
  <body>
	<div class="loader">
		<div class="spinner">
			<img src="<?= base_url() ?>assets/frontend/images/loader.gif" alt="loader"/>
		</div> 
	</div>
	<!--=== start Main wraapper ===-->
	<div class="plr_main_wrapper">
	
		<!--===Header Section Start===-->
		<div class="plr_header_section">
			<div class="plr_header_box">
				<div class="plr_header_logo">
					<a href="<?= $this->session->userdata['web_url'] ?>">
                    <?php if($websiteData['w_logourl'] != '') { ?>
                        <img src="<?= base_url() ?>assets/webupload/<?= $websiteData['w_logourl'] ?>" alt="<?= $websiteData['w_title'] ?>"/>
                    <?php } elseif( $websiteData['w_logotext'] != '' ) { ?>
                        <p style="color:<?= $websiteData['w_logocolor'] ?>; font-family:<?= $websiteData['w_logofont'] ?>"><?= $websiteData['w_logotext'] ?></p>
                    <?php } else { ?>
                        <img src="<?= base_url() ?>assets/frontend/images/logo.png" alt="<?= $websiteData['w_title'] ?>"/>
                    <?php } ?>
                    </a>
				</div>
                <?php if( $is_customer == 1 ) { ?>
				<div class="plr_nav_main">
					<div class="plr_profile_box">
						<span class="plr_avtar_box">
							<a href="javascript:;">
								<?= strtoupper($this->session->userdata['c_initials']) ?>
							</a>
						</span>
						<span class="plr_avtar_name">
							<p><?= $this->session->userdata['c_name'] ?></p>
							<div class="plr_profile_popup">
								<ul>
									<li><a href="<?= count($checkURL) == 2 ? base_url('Ga/'.$websiteData['w_siteurl'].'/profile') : $this->session->userdata['web_url'].'/pages/profile' ?>"><img src="<?= base_url() ?>assets/frontend/images/pop_ic1.svg" alt="icon">Profile</a></li>
									<li><a href="<?= count($checkURL) == 2 ? base_url('Ga/'.$websiteData['w_siteurl'].'/profile') : $this->session->userdata['web_url'].'/pages/profile' ?>"><img src="<?= base_url() ?>assets/frontend/images/pop_ic2.svg" alt="icon">My Downloads</a></li>
									<li><a href="<?= base_url('home/frontend_logout/'.$websiteData['w_siteurl']) ?>"><img src="<?= base_url() ?>assets/frontend/images/pop_ic3.svg" alt="icon">Logout</a></li>
								</ul>
							</div>
						</span>
					</div>
				</div>
                <?php } else { ?>
                <div class="plr_nav_main">
					<div class="plr_header_menu">
						 <div class="plr_header_login">
							<a href="javascript:;" class="plr_free_trial">
							    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="17px">
                                <path fill-rule="evenodd"
                                 d="M15.778,0.088 L14.189,0.088 L14.189,1.688 C14.189,2.008 13.924,2.222 13.660,2.222 C13.395,2.222 13.130,2.008 13.130,1.688 L13.130,0.088 L4.655,0.088 L4.655,1.688 C4.655,2.008 4.390,2.222 4.125,2.222 C3.860,2.222 3.595,2.008 3.595,1.688 L3.595,0.088 L2.006,0.088 C1.212,0.088 0.629,0.781 0.629,1.688 L0.629,3.608 L17.579,3.608 L17.579,1.688 C17.579,0.781 16.626,0.088 15.778,0.088 ZM0.629,4.728 L0.629,14.488 C0.629,15.448 1.212,16.088 2.059,16.088 L15.831,16.088 C16.679,16.088 17.632,15.395 17.632,14.488 L17.632,4.728 L0.629,4.728 ZM5.343,13.688 L4.072,13.688 C3.860,13.688 3.648,13.528 3.648,13.261 L3.648,11.928 C3.648,11.715 3.807,11.502 4.072,11.502 L5.396,11.502 C5.608,11.502 5.820,11.661 5.820,11.928 L5.820,13.261 C5.767,13.528 5.608,13.688 5.343,13.688 ZM5.343,8.888 L4.072,8.888 C3.860,8.888 3.648,8.728 3.648,8.462 L3.648,7.128 C3.648,6.915 3.807,6.702 4.072,6.702 L5.396,6.702 C5.608,6.702 5.820,6.862 5.820,7.128 L5.820,8.462 C5.767,8.728 5.608,8.888 5.343,8.888 ZM9.581,13.688 L8.257,13.688 C8.045,13.688 7.833,13.528 7.833,13.261 L7.833,11.928 C7.833,11.715 7.992,11.502 8.257,11.502 L9.581,11.502 C9.793,11.502 10.005,11.661 10.005,11.928 L10.005,13.261 C10.005,13.528 9.846,13.688 9.581,13.688 ZM9.581,8.888 L8.257,8.888 C8.045,8.888 7.833,8.728 7.833,8.462 L7.833,7.128 C7.833,6.915 7.992,6.702 8.257,6.702 L9.581,6.702 C9.793,6.702 10.005,6.862 10.005,7.128 L10.005,8.462 C10.005,8.728 9.846,8.888 9.581,8.888 ZM13.818,13.688 L12.494,13.688 C12.282,13.688 12.070,13.528 12.070,13.261 L12.070,11.928 C12.070,11.715 12.229,11.502 12.494,11.502 L13.818,11.502 C14.030,11.502 14.242,11.661 14.242,11.928 L14.242,13.261 C14.242,13.528 14.083,13.688 13.818,13.688 ZM13.818,8.888 L12.494,8.888 C12.282,8.888 12.070,8.728 12.070,8.462 L12.070,7.128 C12.070,6.915 12.229,6.702 12.494,6.702 L13.818,6.702 C14.030,6.702 14.242,6.862 14.242,7.128 L14.242,8.462 C14.242,8.728 14.083,8.888 13.818,8.888 Z"/>
                                </svg> <?php echo(date("F d, Y"));?></a>
						</div> 
					</div>
				</div>
                <?php } ?>
                
			</div>
			<div class="plr_banner_section">
				<div class="container">
					<div class="plr_banner_heading">
						<a href="<?= isset($ads[0]['home_page_add'])? $ads[0]['home_page_add'] :''?>" target="_blank"><img src="<?= isset($ads[0]['home_page_link'])? $ads[0]['home_page_link'] : base_url()."assets/frontend/images/add.jpg" ?>"  alt="add-img"></a>
					</div>
					<div class="plr_banner_button">
					    <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/submit-giveaways"  class="plr_btn">
					        <?php
					            if($website[0]['w_type']=='1' || $website[0]['w_type']=='5'){
					                ?>
					                Submit GiveAway
					                <?php
					            }else if($website[0]['w_type']=='3'){
					                ?>
					                Submit Contests
					                <?php
					            }else if($website[0]['w_type']=='4'){
					                ?>
					                 Submit Gift Cards
					                <?php
					            }else{
					                 ?>
					                  Submit GiveAway
					                <?php
					            }
				            ?>
					        
					        
					    </a>
					</div>
				</div>
			</div>