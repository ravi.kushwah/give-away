	<!--===Sidebar Section Start===-->
			<div class="col-xl-3 col-lg-3 col-md-12">
				<div class="plr_sidebar_category">
				    <?php 
				   
				        if(empty($website[0]['w_fblink']) && empty($website[0]['w_twitterlink']) && empty($website[0]['w_instalink']) && empty($website[0]['w_linkedinlink'])){
				            $display = "hidden";
				        }else{
				             $display = "test";
				        }
				    ?>
				    <div class="plr_box_search_headbox" <?=$display?>>
						<h2>Follow Socials</h2>
					</div>
					<div class="plr_social_follow" <?=$display?>>
					    <a href="<?= $website[0]['w_fblink'] ?>" target="_blank"><div class="plr_social_flex" <?= empty($website[0]['w_fblink']) ? 'hidden' : '' ?>>
    					    <div class="plr_followicon">
    					        <svg 
                                 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="12px" height="27px">
                                <path fill-rule="evenodd"  fill="rgb(60, 115, 240)"
                                 d="M9.809,4.466 L12.000,4.466 L12.000,0.332 C11.622,0.276 10.322,0.149 8.808,0.149 C5.649,0.149 3.486,2.301 3.486,6.258 L3.486,9.899 L-0.000,9.899 L-0.000,14.520 L3.486,14.520 L3.486,26.149 L7.759,26.149 L7.759,14.521 L11.104,14.521 L11.635,9.900 L7.758,9.900 L7.758,6.716 C7.759,5.380 8.091,4.466 9.809,4.466 Z"/>
                                </svg>
    					    </div>
    					    <div class="plr_followers">
    					        <span>Like</span>
    					    </div>
    					</div></a>
    					<a href="<?= $website[0]['w_twitterlink'] ?>" target="_blank"><div class="plr_social_flex" <?= empty($website[0]['w_twitterlink']) ? 'hidden' : '' ?>>
    					    <div class="plr_followicon">
    					        <svg 
                                 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="26px" height="23px">
                                <path fill-rule="evenodd"  fill="rgb(20, 177, 243)"
                                 d="M26.000,2.753 C25.033,3.195 24.003,3.488 22.929,3.630 C24.034,2.943 24.877,1.863 25.274,0.562 C24.243,1.201 23.106,1.653 21.894,1.905 C20.915,0.821 19.521,0.149 18.000,0.149 C15.049,0.149 12.673,2.643 12.673,5.701 C12.673,6.141 12.709,6.564 12.797,6.967 C8.365,6.742 4.444,4.530 1.810,1.161 C1.350,1.992 1.081,2.943 1.081,3.967 C1.081,5.889 2.031,7.593 3.448,8.580 C2.592,8.563 1.752,8.304 1.040,7.896 C1.040,7.913 1.040,7.935 1.040,7.957 C1.040,10.655 2.888,12.895 5.310,13.411 C4.877,13.535 4.404,13.594 3.913,13.594 C3.572,13.594 3.227,13.574 2.904,13.499 C3.594,15.698 5.554,17.314 7.884,17.366 C6.071,18.844 3.768,19.734 1.276,19.734 C0.838,19.734 0.419,19.714 -0.000,19.658 C2.361,21.243 5.159,22.149 8.177,22.149 C17.985,22.149 23.348,13.687 23.348,6.353 C23.348,6.108 23.340,5.871 23.328,5.635 C24.386,4.854 25.275,3.877 26.000,2.753 Z"/>
                                </svg>
    					    </div>
    					    <div class="plr_followers">
    					        <span>Follow</span>
    					    </div>
    					</div></a>
    					<a href="<?= $website[0]['w_instalink'] ?>" target="_blank"><div class="plr_social_flex" <?= empty($website[0]['w_instalink']) ? 'hidden' : '' ?>>
    					    <div class="plr_followicon">
    					        <svg 
                                 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="24px" height="25px">
                                <defs>
                                <linearGradient id="PSgrad_0" x1="0%" x2="100%" y1="0%" y2="0%">
                                  <stop offset="0%" stop-color="rgb(255,85,62)" stop-opacity="1" />
                                  <stop offset="67%" stop-color="rgb(255,0,101)" stop-opacity="1" />
                                  <stop offset="100%" stop-color="rgb(255,0,101)" stop-opacity="1" />
                                </linearGradient>
                                
                                </defs>
                                <path fill-rule="evenodd"  fill="rgb(240, 87, 40)"
                                 d="M23.930,17.097 C23.873,18.373 23.667,19.245 23.372,20.009 C22.748,21.622 21.472,22.897 19.859,23.521 C19.100,23.816 18.223,24.022 16.947,24.079 C15.667,24.135 15.259,24.149 12.000,24.149 C8.741,24.149 8.333,24.135 7.053,24.079 C5.777,24.022 4.905,23.816 4.141,23.521 C3.343,23.221 2.617,22.747 2.016,22.138 C1.407,21.537 0.933,20.815 0.628,20.009 C0.333,19.249 0.126,18.373 0.070,17.097 C0.014,15.817 -0.000,15.410 -0.000,12.151 C-0.000,8.893 0.014,8.485 0.075,7.200 C0.131,5.925 0.338,5.053 0.633,4.289 C0.933,3.492 1.407,2.765 2.016,2.165 C2.617,1.551 3.339,1.082 4.145,0.777 C4.905,0.482 5.782,0.275 7.057,0.219 C8.338,0.163 8.745,0.149 12.005,0.149 C15.264,0.149 15.672,0.163 16.957,0.224 C18.232,0.280 19.104,0.486 19.869,0.782 C20.666,1.082 21.393,1.555 21.993,2.165 C22.607,2.765 23.076,3.487 23.381,4.293 C23.676,5.053 23.883,5.930 23.939,7.205 C23.995,8.485 24.000,8.893 24.000,12.151 C24.000,15.410 23.986,15.817 23.930,17.097 ZM21.777,7.313 C21.726,6.145 21.529,5.508 21.365,5.086 C21.172,4.565 20.867,4.097 20.469,3.707 C20.084,3.309 19.611,3.004 19.090,2.812 C18.668,2.648 18.035,2.451 16.863,2.399 C15.601,2.343 15.222,2.329 12.014,2.329 C8.811,2.329 8.431,2.343 7.165,2.399 C5.998,2.451 5.360,2.648 4.938,2.812 C4.417,3.004 3.948,3.309 3.559,3.707 C3.156,4.092 2.851,4.565 2.659,5.086 C2.495,5.508 2.298,6.141 2.246,7.313 C2.190,8.574 2.176,8.958 2.176,12.161 C2.176,15.363 2.190,15.743 2.246,17.009 C2.298,18.176 2.495,18.813 2.659,19.235 C2.851,19.756 3.156,20.225 3.554,20.614 C3.939,21.012 4.413,21.317 4.933,21.509 C5.355,21.674 5.988,21.870 7.160,21.922 C8.422,21.978 8.806,21.992 12.009,21.992 C15.212,21.992 15.592,21.978 16.858,21.922 C18.026,21.870 18.663,21.674 19.085,21.509 C20.131,21.106 20.961,20.276 21.365,19.231 C21.529,18.809 21.726,18.176 21.777,17.004 C21.834,15.738 21.848,15.363 21.848,12.161 C21.848,8.958 21.834,8.579 21.777,7.313 ZM18.415,7.182 C17.620,7.182 16.975,6.537 16.975,5.742 C16.975,4.947 17.620,4.303 18.415,4.303 C19.210,4.303 19.855,4.947 19.855,5.742 C19.855,6.537 19.210,7.182 18.415,7.182 ZM12.005,18.316 C8.600,18.316 5.838,15.555 5.838,12.151 C5.838,8.747 8.600,5.986 12.005,5.986 C15.409,5.986 18.171,8.747 18.171,12.151 C18.171,15.555 15.409,18.316 12.005,18.316 ZM12.005,8.152 C9.796,8.152 8.005,9.943 8.005,12.151 C8.005,14.359 9.796,16.150 12.005,16.150 C14.213,16.150 16.005,14.359 16.005,12.151 C16.005,9.943 14.213,8.152 12.005,8.152 Z"/>
                                <path fill="url(#PSgrad_0)"
                                 d="M23.930,17.097 C23.873,18.373 23.667,19.245 23.372,20.009 C22.748,21.622 21.472,22.897 19.859,23.521 C19.100,23.816 18.223,24.022 16.947,24.079 C15.667,24.135 15.259,24.149 12.000,24.149 C8.741,24.149 8.333,24.135 7.053,24.079 C5.777,24.022 4.905,23.816 4.141,23.521 C3.343,23.221 2.617,22.747 2.016,22.138 C1.407,21.537 0.933,20.815 0.628,20.009 C0.333,19.249 0.126,18.373 0.070,17.097 C0.014,15.817 -0.000,15.410 -0.000,12.151 C-0.000,8.893 0.014,8.485 0.075,7.200 C0.131,5.925 0.338,5.053 0.633,4.289 C0.933,3.492 1.407,2.765 2.016,2.165 C2.617,1.551 3.339,1.082 4.145,0.777 C4.905,0.482 5.782,0.275 7.057,0.219 C8.338,0.163 8.745,0.149 12.005,0.149 C15.264,0.149 15.672,0.163 16.957,0.224 C18.232,0.280 19.104,0.486 19.869,0.782 C20.666,1.082 21.393,1.555 21.993,2.165 C22.607,2.765 23.076,3.487 23.381,4.293 C23.676,5.053 23.883,5.930 23.939,7.205 C23.995,8.485 24.000,8.893 24.000,12.151 C24.000,15.410 23.986,15.817 23.930,17.097 ZM21.777,7.313 C21.726,6.145 21.529,5.508 21.365,5.086 C21.172,4.565 20.867,4.097 20.469,3.707 C20.084,3.309 19.611,3.004 19.090,2.812 C18.668,2.648 18.035,2.451 16.863,2.399 C15.601,2.343 15.222,2.329 12.014,2.329 C8.811,2.329 8.431,2.343 7.165,2.399 C5.998,2.451 5.360,2.648 4.938,2.812 C4.417,3.004 3.948,3.309 3.559,3.707 C3.156,4.092 2.851,4.565 2.659,5.086 C2.495,5.508 2.298,6.141 2.246,7.313 C2.190,8.574 2.176,8.958 2.176,12.161 C2.176,15.363 2.190,15.743 2.246,17.009 C2.298,18.176 2.495,18.813 2.659,19.235 C2.851,19.756 3.156,20.225 3.554,20.614 C3.939,21.012 4.413,21.317 4.933,21.509 C5.355,21.674 5.988,21.870 7.160,21.922 C8.422,21.978 8.806,21.992 12.009,21.992 C15.212,21.992 15.592,21.978 16.858,21.922 C18.026,21.870 18.663,21.674 19.085,21.509 C20.131,21.106 20.961,20.276 21.365,19.231 C21.529,18.809 21.726,18.176 21.777,17.004 C21.834,15.738 21.848,15.363 21.848,12.161 C21.848,8.958 21.834,8.579 21.777,7.313 ZM18.415,7.182 C17.620,7.182 16.975,6.537 16.975,5.742 C16.975,4.947 17.620,4.303 18.415,4.303 C19.210,4.303 19.855,4.947 19.855,5.742 C19.855,6.537 19.210,7.182 18.415,7.182 ZM12.005,18.316 C8.600,18.316 5.838,15.555 5.838,12.151 C5.838,8.747 8.600,5.986 12.005,5.986 C15.409,5.986 18.171,8.747 18.171,12.151 C18.171,15.555 15.409,18.316 12.005,18.316 ZM12.005,8.152 C9.796,8.152 8.005,9.943 8.005,12.151 C8.005,14.359 9.796,16.150 12.005,16.150 C14.213,16.150 16.005,14.359 16.005,12.151 C16.005,9.943 14.213,8.152 12.005,8.152 Z"/>
                                </svg>
    					    </div>
    					    <div class="plr_followers">
    					        <span>Follow</span>
    					    </div>
    					</div></a>
    					<a href="<?= $website[0]['w_linkedinlink'] ?>" target="_blank"><div class="plr_social_flex" <?= empty($website[0]['w_linkedinlink']) ? 'hidden' : '' ?>>
    					    <div class="plr_followicon">
    					        <svg 
                                 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="25px" height="26px">
                                <path fill-rule="evenodd"  fill="rgb(69, 135, 234)"
                                 d="M19.812,25.118 L19.812,16.993 C19.812,15.055 19.781,12.586 17.125,12.586 C14.437,12.586 14.031,14.680 14.031,16.868 L14.031,25.149 L8.844,25.149 L8.844,8.461 L13.812,8.461 L13.812,10.743 L13.875,10.743 C14.562,9.430 16.250,8.055 18.781,8.055 C24.031,8.055 25.000,11.492 25.000,15.993 L25.000,25.118 L19.812,25.118 ZM3.000,6.180 C1.344,6.180 -0.000,4.805 -0.000,3.149 C-0.000,1.492 1.344,0.149 3.000,0.149 C4.656,0.149 6.000,1.492 6.000,3.149 C6.000,4.805 4.656,6.180 3.000,6.180 ZM5.594,25.149 L0.406,25.149 L0.406,8.461 L5.594,8.461 L5.594,25.149 Z"/>
                                </svg>
    					    </div>
    					    <div class="plr_followers">
    					        <span>Follow</span>
    					    </div>
    					</div>
					</div></a>
					<div class="plr_box_search_headbox">
						<h2>Your Daily Updates</h2>
					</div>
					<?php 
				    $responder = isset($autoresponder[0]['value']) ? $autoresponder[0]['value'] : '';
					if(!empty(json_decode($responder))){
					   ?>
					    <div class="plr_newsletter_box">
    						<div class="plr_newsletter_form">
    							<h5>Get Updates on All the Latest Giveaways Online and More by Subscribing Below!</h5>
    							<form class="form">
    							    	<div class="plr_input_box">
    							    	<input type="text" placeholder="Your Name Here..." name="news_name" id="news_name">
        							</div>
        							<div class="plr_input_box">
    							    	<input type="text" name="w_userid" value="<?php echo $this->uri->segment(2);?>" id="w_userid"hidden>
        								<input type="text" placeholder="Your Email Here..." name="s_email" id="news_email">
        							</div>
        							<button type="button" class="plr_btn"onclick="submitNewsletter()" >Subscribe</button>
    						    </form>
    						</div>
    					</div>
					    <?php
					}else{
					    if(empty($websiteData['customHTMLnews'])){
					        ?>
    					    <div class="plr_newsletter_box">
        						<div class="plr_newsletter_form">
        							<h5>Get Updates on All the Latest Giveaways Online and More by Subscribing Below!</h5>
        							<form class="form">
        							    	<div class="plr_input_box">
        							    	<input type="text" placeholder="Your Name Here..." name="news_name" id="news_name">
            							</div>
            							<div class="plr_input_box">
        							    	<input type="text" name="w_userid" value="<?php echo $this->uri->segment(2);?>" id="w_userid"hidden>
            								<input type="text" placeholder="Your Email Here..." name="s_email" id="news_email">
            							</div>
            							<button type="button" class="plr_btn"onclick="submitNewsletter()" >Subscribe</button>
        						    </form>
        						</div>
        					</div>
    					    <?php
					    }else{
					        echo htmlspecialchars_decode($websiteData['customHTMLnews']);
					    }
					     
					}
					?>
					<div class="plr_box_search_headbox">
    					<h2>Latest
    					<?php 
    					    if($websiteData['w_type']==2){
    				            echo "Giveaways Videos";
    				        }else if($websiteData['w_type']==6){
    				             echo "Coupons";
    				        }else if($websiteData['w_type']==5){
    				             echo "Games Giveaways";
    				        }else if($websiteData['w_type']==4){
    				             echo "Gift Cards";
    				        }else if($websiteData['w_type']==3){
    				             echo "Contests";
    				        }else if($websiteData['w_type']==1){
    				             echo "Giveaways";
    				        }
    				   ?></h2>
    				</div>
    			<?php
				        foreach($sidebarListleft as $gValue){
                             ?>
                                <div class="plr_news_box plr_article_box">
            						<div class="plr_blog_img">
            						     <?php 
            						        if(isset($gValue['g_priview']) || isset($gValue['thumbnail'])){
            						            ?>
            						            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['g_id']) ? $gValue['g_id'] : $gValue['id'] ?>"><img src="<?= isset($gValue['g_priview']) ? $gValue['g_priview'] : $gValue['thumbnail'] ?>" alt="img"></a>
            						            <?php 
            						        }else if(isset($gValue['brand_logo']) ){
            						             ?>
            						            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['id']) ? $gValue['id'] : '' ?>"><img src="<?= isset($gValue['brand_logo']) ? $gValue['brand_logo'] : '' ?>" alt="img"></a>
            						            <?php 
            						        }else{
            						             ?>
            						            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['v_id']) ? $gValue['v_id'] : '' ?>"><img src="<?= isset($gValue['v_imagelink']) ? $gValue['v_imagelink'] : '' ?>" alt="img"></a>
            						            <?php  
            						        }
            						    ?>
            						</div>
            						<div class="plr_blog_content">
            						    <?php
            						        if(isset($gValue['id'])){
            						            ?>
            						            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['id']) ? $gValue['id'] : '' ?>"><h5><?=isset($gValue['g_offer_name']) ? $gValue['g_offer_name'] : $gValue['title']?></h5></a>
            						            <?php
            						        }else if(isset($gValue['g_id'])){
            						             ?>
            						            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['g_id']) ? $gValue['g_id'] : '' ?>"><h5><?=isset($gValue['g_offer_name']) ? $gValue['g_offer_name'] : $gValue['title']?></h5></a>
            						            <?php
            						        }else{
            						             ?>
            						            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['v_id']) ? $gValue['v_id'] : '' ?>"><h5><?=isset($gValue['v_name']) ? $gValue['v_name'] : '' ?></h5></a>
            						            <?php
            						        }
            						    ?>
            						
            							<div class="plr_blog_btn">
            							    <?php
            							        if(isset($gValue['g_countries']) || isset($gValue['published_date'])){
            							            ?>
            							            <a href="javascript:;"><?=isset($gValue['g_countries'])!=''? '<svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 32 32" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m21.386 10c-1.055-4.9-3.305-8-5.386-8s-4.331 3.1-5.386 8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10 16a30.013 30.013 0 0 0 .267 4h11.466a30.013 30.013 0 0 0 .267-4 30.013 30.013 0 0 0 -.267-4h-11.466a30.013 30.013 0 0 0 -.267 4z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10.614 22c1.055 4.9 3.305 8 5.386 8s4.331-3.1 5.386-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m23.434 10h6.3a15.058 15.058 0 0 0 -10.449-8.626c1.897 1.669 3.385 4.755 4.149 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m30.453 12h-6.7a32.332 32.332 0 0 1 .247 4 32.332 32.332 0 0 1 -.248 4h6.7a14.9 14.9 0 0 0 0-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m19.285 30.626a15.058 15.058 0 0 0 10.451-8.626h-6.3c-.766 3.871-2.254 6.957-4.151 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8.566 22h-6.3a15.058 15.058 0 0 0 10.451 8.626c-1.899-1.669-3.387-4.755-4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m12.715 1.374a15.058 15.058 0 0 0 -10.451 8.626h6.3c.766-3.871 2.254-6.957 4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8 16a32.332 32.332 0 0 1 .248-4h-6.7a14.9 14.9 0 0 0 0 8h6.7a32.332 32.332 0 0 1 -.248-4z" fill="currentColor" data-original="#000000"></path></g></svg>'.$gValue['g_countries'] : '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><g xmlns="http://www.w3.org/2000/svg" id="e"><path d="m512 363.78c-1.2-66.8-9.09-134.35-22.03-202.53-10.54-47.37-48.46-89.56-109.05-92.65-44.73-1.84-53.38 23.64-104.06 23.15-13.88-.09-27.75-.09-41.63 0-50.69.49-59.36-24.99-104.07-23.15-60.6 3.09-99.7 45.17-109.09 92.65-12.95 68.18-20.84 135.72-22.03 202.52-.29 46.51 45.63 77.45 75.93 79.57 58.53 4.42 105.03-98.79 140.46-98.8 26.41.15 52.81.16 79.22 0 35.44 0 81.9 103.23 140.47 98.81 30.29-2.12 77.4-33.27 75.89-79.57zm-321.06-130.34h-27.3v27.3c0 11.52-9.34 20.86-20.86 20.86s-20.86-9.34-20.86-20.86v-27.3h-27.3c-11.52 0-20.86-9.34-20.86-20.86s9.34-20.86 20.86-20.86h27.3v-27.3c0-11.52 9.34-20.86 20.86-20.86s20.86 9.34 20.86 20.86v27.3h27.3c11.52 0 20.86 9.34 20.86 20.86s-9.34 20.86-20.86 20.86zm168.57 48.15c-16.33.44-29.91-12.46-30.35-28.78-.43-16.38 12.48-29.98 28.8-30.4 16.34-.41 29.94 12.48 30.36 28.82.41 16.34-12.48 29.94-28.81 30.36zm49.25-78.85c-16.33.46-29.94-12.45-30.39-28.78-.41-16.36 12.48-29.94 28.82-30.39 16.36-.43 29.94 12.48 30.38 28.82.43 16.33-12.49 29.94-28.81 30.35z" fill="currentColor" data-original="#000000"></path></g></g></svg>'.$gValue['type'] ?></a>
                    								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                    <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                    d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                    </svg><?=isset($gValue['g_last_update']) ? $gValue['g_last_update'] : date("M d, Y", strtotime($gValue['published_date'])) ?></span>
            							            <?php
            							        }else if(isset($gValue['end_date']) || isset($gValue['type'])){
            							            ?>
            							            <a href="javascript:;"><?=isset($gValue['g_countries'])!=''? '<svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 32 32" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m21.386 10c-1.055-4.9-3.305-8-5.386-8s-4.331 3.1-5.386 8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10 16a30.013 30.013 0 0 0 .267 4h11.466a30.013 30.013 0 0 0 .267-4 30.013 30.013 0 0 0 -.267-4h-11.466a30.013 30.013 0 0 0 -.267 4z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10.614 22c1.055 4.9 3.305 8 5.386 8s4.331-3.1 5.386-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m23.434 10h6.3a15.058 15.058 0 0 0 -10.449-8.626c1.897 1.669 3.385 4.755 4.149 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m30.453 12h-6.7a32.332 32.332 0 0 1 .247 4 32.332 32.332 0 0 1 -.248 4h6.7a14.9 14.9 0 0 0 0-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m19.285 30.626a15.058 15.058 0 0 0 10.451-8.626h-6.3c-.766 3.871-2.254 6.957-4.151 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8.566 22h-6.3a15.058 15.058 0 0 0 10.451 8.626c-1.899-1.669-3.387-4.755-4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m12.715 1.374a15.058 15.058 0 0 0 -10.451 8.626h6.3c.766-3.871 2.254-6.957 4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8 16a32.332 32.332 0 0 1 .248-4h-6.7a14.9 14.9 0 0 0 0 8h6.7a32.332 32.332 0 0 1 -.248-4z" fill="currentColor" data-original="#000000"></path></g></svg>'.$gValue['g_countries'] : '<svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 32 32" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m21.386 10c-1.055-4.9-3.305-8-5.386-8s-4.331 3.1-5.386 8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10 16a30.013 30.013 0 0 0 .267 4h11.466a30.013 30.013 0 0 0 .267-4 30.013 30.013 0 0 0 -.267-4h-11.466a30.013 30.013 0 0 0 -.267 4z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10.614 22c1.055 4.9 3.305 8 5.386 8s4.331-3.1 5.386-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m23.434 10h6.3a15.058 15.058 0 0 0 -10.449-8.626c1.897 1.669 3.385 4.755 4.149 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m30.453 12h-6.7a32.332 32.332 0 0 1 .247 4 32.332 32.332 0 0 1 -.248 4h6.7a14.9 14.9 0 0 0 0-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m19.285 30.626a15.058 15.058 0 0 0 10.451-8.626h-6.3c-.766 3.871-2.254 6.957-4.151 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8.566 22h-6.3a15.058 15.058 0 0 0 10.451 8.626c-1.899-1.669-3.387-4.755-4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m12.715 1.374a15.058 15.058 0 0 0 -10.451 8.626h6.3c.766-3.871 2.254-6.957 4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8 16a32.332 32.332 0 0 1 .248-4h-6.7a14.9 14.9 0 0 0 0 8h6.7a32.332 32.332 0 0 1 -.248-4z" fill="currentColor" data-original="#000000"></path></g></svg>'.$gValue['type'] ?></a>
                    								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                    <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                    d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                    </svg><?=isset($gValue['end_date']) && !empty($gValue['end_date']) ? date("M d, Y", strtotime($gValue['end_date'])) : 'Life Time'?></span>
            							            <?php
            							        }else{
            							            
            							        }
            							    ?>
            							    
            							</div>
            						</div> 
            					</div>
                             <?php
                        }
    				?>
					<div class="plr_addimg_box">
					    <a href="<?= isset($ads[0]['home_page_sidebar_ad'])? $ads[0]['home_page_sidebar_ad'] :''?>" target="_blank"><img src="<?= isset($ads[0]['home_page_sidebar_ink'])? $ads[0]['home_page_sidebar_ink'] : base_url()."assets/frontend/images/add2.jpg" ?>"  alt="add-img"></a>
					</div>
					<?php if(!empty($adsArray) && $adsArray['sidebar_image'] != ''){ ?>
					<div class="plr_addimg_box">
						<a href="<?= $adsArray['sidebar_url'] ?>" target="_blank"><img src="<?= $adsArray['sidebar_image'] ?>" alt="add-img"></a>
					</div>
					<?php } ?>
					<div class="plr_box_search_headbox">
    					<h2>Hot & Fresh
    					<?php 
    					    if($websiteData['w_type']==2){
    				            echo "Giveaways Videos";
    				        }else if($websiteData['w_type']==6){
    				             echo "Coupons";
    				        }else if($websiteData['w_type']==5){
    				             echo "Games Giveaways";
    				        }else if($websiteData['w_type']==4){
    				             echo "Gift Cards";
    				        }else if($websiteData['w_type']==3){
    				             echo "Contests";
    				        }else if($websiteData['w_type']==1){
    				             echo "Giveaways";
    				        }
    				   ?></h2>
    				</div>
    				<div class="plr_social_follow">
    				    <?php
        				    foreach($sidebarListleft as $gValue){
                             ?>
                				<div class="plr_news_box plr_fresh_box">
            						<div class="plr_blog_img">
            						    <?php 
            						        if(isset($gValue['g_priview']) || isset($gValue['thumbnail'])){
            						            ?>
            						            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['g_id']) ? $gValue['g_id'] : $gValue['id'] ?>"><img src="<?= isset($gValue['g_priview']) ? $gValue['g_priview'] : $gValue['thumbnail'] ?>" alt="img"></a>
            						            <?php 
            						        }else if(isset($gValue['id'])){
            						             ?>
            						            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['id']) ? $gValue['id'] : '' ?>"><img src="<?= isset($gValue['brand_logo']) ? $gValue['brand_logo'] : '' ?>" alt="img"></a>
            						            <?php 
            						        }else{
            						            ?>
            						            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['v_id']) ? $gValue['v_id'] : '' ?>"><img src="<?= isset($gValue['v_imagelink']) ? $gValue['v_imagelink'] : '' ?>" alt="img"></a>
            						            <?php  
            						        }
            						    ?>
            							
            						</div>
            						<div class="plr_blog_content">
            						     <?php 
            						        if(isset($gValue['g_offer_name'])){
            						            ?>
            						            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=$gValue['g_id']?>"><h5><?=isset($gValue['g_offer_name']) ? $gValue['g_offer_name'] : $gValue['title']?></h5></a>
            						            <?php 
            						        }else if(isset($gValue['title'])){
            						             ?>
            						             <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($gValue['s_id']) ? $gValue['g_id'] : '' ?>"><h5><?=isset($gValue['title']) ? $gValue['title'] : '' ?></h5></a>
            						            <?php 
            						        }else{
            						             ?>
            						             <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=$gValue['v_id']?>"><h5><?=isset($gValue['v_name']) ? $gValue['v_name'] : '' ?></h5></a>
            						            <?php 
            						        }
            						    ?>
            						</div>
            					</div>
            					<?php
        				    }
    				     ?>
    					
					</div>
				</div>
			</div>
			<!--===Sidebar Section End===-->