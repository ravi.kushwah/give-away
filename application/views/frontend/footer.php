<?= isset($compliance_text) ? $compliance_text : '' ?>
<!--=== footer Start===-->
	<div class="plr_footer_section">
			<div class="container">
				<div class="plr_address_main">
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<div class="plr_footer_box">
								<a href="<?= $this->session->userdata['web_url'] ?>">
								<?php if($websiteData['w_logourl'] != '') { ?>
									<img src="<?= base_url() ?>assets/webupload/<?= $websiteData['w_logourl'] ?>" alt="<?= $websiteData['w_title'] ?>"/>
								<?php } elseif( $websiteData['w_logotext'] != '' ) { ?>
									<p style="color:<?= $websiteData['w_logocolor'] ?>; font-family:<?= $websiteData['w_logofont'] ?>"><?= $websiteData['w_logotext'] ?></p>
								<?php } else { ?>
									<img src="<?= base_url() ?>assets/frontend/images/logo.png" alt="<?= $websiteData['w_title'] ?>"/>
								<?php } ?>
								
								</a>
							    <p><?php
								if(isset($websiteData['w_footertext'])!=""){
								    echo $websiteData['w_footertext'];
								}else{
								    ?>
								    GiveSites Pro features the latest online sweepstakes, contests and giveaways. Join the best online community to win cash, gifts and other prizes of your dreams. Sign up for our email newsletter to receive the latest giveaway updates. Happy Winning!
								    <?php
								}
								
								?></p>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="plr_footer_box">
								<div class="plr_product_detailhead">
        							<h4>
        							    <?php 
        							        if($websiteData['w_type']==2){
        							            echo "GiveAway Videos";
        							        }else if($websiteData['w_type']==6){
        							             echo "Coupons";
        							        }else if($websiteData['w_type']==5){
        							             echo "Games GiveAways";
        							        }else if($websiteData['w_type']==4){
        							             echo "Gift Cards";
        							        }else if($websiteData['w_type']==3){
        							             echo "Contests";
        							        }else if($websiteData['w_type']==1){
        							             echo "GiveAway";
        							        }
        							    ?>
    							    </h4>
        						</div>
        						<div class="plr_footer_list">
        						     <ul>
        						    <?php
                        			foreach($footerGiveaways as $value){
                				           if(isset($value['g_offer_name'])){
                                                ?>
                                                <li><a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['g_id']) ? $value['g_id'] : $value['id'] ?>"><h5><?=isset($value['g_offer_name']) ? $value['g_offer_name'] : $value['title']?></h5></a></li>
                                                <?php
                                            }else if(isset($value['title'])){
                                                 ?>
                                                 <li><a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['id']) ? $value['id'] : ''?>"><h5><?=isset($value['title']) ? $value['title'] : '' ?></h5></a></li>
                                                <?php
                                            }else{
                                                ?>
                                                <li><a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['v_id']) ? $value['v_id'] : ''?>"><h5><?=isset($value['v_name']) ? $value['v_name'] : '' ?></h5></a></li>
                                                <?php
                                            }
                				        }
                				    ?>
        						    </ul>
        						</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="plr_footer_box">
								<div class="plr_product_detailhead">
        							<h4>Your Daily Updates</h4>
        						</div>
        						<p>Get Updates on All the Latest Giveaways Online and More by Subscribing Below!</p>
        						<form class="form">
            						<div class="plr_newsletter">
        						    	<input type="text" name="w_userid" value="<?php echo $this->uri->segment(2);?>" id="w_userid"hidden>
        						    	<input type="text" value="Test" name="news_name" hidden>
    									<input type="text" placeholder="Enter Your Email..." name="news_email">
    									<button type="button" class="Subscribe">Subscribe</button>
    								</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="plr_footer_mini">
				<div class="container">
					<div class="plr_footer_btm">
						<p>&copy;<?= date('Y') ?>, <?= $websiteData['w_title'] ?>, All Rights Reserved</p>
						<ul>
							<li><a href="<?= $this->session->userdata['web_url'].'/p/terms-and-conditions' ?>">Terms & conditions</a></li>
							<li><a href="<?= $this->session->userdata['web_url'].'/p/privacy-policy' ?>">Privacy policy</a></li>
							<li><a href="<?= $this->session->userdata['web_url'].'/p/legal-disclaimer' ?>">Legal Disclaimer</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--=== footer End===-->
		
		<!--===Bottom To Top===-->
		<?php 
		    if(isset($_SESSION['is_fe'])==1 && $_SESSION['is_oto1'] == 0 && $_SESSION['is_oto2'] == 0 && $_SESSION['is_oto3'] == 0 && $_SESSION['is_oto4'] == 0 && $_SESSION['is_oto5'] == 0 && $_SESSION['is_oto6'] == 0 && $_SESSION['is_oto7'] == 0 && $_SESSION['is_oto8'] == 0 ){
		       if($_SESSION['email']!='tonysornat@gmail.com'){
		        ?>
		        <div class="plr_top_icon">
        			<div class="plr_give_logo">
        			    <h6>Powered By -</h6>
        				<a href="https://warriorplus.com/o2/v/dg3cvx/h7yh8k/badge" target="_blank"><img src="<?= base_url() ?>assets/frontend/images/logo_dark.png" alt=""></a>
        			</div>
        		</div>
		        <?php 
		    }
		         
	       }
		
		?>
		<!--===Bottom To Top===-->
		
	</div>
	<!--=== End Main wraapper ===-->

	<!--=== Payment Modal ===-->
	<div class="modal fade" id="PaymentModal" tabindex="-1" aria-labelledby="PaymentModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="plr_login_main">
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="plr_login_auth">
						<h1>Choose a payment method which suits you!</h1>
						<div class="plr_login_form">
							<div class="login-content">
								<div class="plr_input_main">
									<div class="plr_input">
										<select id="choose_payment">
											<?php if( !empty($paymentSettings) ) {
												foreach($paymentSettings as $soloPayment) {
													echo '<option value="'.$soloPayment['payment_type'].'">'.strtoupper($soloPayment['payment_type']).'</option>';
												}
											} ?>
										</select>
									</div>
								</div>
							</div>
							<div class="plr_login_btn">
							    <input type="hidden" name="w_id" value="<?= $websiteData['w_id'] ?>">
							    <a href="javascript:;" class="plr_btn" id="planID" data-planID="0">Continue</a> 
							</div>
						</div>
					</div>
				  </form>
				</div>
			</div>
		</div>
  	</div>
	<!--=== Payment Modal ===-->

	<!--=== Login Modal ===-->
	<div class="modal fade" id="LoginModal" tabindex="-1" aria-labelledby="LoginModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="plr_login_main">
				   <form method="post" action="<?= base_url() ?>authenticate/getCustomerLogin"  id="login_form">
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="plr_login_auth">
						<h1>Hello!<br>Introducing <span><?= $websiteData['w_title'] ?></span></h1>
						<h5>Welcome Back, Please login to your account.</h5>
						<div class="plr_login_form">
							<div class="login-content">
								<form action="">
									<div class="plr_input_main">
										<label>Email Address</label>
										<div class="plr_input">
											<input type="text" placeholder="Enter Your Email"  value="<?= isset($_COOKIE['c_emanu']) ? $_COOKIE['c_emanu'] : '' ?>" name="auth_email" class="required" data-valid="email"/>
											<img src="<?= base_url() ?>assets/frontend/images/msg.svg" alt=""/>
										</div>
										<label>Password</label>
										<div class="plr_input">
											<input type="password" placeholder="Enter Your Password" value="<?= isset($_COOKIE['c_dwp']) ? $_COOKIE['c_dwp'] : '' ?>"  name="auth_password" class=" required" data-valid="password"/>
											<img src="<?= base_url() ?>assets/frontend/images/password.svg" alt=""/>
										</div>
									</div>
								</form>
							</div>
							<div class="plr_check_section">
								<ul>
									<li>
										<div class="plr_checkbox">
										  <input type="checkbox" id="auth_remember" <?= isset($_COOKIE['c_emanu']) ? 'checked' : '' ?> name="auth_remember" value="1"> 
										  <label for="auth_remember">remember me</label>
										</div>
										<span><a href="#Forgot" data-bs-toggle="modal" data-bs-target="#Forgot">forgot password?</a></span>
									</li>
								</ul>	
							</div>
							<div class="plr_login_btn">
							    <input type="hidden" name="w_id" value="<?php echo $websiteData['w_id']; ?>">
							    <a href="javascript:;" class="plr_btn submitFormValid" >login now</a> 
							</div>
						</div>
					</div>
				  </form>
				</div>
			</div>
		</div>
  	</div>
	<!--=== Login Modal ===-->

	<!--=== Signup Modal ===-->
	<div class="modal fade" id="Signup" tabindex="-1" aria-labelledby="Signup" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="plr_login_main">
				   <form method="post" action="<?= base_url() ?>authenticate/getCustomerSignUp"  id="signup_form" autocomplete="off">
				    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="plr_login_auth">
						<h1>Welcome Back to <span><?= $websiteData['w_title'] ?></span></h1>
						<h5>Please Register To Continue your account.</h5>
						<div class="plr_login_form">
							<div class="login-content">
								<form action="">
									<div class="plr_input_main">
										<label>Name</label>
										<div class="plr_input">
											<input type="text"  placeholder="Enter Your Name" class="input required" name="auth_name">
											<img src="<?= base_url() ?>assets/frontend/images/user.svg" alt=""/>
										</div>
										<label>Email Address</label>
										<div class="plr_input">
											<input type="text" placeholder="Enter Your Email"  value="" name="auth_email" class="required" data-valid="email"/>
											<img src="<?= base_url() ?>assets/frontend/images/msg.svg" alt=""/>
										</div>
										<label>Password</label>
										<div class="plr_input">
											
											<input type="password"  placeholder="Enter Your password"class="input required" name="auth_pass">
											<img src="<?= base_url() ?>assets/frontend/images/password.svg" alt=""/>
										</div>
									</div>
								</form>
							</div>
							<div class="plr_login_btn">
							    <input type="hidden" name="w_id" value="<?php echo $websiteData['w_id']; ?>">
								<a href="javascript:;" class="plr_btn submitFormValid" >sign up</a> 
							</div>
						</div>
					</div>
				   </form>
					
				</div>
			</div>
		</div>
  	</div>
	<!--=== Signup Modal ===-->

	<!--=== Forgot Modal ===-->
	<div class="modal fade" id="Forgot" tabindex="-1" aria-labelledby="Forgot" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="plr_login_main">
				<form method="post" action="<?php echo base_url()?>authenticate/getCustomerForgetPassword"  id="forget_form">
				    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="plr_login_auth">
						<h1>Welcome Back to <span><?= $websiteData['w_title'] ?></span></h1>
						<h5>Enter your email and we will send you a reset link.</h5>
						<div class="plr_login_form">
							<div class="login-content">
								<form action="">
									<div class="plr_input_main">
										<label>Email Address</label>
										<div class="plr_input">
											
											<input type="text"   name="auth_email" class="input required"  placeholder="Enter Your Email"data-valid="email"/>
											<img src="<?= base_url() ?>assets/frontend/images/msg.svg" alt=""/>
										</div>
									</div>
								</form>
							</div>
							<div class="plr_login_btn">
								<input type="hidden" name="w_id" value="<?php echo $websiteData['w_id']; ?>">
								<a href="javascript:;" class="plr_btn submitFormValid" >send password</a> 
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
  	</div>
	<!--=== Forgot Modal ===-->
	
	<!--=== Payment Modal ===-->
	<div class="modal fade" id="PricePaymentModal" tabindex="-1" aria-labelledby="paymentModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="plr_login_main">
				   <!--<form method="post" action="<?= base_url() ?>authenticate/getCustomerLogin"  id="login_form">-->
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="plr_login_auth">
						<h1>Select Payment Mehod</h1>
						<div class="plr_login_form">
						    <input type="hidden" name="planType" data-plan="">
							<div class="ss_payment_mod_box">
                    			<ul>
                    				<li>
                    					<a href="javascript:void(0);" class="payment_mod_paypal payment_btn" data-type="1" style="background-color: #fff">
                    						<img src="<?=base_url('assets/frontend/images/paypal.png')?>" alt="">
                    					</a>
                    				</li>
                    				<li>
                    					<a href="javascript:void(0);" class="payment_mod_stripe payment_btn" data-type="2" style="background-color: #fff">
                    						<img src="<?=base_url('assets/frontend/images/stripe.png')?>" alt="">
                    					</a>
                    				</li>
                    			</ul>
                    		</div>
						</div>
					</div>
				  <!--</form>-->
				</div>
			</div>
		</div>
  	</div>
	<!--=== Payment Modal ===-->
	
	<!--=== Payment Modal ===-->
	<div class="modal fade" id="Giveaway" tabindex="-1" aria-labelledby="Giveaway" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="plr_login_main">
				   <!--<form method="post" action="<?= base_url() ?>authenticate/getCustomerLogin"  id="login_form">-->
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="plr_login_auth">
						<h1>Send Your Request</h1>
						<div class="plr_login_form">
							<div class="login-content">
		    					<?php
		    					if(isset($submit_giveaway) && !empty($submit_giveaway)){
							         	$prodname =isset($submit_giveaway[0]['g_offer_name']) ? $submit_giveaway[0]['g_offer_name'] : '' ;
							      }else{
							         if(isset($prodList[0]['g_offer_name'])){
							         $prodname=isset($prodList[0]['g_offer_name']) ? $prodList[0]['g_offer_name'] : '';
								     }else if(isset($prodList[0]['title'])){
								       $prodname=isset($prodList[0]['title']) ? $prodList[0]['title'] : '';
								     }else{
								        $prodname=isset($prodList[0]['v_name']) ? $prodList[0]['v_name'] : '';
							         }
							      }
						           
						       
		    					$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		    					  $responder = isset($autoresponder[0]['value']) ? $autoresponder[0]['value'] : '';
                					if(!empty(json_decode($responder))){
                					   ?>
        								<form action=""class="form">
        									<div class="plr_input_main">
        										<label>Name</label>
        										<div class="plr_input">
        											<input type="text"  placeholder="Enter Your Name" class="input required" id="l_name" name="l_name">
        											<img src="<?= base_url() ?>assets/frontend/images/user.svg" alt=""/>
        										</div>
        										<label>Email Address</label>
        										<div class="plr_input">
        											<input type="text" placeholder="Enter Your Email"  value="" id="l_email" name="l_email" class="required" data-valid="email"/>
        											<img src="<?= base_url() ?>assets/frontend/images/msg.svg" alt=""/>
        										</div>
        									</div>
        									<div class="plr_login_btn">
        									    <input type="text" name="w_userid" value="<?php echo $this->uri->segment(2);?>" id="w_userid"hidden>
            							    	<input type="hidden" name="l_webid" value="<?php echo $websiteData['w_id']; ?>">
            							    	<input type="hidden" name="giveaway_link" value="<?php echo $actual_link; ?>">
        							    		<input type="hidden" name="giveaway_name" value="<?php echo $prodname; ?>">
            							    	<input type="hidden" name="giveaways_id" value="<?php echo $this->uri->segment(4);?>"> 
                								<button type="button" class="plr_btn submitLeadForm" >Submit</button> 
                							</div>
        								</form>
        								<?php
                					}else{
                					    if(!empty($websiteData['customHTMLgiveaways'])){
                					         echo htmlspecialchars_decode($websiteData['customHTMLgiveaways']);
                					    }else{
                					        ?>
                					        	<form action=""class="form">
                									<div class="plr_input_main">
                										<label>Name</label>
                										<div class="plr_input">
                											<input type="text"  placeholder="Enter Your Name" class="input required" id="l_name" name="l_name">
                											<img src="<?= base_url() ?>assets/frontend/images/user.svg" alt=""/>
                										</div>
                										<label>Email Address</label>
                										<div class="plr_input">
                											<input type="text" placeholder="Enter Your Email"  value="" id="l_email" name="l_email" class="required" data-valid="email"/>
                											<img src="<?= base_url() ?>assets/frontend/images/msg.svg" alt=""/>
                										</div>
                									</div>
                									<div class="plr_login_btn">
                									    <input type="text" name="w_userid" value="<?php echo $this->uri->segment(2);?>" id="w_userid"hidden>
                    							    	<input type="hidden" name="l_webid" value="<?php echo $websiteData['w_id']; ?>">
                    							    		<input type="hidden" name="giveaway_link" value="<?php echo $actual_link; ?>">
                    							    		<input type="hidden" name="giveaway_name" value="<?php echo $prodname; ?>">
                    							    		<input type="hidden" name="giveaways_id" value="<?php echo $this->uri->segment(4);?>"> 
                        								<button type="button" class="plr_btn submitLeadForm">Submit</button> 
                        							</div>
                								</form>
                					        <?php 
                					    }
                					    
                					}
                					?>
							</div>
							
						</div>
					</div>
				  <!--</form>-->
				</div>
			</div>
		</div>
  	</div>
	<!--=== Payment Modal ===-->

    <!--===Notification model===-->
	<div class="plr_notification">
		<div class="plr_close_icon">
			<span class='close'>×</span>
		</div>
		<div class="plr_success_flex">
			<div class="plr_happy_img">
				<img src=""/>
			</div>
			<div class="plr_yeah">
				<h5></h5>
				<p></p>
			</div>
		</div>
	</div>
	<!--===Notification model===-->
		
    <!--=== Optional JavaScript ===-->
    <script>window.baseurl = "<?= base_url() ?>"</script>
    <script>window.siteurl = "<?= $websiteData['w_siteurl'] ?>"</script>
	<script src="<?= base_url() ?>assets/frontend/js/jquery.min.js"></script>
	<script src="<?= base_url() ?>assets/frontend/js/bootstrap.bundle.min.js"></script>
	<script src="<?= base_url() ?>assets/frontend/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/frontend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/frontend/js/custom.js?q=<?= date('his') ?>"></script>
	<!--=== Optional JavaScript ===-->
  </body>
</html>