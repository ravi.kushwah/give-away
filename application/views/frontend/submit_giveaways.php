
<!--===Header Section End===-->
<!--===Products Section Start===-->
		<div class="plr_product_gallery">
			<div class="container">
				<div class="row">
					<div class="col-xl-9 col-lg-9 col-md-12 plr_product_box_list">
    				<form class="form"><div class="row">
        					<div class="col-xl-12">
        						<div class="plr_product_single_add">
        						    <a href="<?= isset($ads[0]['single_page_top_ad'])? $ads[0]['single_page_top_ad'] :''?>" target="_blank"><img src="<?= isset($ads[0]['single_page_top_link'])? $ads[0]['single_page_top_link'] : base_url()."assets/frontend/images/add2.jpg" ?>"  alt="add-img"></a>
        						</div>
        						<div class="plr_box_search_headbox">
        							<h2>Submit GiveAway</h2>
        						</div>
        					</div>	
    						<div class="col-lg-12">
    							<div class="plr_submit_giveaway">
    								<div class="plr_blog_content">
    									<div class="plr_social_icons">
    										
    									</div>
    									<div class="plr_payout_box">
    										<div class="plr_description_text">
        								        <p>Hello there! <?= $websiteData['w_title'] ?> is the best place to promote your giveaways, sweepstakes or contest online. Just submit your giveaway using the form below and we will promote it to our huge tribe of giveaway entrants.</p>
    						                </div>
    									</div>
    									<div class="plr_des">
    										<div class="plr_description_text">
        								        <p><span>Giveaway Guidelines:</span> 
        						                    Giveaway must be live upon submission, must have a specific end date, does not contain adult or illegal items, not a duplicate of a giveaway that is already posted, and must have a clear eligibility & end date information.
        						                </p>
        						                <p>
        						                    All listings are manually approved and we reserve the right to modify the formatting of your listings to adapt the style of the site or reject any listing that may not suit the interests of our readers or does not comply with our guidelines.
        						                </p>
    						                </div>
    									</div>
        								<div class="plr_newsletter_form">
        								    <div class="row">
        								        <div class="col-lg-12 col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Giveaway Name</label>
                										<input type="text" placeholder="Enter Giveaway Name" name="g_offer_name" id="g_offer_name">
                									</div>
        								        </div>
        								        <div class="col-lg-6 col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Giveaway URL</label>
                										<input type="text" placeholder="Enter Giveaway URL" name="give_away_url" id="give_away_url">
                									</div>
        								        </div>
        								        <div class="col-lg-6 col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Giveaway Main Prize</label>
                										<input type="text" placeholder="Enter Prize" name="give_away__main_prize" id="give_away__main_prize">
                									</div>
        								        </div>
        								    </div>
        								</div>
            							<div class="plr_des">
    										<div class="plr_description_text">
        						                <p>
        						                    Provide the name of your main prize. This will be used as your listing title so make it as short as possible.
        						                </p>
    						                </div>
    									</div>
    									<div class="plr_newsletter_form">
        								    <div class="row">
        								        <div class="col-lg-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Prize Description</label>
                										<textarea placeholder="Enter to win" name="prize_description" id="prize_description"></textarea>
                									</div>
        								        </div>
        								    </div>
        								</div>
        								<div class="plr_des">
    										<div class="plr_description_text">
        						                <p>
        						                    Provide a detailed description of what you are giving away. You can include other prizes and the details (i.e. retail values), number of winners, and other important details.
        						                </p>
    						                </div>
    									</div>
    									<div class="plr_newsletter_form">
        								    <div class="row">
        								        <div class="col-lg-12 col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label for="date">End Date</label>
                										<input type="date" class="form-control end_date"  name="end_date" id="datepicker"/>
                									</div>
                									
        								        </div>
        								        <div class="col-md-12">
        								            <div class="plr_checkbox_section">
            								            <div class="plr_checkbox">
                    										<input type="radio" id="rememberme1" name="country" class="country" value="Canada">
                    										<label for="rememberme1">Canada</label>
                    									</div>
                    									<div class="plr_checkbox">
                    										<input type="radio" id="rememberme2" name="country" class="country" value="UK">
                    										<label for="rememberme2">UK</label>
                    									</div>
                    									<div class="plr_checkbox">
                    										<input type="radio" id="rememberme3" name="country" class="country" value="USA">
                    										<label for="rememberme3">USA</label>
                    									</div>
                    									<div class="plr_checkbox">
                    										<input type="radio" id="rememberme4" name="country" class="country" value="OtherLocation">
                    										<label for="rememberme4">Other Location</label>
                    									</div>
                    									<div class="plr_checkbox">
                    										<input type="radio" id="rememberme5" name="country"class="country" value="Worldwide">
                    										<label for="rememberme5">Worldwide</label>
                    									</div>
                    								</div>	
        								        </div>
        								        <div class="col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Giveaway Category</label>
                									    <select class="form-select" id="give_away_categories"name="give_away_categories">
                                                            <?php foreach($categories as $soloCate) { 
                                                                echo '<option value="'.$soloCate['cate_name'].'">'.$soloCate['cate_name'].'</option>';
                                                            } ?>
                                                        </select>
                									</div>
        								        </div>
        								        <div class="col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Giveaway Image</label>
                										<input type="file" id="give_away_image" name="give_away_image">
                										<p class="file_box">PNG, JPG or JPEG files only. Less than 1MB.</p>
                									</div>
        								        </div>
        								        <div class="col-md-12">
            								        <div class="plr_des">
                										<div class="plr_description_text">
                    						                <p>
                    						                    Your personal data will be used for contact purposes only and will not be displayed on your listing. We will manually reach to you once you submit your giveaway.
                    						                </p>
                						                </div>
                									</div>
            									</div>
        								        <div class="col-lg-6 col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Name</label>
                										<input type="text" placeholder="Enter Name" id="name"name="name">
                									</div>
        								        </div>
        								        <div class="col-lg-6 col-md-12">
        								            <div class="plr_input_box form-group plr_require">
                									    <label>Email</label>
                										<input type="email" placeholder="Enter Email" id="email"name="email">
                									</div>
        								        </div>
        								    </div>
        								</div>
    									
						                <div class="plr_giveaway_btn">
					                    	<input type="text" placeholder="Enter Name" id="" name="siteName" value="<?php echo  $this->uri->segment(2);?>" hidden>
											<a href="javascript:;" class="plr_btn submitGiveaways">Submit Giveaway</a>
										</div>
    								</div>
    							</div>
    						</div>
    					</div></form>
					</div>
					<!--===Sidebar Section Start===-->
					<?php include('sidebar.php'); ?>
					<!--===Sidebar Section End===-->
				</div>
			</div>
		</div>
		<!--===Products Section End===-->
  

