<div class="plr_banner_section" style="background: url(<?= base_url()?>assets/common/banners/<?= $websiteData['banner_image'] == '' ? $cateBanner : $websiteData['banner_image'] ?>.png) , -webkit-linear-gradient(-79deg, var(--pink-color1) 34%, var(--pink-color2) 100%); background-repeat: no-repeat;  background-position: right;background-size: auto;">
		<div class="container">
			<div class="plr_banner_heading">
				<h5><?= $websiteData['banner_headline'] == '' ? $websiteData['w_title'] : $websiteData['banner_headline'] ?></h5>
				<h1><?= $websiteData['banner_text'] ?></h1>
				<!-- <div class="plr_banner_btn">
					<a href="javascript:;" class="plr_btn">View Products</a>
				</div> -->
			</div>
		</div>
	</div>
</div>
<!--===Header Section End===-->

<!--===Products Section Start===-->
<div class="plr_product_gallery">
	<div class="container">
		<div class="row">
			<div class="col-xl-9 col-lg-8 col-md-7 plr_product_box_list">
				<div class="plr_box_search_headbox">
					<div class="plr_main_heading">
						<h2><?= $cateName ?> Products</h2>
					</div>
					<!--<div class="plr_search_box">
						<input type="text" placeholder="Search Here">
					</div>-->
				</div>
                <?php if(!empty($prodList)) { ?>
				<div class="row" id="showProduct">
				<?php foreach($prodList as $soloProd) { ?>
					<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
						<div class="plr_pro_box plr_animation_box">
							<a href="<?= count($checkURL) == 2 ? base_url('plr/'.$websiteData['w_siteurl'].'/'.$soloProd['p_urlname']) : $this->session->userdata['web_url'].'/pages/'.$soloProd['p_urlname'] ?>">
								<div class="grid_img">
									<span class="plr_product_list_img plr_animation">
										<img src="<?= $soloProd['p_imagelink'] ?>" class="plr_animation_img" alt="<?= $soloProd['p_name'] ?>">
									</span>
								</div>
							</a>	
							<div class="bottom_content">
								<h5><a href="javascript:;"><?= $soloProd['p_name'] ?></a></h5>
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
				<div class="plr_pagination">
					<ul>
						<li><a href="javascript:;" onclick="getOtherProducts('first',<?= $cateId ?>)"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
							<path class="cls-1" d="M120.193,2325.53l3.331-3.34a0.676,0.676,0,0,1,.953.96l-2.856,2.85,2.856,2.86a0.672,0.672,0,0,1,0,.95,0.68,0.68,0,0,1-.953,0l-3.331-3.33a0.672,0.672,0,0,1,0-.95h0Zm4-.21,2.647-2.78a0.656,0.656,0,0,1,.962,0,0.741,0.741,0,0,1,0,1.01l-2.166,2.28,2.166,2.28a0.754,0.754,0,0,1,0,1.02,0.656,0.656,0,0,1-.962,0l-2.647-2.79a0.754,0.754,0,0,1,0-1.02h0Zm0,0" transform="translate(-120 -2322)"/>
							</svg>
							</a></li>
						<?php for($i=1;$i<$numberOfPages+1;$i++) { ?>
							<?php if( $i < 4 || $i > $numberOfPages - 3 ) { ?>
							<li class="changePage"><a href="javascript:;" <?= $i == 1 ? 'class="pagination_a active"' : 'class="pagination_a"' ?> onclick="getNextProducts(<?= $i ?>,<?= $cateId ?>)" data-pagenum="<?= $i ?>"><?= $i ?></a></li>
							<?php } if( $i == $numberOfPages - 4 ) { ?>
								<span class="changePage">...</span>
							<?php } ?>
						<?php } ?>
						<li><a href="javascript:;" onclick="getOtherProducts('last',<?= $cateId ?>)"><svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="7.97" viewBox="0 0 7.969 7.97"><path class="cls-1" d="M407.432,2325.36l-3.318-3.32a0.671,0.671,0,0,0-.949.95l2.845,2.84-2.845,2.85a0.668,0.668,0,0,0,.949.94l3.318-3.32a0.662,0.662,0,0,0,0-.94h0Zm-3.99-.21-2.636-2.77a0.651,0.651,0,0,0-.958,0,0.743,0.743,0,0,0,0,1.01l2.157,2.27-2.157,2.27a0.743,0.743,0,0,0,0,1.01,0.651,0.651,0,0,0,.958,0l2.636-2.77a0.754,0.754,0,0,0,0-1.02h0Zm0,0" transform="translate(-399.656 -2321.84)"/>
							</svg>
							</a></li>
					</ul>
					<input type="hidden" value="<?= $numberOfPages ?>" id="numberOfPages">
                    <input type="hidden" value="<?= $cateId ?>" id="webcateid">
                    <input type="hidden" value="<?= $websiteData['w_id'] ?>" id="webid">
				</div>
                <?php } ?>
			</div>
			<!--===Sidebar Section Start===-->
			<div class="col-xl-3 col-lg-4 col-md-5">
				<div class="plr_sidebar_category">
					<div class="plr_newsletter_box">
						<div class="plr_newsletter_form">
							<img src="<?= base_url() ?>assets/frontend/images/news_msg.png" alt="msg-img">
							<h5>Join Our List For<br>Regular Updates & New Review</h5>
							<form>
								<div class="plr_input_box">
									<label>Name</label>
									<input type="text" placeholder="Your name here..." name="" value="">
								</div>
								<div class="plr_input_box">
									<label>Email</label>
									<input type="text" placeholder="Your email here..." name="" value="">
								</div>
								<button type="button" class="plr_btn">Submit Now</button>
							</form>
						</div>
					</div>
					<div class="plr_leftbar_box">
						<div class="plr_product_detailhead">
							<h4>Product Categories</h4>
						</div>
						<div class="plr_product_detail">
							<ul>
								<?php $sno=1; foreach ($cateList as $soloCate) { 
								$prodCount = $this->DBfile->getCount('products',array('p_categoryid'=>$soloCate['cate_id']));
								$w_blockcategory = $websiteData['w_blockcategory'] == '' ? array() : explode(',',$websiteData['w_blockcategory']);
								if( !in_array($soloCate['cate_id'],$w_blockcategory) ){
									?>
									<li><a href="<?= count($checkURL) == 2 ? base_url('plr/'.$websiteData['w_siteurl'].'/'.$soloCate['cate_slug']) : $this->session->userdata['web_url'].'/pages/'.$soloCate['cate_slug'] ?>"><?= $soloCate['cate_name'] ?></a><span>(<?= $prodCount[0]['totCount'] ?>)</span></li>
								<?php $sno++; } } 
								if(!empty($custom_cateList)) {
									foreach ($custom_cateList as $soloCate) { 
									$prodCount = $this->DBfile->getCount('web_products',array('p_categoryid'=>$soloCate['cate_id']));
									if( $soloCate['cate_status'] == 1 ){
										?>
										<li><a href="<?= count($checkURL) == 2 ? base_url('plr/'.$websiteData['w_siteurl'].'/'.$soloCate['cate_slug']) : $this->session->userdata['web_url'].'/pages/'.$soloCate['cate_slug'] ?>"><?= $soloCate['cate_name'] ?></a><span>(<?= $prodCount[0]['totCount'] ?>)</span></li>
								<?php $sno++; } } } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--===Sidebar Section End===-->
		</div>
	</div>
</div>
<!--===Products Section End===-->


<!--===PLR Category Section Start===-->
<div class="plr_blog_section plr_product_category">
	<div class="container">
		<div class="plr_main_heading">
			<h2>PLR Categories</h2>
		</div>
		<div class="row">
			<?php $sno=1; foreach ($cateList as $soloCate) { 
			$w_blockcategory = $websiteData['w_blockcategory'] == '' ? array() : explode(',',$websiteData['w_blockcategory']);
				if( !in_array($soloCate['cate_id'],$w_blockcategory) ){ ?>
			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="plr_category_box plr_color<?= $sno ?>">
					<h6><a href="<?= count($checkURL) == 2 ? base_url('plr/'.$websiteData['w_siteurl'].'/'.$soloCate['cate_slug']) : $this->session->userdata['web_url'].'/pages/'.$soloCate['cate_slug'] ?>"><?= $soloCate['cate_name'] ?></a></h6>
				</div>
			</div>
			<?php $sno++; } }
			if(!empty($custom_cateList)) {
				foreach ($custom_cateList as $soloCate) { 
					$prodCount = $this->DBfile->getCount('web_products',array('p_categoryid'=>$soloCate['cate_id']));
					if( $soloCate['cate_status'] == 1 ){
						?>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="plr_category_box plr_color<?= $sno ?>">
								<h6><a href="<?= count($checkURL) == 2 ? base_url('plr/'.$websiteData['w_siteurl'].'/'.$soloCate['cate_slug']) : $this->session->userdata['web_url'].'/pages/'.$soloCate['cate_slug'] ?>"><?= $soloCate['cate_name'] ?></a></h6>
							</div>
						</div>
			<?php $sno++; } } }?>
		</div>
	</div>
</div>
<!--===PLR Category Section End===-->
