
<div class="plr_policy_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="plr_policy_detail">
                    <h2>Terms and Conditions</h2>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>1. Scope</h5>
                        <p>Welcome to the Terms of Service for our website, located at {BUSINESS-URL} proudly provided to you by {BUSINESS-NAME}. You must carefully read these Terms and our Privacy Policy . The Terms shall also include any additional guidelines and/or policies thereof provided to you by {BUSINESS-NAME}.<br>

                        You acknowledge and agree that, through the access and use of our Site and/or products and services you have read, understand and agree to be bound by these Terms, whether or not you have registered into the Site. If you do not agree to these terms, then please exit our Sit and stop using any of our Services.<br>

                        {BUSINESS-NAME} is a company that provides with its users with a web platform where they can access and purchase quality digital web design elements, themes and items designed for easy implementation into WordPress web sites and content management systems, among other functionalities. Moreover, registered users may also offer, promote and sell them over its marketplace and other functionalities. {BUSINESS-NAME} constantly innovates in order to provide you the best possible user experience.</p>
                    </div>

                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>2. Minors</h5>
                        <p>{BUSINESS-NAME} seriously undertakes the protection of the rights of children and minors, and thereby encourages parents, legal guardians and responsible adults to be actively involved in the safe use of nutrition plans and strength programs by their minors. Through the use and/or simple access to the Site, you declare and acknowledge that you are, at the least, thirteen (13) years of age, or older, as of the date of first access to our Site and, if you are still a minor and therefore using the Site under the direct supervision of your parent, legal guardian or responsible adult. Please, tell your parent or guardian or any adult to read these Terms with you.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>3. Changes, Updates</h5>
                        <p>By continuing to access or use the Site or Services after we notify you of any and all modifications on the Site, you are therefore acknowledging that you agree to be bound by the modified Terms. We hereby reserve the right to update, modify, discontinue or terminate the Site, the Terms and the Policy, at our sole discretion. Any changes to these Terms may be displayed in the Site, and we may also notify you by email. You understand and agree that the Services which we provide may be subject to changes without prior notice to you, at {BUSINESS-NAME} sole and final discretion.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>4. Service</h5>
                        <p>{BUSINESS-NAME} includes the functionalities to allow registered members to buy and digital items like website themes, project files, motion graphics, code, vectors and images, for example. Its online marketplace allows buyers and sellers to communicate among each other in order for the transactions to take place. You can see more information about your activities in our Service via your profile, which shall include any and payments made and/or received by you. You can browse our Site for free but in order to buy and/or sell any digital items, you will need to register in order to buy and sell any items, post in forums, and in general to enjoy the functionalities of our platform.<br>

                        When you buy a digital item trough our Services, you are acquiring a license for them, subject to the specific terms of use of the seller. Sellers may provide support for their items, or not. Please revise each seller’s profile in the Site and any communications he sends to you.<br>

                        You must get, at your sole cost and expense, all equipment and services needed to access our Services, including but not limiting to, Word Press account, hosting account, Internet Domain provider services, high-speed Internet address, suitable video and image editing software. Items within the Site will be able to be purchased and sell through PayPal and/or any other third party payment processing company, not through our Site.<br>

                        {BUSINESS-NAME} and/or its third party service providers will only be responsible for collecting billing information from you, but not for processing payments or purchases via the Site, which are currently undertook by PayPal and/or any other third party payment processing company. Therefore, the applicable third party website’s terms and conditions and privacy policy will be applicable for the billing information, for processing payments, and to remit applicable payments.<br>

                        You agree not to, and will not undertake, motivate, or facilitate the use or access of the Site to:<br>

                        a.	Plagiarize or infringe the intellectual property rights of a third party;<br>
                        b.	Use any automatic mean to enter or use our services or any process, whether automated or manual, to capture data or content from any of our services for any reason;<br>
                        c.	Send any other unsolicited bulk communication to any of our users or to any third party;<br>
                        d.	Disrupt the normal flow of dialogue and/or exchange on the Site;<br>
                        e.	Help, motivate, or enable others to infringe the Terms and/or the Policy;<br>
                        f.	Upload, post or otherwise disseminate any Content that infringes any intellectual property right, including patent, trademark, trade secret, copyright or other proprietary rights of any third party, or in any way exploit the Site other than as specifically sanctioned by {BUSINESS-NAME}.<br>
                        g.	Remove or modify any copyright, trademark or other proprietary rights notice that appears on any portion of the Site or on any materials printed or copied from the Site.<br>
                        h.	Engage in an activity that is harmful to {BUSINESS-NAME} or our clients, advertisers, affiliates or authors;<br>
                        i.	Copy, modify, create a derivative work of, reverse engineer or decompile the Site or any part thereof.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>5. Third Parties</h5>
                        <p>Occasionally, we may use the Site to display hyperlinks to the websites of third parties. We are not responsible for the examination or evaluation of their businesses, services or the content of their websites. You should review their privacy policies, disclaimers and their terms of use, which will rule and oversaw any and all access and/or use of their services by you. These Terms and our Policy are not applicable and will not govern your use of any other website. We are not responsible for the content, availability or accuracy of such websites, along with the content, products, or services on or available thereinto.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>6. License And Rights</h5>
                        <p>You hereby grant {BUSINESS-NAME} an unlimited, non-exclusive, assignable, royalty-free, perpetual, irrevocable, for all the countries and territories in the world, fully sub-licensable right and license to {BUSINESS-NAME} and/or its licensors, partners, sponsors, affiliates and/or agents, to use, reproduce, translate, modify, adapt, create derivative works from, distribute, publish and display any content you may upload, disseminate, deliver, create or transfer throughout the Site. You hereby represent and warrant to {BUSINESS-NAME} that you have all the rights, licenses, authorizations and authority necessary to grant the above mentioned license.<br>

	                    {BUSINESS-NAME} hereby grants you a personal, limited, revocable, worldwide, royalty-free, non-assignable, non-sub licensable and non-exclusive right to use the Services and/or software provided to you by {BUSINESS-NAME} through the Site. This is for the sole and final purpose of allowing you to use and receive the Services via the Site, only as provided for in these Terms, and subject to your compliance thereof.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>7. DCMA Notice</h5>
                        <p>If you believe that any content or other material provided through the Site allegedly infringes the copyright of you or of a third party, please notify us of your claim to {BUSINESS-EMAIL} (subject: "Takedown Request"). {BUSINESS-NAME} may then remove any content if it believes or has reason to believe such content infringes on another’s copyright, without prior notice and at any time and at its sole discretion.<br>

	                    The notification must be in writing and must contain the following information, at the least: (i) a signature and identification of the copyright owner or the person authorized to act; (ii) a description of the copyrighted work that allegedly has been infringed; (iii) contact information, such as your address or email address (for us to deliver our response to you); and (iv) a statement indicating that the information provided in the notice is true and accurate.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>8. Ownership</h5>
                        <p>All content included within the Site, such as graphics, logos, page headers, button icons, scripts, and service names are trademarks, certification marks, service marks, or other trade dresses the property of {BUSINESS-NAME} and protected by U.S.A. and international copyright laws. As between you and us, you own your content and we own our content, including but not limited to visual interfaces, interactive features, graphics and design or the Site and other products, software, aggregate user review ratings, feedback and all other elements and components of the Site, excluding your content. As such, you may not modify, reproduce, distribute, create derivative works or adaptations of, publicly display or in any way exploit any of our content, in whole or in part, except as expressly authorized by us. . As between you and the sellers of digital items in the Site, they own their content and you purchase a license for it, under their terms and conditions, not ours.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>9. Indemnification</h5>
                        <p>You hereby agree to indemnify, defend and hold harmless {BUSINESS-NAME} and/or its successors, affiliates and successors, and its and their managers, employees, agents or other officers, from and against any and all claims, actions, liabilities, demands, losses, causes of action, procedures, orders, damages, costs and expenses (including any reasonable attorney's fees and expert witnesses therefrom) of any type of nature, incurred by {BUSINESS-NAME} and/or its successors, affiliates and successors, and its and their managers, employees, agents or other officers, arising out of or relating to your use of the Site, your breach of this Terms or the Policy, or your infringement of any rights of any third party.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>10. Disclaimer Of Warranties And Limitations On Liability</h5>
                        <p>By accessing or using the site, you represent and warrant that you have read, understood, and agreed to be bound by and under these terms. We supply our Services including all content, software, materials, services, functions, and/or information made available through. Accordingly, {BUSINESS-NAME} is not liable to you for any loss or damage that might arise therefrom and {BUSINESS-NAME} hereby disclaims any express or implied warranties, including warranties as to the products or services offered by third parties listed on the Site, non-infringement, merchantability, and fitness for a particular purpose. The use on your part of the products and/or services provided through the site is at your own and final discretion. No oral or written information or advice provided to you by {BUSINESS-NAME} shall create a representation or warranty of any kind.<br>

                        We shall not be liable for any direct, indirect, special, incidental, consequential, punitive or exemplary damages arising from your use of the Site, or regarding hyperlinks to third party websites, or for any security breach associated with the transmission of personal data through the site or any other website, for loss of profits, loss of goodwill, products or Services obtained through the Site, or otherwise arising out of the use of such, whether based on contract, tort, strict liability or other legal theory, even if {BUSINESS-NAME} has been advised of the possibility of damages and even if such damages result from our negligence or gross negligence.<br>
                        
                        Some states do not allow us to limit our liability for consequential or incidental damages or exclude certain warranties. In those states, our liability and warranties shall be limited to the fullest extent permitted by applicable law.<br>

                        {BUSINESS-NAME} maximum cumulative liability to you for losses or damages arising in connection with your access of the Site under these Terms shall be limited to: (i) the amount paid, if any, by you to us and/or a seller related to the Site during the 12 months prior to the action giving rise to such liability; or (ii) USD$100 (One Hundred United States Dollars), whichever results inferior.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>11. Arbitration, Governing Law</h5>
                        <p>Any dispute arising from the use of the Site or the interpretation of the Terms, including any proceed thereto associated to the breach or contravention of proprietary copyright rights of either party, to binding arbitration to be held in Mumbai, India. Arbitration shall be in the English language with one or three Arbitrators, in lieu of either party’s right to file suit. Any arbitration shall be final and binding and the arbitrator's order will be enforceable in any court of competent jurisdiction.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>12. Generals</h5>
                        <p>The failure of {BUSINESS-NAME} to exercise or enforce any right or provision of this Agreement shall not constitute a waiver of such right or provision. If any part of these Terms is found to be invalid, then only that part or section will be amende or eliminated to the minimum extent necessary so that the Terms shall otherwise remain in full force and effect and enforceable.<br>

                        You shall be responsible for currency expenses and conversion fees related to the Service, along with payment processors’ fees, if any.<br>

                        We will not be liable for failing to perform under these Terms because of any event beyond our reasonable control, including, without limitation, interruption of Internet service, fire, terrorism, natural disaster, Acts of God or war.<br>

                        Any notices or other communications required hereunder, including those regarding modifications to these Terms, will be in writing and delivered by email (the address you provided) or by posting to the Site. For notices made by e-mail, the date of receipt will be deemed the date on which such notice is transmitted.<br>

                        You may not assign or transfer these Terms, by operation of law or otherwise, without {BUSINESS-NAME} prior written consent. Notwithstanding the foregoing, we may assign any rights or obligations hereunder.<br>

                        You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Site must be filed within one year after such claim or cause of action arose or be forever barred.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>