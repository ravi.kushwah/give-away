
<div class="plr_policy_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="plr_policy_detail">
                    <h2>Legal Disclaimer</h2>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>1. Scope</h5>
                        <p>All data and information displayed in our website, proudly located at {BUSINESS-URL} proudly provided to you by {BUSINESS-NAME}. While we constantly stride to keep {BUSINESS-NAME} content up to date and reviewed, please understand that we make no representations or warranties of any kind, either express or implied, regarding the accuracy, correctness, consistency, completeness, suitability and/or accessibility therefrom.<br> 
	                    This disclaimer rules and your use and access of {BUSINESS-NAME}. By using {BUSINESS-NAME} and any section thereof, you hereby acknowledge and agree to this disclaimer in full; therefore, if you differ or oppose any part therefrom please stop use of {BUSINESS-NAME} and exit it. Please, read carefully this disclaimer along with our Terms and Conditions and our Privacy Policy .</p>
                    </div>

                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>2. Intellectual Property Rights</h5>
                        <p>Unless otherwise indicated to the contrary, we and/or our licensors own, license, operate and/or otherwise control the intellectual property rights arising out from {BUSINESS-NAME}, and any and all intellectual property rights thereinto are thereby reserved by their respective holders.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>3. Representations And Warranties And Limitations Of Liability</h5>
                        <p>Any and all item, vector, image, theme, archive, code, design, content, information, opinion, article, recommendation, metric, post, blog, image, idea or commentary displayed on {BUSINESS-NAME} is provided free of charge and for information and reference purposes only. No liability for its correctness and suitability is assumed by us. The aforementioned content is not intended to amount to commercial, marketing, Internet or advice to any third party on a specific subject, topic, matter or industry.<br>
                        Through {BUSINESS-NAME}, you may be able to access other third party websites not under our control. We have no power over the scope, accessibility, content, data, services, products, images and/or related multimedia of such websites, and the same are displayed only for convenience. The insertion of a hyperlink or banner does not entail or imply an endorsement of any products and services thereinto by us. Please, read the applicable terms and policies of such third party websites.<br>
                        Under no circumstance whatsoever, shall {BUSINESS-NAME} be legally responsible for any loss or damage, including without limitation, direct, indirect, consequential, punitive, exemplary, special losses or damages, or any other damage of any kind arising out of or related to, the use and access of {BUSINESS-NAME} by you or any person.<br>
                        To the maximum extent allowed by applicable law, we hereby exclude all representations, warranties and conditions relating to {BUSINESS-NAME} and use and access thereof (including, without limitation, any express or implied warranties of satisfactory workmanship or quality, fitness for a particular purpose, non-infringement and/or the use of reasonable degree of care or skill). Accordingly, we will not be liable for any loss of goodwill, income, profit, revenue, business, data, or information.<br>
                        Nothing in this disclaimer (or elsewhere on {BUSINESS-NAME}) will exclude or limit our liability caused by our negligence, or for any other liability which cannot be excluded or limited under applicable law.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>4. Updates</h5>
                        <p>{BUSINESS-NAME} hereby reserves the right to amend, update and change this disclaimer from time to time, without prior notice, and its sole and final discretion. The updated disclaimer will apply to and rule any and all use and/or access of Site by you from the date such changes are posted through {BUSINESS-NAME}. Please browse this page regularly to read the latest version.Generals.This disclaimer, together with our Terms and Conditions and our Privacy Policy, constitute the entire agreement between you and us in relation to your access and use of the Site, and supersedes all prior appertaining communications and agreements of any kind regarding the subject matter hereof.<br> 
	                    Please refer to our Privacy Policy for information regarding advertisement and opt-out mechanisms. Any discrepancy arising from this disclaimer will be construed and governed under the applicable laws of India.</p>
                    </div>
                    <div class="plr_privacy_list_box">
                        <h5><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                          <path class="cls-1" d="M383,453a8,8,0,1,0,8,8A8.009,8.009,0,0,0,383,453Zm2.471,8.471-3.333,3.333a0.666,0.666,0,0,1-.943-0.942L384.057,461l-2.862-2.862a0.667,0.667,0,0,1,.943-0.943l3.333,3.333A0.666,0.666,0,0,1,385.471,461.471Z" transform="translate(-375 -453)"/>
                        </svg>5. Personal Data</h5>
                        <p>We may collect and use personal information about your usage of {BUSINESS-NAME}, including certain types of information from and about you. {BUSINESS-NAME} may use this information for:<br>
                            i)Its own business purposes, provided that such information will not personally identify you.<br>
                            ii)Create and maintain a database of our users and slice and dice data obtained therefrom.<br>
                            iii)Provide you with products, goods and services sold through {BUSINESS-NAME} marketplace.<br>
                            iv)Use of anonymized, non-personally identifiable data and info of you and other users, in order to create and display browsing and consumption preferences profiles, charts, and aggregated statistics and databases.<br>

                        Henceforth, we will endeavor to reasonably protect your data, to the fullest extent permitted by applicable law.<br>
                        If you have any further questions or inquire, do not hesitate to contact us at:<br>
                        Address : {BUSINESS-ADDRESS}<br>
                        Phone : {BUSINESS-PHONE} <br>
                        Email : {BUSINESS-EMAIL} 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>