
<!--===Header Section End===-->
<!--===Products Section Start===-->
		<div class="plr_product_gallery">
			<div class="container">
				<div class="row">
					<div class="col-xl-9 col-lg-9 col-md-12 plr_product_box_list">
						<div class="row">
							<div class="col-xl-12">
								<div class="plr_product_single_add">
								   <a href="<?= isset($ads[0]['single_page_top_ad'])? $ads[0]['single_page_top_ad'] :''?>" target="_blank"><img src="<?= isset($ads[0]['single_page_top_link'])? $ads[0]['single_page_top_link'] : base_url()."assets/frontend/images/add2.jpg" ?>"  alt="add-img"></a>
								</div>
								<div class="plr_box_search_headbox">
								    <?php
								      if(isset($submit_giveaway) && !empty($submit_giveaway)){
								           ?>
								         	<h2><?=isset($submit_giveaway[0]['g_offer_name']) ? $submit_giveaway[0]['g_offer_name'] : '' ?></h2>
								         <?php
								      }else{
								         if(isset($prodList[0]['g_offer_name'])){
								         ?>
    								         	<h2><?=isset($prodList[0]['g_offer_name']) ? $prodList[0]['g_offer_name'] : '' ?></h2>
    								         <?php
    								     }else if(isset($prodList[0]['title'])){
    								         ?>
    								         	<h2><?=isset($prodList[0]['title']) ? $prodList[0]['title'] : '' ?></h2>
    								         <?php
    								     }else{
    								          ?>
    								         	<h2><?=isset($prodList[0]['v_name']) ? $prodList[0]['v_name'] : '' ?></h2>
    								         <?php
								         }
								      }
								    ?>
								</div>
							</div>	
							<div class="col-lg-12">
								<div class="plr_news_box plr_hosted_box plr_product_single_box">
									<div class="plr_blog_img plr_animation_box">
										<span class="plr_product_list_img plr_animation">
										    <?php
										    if(isset($submit_giveaway) && !empty($submit_giveaway)){
										         ?>
										        	<a href="javascript:;"><img src="<?= isset($submit_giveaway[0]['give_away_image'])? base_url().'/assets/giveaways/'.$submit_giveaway[0]['give_away_image'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
										        <?php  
										    }else{
										        if(isset($prodList[0]['g_priview']) || isset($prodList[0]['thumbnail'])){
										             ?>
    										        	<a href="javascript:;"><img src="<?= isset($prodList[0]['g_priview'])? $prodList[0]['g_priview'] : $prodList[0]['thumbnail'] ?>" class="plr_animation_img" alt="product-img"></a>
    										        <?php         
    										     }else if(isset($prodList[0]['brand_logo'])){
    										          ?>
    										        	<a href="javascript:;"><img src="<?= isset($prodList[0]['brand_logo'])? $prodList[0]['brand_logo'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
    										        <?php  
    										     }else{
    										           ?>
    										        	<a href="javascript:;"><img src="<?= isset($prodList[0]['v_imagelink'])? $prodList[0]['v_imagelink'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
    										        <?php 
    										     } 
										    }
										    ?>
										
										</span>
									</div>
									<div class="plr_blog_content">
										<div class="plr_blog_btn">
										    <?php
										      if(isset($submit_giveaway) && !empty($submit_giveaway)){
										           ?>
                                                    <span><svg 
        												xmlns="http://www.w3.org/2000/svg"
        												xmlns:xlink="http://www.w3.org/1999/xlink"
        												width="17px" height="17px">
        											   <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
        												d="M15.144,-0.000 L13.555,-0.000 L13.555,1.700 C13.555,2.040 13.290,2.267 13.025,2.267 C12.761,2.267 12.496,2.040 12.496,1.700 L12.496,-0.000 L4.021,-0.000 L4.021,1.700 C4.021,2.040 3.756,2.267 3.491,2.267 C3.226,2.267 2.961,2.040 2.961,1.700 L2.961,-0.000 L1.372,-0.000 C0.578,-0.000 -0.005,0.737 -0.005,1.700 L-0.005,3.740 L16.945,3.740 L16.945,1.700 C16.945,0.737 15.992,-0.000 15.144,-0.000 ZM-0.005,4.930 L-0.005,15.300 C-0.005,16.320 0.578,17.000 1.425,17.000 L15.197,17.000 C16.045,17.000 16.998,16.263 16.998,15.300 L16.998,4.930 L-0.005,4.930 ZM4.709,14.450 L3.438,14.450 C3.226,14.450 3.014,14.280 3.014,13.997 L3.014,12.580 C3.014,12.353 3.173,12.127 3.438,12.127 L4.762,12.127 C4.974,12.127 5.186,12.297 5.186,12.580 L5.186,13.997 C5.133,14.280 4.974,14.450 4.709,14.450 ZM4.709,9.350 L3.438,9.350 C3.226,9.350 3.014,9.180 3.014,8.897 L3.014,7.480 C3.014,7.253 3.173,7.027 3.438,7.027 L4.762,7.027 C4.974,7.027 5.186,7.197 5.186,7.480 L5.186,8.897 C5.133,9.180 4.974,9.350 4.709,9.350 ZM8.947,14.450 L7.623,14.450 C7.411,14.450 7.199,14.280 7.199,13.997 L7.199,12.580 C7.199,12.353 7.358,12.127 7.623,12.127 L8.947,12.127 C9.159,12.127 9.371,12.297 9.371,12.580 L9.371,13.997 C9.371,14.280 9.212,14.450 8.947,14.450 ZM8.947,9.350 L7.623,9.350 C7.411,9.350 7.199,9.180 7.199,8.897 L7.199,7.480 C7.199,7.253 7.358,7.027 7.623,7.027 L8.947,7.027 C9.159,7.027 9.371,7.197 9.371,7.480 L9.371,8.897 C9.371,9.180 9.212,9.350 8.947,9.350 ZM13.184,14.450 L11.860,14.450 C11.648,14.450 11.436,14.280 11.436,13.997 L11.436,12.580 C11.436,12.353 11.595,12.127 11.860,12.127 L13.184,12.127 C13.396,12.127 13.608,12.297 13.608,12.580 L13.608,13.997 C13.608,14.280 13.449,14.450 13.184,14.450 ZM13.184,9.350 L11.860,9.350 C11.648,9.350 11.436,9.180 11.436,8.897 L11.436,7.480 C11.436,7.253 11.595,7.027 11.860,7.027 L13.184,7.027 C13.396,7.027 13.608,7.197 13.608,7.480 L13.608,8.897 C13.608,9.180 13.449,9.350 13.184,9.350 Z"/>
        											   </svg><?=isset($submit_giveaway[0]['end_date']) ? date("M d, Y", strtotime($submit_giveaway[0]['end_date'])) : '' ?></span>
        											   <span><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 32 32" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m21.386 10c-1.055-4.9-3.305-8-5.386-8s-4.331 3.1-5.386 8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10 16a30.013 30.013 0 0 0 .267 4h11.466a30.013 30.013 0 0 0 .267-4 30.013 30.013 0 0 0 -.267-4h-11.466a30.013 30.013 0 0 0 -.267 4z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10.614 22c1.055 4.9 3.305 8 5.386 8s4.331-3.1 5.386-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m23.434 10h6.3a15.058 15.058 0 0 0 -10.449-8.626c1.897 1.669 3.385 4.755 4.149 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m30.453 12h-6.7a32.332 32.332 0 0 1 .247 4 32.332 32.332 0 0 1 -.248 4h6.7a14.9 14.9 0 0 0 0-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m19.285 30.626a15.058 15.058 0 0 0 10.451-8.626h-6.3c-.766 3.871-2.254 6.957-4.151 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8.566 22h-6.3a15.058 15.058 0 0 0 10.451 8.626c-1.899-1.669-3.387-4.755-4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m12.715 1.374a15.058 15.058 0 0 0 -10.451 8.626h6.3c.766-3.871 2.254-6.957 4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8 16a32.332 32.332 0 0 1 .248-4h-6.7a14.9 14.9 0 0 0 0 8h6.7a32.332 32.332 0 0 1 -.248-4z" fill="currentColor" data-original="#000000"></path></g></svg><?=isset($submit_giveaway[0]['country']) ? $submit_giveaway[0]['country'] : ''?></span>
                                                    <?php
										      }else{
										          if(isset($prodList[0]['g_last_update']) || isset($prodList[0]['published_date'])){
                                                    ?>
                                                        <span><svg 
            												xmlns="http://www.w3.org/2000/svg"
            												xmlns:xlink="http://www.w3.org/1999/xlink"
            												width="17px" height="17px">
            											   <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
            												d="M15.144,-0.000 L13.555,-0.000 L13.555,1.700 C13.555,2.040 13.290,2.267 13.025,2.267 C12.761,2.267 12.496,2.040 12.496,1.700 L12.496,-0.000 L4.021,-0.000 L4.021,1.700 C4.021,2.040 3.756,2.267 3.491,2.267 C3.226,2.267 2.961,2.040 2.961,1.700 L2.961,-0.000 L1.372,-0.000 C0.578,-0.000 -0.005,0.737 -0.005,1.700 L-0.005,3.740 L16.945,3.740 L16.945,1.700 C16.945,0.737 15.992,-0.000 15.144,-0.000 ZM-0.005,4.930 L-0.005,15.300 C-0.005,16.320 0.578,17.000 1.425,17.000 L15.197,17.000 C16.045,17.000 16.998,16.263 16.998,15.300 L16.998,4.930 L-0.005,4.930 ZM4.709,14.450 L3.438,14.450 C3.226,14.450 3.014,14.280 3.014,13.997 L3.014,12.580 C3.014,12.353 3.173,12.127 3.438,12.127 L4.762,12.127 C4.974,12.127 5.186,12.297 5.186,12.580 L5.186,13.997 C5.133,14.280 4.974,14.450 4.709,14.450 ZM4.709,9.350 L3.438,9.350 C3.226,9.350 3.014,9.180 3.014,8.897 L3.014,7.480 C3.014,7.253 3.173,7.027 3.438,7.027 L4.762,7.027 C4.974,7.027 5.186,7.197 5.186,7.480 L5.186,8.897 C5.133,9.180 4.974,9.350 4.709,9.350 ZM8.947,14.450 L7.623,14.450 C7.411,14.450 7.199,14.280 7.199,13.997 L7.199,12.580 C7.199,12.353 7.358,12.127 7.623,12.127 L8.947,12.127 C9.159,12.127 9.371,12.297 9.371,12.580 L9.371,13.997 C9.371,14.280 9.212,14.450 8.947,14.450 ZM8.947,9.350 L7.623,9.350 C7.411,9.350 7.199,9.180 7.199,8.897 L7.199,7.480 C7.199,7.253 7.358,7.027 7.623,7.027 L8.947,7.027 C9.159,7.027 9.371,7.197 9.371,7.480 L9.371,8.897 C9.371,9.180 9.212,9.350 8.947,9.350 ZM13.184,14.450 L11.860,14.450 C11.648,14.450 11.436,14.280 11.436,13.997 L11.436,12.580 C11.436,12.353 11.595,12.127 11.860,12.127 L13.184,12.127 C13.396,12.127 13.608,12.297 13.608,12.580 L13.608,13.997 C13.608,14.280 13.449,14.450 13.184,14.450 ZM13.184,9.350 L11.860,9.350 C11.648,9.350 11.436,9.180 11.436,8.897 L11.436,7.480 C11.436,7.253 11.595,7.027 11.860,7.027 L13.184,7.027 C13.396,7.027 13.608,7.197 13.608,7.480 L13.608,8.897 C13.608,9.180 13.449,9.350 13.184,9.350 Z"/>
            											   </svg><?=isset($prodList[0]['g_last_update']) ? date("M d, Y", strtotime($prodList[0]['g_last_update'])) : date("M d, Y", strtotime($prodList[0]['published_date'])) ?></span>
            											   <span><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 32 32" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m21.386 10c-1.055-4.9-3.305-8-5.386-8s-4.331 3.1-5.386 8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10 16a30.013 30.013 0 0 0 .267 4h11.466a30.013 30.013 0 0 0 .267-4 30.013 30.013 0 0 0 -.267-4h-11.466a30.013 30.013 0 0 0 -.267 4z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10.614 22c1.055 4.9 3.305 8 5.386 8s4.331-3.1 5.386-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m23.434 10h6.3a15.058 15.058 0 0 0 -10.449-8.626c1.897 1.669 3.385 4.755 4.149 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m30.453 12h-6.7a32.332 32.332 0 0 1 .247 4 32.332 32.332 0 0 1 -.248 4h6.7a14.9 14.9 0 0 0 0-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m19.285 30.626a15.058 15.058 0 0 0 10.451-8.626h-6.3c-.766 3.871-2.254 6.957-4.151 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8.566 22h-6.3a15.058 15.058 0 0 0 10.451 8.626c-1.899-1.669-3.387-4.755-4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m12.715 1.374a15.058 15.058 0 0 0 -10.451 8.626h6.3c.766-3.871 2.254-6.957 4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8 16a32.332 32.332 0 0 1 .248-4h-6.7a14.9 14.9 0 0 0 0 8h6.7a32.332 32.332 0 0 1 -.248-4z" fill="currentColor" data-original="#000000"></path></g></svg><?=isset($prodList[0]['g_countries']) ? $prodList[0]['g_countries'] : $prodList[0]['type']?></span>
                                                        <?php
                                                    }else if(isset($prodList[0]['end_date']) || isset($prodList[0]['g_countries'])){
                                                         ?>
                                                        <span><svg 
            												xmlns="http://www.w3.org/2000/svg"
            												xmlns:xlink="http://www.w3.org/1999/xlink"
            												width="17px" height="17px">
            											   <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
            												d="M15.144,-0.000 L13.555,-0.000 L13.555,1.700 C13.555,2.040 13.290,2.267 13.025,2.267 C12.761,2.267 12.496,2.040 12.496,1.700 L12.496,-0.000 L4.021,-0.000 L4.021,1.700 C4.021,2.040 3.756,2.267 3.491,2.267 C3.226,2.267 2.961,2.040 2.961,1.700 L2.961,-0.000 L1.372,-0.000 C0.578,-0.000 -0.005,0.737 -0.005,1.700 L-0.005,3.740 L16.945,3.740 L16.945,1.700 C16.945,0.737 15.992,-0.000 15.144,-0.000 ZM-0.005,4.930 L-0.005,15.300 C-0.005,16.320 0.578,17.000 1.425,17.000 L15.197,17.000 C16.045,17.000 16.998,16.263 16.998,15.300 L16.998,4.930 L-0.005,4.930 ZM4.709,14.450 L3.438,14.450 C3.226,14.450 3.014,14.280 3.014,13.997 L3.014,12.580 C3.014,12.353 3.173,12.127 3.438,12.127 L4.762,12.127 C4.974,12.127 5.186,12.297 5.186,12.580 L5.186,13.997 C5.133,14.280 4.974,14.450 4.709,14.450 ZM4.709,9.350 L3.438,9.350 C3.226,9.350 3.014,9.180 3.014,8.897 L3.014,7.480 C3.014,7.253 3.173,7.027 3.438,7.027 L4.762,7.027 C4.974,7.027 5.186,7.197 5.186,7.480 L5.186,8.897 C5.133,9.180 4.974,9.350 4.709,9.350 ZM8.947,14.450 L7.623,14.450 C7.411,14.450 7.199,14.280 7.199,13.997 L7.199,12.580 C7.199,12.353 7.358,12.127 7.623,12.127 L8.947,12.127 C9.159,12.127 9.371,12.297 9.371,12.580 L9.371,13.997 C9.371,14.280 9.212,14.450 8.947,14.450 ZM8.947,9.350 L7.623,9.350 C7.411,9.350 7.199,9.180 7.199,8.897 L7.199,7.480 C7.199,7.253 7.358,7.027 7.623,7.027 L8.947,7.027 C9.159,7.027 9.371,7.197 9.371,7.480 L9.371,8.897 C9.371,9.180 9.212,9.350 8.947,9.350 ZM13.184,14.450 L11.860,14.450 C11.648,14.450 11.436,14.280 11.436,13.997 L11.436,12.580 C11.436,12.353 11.595,12.127 11.860,12.127 L13.184,12.127 C13.396,12.127 13.608,12.297 13.608,12.580 L13.608,13.997 C13.608,14.280 13.449,14.450 13.184,14.450 ZM13.184,9.350 L11.860,9.350 C11.648,9.350 11.436,9.180 11.436,8.897 L11.436,7.480 C11.436,7.253 11.595,7.027 11.860,7.027 L13.184,7.027 C13.396,7.027 13.608,7.197 13.608,7.480 L13.608,8.897 C13.608,9.180 13.449,9.350 13.184,9.350 Z"/>
            											   </svg><?=isset($prodList[0]['end_date']) ? date("M d, Y", strtotime($prodList[0]['end_date'])) : '' ?></span>
            											   <span><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" x="0" y="0" viewBox="0 0 32 32" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m21.386 10c-1.055-4.9-3.305-8-5.386-8s-4.331 3.1-5.386 8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10 16a30.013 30.013 0 0 0 .267 4h11.466a30.013 30.013 0 0 0 .267-4 30.013 30.013 0 0 0 -.267-4h-11.466a30.013 30.013 0 0 0 -.267 4z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m10.614 22c1.055 4.9 3.305 8 5.386 8s4.331-3.1 5.386-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m23.434 10h6.3a15.058 15.058 0 0 0 -10.449-8.626c1.897 1.669 3.385 4.755 4.149 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m30.453 12h-6.7a32.332 32.332 0 0 1 .247 4 32.332 32.332 0 0 1 -.248 4h6.7a14.9 14.9 0 0 0 0-8z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m19.285 30.626a15.058 15.058 0 0 0 10.451-8.626h-6.3c-.766 3.871-2.254 6.957-4.151 8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8.566 22h-6.3a15.058 15.058 0 0 0 10.451 8.626c-1.899-1.669-3.387-4.755-4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m12.715 1.374a15.058 15.058 0 0 0 -10.451 8.626h6.3c.766-3.871 2.254-6.957 4.151-8.626z" fill="currentColor" data-original="#000000"></path><path xmlns="http://www.w3.org/2000/svg" d="m8 16a32.332 32.332 0 0 1 .248-4h-6.7a14.9 14.9 0 0 0 0 8h6.7a32.332 32.332 0 0 1 -.248-4z" fill="currentColor" data-original="#000000"></path></g></svg> <?=isset($prodList[0]['g_countries']) ? $prodList[0]['g_countries'] : $prodList[0]['type']?></span>
                                                        <?php
                                                    }else{
                                                        
                                                    }
										      }
                                            ?>
										</div>
										<div class="plr_payout_box">
											<ul>
											    <?php 
											     if(isset($submit_giveaway) && !empty($submit_giveaway)){
											         ?><li>End Date:<span><?=$submit_giveaway[0]['end_date'] ?></span></li><?php
											     }else{
											        if(isset($prodList[0]['g_payout'])){
											            ?>
											                <!--<li>Payout:<span><?=$prodList[0]['g_payout']?></span></li>-->
											            <?php
											        }
											        if(isset($cat[0]['cate_name'])){
											            ?>
											               <li>Categories:<span><?=$cat[0]['cate_name']?></span></li>
											            <?php
											        }else{
											             ?>
											                <!--<li>Platform:<span><?=$prodList[0]['type']?></span></li>-->
											            <?php
											        }
											       
                                                   $date = Date('Y-m-d');
                                                   $time = strtotime($date);
                                                   $final = date("M-d-Y", strtotime("+1 day", $time));
                                                 ?><li>End Date:<span><?=$final ?></span></li><?php
											     }
											     
											    ?>
												<!--<li>Description:</li>-->
											</ul>
											<div class="plr_description_text">
        								        <p>Hello there, Peeps! Do you want to win <?php 
        								        if(isset($submit_giveaway) && !empty($submit_giveaway)){
            								           ?>
            								         	<span><?=isset($submit_giveaway[0]['g_offer_name']) ? $submit_giveaway[0]['g_offer_name'] : '' ?></span>
            								         <?php
            								      }else{
            								         if(isset($prodList[0]['g_offer_name'])){
            								         ?>
                								         	<span><?=isset($prodList[0]['g_offer_name']) ? $prodList[0]['g_offer_name'] : '' ?></span>
                								         <?php
                								     }else if(isset($prodList[0]['title'])){
                								         ?>
                								         	<span><?=isset($prodList[0]['title']) ? $prodList[0]['title'] : '' ?></span>
                								         <?php
                								     }else{
                								          ?>
                								         	<span><?=isset($prodList[0]['v_name']) ? $prodList[0]['v_name'] : '' ?></span>
                								         <?php
            								         }
            								      }
        								           
        								        ?>
        						                    for free? Your dream is only a sweepstakes entry away. After all, who wouldn't 
        						                    want a brand new car or phone with no down payment or EMI? Enter some fun time sweepstakes contests with prizes and you could be the proud owner of one of them. So why incur the expense of
        						                    purchasing one when you can win one for free? To be eligible to win, you must enter a sweepstakes contest, which only takes a minute to complete by entering your name, email address, or 
        						                    so  and continuing with the contest. The sweepstakes draw ensures that the stunning prizes are yours.
        						                </p>
    						                </div>
										</div>
										
										<div class="plr_social_icons">
											<ul>
												<li>Share:</li>
												<li> 
												    <a target="_blank" href="https://www.facebook.com/sharer.php?display=page&t=&u=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/social1.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Facebook</p>
												    </div>
												</a></li>
												<li><a target="_blank" href="https://twitter.com/intent/tweet/?text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>&via=jonsuh"><img src="<?= base_url() ?>assets/frontend/images/social2.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Twitter</p>
												    </div>
												</a></li>
												<li><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>&title=&source=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/social4.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>linkedin</p>
												    </div>
												</a></li>
												<li><a target="_blank" href="https://in.pinterest.com/pin/create/bookmarklet/?&text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>&description=Subscribe%20to%20the%20YouTube%20Channel"><img src="<?= base_url() ?>assets/frontend/images/social5.png" alt="Icon">
												     <div class="plr_tooltip_show">
												        <p>Pinterest</p>
												    </div>
												</a></li>
												<li><a target="_blank" href="https://web.whatsapp.com/send?text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/social6.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Whatsapp</p>
												    </div>
												</a></li>
												<li><a target="_blank" href="https://reddit.com/submit?text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/social7.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Reddit</p>
												    </div>
												</a></li>
												<li><a target="_blank" href="https://www.tumblr.com/share?text= &url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/social8.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Tumblr</p>
												    </div>
												</a></li>
												<li><a target="_blank" href="https://wordpress.com/wp-admin/press-this.php?text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/social9.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Wordpress</p>
												    </div>
												</a></li>
													<li><a target="_blank" href="https://buffer.com/add?text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/socia20.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Buffer</p>
												    </div>
												</a></li>
													<li><a target="_blank" href="https://refind.com/?text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/socia21.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Refind</p>
												    </div>
												</a></li>
													<li><a target="_blank" href="http://www.sitejot.com/loginform.php?text=&url=<?= base_url() ?>ga/give-aways/single_page/<?=$this->uri->segment(4)?>"><img src="<?= base_url() ?>assets/frontend/images/socia22.png" alt="Icon">
												    <div class="plr_tooltip_show">
												        <p>Sitejot</p>
												    </div>
												</a></li>
											</ul>
										</div>
										<div class="plr_giveaway_btn">
										   <?php 
										    if($websiteData['w_type']==2){
        							           if(empty($website[0]['while_claiming_the_offer'])){
										           ?>
										            <a href="#Giveaway" class="plr_btn" data-bs-toggle="modal" data-bs-target="#Giveaway">Get Access To The Video</a>
    										       <?php
    										   }else{
    										        ?>
    										            <a href="<?=$website[0]['while_claiming_the_offer']?>" target="_blank" class="plr_btn">Get Access To The Video</a>
    										       <?php
    										   }
        							        }else if($websiteData['w_type']==6){
        							           if(empty($website[0]['while_claiming_the_offer'])){
										           ?>
										            <a href="#Giveaway" class="plr_btn" data-bs-toggle="modal" data-bs-target="#Giveaway">Grab Your Coupon Code</a>
    										       <?php
    										   }else{
    										        ?>
    										            <a href="<?=$website[0]['while_claiming_the_offer']?>" target="_blank" class="plr_btn">Grab Your Coupon Code</a>
    										       <?php
    										   }
        							        }else if($websiteData['w_type']==5){
        							         if(empty($website[0]['while_claiming_the_offer'])){
										           ?>
										            <a href="#Giveaway" class="plr_btn" data-bs-toggle="modal" data-bs-target="#Giveaway">Enter Giveaway Here</a>
    										       <?php
    										   }else{
    										        ?>
    										            <a href="<?=$website[0]['while_claiming_the_offer']?>" target="_blank" class="plr_btn">Enter Giveaway Here</a>
    										       <?php
    										   }
        							        }else if($websiteData['w_type']==4){
        							           if(empty($website[0]['while_claiming_the_offer'])){
										           ?>
										            <a href="#Giveaway" class="plr_btn" data-bs-toggle="modal" data-bs-target="#Giveaway">Grab Your gift card</a>
    										       <?php
    										   }else{
    										        ?>
    										            <a href="<?=$website[0]['while_claiming_the_offer']?>" target="_blank" class="plr_btn">Grab Your gift card</a>
    										       <?php
    										   }
        							        }else if($websiteData['w_type']==3){
        							            if(empty($website[0]['while_claiming_the_offer'])){
										           ?>
										            <a href="#Giveaway" class="plr_btn" data-bs-toggle="modal" data-bs-target="#Giveaway">Enter into the contests</a>
    										       <?php
    										   }else{
    										        ?>
    										            <a href="<?=$website[0]['while_claiming_the_offer']?>" target="_blank" class="plr_btn">Enter into the contests</a>
    										       <?php
    										   }
        							        }else if($websiteData['w_type']==1){
        							           if(empty($website[0]['while_claiming_the_offer'])){
										           ?>
										            <a href="#Giveaway" class="plr_btn" data-bs-toggle="modal" data-bs-target="#Giveaway">Enter Giveaway Here</a>
    										       <?php
    										   }else{
    										        ?>
    										            <a href="<?=$website[0]['while_claiming_the_offer']?>" target="_blank" class="plr_btn">Enter Giveaway Here</a>
    										       <?php
    										   }
        							        }
										   ?>
											
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-12">
								<div class="plr_product_single_two">
								    <a href="<?= isset($ads[0]['single_page_bottom_ad'])? $ads[0]['single_page_bottom_ad'] :''?>" target="_blank"><img src="<?= isset($ads[0]['single_page_bottom_link'])? $ads[0]['single_page_bottom_link'] : base_url()."assets/frontend/images/add2.jpg" ?>"  alt="add-img"></a>
								</div>
							</div>
							<div class="col-xl-12">
								<div class="plr_box_search_headbox">
									<h2>Related Giveaways</h2>
								</div>
							</div>
    						 <?php 
        				        foreach($relatedData as $value){
        				            ?>
        				            <div class="col-lg-4 col-md-6 col-sm-6">
                						<div class="plr_news_box">
                							<div class="plr_blog_img plr_animation_box">
                								<span class="plr_product_list_img plr_animation">
                								    <?php
                                                    if(isset($value['g_priview']) || isset($value['thumbnail'])){
                                                        ?>
                                                        <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=$value['g_id']?>"><img src="<?= isset($value['g_priview'])? $value['g_priview'] : $value['thumbnail'] ?>" class="plr_animation_img" alt="product-img"></a>
                                                        <?php
                                                    }else if(isset($value['brand_logo'])){
                                                         ?>
                                                        <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=$value['id']?>"><img src="<?= isset($value['brand_logo'])? $value['brand_logo'] : ''?>" class="plr_animation_img" alt="product-img"></a>
                                                        <?php
                                                    }else{
                                                         ?>
                                                        <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=$value['v_id']?>"><img src="<?= isset($value['v_imagelink'])? $value['v_imagelink'] : ''?>" class="plr_animation_img" alt="product-img"></a>
                                                        <?php
                                                    }
                                                    ?>
                									
                								</span>
                								<div class="plr_blog_btn">
                								    <?php 
                								        if(isset($value['g_countries']) || isset($value['g_countries'])){
                								            ?>
            								            
                								            		<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['id']) ? $value['id'] : '' ?>"><?=isset($value['g_countries'])!=''? $value['g_countries'] : $value['type'] ?></a>
                								            <?php
                								        }else if(isset($value['type'])){
                								             ?>
                								            	<a href="javascript:;"><?=isset($value['type'])!=''? $value['type'] : '' ?></a>
                								            <?php
                								        }else{
                								            
                								        }
                								    ?>
                									<!--<a href="javascript:;"><?=isset($value['g_countries']) ? $value['g_countries'] : $value['type']?></a>-->
                								</div>
                							</div>
                							<div class="plr_blog_content">
                								 <?php
                                                if(isset($value['g_last_update']) || isset($value['published_date'])){
                                                ?>
                                                <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                    <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                     d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                    </svg>
                                                    <?=isset($value['g_last_update']) ? $value['g_last_update'] : date("M d, Y", strtotime($value['published_date'])) ?>
                                                    </span>
                                                    <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['g_id']) ? $value['g_id'] : $value['id'] ?>"><h5><?=isset($value['g_offer_name']) ? $value['g_offer_name'] : $value['title']?></h5></a>
                                                    <?php
                                                    }else if(isset($value['end_date']) || isset($value['title'])){
                                                         ?>
                                                        <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                            <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                             d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                            </svg>
                                                            <?=isset($value['end_date']) ? date("M d, Y", strtotime($value['end_date'])) : '' ?>
                                                            </span>
                                                            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['id']) ? $value['id'] : ''?>"><h5><?=isset($value['title']) ? $value['title'] : '' ?></h5></a>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['v_id']) ? $value['v_id'] : ''?>"><h5><?=isset($value['v_name']) ? $value['v_name'] : '' ?></h5></a>
                                                        <?php
                                                    }
                                                ?>
            								
                							</div>
                						</div>
                					</div>
        				            <?php 
        				        }
        				    ?>
						</div>
					</div>
					<!--===Sidebar Section Start===-->
					<?php include('sidebar.php'); ?>
					<!--===Sidebar Section End===-->
				</div>
			</div>
		</div>
		<!--===Products Section End===-->
  
