
<!--===Products Section Start===-->
<div class="plr_product_gallery">
	<div class="container">
		<div class="row">
			<div class="col-xl-9 col-lg-9 col-md-12 plr_product_box_list">
				<div class="plr_box_search_headbox">
					<h2>Hot & Fresh 
					<?php 
				// 	print_r($submit_giveaway);
    					    if($websiteData['w_type']==2){
    				            echo "Giveaways Videos";
    				        }else if($websiteData['w_type']==6){
    				             echo "Coupons";
    				        }else if($websiteData['w_type']==5){
    				             echo "Games Giveaways";
    				        }else if($websiteData['w_type']==4){
    				             echo "Gift Cards";
    				        }else if($websiteData['w_type']==3){
    				             echo "Contests";
    				        }else if($websiteData['w_type']==1){
    				             echo "Giveaways";
    				        }
    				   ?></h2>
					<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/view_all">See More Giveaways</a>
				</div>
				<div class="row">
				    <?php
				    if(isset($submit_giveaway) && !empty($submit_giveaway)){
				        foreach($submit_giveaway as $sgValue){
				        ?>
				            <div class="col-lg-4 col-md-6 col-sm-6">
        						<div class="plr_news_box">
        							<div class="plr_blog_img plr_animation_box">
        								<span class="plr_product_list_img plr_animation">
    									       <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($sgValue['s_id']) ? $sgValue['s_id'] : '' ?>">
									           <img src="<?= base_url() ?>assets/giveaways/<?= isset($sgValue['give_away_image'])? $sgValue['give_away_image'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
        									</a>
        								</span>
        								<div class="plr_blog_btn">
        								   <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($sgValue['id']) ? $sgValue['id'] : '' ?>"><?=isset($sgValue['country'])!=''? $sgValue['country'] : '' ?></a>
        								</div>
        							</div>
        							<div class="plr_blog_content">
        							    <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                        <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                         d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                        </svg>
                                        <?=isset($sgValue['end_date']) ? date("M d, Y", strtotime($sgValue['end_date'])) : '' ?>
                                        </span>
                                        <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($sgValue['s_id']) ? $sgValue['s_id'] :''?>"><h5><?=isset($sgValue['g_offer_name']) ? $sgValue['g_offer_name'] : ''?></h5></a>
        							</div>
        						</div>
        					</div>
				            <?php 
				        }
				    }
				    // for($i= 0; $i<$submit_giveaway.leanth; $i++){
				        
				    // }
        			foreach($prodList as $value){
				            ?>
				            <div class="col-lg-4 col-md-6 col-sm-6">
        						<div class="plr_news_box">
        							<div class="plr_blog_img plr_animation_box">
        								<span class="plr_product_list_img plr_animation">
        									    <?php 
        									        if(isset($value['g_priview']) || isset($value['thumbnail'])){
        									            ?>
    									            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['g_id']) ? $value['g_id'] : $value['id'] ?>">
        									            <img src="<?= isset($value['g_priview'])? $value['g_priview'] : $value['thumbnail'] ?>" class="plr_animation_img" alt="product-img"></a>
        									            <?php
        									        }else if(isset($value['brand_logo'])){
        									            ?>
        									            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['id']) ? $value['id'] : '' ?>">
        									            <img src="<?= isset($value['brand_logo'])? $value['brand_logo'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
        									            <?php
        									        }else{
        									             ?>
        									            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['v_id']) ? $value['v_id'] : '' ?>">
        									            <img src="<?= isset($value['v_imagelink'])? $value['v_imagelink'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
        									            <?php
        									        }
        									    ?>
        									</a>
        								</span>
        								<div class="plr_blog_btn">
        								    <?php 
        								        if(isset($value['g_countries']) || isset($value['g_countries'])){
        								            ?>
        								            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['id']) ? $value['id'] : '' ?>"><?=isset($value['g_countries'])!=''? $value['g_countries'] : $value['type'] ?></a>
        								            <?php
        								        }else if(isset($value['type'])){
        								             ?>
        								            	<a href="javascript:;"><?=isset($value['type'])!=''? $value['type'] : '' ?></a>
        								            <?php
        								        }else{
        								            
        								        }
        								    ?>
        								
        								</div>
        							</div>
        							<div class="plr_blog_content">
        							     <?php
                                            if(isset($value['g_last_update']) || isset($value['published_date'])){
                                                ?>
                                                <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                    <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                     d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                    </svg>
                                                    <?=isset($value['g_last_update']) ? $value['g_last_update'] : date("M d, Y", strtotime($value['published_date'])) ?>
                                                    </span>
                                                    <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['g_id']) ? $value['g_id'] : $value['id'] ?>"><h5><?=isset($value['g_offer_name']) ? $value['g_offer_name'] : $value['title']?></h5></a>
                                                <?php
                                            }else if(isset($value['end_date'])){
                                                 ?>
                                                <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                    <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                     d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                    </svg>
                                                    <?=isset($value['end_date']) ? date("M d, Y", strtotime($value['end_date'])) : '' ?>
                                                    </span>
                                                    <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['id']) ? $value['id'] : ''?>"><h5><?=isset($value['title']) ? $value['title'] : '' ?></h5></a>
                                                <?php
                                            }else{
                                                ?>
                                                 <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($value['v_id']) ? $value['v_id'] : ''?>"><h5><?=isset($value['v_name']) ? $value['v_name'] : '' ?></h5></a>
                                                <?php
                                            }
                                        ?>
        								
        							</div>
        						</div>
        					</div>
				            <?php 
				        }
				    ?>
    				<div class="col-xl-12">
        				<div class="plr_box_search_headbox">
        					<h2>Hottest 
        					     <?php 
								    if($websiteData['w_type']==2){
							            echo "Giveaways Videos";
							        }else if($websiteData['w_type']==6){
							             echo "Coupons";
							        }else if($websiteData['w_type']==5){
							             echo "Games Giveaways";
							        }else if($websiteData['w_type']==4){
							             echo "Gift Cards";
							        }else if($websiteData['w_type']==3){
							             echo "Contests";
							        }else if($websiteData['w_type']==1){
							             echo "Giveaways";
							        }
								   ?></h2>
        						<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/view_all">See More Giveaways</a>
        				</div>
        			</div>	
        			<?php 
        			foreach($sidebarList as $hostedvalue){
        			        ?>
        			        <div class="col-lg-6 col-md-6 col-sm-6">
        						<div class="plr_news_box plr_hosted_box">
        							<div class="plr_blog_img plr_animation_box">
        								<span class="plr_product_list_img plr_animation">
        								 <?php 
    									        if(isset($hostedvalue['g_priview']) || isset($hostedvalue['thumbnail'])){
    									            ?>
									            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($hostedvalue['g_id']) ? $hostedvalue['g_id'] : $hostedvalue['id']?>">
    									            <img src="<?= isset($hostedvalue['g_priview'])? $hostedvalue['g_priview'] : $hostedvalue['thumbnail'] ?>" class="plr_animation_img" alt="product-img"></a>
    									            <?php
    									        }else if(isset($hostedvalue['brand_logo'])){
    									            ?>
									            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($hostedvalue['id']) ? $hostedvalue['id'] : '' ?>">
    									            <img src="<?= isset($hostedvalue['brand_logo'])? $hostedvalue['brand_logo'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
    									            <?php
    									        }else{
    									             ?>
									            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($hostedvalue['v_id']) ? $hostedvalue['v_id'] : '' ?>">
    									            <img src="<?= isset($hostedvalue['v_imagelink'])? $hostedvalue['v_imagelink'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
    									            <?php
    									        }
    									    ?>
        								</span>
        							</div>
        							<div class="plr_blog_content">
        								<div class="plr_blog_btn">
        								     <?php 
        								        if(isset($value['g_countries']) || isset($sValue['published_date'])){
        								            ?>
        								            	<a href="javascript:;"><?=isset($value['g_countries'])!=''? $value['g_countries'] : $value['type'] ?></a>
        								            	<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                        <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                         d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                        </svg><?=isset($hostedvalue['g_last_update']) ? $hostedvalue['g_last_update'] : date("M d, Y", strtotime($sValue['published_date'])) ?></span>
        								            <?php
        								        }else if(isset($value['type']) || isset($hostedvalue['end_date'])){
        								             ?>
        								            	<a href="javascript:;"><?=isset($value['type'])!=''? $value['type'] : '' ?></a>
        								            		<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                                        <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                                         d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                                        </svg><?=isset($hostedvalue['end_date']) ? date("M d, Y", strtotime($hostedvalue['end_date'])) : 'Life Time' ?></span>
        								            <?php
        								        }else{
        								            
        								        }
        								    ?>
        								
        								</div>
        								<?php
        								    if(isset($hostedvalue['g_id']) || isset($hostedvalue['id'])){
        								        ?>
        								        	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($hostedvalue['g_id']) ? $hostedvalue['g_id'] : $hostedvalue['id']?>"><h5><?=isset($hostedvalue['g_offer_name']) ? $hostedvalue['g_offer_name'] : $hostedvalue['title']?></h5></a>
        								        <?php
        								    }else if(isset($hostedvalue['g_offer_name']) || isset($hostedvalue['title'])){
        								         ?>
        								        	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($hostedvalue['id']) ? $hostedvalue['id'] : '' ?>"><h5><?=isset($hostedvalue['g_offer_name']) ? $hostedvalue['g_offer_name'] : $hostedvalue['title']?></h5></a>
        								        <?php
        								    }else{
        								        ?>
        								        	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($hostedvalue['v_id']) ? $hostedvalue['v_id'] : '' ?>"><h5><?=isset($hostedvalue['v_name']) ? $hostedvalue['v_name'] : '' ?></h5></a>
        								        <?php
        								    }
        								?>
        							
        							</div>
        						</div>
        					</div>
        			        <?php
        			    } 
        			?>
				</div>
			</div>
				<?php  include('sidebar.php'); ?>
			<!--===Sidebar Section End===-->
		</div>
	</div>
</div>
<!--===Products Section End===-->

