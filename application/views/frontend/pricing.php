<div class="plr_breadcrumbs_section">
    <div class="container">
        <div class="plr_breadcrumbs_box">
            <h2>Pricing Plan</h2>
            <span><a href="<?= $this->session->userdata['web_url'] ?>">Home</a></span>
        </div>
    </div>
</div>
</div>
<!--===Header Section End===-->

<!--===Pricing Section Start===-->
<div class="plr_pricing_wrapper">
<div class="container">
    <div class="plr_main_heading">
        <h2>Our Plan & Pricing</h2>
    </div>
    <div class="row">
        <?php if($websiteData['w_plansettings'] != '') 
        {
            $planArr = json_decode($websiteData['w_plansettings'],true);
            foreach($planArr as $key=>$soloPlan) {
            $planData = json_decode($soloPlan,true); 
            $counter = explode('_',$key)[1];
            if( $planData['planname_'.$counter] != '' ) {
        ?>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="plr_price_box <?= !empty($userList) ? $userList[0]['c_planid'] == $counter ? 'active' : '' : '' ?>">
                    <div class="plr_price_plan">
                        <h4><?= $planData['planname_'.$counter]?></h4>
                        <h1>USD <?= $planData['amount_'.$counter]?> <span> / <?= $planData['duration_'.$counter] ?></span></h1>
                    </div>
                    <div class="plr_price_list">
                        <ul>
                            <li><img src="<?= base_url() ?>assets/frontend/images/green_check.png" alt="icon"><?= $planData['numberofdownloads_'.$counter] == 0 ? 'Unlimited' : $planData['numberofdownloads_'.$counter] ?> Downloads</li>
                            <li><img src="<?= base_url() ?>assets/frontend/images/green_check.png" alt="icon"> PLR Products </li>
                            <li><img src="<?= base_url() ?>assets/frontend/images/green_check.png" alt="icon"> Updates and Offers on Newletters </li>
                        </ul>
                    </div>
                    <div class="plr_price_btn">
                        <?php if($_SESSION['id'] == 1){?>
                            <a href="javascript:;" class="plr_btn pricePlan" data-plan="<?= $counter ?>""><?= !empty($userList) ? $userList[0]['c_planid'] == $counter ? 'Selected' : 'Choose this plan' : 'Choose this plan' ?></a>
                        <?php }else{?>
                            <a href="javascript:;" class="plr_btn" onclick="choosePlan(<?= $counter ?>,<?= !empty($paymentSettings) ? 1 : 0 ?>)"><?= !empty($userList) ? $userList[0]['c_planid'] == $counter ? 'Selected' : 'Choose this plan' : 'Choose this plan' ?></a>
                        <?php }?>
                    </div>
                </div>
            </div>
        <?php } } }  ?>
        <input type="hidden" value="<?= isset($this->session->userdata['c_loggedin']) ? $this->session->userdata['c_id'] : 0 ?>" id="loggedInUser">
    </div>
</div>
</div>
<div id="shoot" style="display:none"></div>
<!--===Pricing Section End===-->