<div class="plr_breadcrumbs_section">
    <div class="container">
        <div class="plr_breadcrumbs_box">
            <h2>Profile & My Downloads</h2>
            <span><a href="<?= $this->session->userdata['web_url'] ?>">Home</a></span>
        </div>
    </div>
</div>
</div>
<!--===Header Section End===-->

<!--===Profile Section Start===-->
<div class="plr_profile_wrapper">
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="plr_template_box">
                <div class="plr_tem_tab_buttom">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Profile</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">My Downloads</button>
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-5 align-self-center">
                                <div class="plr_edit_profile">
                                    <div class="plr_edit_box">
                                        <?= strtoupper($this->session->userdata['c_initials']) ?>
                                    </div>
                                    <div class="plr_profile_name">
                                        <h5><?= $this->session->userdata['c_name'] ?></h5>
                                        <p><?= $this->session->userdata['c_email'] ?></p>
                                    </div>
                                    <div class="plr_profile_plan_name">
                                        <h5><?= $userList[0]['c_plan'] ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-7 align-self-center">
                                <div class="plr_profile_form">
                                    <h2>Edit Profile</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="plr_input_box">
                                                <label>Name</label>
                                                <input type="text" placeholder="Enter Your Name" id="c_name" value="<?= $userList[0]['c_name'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="plr_input_box">
                                                <label>Email Address</label>
                                                <input type="text" placeholder="Email Address" readonly value="<?= $userList[0]['c_email'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="plr_input_box">
                                                <label>New Password</label>
                                                <input type="password" placeholder="New Password" id="c_pwd">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="plr_form_btn">
                                                <button type="button" class="plr_btn" onclick="saveProfile()">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="plr_table_section">
                            <div class="plr_my_order">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product Name</th>
                                                <th>Download Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($productDownload)) { 
                                                $sno = 0;
                                            foreach($productDownload as $soloProd) { $sno++; ?>
                                            <tr>
                                                <td><?= $sno ?></td>
                                                <td><?= $soloProd['p_name'] ?></td>
                                                <td><?= date_format(date_create($soloProd['pd_date']),"jS F, Y") ?></td>
                                                <td>
                                                    <div class="plr_action_icon">
                                                        <a href="<?= count($checkURL) == 2 ? base_url('plr/'.$websiteData['w_siteurl'].'/'.$soloProd['p_urlname']) : $this->session->userdata['web_url'].'/pages/'.$soloProd['p_urlname'] ?>" target="_blank">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14" viewBox="0 0 20 14">
                                                            <path class="cls-1" d="M1414,522c-4.28,0-8,2.82-10,7,2,4.181,5.72,7,10,7s8-2.819,10-7C1422,524.82,1418.28,522,1414,522Zm0,11.375a4.38,4.38,0,1,1,4.17-4.375A4.278,4.278,0,0,1,1414,533.375Zm0-7a2.628,2.628,0,1,1-2.5,2.625A2.565,2.565,0,0,1,1414,526.375Z" transform="translate(-1404 -522)"/>
                                                            </svg>
                                                            <div class="plr_tooltip_show">
                                                                <p>View</p>
                                                            </div>
                                                        </a>
                                                        <a href="<?= base_url() ?>home/access_download/<?= $websiteData['w_id'] ?>/<?= $this->session->userdata['c_id'] ?>/<?= $soloProd['p_id'] ?>">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 16 14">
                                                            <path class="cls-1" d="M1485,531.8a0.473,0.473,0,0,1-.36-0.163l-3.5-3.85a0.529,0.529,0,0,1,.36-0.887h1.83v-4.025a0.861,0.861,0,0,1,.84-0.875h1.66a0.861,0.861,0,0,1,.84.875V526.9h1.83a0.529,0.529,0,0,1,.36.887l-3.5,3.85A0.473,0.473,0,0,1,1485,531.8Zm6.83,4.2h-13.66a1.2,1.2,0,0,1-1.17-1.225v-0.35a1.2,1.2,0,0,1,1.17-1.225h13.66a1.2,1.2,0,0,1,1.17,1.225v0.35A1.2,1.2,0,0,1,1491.83,536Z" transform="translate(-1477 -522)"/>
                                                            </svg>
                                                            <div class="plr_tooltip_show">
                                                                <p>Download</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- <div class="plr_pagination text-center">
                                <ul>
                                    <li><a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
                                        <path class="cls-1" d="M120.193,2325.53l3.331-3.34a0.676,0.676,0,0,1,.953.96l-2.856,2.85,2.856,2.86a0.672,0.672,0,0,1,0,.95,0.68,0.68,0,0,1-.953,0l-3.331-3.33a0.672,0.672,0,0,1,0-.95h0Zm4-.21,2.647-2.78a0.656,0.656,0,0,1,.962,0,0.741,0.741,0,0,1,0,1.01l-2.166,2.28,2.166,2.28a0.754,0.754,0,0,1,0,1.02,0.656,0.656,0,0,1-.962,0l-2.647-2.79a0.754,0.754,0,0,1,0-1.02h0Zm0,0" transform="translate(-120 -2322)"/>
                                    </svg>
                                    </a></li>
                                    <li><a href="javascript:;">01</a></li>
                                    <li><a href="javascript:;" class="active">02</a></li>
                                    <li><a href="javascript:;">...</a></li>
                                    <li><a href="javascript:;">09</a></li>
                                    <li><a href="javascript:;">10</a></li>
                                    <li><a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="7.97" viewBox="0 0 7.969 7.97"><path class="cls-1" d="M407.432,2325.36l-3.318-3.32a0.671,0.671,0,0,0-.949.95l2.845,2.84-2.845,2.85a0.668,0.668,0,0,0,.949.94l3.318-3.32a0.662,0.662,0,0,0,0-.94h0Zm-3.99-.21-2.636-2.77a0.651,0.651,0,0,0-.958,0,0.743,0.743,0,0,0,0,1.01l2.157,2.27-2.157,2.27a0.743,0.743,0,0,0,0,1.01,0.651,0.651,0,0,0,.958,0l2.636-2.77a0.754,0.754,0,0,0,0-1.02h0Zm0,0" transform="translate(-399.656 -2321.84)"/>
                                    </svg>
                                    </a></li>
                                </ul>
                            </div> -->
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--===Profile Section End===-->