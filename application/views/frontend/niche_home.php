
<!--===Header Section End===-->

<!--===Products Section Start===-->
<div class="plr_product_gallery">
	<div class="container">
		<div class="row">
			<div class="col-xl-9 col-lg-8 col-md-7 plr_product_box_list">
				<div class="plr_box_search_headbox">
					<div class="plr_main_heading">
						<h2>The Latest Give Aways</h2>
					</div>
					<!-- <div class="plr_search_box">
						<input type="text" placeholder="Search Here">
					</div> -->
				</div>
				<div class="row" id="showProduct">
				<?php foreach($prodList as $soloProd) { ?>
					<div class="col-lg-4 col-md-6 col-sm-6">
						<div class="plr_news_box">
							<div class="plr_blog_img plr_animation_box">
								<span class="plr_product_list_img plr_animation">
								    <?php 
								        if(isset($soloProd['g_priview']) || isset($soloProd['thumbnail'])){
								            ?>
							            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['g_id']) ? $soloProd['g_id'] : $soloProd['id'] ?>">
								            <img src="<?= isset($soloProd['g_priview'])? $soloProd['g_priview'] : $soloProd['thumbnail'] ?>" class="plr_animation_img" alt="product-img"></a>
								            <?php
								        }else if(isset($soloProd['brand_logo'])){
								            ?>
								            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['id']) ? $soloProd['id'] : '' ?>">
								            <img src="<?= isset($soloProd['brand_logo'])? $soloProd['brand_logo'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
								            <?php
								        }else{
								             ?>
								            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['v_id']) ? $soloProd['v_id'] : '' ?>">
								            <img src="<?= isset($soloProd['v_imagelink'])? $soloProd['v_imagelink'] : '' ?>" class="plr_animation_img" alt="product-img"></a>
								            <?php
								        }
								    ?>
									<!--<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['g_id']) ? $soloProd['g_id']  : $soloProd['g_id'] ?>"><img src="<?= isset($soloProd['g_priview'])? $soloProd['g_priview'] : $soloProd['thumbnail'] ?>" class="plr_animation_img" alt="product-img"></a>-->
								</span>
								<div class="plr_blog_btn">
								    <?php 
								        if(isset($soloProd['g_countries']) || isset($soloProd['g_countries'])){
								            ?>
								            	<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['id']) ? $soloProd['id'] : '' ?>"><?=isset($soloProd['g_countries'])!=''? $soloProd['g_countries'] : $soloProd['type'] ?></a>
								            <?php
								        }else if(isset($soloProd['type'])){
								             ?>
								            	<a href="javascript:;"><?=isset($soloProd['type'])!=''? $soloProd['type'] : '' ?></a>
								            <?php
								        }else{
								            
								        }
								    ?>
									<!--<a href="javascript:;"><?=isset($soloProd['g_countries']) ? $soloProd['g_countries'] : $soloProd['type'] ?></a>-->
								</div>
							</div>
							<div class="plr_blog_content">
							    <?php
                                    if(isset($soloProd['g_last_update']) || isset($soloProd['published_date'])){
                                        ?>
                                        <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                            <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                             d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                            </svg>
                                            <?=isset($soloProd['g_last_update']) ? $soloProd['g_last_update'] : date("M d, Y", strtotime($soloProd['published_date'])) ?>
                                            </span>
                                            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['g_id']) ? $soloProd['g_id'] : $soloProd['id'] ?>"><h5><?=isset($soloProd['g_offer_name']) ? $soloProd['g_offer_name'] : $soloProd['title']?></h5></a>
                                        <?php
                                    }else if(isset($soloProd['end_date'])){
                                         ?>
                                        <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">
                                            <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"
                                             d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>
                                            </svg>
                                            <?=isset($soloProd['end_date']) ? date("M d, Y", strtotime($soloProd['end_date'])) : '' ?>
                                            </span>
                                            <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['id']) ? $soloProd['id'] : ''?>"><h5><?=isset($soloProd['title']) ? $soloProd['title'] : '' ?></h5></a>
                                        <?php
                                    }else{
                                        ?>
                                         <a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['v_id']) ? $soloProd['v_id'] : ''?>"><h5><?=isset($soloProd['v_name']) ? $soloProd['v_name'] : '' ?></h5></a>
                                        <?php
                                    }
                                ?>
								<!--<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">-->
        <!--                        <path fill-rule="evenodd"  fill="rgb(121, 121, 121)"-->
        <!--                         d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>-->
        <!--                        </svg><?=isset($soloProd['g_last_update']) ? $soloProd['g_last_update'] : $soloProd['published_date'] ?></span>-->
								<!--<a href="<?= base_url() ?>ga/<?= $websiteData['w_siteurl'] ?>/single_page/<?=isset($soloProd['g_id']) ? $soloProd['g_id']  : $soloProd['g_id'] ?>"><h5><?=isset($soloProd['g_offer_name']) ? $soloProd['g_offer_name'] : $soloProd['title'] ?></h5></a>-->
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
				<div class="plr_pagination">
					<ul>
						<li><a href="javascript:;" onclick="getOtherProducts('first','<?= $website[0]['w_id'] ?>')"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
							<path class="cls-1" d="M120.193,2325.53l3.331-3.34a0.676,0.676,0,0,1,.953.96l-2.856,2.85,2.856,2.86a0.672,0.672,0,0,1,0,.95,0.68,0.68,0,0,1-.953,0l-3.331-3.33a0.672,0.672,0,0,1,0-.95h0Zm4-.21,2.647-2.78a0.656,0.656,0,0,1,.962,0,0.741,0.741,0,0,1,0,1.01l-2.166,2.28,2.166,2.28a0.754,0.754,0,0,1,0,1.02,0.656,0.656,0,0,1-.962,0l-2.647-2.79a0.754,0.754,0,0,1,0-1.02h0Zm0,0" transform="translate(-120 -2322)"/>
							</svg>
							</a></li>
						<?php  for($i=1;$i<$numberOfPages+1;$i++) { ?>
							<?php if( $i < 4 || $i > $numberOfPages - 3 ) { ?>
							<li class="changePage"><a href="javascript:;" <?= $i == 1 ? 'class="pagination_a active"' : 'class="pagination_a"' ?> onclick="getNextProducts(<?= $i ?>,'<?= $website[0]['w_id'] ?>')" data-pagenum="<?= $i ?>"><?= $i ?></a></li>
							<?php } if( $i == $numberOfPages - 4 ) { ?>
								<span class="changePage">...</span>
							<?php } ?>
						<?php } ?>
						<li><a href="javascript:;" onclick="getOtherProducts('last','<?= $website[0]['w_id'] ?>')"><svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="7.97" viewBox="0 0 7.969 7.97"><path class="cls-1" d="M407.432,2325.36l-3.318-3.32a0.671,0.671,0,0,0-.949.95l2.845,2.84-2.845,2.85a0.668,0.668,0,0,0,.949.94l3.318-3.32a0.662,0.662,0,0,0,0-.94h0Zm-3.99-.21-2.636-2.77a0.651,0.651,0,0,0-.958,0,0.743,0.743,0,0,0,0,1.01l2.157,2.27-2.157,2.27a0.743,0.743,0,0,0,0,1.01,0.651,0.651,0,0,0,.958,0l2.636-2.77a0.754,0.754,0,0,0,0-1.02h0Zm0,0" transform="translate(-399.656 -2321.84)"/>
							</svg>
							</a></li>
					</ul>
					 <input type="hidden" value="<?= $numberOfPages ?>" id="numberOfPages">
                    <input type="hidden" value="<?= $website[0]['w_id'] ?>" id="webid">
				</div>
			</div>
			<!--===Sidebar Section Start===-->
				<?php include('sidebar.php'); ?>
			<!--===Sidebar Section End===-->
			<!--===Sidebar Section End===-->
		</div>
	</div>
</div>
<!--===Products Section End===-->
