<div class="admin_main">
    <div class="row">
        <div class="col-xl-12 col-md-12">
            <div class="ad_box ad_mBottom30">
                <div class="form-group">
                    <label class="ad_label">Name</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="u_name" value="<?= !empty($userData) ? $userData[0]['u_name'] : '' ?>"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Email</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="u_email" value="<?= !empty($userData) ? $userData[0]['u_email'] : '' ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Password</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="u_password" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Front End - GiveSites Pro</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_fe" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_fe'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_fe'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO1 - GiveSites Unlimited </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto1" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto1'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto1'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO2 - GiveSites Traffic Blaster </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto2" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto2'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto2'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO3 - GiveSites Pro Max </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto3" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto3'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto3'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO4 - Niche Pro Sites </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto4" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto4'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto4'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO5 - DFY Platinum Edition  </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto5" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto5'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto5'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO6 - GiveSites Pro Agency </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto6" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto6'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto6'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO7 - GiveSites Reseller </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto7" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto7'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto7'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="ad_label">OTO8 - GiveSites Pro Whitelabel </label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="is_oto8" class="form-control">
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['is_oto8'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['is_oto8'] == 1 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Status</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="u_status" class="form-control">
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['u_status'] == 1 ? 'selected' : '' ) : ''?>>Active</option>
                                <option value="2" <?= !empty($userData) ? ( $userData[0]['u_status'] == 2 ? 'selected' : '' ) : ''?>>InActive</option>
                            </select>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="<?= !empty($userData) ? $userData[0]['u_id'] : 0 ?>" id="u_id">
                <button class="ad_btn ad_mTop15 bg-blue" onclick="submitUserRecords()"><?= !empty($userData) ? 'Update' : 'Save' ?></button>
            </div>
        </div>
    </div>
</div>