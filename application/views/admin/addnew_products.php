<div class="admin_main">
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="ad_box ad_mBottom30">
                <form method="post" enctype="multipart/form-data" id="uploadForm">
                <div class="form-group">
                    <label class="ad_label">Choose Categories</label>
                    <div class="ad_select_box">
                        <select name="p_categoryid" class="ad_select form-control validate" data-placeholder="Select Categories" data-isselect=1>
                            <?php if(!empty($categoriesList)) { 
                            foreach($categoriesList as $soloList) { 
                                $sel = !empty($resultSet) && $resultSet['p_categoryid'] == $soloList['cate_id'] ? 'selected' : '' ;
                                echo '<option '.$sel.' value="'.$soloList['cate_id'].'">'.$soloList['cate_name'].'</option>';
                            } } else {
                                echo '<option value="0">Please add category first</option>';
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Product Name</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control validate" name="p_name" data-maxlength=251 data-minlength=5 value="<?= !empty($resultSet) ? $resultSet['p_name'] : '' ?>"/>
                            <span style="color: #3a8ffd;">It should be between 5 to 250 Characters.</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Product URL</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control validate" name="p_urlname" value="<?= !empty($resultSet) ? $resultSet['p_urlname'] : '' ?>" />
                            <span style="color: #3a8ffd;">It should not contain space and symbols.</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Description</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <textarea class="form-control validate" name="p_description"  /><?= !empty($resultSet) ? $resultSet['p_description'] : '' ?></textarea>
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="ad_label">Product Image <?= !empty($resultSet) ? ' <a style="color: #007bff;font-size: 10px;font-weight: bold;margin-left: 20px;" target="_blank" href="'.$resultSet['p_imagelink'].'"> Download</a>' : '' ?></label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="file" class="form-control" name="p_imagelink" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Product Final Zip <?= !empty($resultSet) ? ' <a style="color: #007bff;font-size: 10px;font-weight: bold;margin-left: 20px;" target="_blank" href="'.$resultSet['p_finalprod'].'"> Download</a>' : '' ?></label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="file" class="form-control" name="p_finalprod"  />
                        </div>
                    </div>
                </div>

                <p>OR</p>
                
                <div class="form-group">
                    <label class="ad_label">Generate Image Name <a style="color: #007bff;font-size: 10px;font-weight: bold;margin-left: 20px;" href="javascript:;" onclick="generatestring('imageName')"> Click here</a></label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="imageName"  />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Product Image URL</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" name="p_imagelink_url"  />
                        </div>
                    </div>
                </div>

                
                <div class="form-group">
                    <label class="ad_label">Generate Zip Name <a style="color: #007bff;font-size: 10px;font-weight: bold;margin-left: 20px;" href="javascript:;" onclick="generatestring('zipName')"> Click here</a></label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="zipName"  />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Product Final Zip URL</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" name="p_finalprod_url"  />
                        </div>
                    </div>
                </div>

                <input type="hidden" name="uniqeid" value="<?= !empty($resultSet) ? $resultSet['p_id'] : 0 ?>">
                <input type="hidden" name="pageType" value="product">
                </form>
                <button class="ad_btn ad_mTop15 bg-blue" onclick="validateAndSubmit('product')"><?= !empty($resultSet) ? 'Update' : 'Save' ?></button>
            </div>
            
        </div>
    </div>
</div>