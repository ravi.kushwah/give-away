                    </div>
				</div>
				<div class="plr_login_vector">
					<img src="<?= base_url() ?>assets/auth/images/mockup.png" alt=""/>
					<div class="plr_quality_text">
						<h4>GiveSites Pro is your one-stop source for incredible online<br> giveaways, freebies, sweepstakes, contests or competitions.</h4>
					</div>
				</div>
			</div>
		</div>
		<!--===Login Section End===-->
		
		<!--===Notification model===-->
		<div class="plr_notification">
			<div class="plr_close_icon">
				<span class='close'>×</span>
			</div>
			<div class="plr_success_flex">
				<div class="plr_happy_img">
					<img src=""/>
				</div>
				<div class="plr_yeah">
					<h5></h5>
					<p></p>
				</div>
			</div>
		</div>
		<!--===Notification model===-->
		
	</div>
	<!--=== End Main wraapper ===-->
	
    <script>window.baseurl = "<?= base_url() ?>"</script>
    <script src="<?= base_url() ?>assets/common/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/auth/js/custom.js?q=<?= date('his') ?>"></script>
  </body>
</html>