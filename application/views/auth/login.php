<h1>Hello!<br>Introducing <span><?= SITENAME ?></span></h1>
<h5>Welcome Back, Please login to your account.</h5>
<div class="plr_login_form">
    <div class="login-content">
        <div class="plr_input_main">
            <div class="plr_input">
                <div class="plr_label_box">
					<label>Email Address</label>
				</div>
                <input type="text" value="<?= isset($_COOKIE['uem']) ? $_COOKIE['uem'] : '' ?>" placeholder="Enter Your Email" id="em" >
                <img src="<?= base_url() ?>assets/auth/images/msg.svg" alt=""/>
            </div>
            <div class="plr_input">
                <div class="plr_label_box">
					<label>Password</label>
				</div>
                <input type="password" value="<?= isset($_COOKIE['uem']) ? $_COOKIE['upwd'] : '' ?>" placeholder="Enter Your Password" id="pwd">
                <img src="<?= base_url() ?>assets/auth/images/password.svg" alt=""/>
            </div>
        </div>
    </div>
    <div class="plr_check_section">
        <ul>
            <li>
                <div class="plr_checkbox">
                    <input type="checkbox" <?= isset($_COOKIE['uem']) ? 'checked' : '' ?> id="rememberme" value="1">
                    <label for="rememberme">remember me</label>
                </div>
                <span><a href="<?= base_url('home/forgotpassword') ?>">forgot password?</a></span>
            </li>
        </ul>	
    </div>
    <div class="plr_login_btn">
        <a href="javascript:;" class="plr_btn" onclick="loginSection()">Login Now</a>
        <!-- <p>Don’t have an account? <a href="register.html">Sign Up</a></p> -->
    </div>
</div>