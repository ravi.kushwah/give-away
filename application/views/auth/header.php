<!DOCTYPE html>
<!-- 
Project: PLR Site Builder
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Begin Head -->

  <head>
    <!--=== Required meta tags ===-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=== custom css ===-->
	<link href="https://fonts.googleapis.com/css2?family=Outfit:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"/>
	<link rel="shortcut icon" type="image/ico" href="<?= base_url() ?>assets/auth/images/favicon.png"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/auth/css/auth.css"/>
	<!--=== custom css ===-->
    <title><?= SITENAME ?><?= $pageName != '' ? ' | '.$pageName : '' ?></title>
  </head>
  <body>
	<!--=== start Main wraapper ===-->
	<div class="plr_main_wrapper">
	
		<!--===Login Section Start===-->
		<div class="plr_login_section"> 
			<div class="plr_login_flex">
				<div class="plr_login_main">
					<div class="plr_login_auth">
						<img src="<?= base_url() ?>assets/auth/images/logo.png" alt=""/>