<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response extends CI_Controller {
		
	public function index(){
		die('Unauthorize Access!!');
	}
		
    function ipn(){
		if(isset($_POST['WP_BUYER_NAME'])){

			/*
			
			GiveSites Pro (wso_yzy80z) - is_fe
			GiveSites Unlimited (wso_y3mmzy) - is_oto1
			GiveSites Traffic Blaster (wso_m8nrrb) - is_oto2
			GiveSites Pro Max  (wso_dvpjjc) - is_oto3
			Niche Pro Sites  (wso_txk1xc) - is_oto4
			DFY Platinum Edition (wso_y901b6) - is_oto5
			GiveSites Pro Agency (wso_yr6vj3) - is_oto6
			GiveSites Reseller (wso_g5t398) - is_oto7
			GiveSites Pro Whitelabel  (wso_m5vgcw) - is_oto8


			*/
			
            $this->DBfile->put_data('user_records' , array('recdata'=>json_encode($_POST),'recemail'=>$_POST['WP_BUYER_EMAIL'],'recdate'=>date('d-m-Y'),'product_id'=>$_POST['WP_ITEM_NUMBER'],'payment'=>$_POST['WP_SALE_AMOUNT']));
			
            $userDetails = $this->DBfile->get_data('usertbl' , array('u_email' => $_POST['WP_BUYER_EMAIL']),'*');			
			$is_fe = $is_oto1 = $is_oto2 = $is_oto3 = $is_oto4 = $is_oto5 = $is_oto6 = $is_oto7 = $is_oto8 = 0;
              
            $name = $_POST['WP_BUYER_NAME'];
			
			if( $_POST['WP_ITEM_NUMBER'] == 'wso_yzy80z' ){
				$is_fe = 1;
				$prodName = 'GiveSites Pro';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_y3mmzy' ){
				$is_oto1 = 1;
				$prodName = 'GiveSites Pro Unlimited';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_m8nrrb' ){
				$is_oto2 = 1;
				$prodName = 'GiveSites Pro Traffic Blaster';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_dvpjjc' ){
				$is_oto3 = 1;
				$prodName = 'GiveSites Pro Max';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_txk1xc' ){
				$is_oto4 = 1;
				$prodName = 'Niche Pro Sites';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_y901b6' ){
				$is_oto5 = 1;
				$prodName = 'DFY Platinum Edition';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_yr6vj3' ){
				$is_oto6 = 1;
				$prodName = 'GiveSites Pro Agency';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_g5t398' ){
				$is_oto7 = 1;
				$prodName = 'GiveSites Pro Reseller';
			} elseif( $_POST['WP_ITEM_NUMBER'] == 'wso_m5vgcw' ){
				$is_oto8 = 1;
				$prodName = 'GiveSites Pro WhiteLabel';
			}

			if(empty($userDetails)) { // NEW USER
				$password = substr(md5(date('his')), 0, 8);
				$user_details = array( 
					'u_name' => $name,
					'u_email' => $_POST['WP_BUYER_EMAIL'],
					'u_password' => md5($password),
					'u_status' => 1,
					'u_type' => 2,
					'is_fe'=> $is_fe,
					'is_oto1'=> $is_oto1,
					'is_oto2'=> $is_oto2,
					'is_oto3'=> $is_oto3,
					'is_oto4'=> $is_oto4,
					'is_oto5'=> $is_oto5,
					'is_oto6'=> $is_oto6,
					'is_oto7'=> $is_oto7,
					'is_oto8'=> $is_oto8,
					'u_purchaseddate'=> date('d-m-Y h:i:s')
				);
				$this->DBfile->put_data('usertbl' , $user_details);    
				
				/* GetResponse */

				$listID = 'LADAs'; // PLR Site Builder List Token
				$args = array(
					'campaign' => array('campaignId'=>$listID),
					'email' => $_POST['WP_BUYER_EMAIL'],
					'name'  =>  $_POST['WP_BUYER_NAME'],
					'dayOfCycle'=>0,
				);                                                                
				$data_string = json_encode($args);                                                                                   
				$ch = curl_init('https://api.getresponse.com/v3/contacts');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
					'X-Auth-Token:api-key 7p55ps3l2j6rvww0e7xyi0ir94pzv6j5',                                                                      
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string))                                                                       
				);
				$result = curl_exec($ch);

				/* GetResponse */


			}else { // OLD USER

			if( $_POST['WP_ITEM_NUMBER'] == 'wso_yzy80z' )
				$is_fe = 1; 
			else
				$is_fe = $userDetails[0]['is_fe'];
			
			if( $_POST['WP_ITEM_NUMBER'] == 'wso_y3mmzy' )
				$is_oto1 = 1;
			else
				$is_oto1 = $userDetails[0]['is_oto1'];
			
			if( $_POST['WP_ITEM_NUMBER'] == 'wso_m8nrrb' )
				$is_oto2 = 1;
			else
				$is_oto2 = $userDetails[0]['is_oto2'];
			
			if( $_POST['WP_ITEM_NUMBER'] == 'wso_dvpjjc' )
				$is_oto3 = 1;
			else
				$is_oto3 = $userDetails[0]['is_oto3'];

			if( $_POST['WP_ITEM_NUMBER'] == 'wso_txk1xc' )
				$is_oto4 = 1;
			else
				$is_oto4 = $userDetails[0]['is_oto4'];
			
			if( $_POST['WP_ITEM_NUMBER'] == 'wso_y901b6' )
				$is_oto5 = 1;
			else
				$is_oto5 = $userDetails[0]['is_oto5'];
			
			if( $_POST['WP_ITEM_NUMBER'] == 'wso_yr6vj3' )
				$is_oto6 = 1;
			else
				$is_oto6 = $userDetails[0]['is_oto6'];
			
			if( $_POST['WP_ITEM_NUMBER'] == 'wso_g5t398' )
				$is_oto7 = 1;
			else
				$is_oto7 = $userDetails[0]['is_oto7'];

			if( $_POST['WP_ITEM_NUMBER'] == 'wso_m5vgcw' )
				$is_oto8 = 1;
			else
				$is_oto8 = $userDetails[0]['is_oto8'];

				$uid = $userDetails[0]['u_id'];
				$user_details = array(
					'u_status' => 1,
					'is_fe'=> $is_fe,
					'is_oto1'=> $is_oto1,
					'is_oto2'=> $is_oto2,
					'is_oto3'=> $is_oto3,
					'is_oto4'=> $is_oto4,
					'is_oto5'=> $is_oto5,
					'is_oto6'=> $is_oto6,
					'is_oto7'=> $is_oto7,
					'is_oto8'=> $is_oto8
				);
				$password = "EXISTING password will work"; 
                $this->DBfile->set_data('usertbl' , $user_details ,array('u_id' => $uid));
			} 
			$user_email = strtolower($_POST['WP_BUYER_EMAIL']);
            
			$body = '<p>Hello '.$name.',</p>';
			$body .= '<p>Thank you for buying - '.$prodName.'.</p>';
			$body .= '<p> Here are your login details <br/> ';
            $body .= 'Login URL : '.base_url().'<br/>';
            $body .= 'Email : '.$user_email.'<br/>';
            $body .= 'Password : '.$password.'<br/>';
            $body .= '</p>';
            $body .= '<p>You can change your password from your profile page.</p>';
            $body .= '<br/><p>Note: For any query, please email us at tickets@givesites-pro.p.tawk.email</p>';
			$body .= '<p>To your success.<br/>Team, '.SITENAME.'</p>';
			sendUserEmailMandrill($user_email,'Product Access ['.SITENAME.']',$body);
		}
	}
}
?>