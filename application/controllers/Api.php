<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
		
	function general_giveAways(){
	    $multiJoin = array('multiple',array(array('categories','categories.cate_id=giveaway.g_categories')));
        $giveaway = $this->DBfile->select_data('giveaway.*,categories.cate_name','giveaway','','','','',$multiJoin);
        $webSite = $this->DBfile->select_data('website.w_id,website.w_siteurl,website.while_claiming_the_offer,website.w_type,website.w_userid','website','','','','');
        $resp = array('status'=>1,'type'=>'General GiveAways','websiteData'=>$webSite,'Data'=>$giveaway);
	    echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}
	function gmaes_giveAways(){
	    $data = $this->DBfile->get_data('giveaway_games' ,'','','','');
	    $webSite = $this->DBfile->select_data('website.w_id,website.w_siteurl,website.while_claiming_the_offer,website.w_type','website','','','','');
	     $resp = array('status'=>1,'type'=>'Gmaes GiveAways','websiteData'=>$webSite,'Data'=>$data);
	     echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}
	function training_videos(){
	    $data = $this->DBfile->get_data('training_videos' ,'','','','');
	    $webSite = $this->DBfile->select_data('website.w_id,website.w_siteurl,website.while_claiming_the_offer,website.w_type','website','','','','');
	     $resp = array('status'=>1,'type'=>'training videos','websiteData'=>$webSite,'Data'=>$data);
	     echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}
	function coupon(){
	    $data = $this->DBfile->get_data('coupons' ,'','','','');
	    $webSite = $this->DBfile->select_data('website.w_id,website.w_siteurl,website.while_claiming_the_offer,website.w_type','website','','','','');
	     $resp = array('status'=>1,'type'=>'training videos','websiteData'=>$webSite,'Data'=>$data);
	     echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}
	function contests(){
	     $multiJoin = array('multiple',array(array('categories','categories.cate_id=Contests.g_categories')));
        $data = $this->DBfile->select_data('Contests.*,categories.cate_name','Contests','','','','',$multiJoin);
	    $webSite = $this->DBfile->select_data('website.w_id,website.w_siteurl,website.while_claiming_the_offer,website.w_type','website','','','','');
	     $resp = array('status'=>1,'type'=>'Contests','websiteData'=>$webSite,'Data'=>$data);
	     echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}
	function gift_cards(){
	    $multiJoin = array('multiple',array(array('categories','categories.cate_id=Gift_Cards.g_categories')));
        $data = $this->DBfile->select_data('Gift_Cards.*,categories.cate_name','Gift_Cards','','','','',$multiJoin);
	    $webSite = $this->DBfile->select_data('website.w_id,website.w_siteurl,website.while_claiming_the_offer,website.w_type','website','','','','');
	     $resp = array('status'=>1,'type'=>'Gift Cards','websiteData'=>$webSite,'Data'=>$data);
	     echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}
	function news_letter (){
	     $webRes = $this->DBfile->get_data('website',array('w_siteurl'=>$_POST['w_siteurl']));
	    if( isset($_POST['news_email']) ){
            sendToAutoResponder($_POST['news_email'],$_POST['news_name'],$webRes[0]['w_id'],'w_newsletterlist');
           $resp = array('status'=>1,'smg'=>'Congratulations! Thanks for subscribing to our newsletter.');
        }
        else
           $resp = array('status'=>0,'smg'=>'Something went wrong');
		echo json_encode($resp,JSON_UNESCAPED_SLASHES);
    }
    function VerifyAPIKey($key=''){
        if(!empty($_POST['key'])){
            $webRes = $this->DBfile->get_data('website',array('w_site_api_key'=>$_POST['key']));
             $web_giveaway = $this->DBfile->get_data('web_giveaway',array('g_webid'=>$webRes[0]['w_userid']));
            if(!empty($webRes)){
                $resp = array('status'=>1,'data'=>$webRes,'web_giveaways'=>$web_giveaway);
            }else{
                $resp = array('status'=>0,'smg'=>'API Key Is Wrong Please try Again ');
            }
         
        }else{
          $resp = array('status'=>0,'smg'=>'Something went wrong');
        }
	echo json_encode($resp,JSON_UNESCAPED_SLASHES);
    }
}
?>