<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Ar_setting extends CI_Controller{
    public $CI;
     
	function __Construct()
	{
		parent::__Construct();
	    $this->load->model('my_model');
    }

    
    public function autoresponder(){
        // if($_SESSION['id'] == 1){
            // echo"<pre>";print_r($_POST);
        //     echo"<pre>";print_r($_SESSION['id']);
        //     // print_r($data);
        //     die;
        // }
		$memberId = $_SESSION['id']; 
		if(empty($_POST['responder'])){
			echo json_encode(array( 'error' => 'Error occur may be something wrong.'));
			return;
		}
		require_once 'subscriber/subscriber.php';
		$objlist = new subscriber();
		$list = $objlist->switch_responder($_POST['apikey'], 'getList', $_POST['responder']);
		if(!isset($list['error'])){ 
			$where = array( 'm_id' => $memberId, 'mkey' => 'autoresponder' );
			$res = $this->my_model->select_data(['field'=>'*','table'=>'autoresponder', 'where'=>$where]);

			if(empty($res)){
				$data = array();
				if($_POST['responder'] != 'Aweber'){
					$data[$_POST['responder']] = $_POST['apikey'];
				}else{
					$data[$_POST['responder']] = $list['data'];
				}

				$array = array(
					'm_id' => $memberId,
					'mkey' => 'autoresponder',
					'value' =>  json_encode($data) 
				);
				
				$this->my_model->insert_data(['table'=>'autoresponder', 'data'=>$array]);
			}else{
				$data = (array) json_decode( $res[0]['value'] );

				if($_POST['responder'] != 'Aweber'){
				    $data[$_POST['responder']] = $_POST['apikey'];
				}else{
					$data[$_POST['responder']] = $list['data'];
				}
				
				$array = array(
					'm_id' => $memberId,
					'mkey' => 'autoresponder',
					'value' =>  json_encode($data) 
				);
				$this->my_model->update_data(['table'=>'autoresponder', 'data'=>$array, 'where'=>$where]);
			}
			echo json_encode(array( 'status' => 1, 'msg'=>'Auto-Responder connected successfully!','data'=>json_encode($data[$_POST['responder']]) ));
		}else{
			echo json_encode(array( 'status' => 0, 'msg' => 'An error occurred while getting your list.'));
		}
	}

	public function disconnect_autoresponder(){
		$memberId = $_SESSION['id'];
		// print_r($_GET); exit;
		if(empty($_GET['ar'])){
			echo json_encode(array( 'error' => 'Error occur may be something wrong.'));
			return;
		}
		
		$where = array( 'm_id' => $memberId, 'mkey' => 'autoresponder' );
		$res = $this->my_model->select_data(['field'=>'*','table'=>'autoresponder','where'=>$where]);
		
		$data = (array) json_decode( $res[0]['value'] );
		
		unset( $data[$_GET['ar']] );
		
		$array = array(
			'm_id' => $memberId,
			'mkey' => 'autoresponder',
			'value' =>  json_encode($data) 
		);
		
		$this->my_model->update_data(['table'=>'autoresponder', 'data'=>$array, 'where'=>$where , 'limit' => 1]);
		
		echo json_encode(array( 'status' => 1, 'msg'=>'Auto Responder disconnected successfully!' ));
		die();
	}

	public function custom_html(){
        $keyArr = [];
		$memberId = $_SESSION['id'];
		$res = $this->my_model->select_data(['field'=>'*', 'table'=>'autoresponder', 'where'=>array('m_id'=>$memberId)]);
		if(empty($res)){
		    $Formdata = array(
					'title' => $_POST['form_name'],
					'user_id' => $memberId,
					'form_code' => $_POST['customhtml']
    			);
    		$ins = $this->my_model->insert_data(['table'=>'ar_custom_html','data'=>$Formdata]);
    		$inserted_id = $this->db->insert_id();
		    $in_data = array(
		        'm_id'=> $memberId,
		        'mkey'=>'autoresponder',
		        'value'=> json_encode(array('CustomHTML'=>[$inserted_id]))
		        );
		    $in = $this->my_model->insert_data(['table'=>'autoresponder','data'=>$in_data]);
		    if($ins && $in){
		        echo json_encode(array( 'status' => 1, 'msg'=>'Custom HTML saved successfully!','id'=>$inserted_id ));
		    }else{
		        echo json_encode(array( 'status' => 0, 'msg'=>'Something went wrong.' ));
		    }
		}else{
		    $data = json_decode($res[0]['value'],true);
		   
		    if(isset($_POST['form_id']) && isset($_POST['add']) && $_POST['add']){
    			foreach($data as $key=>$val){
    		        if($key == 'CustomHTML'){
    		            $check = 1;
    		            break;
    		        }else{
    		            $check = 0;
    		        }
    		    }
    		    if($check == 0 || ($check && $_POST['entry_type'] == 1)){ // insert
    		        $checkForm = $this->my_model->select_data(['field'=>'id','table'=>'ar_custom_html','where'=>array('user_id'=>$memberId,'title'=>$_POST['form_name'])]);
    		        if(!empty($checkForm)){
    		            echo json_encode(array( 'status' => 0, 'msg'=>'Form name is already taken. Please try another.' ));
    		            return;
    		        }else{
        			    $Formdata = array(
        					'title' => $_POST['form_name'],
        					'user_id' => $memberId,
        					'form_code' => $_POST['customhtml']
        				);
        				$ins = $this->my_model->insert_data(['table'=>'ar_custom_html','data'=>$Formdata]);
        				if($ins){
        				    if($check && $_POST['entry_type'] == 1){
        				        array_push($data['CustomHTML'],$this->db->insert_id());    
        				    }
        				    else{
        				        $data['CustomHTML'] = [$this->db->insert_id()];
        				    }
        				    $last_id = $this->db->insert_id();
        				    $up = $this->my_model->update_data(['table'=>'autoresponder','data'=>array('value'=>json_encode($data)),'where'=>array('m_id'=>$memberId)]);
        				    if($up){
        				        echo json_encode(array( 'status' => 1, 'msg'=>'Custom HTML saved successfully!','id'=>$last_id ));
        				    }else{
        				        echo json_encode(array( 'status' => 0, 'msg'=>'Something went wrong.' ));
        				    }
        				}
    		        }
    			}else if($check == 1 && $_POST['entry_type'] == 2){ //update
    			    $Formdata = array(
    					'title' => $_POST['form_name'],
    					'form_code' => $_POST['customhtml']
    				);
    			    $updatee = $this->my_model->update_data(['table'=>'ar_custom_html','data'=>$Formdata,'where'=>array('user_id'=>$memberId,'id'=>$_POST['form_id'])]);
    			    if($updatee){
    			        echo json_encode(array( 'status' => 1, 'msg'=>'Custom HTML saved successfully!' ));
    			    }else{
    			        echo json_encode(array( 'status' => 0, 'msg'=>'Something went wrong.' ));
    			    }
    			}
    		}
    		
    		if(isset($_POST['form_id']) && isset($_POST['remove']) && $_POST['remove']){
    		    $custom_html = $this->my_model->select_data(['field'=>'id','table'=>'ar_custom_html','where'=>array('id','desc')]);
                $del = $this->my_model->delete_data(['table'=>'ar_custom_html','where'=>array('id'=>$_POST['form_id'],'user_id'=>$memberId)]);
                if($del){
    		        if(($key = array_search($_POST['form_id'], $data['CustomHTML'])) !== false) {
    		            if(count($data['CustomHTML']) == 1){
    		                unset($data['CustomHTML']);   
    		                $data['CustomHTML'] = [];
    		            }else{
    		                 unset($data['CustomHTML'][$key]);
    		                 $newarr = array_values($data['CustomHTML']);
    		                 $data['CustomHTML'] = $newarr;
    		            }
		            
                    $up = $this->my_model->update_data(['table'=>'autoresponder', 'data'=>array('value'=>json_encode($data)), 'where'=>array('m_id'=>$memberId)]);
                    	if($up){
            			    $id = 0;
            			    if(!empty($custom_html)){
            			        $id = $custom_html[0]['id'];
            			    }
            			    echo json_encode(array( 'status' => 1, 'msg'=>'Custom HTML removed successfully!','id'=> $id));
        		        }else{
        		            echo json_encode(array( 'status' => 0, 'msg'=>'Something went wrong.' ));
        		        }
        	    	}else{
                        echo json_encode(array( 'status' => 0, 'msg'=>'Something went wrong.' ));
        	    	}
		      }else{
		           	echo json_encode(array( 'status' => 0, 'msg'=>'Something went wrong.' ));
		      }
	        }
		}
	
		
		die();
    }

    public function get_list($autoresponder = ''){
        $autoresponder = ($autoresponder != '')?$autoresponder:'';
	    $memberId = $_SESSION['id'];
		if(empty($autoresponder)){
			echo json_encode(array( 'error' => 'Error occur may be something wrong.'));
			return;
		}else if(!empty($autoresponder) && $autoresponder == 'CustomHTML'){
		    $getData = $this->my_model->select_data([
                'field' => '*',
                'table' => 'website',
                'where' => array('w_userid'=>$memberId)
            ]);
		    if(!empty($getData)){
		        if(!empty($custom_html)){
		            $list = array();
		            for($j=0; $j<count($custom_html); $j++){
		                  $getName = $this->my_model->select_data([
                            'field' => '*',
                            'table' => 'website',
                            'where' => array('w_userid'=>$memberId)
                        ]);
                        if(!empty($getName)){
                            $list[$custom_html[$j]] = $getName[0]['title'];
                        }
		            }
	                $list = array('list' => $list,'error'=>false);
	                echo json_encode([
                        'status' => 1,
                        'data' => $list
                    ]);
		        }
		    }
		}else{
    		if(!empty($autoresponder)){
    			$where = array('m_id' => $memberId, 'mkey' => 'autoresponder');
    			$res = $this->my_model->select_data([
                    'field' => '*',
                    'table' => 'autoresponder',
                    'where' => $where
                ]);
    			$data = (array) json_decode( $res[0]['value'] );
    			if(empty($data[$autoresponder])){
    				echo json_encode(array('error'=>'Please check settings.'));
    				die();
    			}
    			$api = (array) $data[$autoresponder];
    			require_once 'subscriber/subscriber.php';
    			$objlist = new subscriber();
    			$list = $objlist->switch_responder($api, 'getList', $autoresponder);
    			
    			echo json_encode([
                    'status' => 1,
                    'data' => $list
                ]);
    		}	
		}
		die();
    }
    
    
}
