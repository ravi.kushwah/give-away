<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');

class ga extends CI_Controller {
		
	public function _remap($websiteName='',$params = array()){
	   // print_r($params);
	   // die();
        if( $websiteName == 'index' )
            redirect(base_url());
        else{
            $webRes = $this->DBfile->get_data('website',array('w_siteurl'=>$websiteName));
            if(empty($webRes))
                redirect(base_url());
                
            $this->session->set_userdata('web_id', $webRes[0]['w_id']);
            $this->session->set_userdata('web_url', base_url().'ga/'.$webRes[0]['w_siteurl']);

            if(isset($this->session->userdata['c_loggedin']) )
                $data['is_customer'] = 1;
            else
                $data['is_customer'] = 0;

            $data['websiteData'] = $webRes[0];
            $data['cateList'] = $this->DBfile->get_data('categories');
            $data['custom_cateList'] = $this->DBfile->get_data('web_categories',array('cate_webid'=>$webRes[0]['w_id']));
            if(isset($this->session->userdata['id'])){
                $id= $this->session->userdata['id'];
            }else{
                $id ="";
            }
            $data['autoresponder'] = $this->DBfile->get_data('autoresponder',array('m_id'=>$id));
            $website = $this->session->userdata['web_url'];
			$data['checkURL'] = explode('ga/',$website);
            
            if( !empty($params) ){
                $cateDetail = $this->DBfile->get_data('categories' , array('cate_slug'=>$params[0]));
                $soloProdDetail = $this->DBfile->get_data('giveaway' , array('g_id'=>$params[0]));
                if( $params[0] == 'p' ){
                    $compArray = $webRes[0]['w_compliancesettings'] != '' ? json_decode($webRes[0]['w_compliancesettings'],true) : array() ;
                    $comText = $this->load->view('frontend/compliance/'.$params[1],$data,true);
                    $final_content = str_replace("{BUSINESS-NAME}", !empty($compArray) ? $compArray['business_name'] : $webRes[0]['w_title'] ,$comText);
					$final_content = str_replace("{BUSINESS-EMAIL}", !empty($compArray) ? $compArray['business_email'] : '' ,$final_content);
					$final_content = str_replace("{BUSINESS-PHONE}", !empty($compArray) ? $compArray['business_phone'] : '' ,$final_content);
					$final_content = str_replace("{BUSINESS-URL}", !empty($compArray) ? $compArray['business_url'] : base_url().'ga/'.$webRes[0]['w_siteurl'] ,$final_content);
					$final_content = str_replace("{BUSINESS-WEBSITE}", !empty($compArray) ? $compArray['business_url'] : base_url().'ga/'.$webRes[0]['w_siteurl'] ,$final_content);
					$final_content = str_replace("{BUSINESS-ADDRESS}", !empty($compArray) ? $compArray['business_address'] : '' ,$final_content);

                    $data['compliance_text'] = $final_content;
                    $data['pageName'] = ucwords( str_replace('-',' ',$params[1]) );
                    $viewName = '';
                }
                elseif( $params[0] == 'newsletter' ){
                    if( isset($_POST['news_email']) ){
                        sendToAutoResponder($_POST['news_email'],$_POST['news_name'],$webRes[0]['w_id'],'w_newsletterlist');
                        echo 1;
                    }
                    else
                        echo 0;
                    die();
                }
                elseif( $params[0] == 'single_page'){
                    if(!empty($params[1])){
                        if(is_numeric($params[1])){
                         $data['website'] = $this->DBfile->get_data('website' ,array('w_siteurl'=>$this->uri->segment(2)),'','',array(0,1));
                            if($data['website'][0]['w_type']==1){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_giveaway' ,array('s_id'=>$params[1],'status'=>1),'','',array(0,6));
                                if(empty( $data['submit_giveaway'])){
                                    $data['prodList'] = $this->DBfile->get_data('giveaway' ,array('g_id'=>$params[1]),'','',array(0,6));
                                    $prod_id = empty($data['prodList']) ? '' : $data['prodList'][0]['g_categories'];
                                    if(!empty($prod_id)){
                                        $data['cat'] = $this->DBfile->get_data('categories' ,array('cate_id'=>$prod_id),'','',array(0,1));
                                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 6');
                                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                                        $data['relatedData'] = $this->DBfile->query('SELECT * FROM `giveaway` WHERE `g_categories`='.$prod_id.' ORDER BY RAND() LIMIT 6');
                                    }else{
                                        redirect(base_url('ga/'.$this->uri->segment(2)));
                                    }
                                }else{
                                  if(!empty($data['submit_giveaway'])){
                                    $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 6');
                                    $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                                    $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                                    $data['relatedData'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 6');
                                  }else{
                                        redirect(base_url('ga/'.$this->uri->segment(2)));
                                }
                                }
                                
                                
                            }else if($data['website'][0]['w_type']==5){
                                  $data['submit_giveaway'] = $this->DBfile->get_data('submit_games_giveaway' ,array('s_id'=>$params[1],'status'=>1),'','',array(0,6));
                                if(empty( $data['submit_giveaway'])){
                                    $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,array('g_id'=>$params[1]),'','',array(0,6));
                                    $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                                    $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                                    $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                                    $data['relatedData'] = $this->DBfile->query('SELECT * FROM `giveaway_games`  ORDER BY RAND() LIMIT 6'); 
                                }else{
                                    $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                                    $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                                    $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                                    $data['relatedData'] = $this->DBfile->query('SELECT * FROM `giveaway_games`  ORDER BY RAND() LIMIT 6'); 
                                }
                               
                            }else if($data['website'][0]['w_type']==3){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_giveaway' ,array('s_id'=>$params[1],'status'=>1),'','',array(0,6));
                                if(empty( $data['submit_giveaway'])){
                                    $data['prodList'] = $this->DBfile->get_data('Contests' ,array('g_id'=>$params[1]),'','',array(0,6));
                                    $prod_id = empty($data['prodList']) ? '' : $data['prodList'][0]['g_categories'];
                                    if(!empty($prod_id)){
                                        $data['cat'] = $this->DBfile->get_data('categories' ,array('cate_id'=>$prod_id),'','',array(0,1));
                                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 6');
                                        $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                                        $data['relatedData'] = $this->DBfile->query('SELECT * FROM `Contests` WHERE `g_categories`='.$prod_id.' ORDER BY RAND() LIMIT 6');
                                    }else{
                                        redirect(base_url('ga/'.$this->uri->segment(2)));
                                    }
                                }else{
                                    if(!empty($data['submit_giveaway'])){
                                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 6');
                                        $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                                        $data['relatedData'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 6');
                                    }else{
                                        redirect(base_url('ga/'.$this->uri->segment(2)));
                                    }
                                }
                            }else if($data['website'][0]['w_type']==4){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_gift_cards' ,array('s_id'=>$params[1],'status'=>1),'','',array(0,6));
                                if(empty( $data['submit_giveaway'])){
                                    $data['prodList'] = $this->DBfile->get_data('Gift_Cards' ,array('g_id'=>$params[1]),'','',array(0,6));
                                    $prod_id = empty($data['prodList']) ? '' : $data['prodList'][0]['g_categories'];
                                    if(!empty($prod_id)){
                                        $data['cat'] = $this->DBfile->get_data('categories' ,array('cate_id'=>$prod_id),'','',array(0,1));
                                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                                        $data['relatedData'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` WHERE `g_categories`='.$prod_id.' ORDER BY RAND() LIMIT 6');
                                    }else{
                                        redirect(base_url('ga/'.$this->uri->segment(2)));
                                    }
                                }else{
                                    if(!empty($data['submit_giveaway'])){
                                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                                        $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                                        $data['relatedData'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                                    }else{
                                        redirect(base_url('ga/'.$this->uri->segment(2)));
                                    }
                                }
                            }else if($data['website'][0]['w_type']==6){
                                 $data['prodList'] = $this->DBfile->get_data('coupons' ,array('id'=>$params[1]),'','',array(0,6));
                                if(!empty( $data['prodList'])){
                                    $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 6');
                                     $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 4');
                                    $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 2');
                                    $data['relatedData'] = $this->DBfile->query('SELECT * FROM `coupons`  ORDER BY RAND() LIMIT 6');
                                }else{
                                    redirect(base_url('ga/'.$this->uri->segment(2)));
                                }
                            }else if($data['website'][0]['w_type']==2){
                                 $data['prodList'] = $this->DBfile->get_data('training_videos' ,array('v_id'=>$params[1]),'','',array(0,6));
                                if(!empty( $data['prodList'])){
                                    $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 6');
                                     $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 4');
                                    $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 2');
                                    $data['relatedData'] = $this->DBfile->query('SELECT * FROM `training_videos`  ORDER BY RAND() LIMIT 6');
                                }else{
                                    redirect(base_url('ga/'.$this->uri->segment(2)));
                                }
                            }
                            $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                            $viewName = 'single_page';
                        }else{
                            redirect(base_url('ga/'.$this->uri->segment(2)));
                        }
                    }else{
                         redirect(base_url('ga/'.$this->uri->segment(2)));
                    }
                    
                    
                } elseif( $params[0] == 'view_all'){
                    $data['website'] = $this->DBfile->get_data('website' ,array('w_siteurl'=>$this->uri->segment(2)),'','',array(0,1));
                    if($data['website'][0]['w_type']==1){
                        $data['prodList'] = $this->DBfile->get_data('giveaway' ,'','','',array(0,15));
                        $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,15));
                        $data['web_giveawayC'] = $this->DBfile->getCount('web_giveaway' ,'');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('giveaway','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==5){
                        $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,'','','',array(0,15));
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('giveaway_games','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==3){
                       $data['prodList'] = $this->DBfile->get_data('Contests' ,'','','',array(0,15));
                        $data['web_giveaway'] = $this->DBfile->get_data('Contests' ,'','','',array(0,15));
                        $data['web_giveawayC'] = $this->DBfile->getCount('Contests' ,'');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('Contests','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==4){
                         $data['prodList'] = $this->DBfile->get_data('Gift_Cards' ,'','','',array(0,15));
                        $data['web_giveaway'] = $this->DBfile->get_data('Gift_Cards' ,'','','',array(0,15));
                        $data['web_giveawayC'] = $this->DBfile->getCount('Gift_Cards' ,'');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('Gift_Cards','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==6){
                         $data['prodList'] = $this->DBfile->get_data('coupons' ,'','','',array(0,15));
                        $data['web_giveawayC'] = $this->DBfile->getCount('coupons' ,'');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('coupons','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==2){
                         $data['prodList'] = $this->DBfile->get_data('training_videos' ,'','','',array(0,15));
                        $data['web_giveawayC'] = $this->DBfile->getCount('training_videos' ,'');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('training_videos','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }
                    $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                    $viewName = 'view_all';
                } elseif( $params[0] == 'submit-giveaways'){
                    $data['website'] = $this->DBfile->get_data('website' ,array('w_siteurl'=>$this->uri->segment(2)),'','',array(0,1));
                    if($data['website'][0]['w_type']==1){
                        $data['prodList'] = $this->DBfile->get_data('giveaway' ,'','','',array(0,15));
                        $data['categories'] = $this->DBfile->get_data('categories' ,'','','','');
                        $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,15));
                        $data['web_giveawayC'] = $this->DBfile->getCount('web_giveaway' ,'');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 6');
                           $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('giveaway','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==5){
                        $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,'','','',array(0,15));
                        $data['categories'] = $this->DBfile->get_data('categories' ,'','','','');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                           $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('giveaway_games','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==3){
                        $data['prodList'] = $this->DBfile->get_data('Contests' ,'','','',array(0,15));
                        $data['categories'] = $this->DBfile->get_data('categories' ,'','','','');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 6');
                           $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('Contests','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==4){
                        $data['prodList'] = $this->DBfile->get_data('Gift_Cards' ,'','','',array(0,15));
                        $data['categories'] = $this->DBfile->get_data('categories' ,'','','','');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                           $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('Gift_Cards','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==6){
                        $data['prodList'] = $this->DBfile->get_data('coupons' ,'','','',array(0,15));
                        $data['categories'] = $this->DBfile->get_data('categories' ,'','','','');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 6');
                           $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('coupons','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }else if($data['website'][0]['w_type']==2){
                        $data['prodList'] = $this->DBfile->get_data('training_videos' ,'','','',array(0,15));
                        $data['categories'] = $this->DBfile->get_data('categories' ,'','','','');
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 6');
                           $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 2');
                        $totProd = $this->DBfile->get_data_limit('training_videos','','','','','','');
                        $data['numberOfPages'] = ceil(count($totProd)/15) ;
                    }
                    $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                    $viewName = 'submit_giveaways';
                }elseif( $params[0] !='single_page' && $params[0] !='view_all' && $params[0] !='newsletter' && $params[0] !=''){
                    $data['website'] = $this->DBfile->get_data('website' ,array('w_siteurl'=>$this->uri->segment(2)),'','',array(0,1));
                    if(isset($params[1])=='web-giveAway'){
                        $categories = $this->DBfile->get_data('web_categories' ,array('cate_slug'=>$params[2]),'','',array(0,1));
                        if(empty($categories)){
                            if($data['website'][0]['w_type']==1){
                                $data['prodList'] = $this->DBfile->get_data('web_giveaway' ,array('slug'=>$params[0]),'','',array(0,6));
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `web_giveaway` ORDER BY RAND() LIMIT 4');
                                $data['cat'] = $this->DBfile->get_data('web_categories' ,array('cate_id'=>$data['prodList'][0]['g_categories']),'','',array(0,1));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `web_giveaway` ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `web_giveaway` ORDER BY RAND() LIMIT 2');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `web_giveaway` WHERE `g_categories`='.$data['prodList'][0]['g_categories'].' ORDER BY RAND() LIMIT 6');
                            }else if($data['website'][0]['w_type']==5){
                                $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,array('g_id'=>$params[1]),'','',array(0,6));
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 8');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `giveaway_games`  ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                            }else if($data['website'][0]['w_type']==3){
                                $data['prodList'] = $this->DBfile->get_data('Contests' ,array('slug'=>$params[0]),'','',array(0,6));
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                                $data['cat'] = $this->DBfile->get_data('web_categories' ,array('cate_id'=>$data['prodList'][0]['g_categories']),'','',array(0,1));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `Contests` WHERE `g_categories`='.$data['prodList'][0]['g_categories'].' ORDER BY RAND() LIMIT 6');
                            }else if($data['website'][0]['w_type']==4){
                                $data['prodList'] = $this->DBfile->get_data('Gift_Cards' ,array('slug'=>$params[0]),'','',array(0,6));
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                                $data['cat'] = $this->DBfile->get_data('web_categories' ,array('cate_id'=>$data['prodList'][0]['g_categories']),'','',array(0,1));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` WHERE `g_categories`='.$data['prodList'][0]['g_categories'].' ORDER BY RAND() LIMIT 6');
                            }else if($data['website'][0]['w_type']==6){
                                $data['prodList'] = $this->DBfile->get_data('coupons' ,array('slug'=>$params[0]),'','',array(0,6));
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 2');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `coupons`  ORDER BY RAND() LIMIT 6');
                            }else if($data['website'][0]['w_type']==2){
                                $data['prodList'] = $this->DBfile->get_data('training_videos' ,array('slug'=>$params[0]),'','',array(0,6));
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 2');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `training_videos`  ORDER BY RAND() LIMIT 6');
                            }
                            $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                            $viewName = 'single_page';
                        }else{
                            $cid= $categories[0]['cate_id'];
                            if($data['website'][0]['w_type']==1){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_giveaway' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                $data['prodList'] = $this->DBfile->get_data('giveaway' ,array('g_categories'=>$cid),'','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,array('g_categories'=>$cid),'','',array(0,6));
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway`WHERE g_categories='.$cid.' ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                            }else if($data['website'][0]['w_type']==5){
                                 $data['submit_giveaway'] = $this->DBfile->get_data('submit_games_giveaway' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,'','','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,6));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                              }else if($data['website'][0]['w_type']==3){
                                   $data['submit_giveaway'] = $this->DBfile->get_data('submit_contests' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                    $data['prodList'] = $this->DBfile->get_data('Contests' ,array('g_categories'=>$cid),'','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,array('g_categories'=>$cid),'','',array(0,6));
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests`WHERE g_categories='.$cid.' ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                              }else if($data['website'][0]['w_type']==4){
                                   $data['submit_giveaway'] = $this->DBfile->get_data('submit_gift_cards' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                    $data['prodList'] = $this->DBfile->get_data('Gift_Cards' ,array('g_categories'=>$cid),'','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,array('g_categories'=>$cid),'','',array(0,6));
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards`WHERE g_categories='.$cid.' ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                              }else if($data['website'][0]['w_type']==6){
                                    $data['prodList'] = $this->DBfile->get_data('coupons' ,array('g_categories'=>$cid),'','',array(0,6));
                                     $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 6');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 2');
                              }else if($data['website'][0]['w_type']==2){
                                $data['prodList'] = $this->DBfile->get_data('training_videos' ,array('g_categories'=>$cid),'','',array(0,6));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 6');
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 2');
                              }
                            $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                            $data['pageName'] = 'Home';
                            $viewName = 'home';
                        } 
                    }else{
                        $categories = $this->DBfile->get_data('categories' ,array('cate_slug'=>$params[0]),'','',array(0,1));
                        if(empty($categories)){
                            if($data['website'][0]['w_type']==1){
                                $prodList = $this->DBfile->get_data('giveaway' ,array('slug'=>$params[0]),'','',array(0,6));
                               if(!empty($prodList)){
                                $data['prodList'] = $prodList;
                                $data['cat'] = $this->DBfile->get_data('categories' ,array('cate_id'=>$data['prodList'][0]['g_categories']),'','',array(0,1));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 8');
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `giveaway` WHERE `g_categories`='.$data['prodList'][0]['g_categories'].' ORDER BY RAND() LIMIT 6');
                               }else{
                                  redirect(base_url('page-not-found'));
                                   die();
                               }
                            }else{
                                $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,array('g_id'=>$params[0]),'','',array(0,6));
                             if(!empty($data['prodList'])){
                                $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,array('g_id'=>$params[1]),'','',array(0,6));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                                $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                                $data['relatedData'] = $this->DBfile->query('SELECT * FROM `giveaway_games`  ORDER BY RAND() LIMIT 6'); 
                             }else{
                               redirect(base_url('page-not-found'));
                                    die();
                             }
                            }
                            $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                            $viewName = 'single_page';
                        }else{
                            $cid= $categories[0]['cate_id'];
                            if($data['website'][0]['w_type']==1){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_giveaway' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                $data['prodList'] = $this->DBfile->get_data('giveaway' ,array('g_categories'=>$cid),'','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,array('g_categories'=>$cid),'','',array(0,6));
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway`WHERE g_categories='.$cid.' ORDER BY RAND() LIMIT 6');
                            }else if($data['website'][0]['w_type']==5){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_games_giveaway' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,'','','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,6));
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                            }else if($data['website'][0]['w_type']==3){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_contests' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                $data['prodList'] = $this->DBfile->get_data('Contests' ,array('g_categories'=>$cid),'','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,array('g_categories'=>$cid),'','',array(0,6));
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests`WHERE g_categories='.$cid.' ORDER BY RAND() LIMIT 6');
                            }else if($data['website'][0]['w_type']==4){
                                $data['submit_giveaway'] = $this->DBfile->get_data('submit_gift_cards' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'','',array(0,6));
                                $data['prodList'] = $this->DBfile->get_data('Gift_Cards' ,'','','',array(0,6));
                                $data['web_giveaway'] = $this->DBfile->get_data('Gift_Cards' ,'','','',array(0,3));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                            }else if($data['website'][0]['w_type']==6){
                                $data['submit_giveaway'] = '';
                                $data['prodList'] = $this->DBfile->get_data('coupons' ,'','','',array(0,6));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 6');
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 2');
                            }else if($data['website'][0]['w_type']==2){
                                $data['submit_giveaway'] = '';
                                $data['prodList'] = $this->DBfile->get_data('training_videos' ,'','','',array(0,6));
                                $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 6');
                                 $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 4');
                                $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 2');
                            }
                            $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                            $data['pageName'] = 'Home';
                            $viewName = 'home';
                        } 
                    }
                }
                else
                    redirect(base_url());
            }
            else{
                if( $webRes[0]['w_cateid'] == 0 && $webRes[0]['w_dfy'] == 0 ) {
                    $data['website'] = $this->DBfile->get_data('website' ,array('w_siteurl'=>$this->uri->segment(2)),'','',array(0,1));
                    // print_r($data['website']);
                    if($data['website'][0]['w_type']==1){
                        $data['submit_giveaway'] = $this->DBfile->get_data('submit_giveaway' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'',array('s_id','desc'),array(0,6));
                        $data['prodList'] = $this->DBfile->get_data('giveaway' ,'','','',array(0,6));
                        $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,6));
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                    }else if($data['website'][0]['w_type']==3){
                         $data['submit_giveaway'] = $this->DBfile->get_data('submit_contests' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'',array('s_id','desc'),array(0,6));
                        $data['prodList'] = $this->DBfile->get_data('Contests' ,'','','',array(0,6));
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Contests` ORDER BY RAND() LIMIT 2');
                    }else if($data['website'][0]['w_type']==5){
                         $data['submit_giveaway'] = $this->DBfile->get_data('submit_games_giveaway' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'',array('s_id','desc'),array(0,6));
                        $data['prodList'] = $this->DBfile->get_data('giveaway_games' ,'','','',array(0,6));
                        $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,6));
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway_games` ORDER BY RAND() LIMIT 2');
                    }else if($data['website'][0]['w_type']==4){
                        $data['submit_giveaway'] = $this->DBfile->get_data('submit_gift_cards' ,array('status'=>1,'w_id'=>$data['website'][0]['w_id']),'',array('s_id','desc'),array(0,6));
                        $data['prodList'] = $this->DBfile->get_data('Gift_Cards' ,'','','',array(0,6));
                        $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,6));
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `Gift_Cards` ORDER BY RAND() LIMIT 2');
                    }else if($data['website'][0]['w_type']==6){
                         $data['submit_giveaway'] ="";
                        $data['prodList'] = $this->DBfile->get_data('coupons' ,'','','',array(0,6));
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `coupons` ORDER BY RAND() LIMIT 2');
                    }else if($data['website'][0]['w_type']==2){
                         $data['submit_giveaway'] ="";
                        $data['prodList'] = $this->DBfile->get_data('training_videos' ,'','','',array(0,6));
                        $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 6');
                         $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 4');
                        $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `training_videos` ORDER BY RAND() LIMIT 2');
                    }
                    $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                    $data['pageName'] = 'Home';
                    $viewName = 'home';
                }
                elseif( $webRes[0]['w_dfy'] != 0 ){
                    $cateArr = explode(',',$webRes[0]['w_dfy']);
                    $data['website'] = $this->DBfile->get_data('website' ,array('w_siteurl'=>$this->uri->segment(2)),'','',array(0,1));
                    $data['prodList'] = $this->DBfile->get_data_limit('giveaway','','',array(0,12),'','','');
                    $data['web_giveaway'] = $this->DBfile->get_data('web_giveaway' ,'','','',array(0,6));
                    $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                    $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 6');
                    $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 2');
                    $totProd = $this->DBfile->get_data_limit('giveaway','','','','','','');
                    $data['numberOfPages'] = ceil(count($totProd)/12) ;
                    $data['pageName'] = 'Home';
                    $viewName = 'home';
                }
                else{
                    $data['website'] = $this->DBfile->get_data('website' ,array('w_siteurl'=>$this->uri->segment(2)),'','',array(0,1));
                    $webcateid = $data['website'][0]['w_cateid'];
                    $data['prodList'] = $this->DBfile->get_data('giveaway' , array('g_categories'=>$data['website'][0]['w_cateid']),'','',array(0,15));
                    $data['sidebarList'] = $this->DBfile->query('SELECT * FROM `giveaway`WHERE g_categories='.$webcateid.' ORDER BY RAND() LIMIT 6');
                      $data['footerGiveaways'] = $this->DBfile->query('SELECT * FROM `giveaway` ORDER BY RAND() LIMIT 4');
                    $data['sidebarListleft'] = $this->DBfile->query('SELECT * FROM `giveaway` WHERE g_categories='.$webcateid.' ORDER BY RAND() LIMIT 2');
                    $totProd = $this->DBfile->getCount('giveaway' ,array('g_categories'=>$data['website'][0]['w_cateid']));
                    $data['numberOfPages'] = ceil($totProd[0]['totCount']/15) ;
                    $data['ads'] = $this->DBfile->get_data('ads' ,array('web_id'=>$data['website'][0]['w_id']),'','',array(0,1));
                    $data['pageName'] = 'Home';
                    $data['cateDetails'] = $this->DBfile->get_data('categories',array('cate_id'=>$webRes[0]['w_cateid']));
                    $viewName = 'niche_home';
                }
                
            }
            $this->load->view('frontend/header',$data);

            if( $viewName != '' )
                $this->load->view('frontend/'.$viewName,$data);

            $this->load->view('frontend/footer',$data);
        }
    }	
}
?>