<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addon extends CI_Controller {
    public $web_id;
    
    function __Construct(){
        Parent::__Construct();
        $server = $_SERVER['HTTP_HOST'];
        $expServer = explode('.', $server);
        if($expServer[0] == 'www'){
            array_shift($expServer);
            $server = implode('.',$expServer);
        }
        $checkWebDomain = $this->DBfile->get_data('website',array('addon_domain'=>$server),'w_id,w_siteurl',array(),'');
        if(!empty($checkWebDomain)){
            $this->session->set_userdata('web_id', $checkWebDomain[0]['w_id']);
            $this->session->set_userdata('web_url', 'https://'.$server);
        }
        else{
            redirect(base_url());
        }
    }
    
    function pages($params='') {
        if( $params != '' ){
            $webRes = $this->DBfile->get_data('website',array('w_id'=>$this->session->userdata['web_id']));
            if(empty($webRes))
                redirect(base_url());

            if(isset($this->session->userdata['c_loggedin']) )
                $data['is_customer'] = 1;
            else
                $data['is_customer'] = 0;

            $data['websiteData'] = $webRes[0];
            $data['cateList'] = $this->DBfile->get_data('categories');
            $data['custom_cateList'] = $this->DBfile->get_data('web_categories',array('cate_webid'=>$webRes[0]['w_id']));

            $cateDetail = $this->DBfile->get_data('categories' , array('cate_slug'=>$params));
            $soloProdDetail = $this->DBfile->get_data('products' , array('p_urlname'=>$params));

            $custom_cateDetail = $this->DBfile->get_data('web_categories' , array('cate_slug'=>$params , 'cate_webid'=>$webRes[0]['w_id']));
            $custom_soloProdDetail = $this->DBfile->get_data('web_products' , array('p_urlname'=>$params,'p_webid'=>$webRes[0]['w_id']));

            $website = $this->session->userdata['web_url'];
			$data['checkURL'] = explode('plr/',$website);

            if( $params == 'profile' ){
                if(isset($_POST['c_name'])){
                    if( $_POST['c_pwd'] != '' ) {
                        $pwd = $_POST['c_pwd'];
                        unset($_POST['c_pwd']);
                        $_POST['c_password'] = md5($pwd);
                    }
                    else
                        unset($_POST['c_pwd']);
        
                    $this->DBfile->set_data( 'customers', $_POST , array('c_id'=>$this->session->userdata['c_id']) );
                    $this->session->userdata['c_name'] = explode(' ',$_POST['c_name'])[0];
                    $this->session->userdata['c_initials'] = substr($_POST['c_name'],0,2);
                    
                    echo '1';
                    die();
                }
                $data['userList'] = $this->DBfile->get_data('customers',array('c_id'=>$this->session->userdata['c_id']),'c_name,c_email,c_plan');
                $data['productDownload'] = $this->DBfile->get_join_data('product_downloads','products','product_downloads.pd_prodid=products.p_id',array('pd_custid'=>$this->session->userdata['c_id']),'p_id,p_name,p_urlname,pd_date');
                $data['pageName'] = 'Profile and Orders';
                $viewName = 'profile';
            }
            elseif( $params == 'newsletter' ){
                if( isset($_POST['news_name']) ){
                    sendToAutoResponder($_POST['news_email'],$_POST['news_name'],$webRes[0]['w_id'],'w_newsletterlist');
                    echo 1;
                }
                else
                    echo 0;
                die();
            }
            elseif( $params == 'pricing' ){
                if(isset($_POST['planID'])){
                    $paymentDetails = $this->DBfile->get_data('payment_settings' , array('payment_uid'=>$webRes[0]['w_userid'],'payment_type'=>'paypal'));

                    $planArr = json_decode($webRes[0]['w_plansettings'],true);
                    $planData = json_decode($planArr['plan_'.$_POST['planID']],true);
                    $planAmount = $planData['amount_'.$_POST['planID']];
                    $planName = $planData['planname_'.$_POST['planID']];
                    $item_number = substr(md5(date('his')),0,5);
                    $formData = '';

                    if( $planAmount != 0 ){
                        $PaypalId = $paymentDetails[0]['payment_data'];
                        if( $PaypalId != '' ){
                            $formData = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="pay_form">
                                <input type="hidden" name="business" value="'.trim($PaypalId).'">
                                <input type="hidden" name="item_name" value="'.$planName.'">
                                <input type="hidden" name="amount" value="'.$planAmount.'">
                                <input type="hidden" name="item_number" value="'.$item_number.'">
                                <input type="hidden" name="no_shipping" value="1">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="cmd" value="_xclick">
                                <input type="hidden" name="handling" value="0">
                                <input type="hidden" name="no_note" value="1">
                                <input type="hidden" name="cpp_logo_image" value="url">
                                <input type="hidden" name="cancel_return" value="'.$this->session->userdata['web_url'].'">
                                <input type="hidden" name="return" value="'.$this->session->userdata['web_url'].'/pages/profile">
                                <input type="hidden" name="notify_url" value="'.base_url('home/track_transactions/'.$_POST['planID'].'/'.$webRes[0]['w_id'].'/'.$this->session->userdata['c_id']).'">
                                <input type="submit" id="paymentButton" value="Pay Now">
                            </form>';
                        }
                        else
                            $formData = 'error';
                    }
                    else{
                		$dataArr = array(
                			'trans_planid'	=>	$_POST['planID'],
                			'trans_webid'	=>	$webRes[0]['w_id'],
                			'trans_customerid'	=>	$this->session->userdata['c_id'],
                			'trans_mcgross'	=>	0,
                			'trans_amount'	=>	$planAmount,
                			'trans_status'	=>	'Completed',
                			'trans_date'	=>	date('Y-m-d h:i:s')
                		);
                		$this->DBfile->put_data('customer_transactions',$dataArr);
                		
            			$soloCust = $this->DBfile->get_data('customers',array('c_id'=>$this->session->userdata['c_id']),'c_name,c_email');
            			sendToAutoResponder($soloCust[0]['c_email'],$soloCust[0]['c_name'],$webRes[0]['w_id'],'w_buyplanlist');
            			$this->DBfile->set_data( 'customers', array('c_plan'=>$planName,'c_planid'=>$_POST['planID']) , array('c_id'=>$this->session->userdata['c_id']) );
                		
                		$formData = 'free';
                    }
                    
                    echo $formData;
                    die();
                }
                $data['paymentSettings'] = $this->DBfile->get_data('payment_settings' , array('payment_uid'=>$webRes[0]['w_userid']));
                $data['userList'] = isset($this->session->userdata['c_id']) ? $this->DBfile->get_data('customers',array('c_id'=>$this->session->userdata['c_id']),'c_planid') : array();
                $data['pageName'] = 'Pricing';
                $viewName = 'pricing';
            }
            elseif(!empty($cateDetail)){
                $data['prodList'] = $this->DBfile->get_data('products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_categoryid'=>$cateDetail[0]['cate_id']),'','',array(0,12));
                $totProd = $this->DBfile->getCount('products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_categoryid'=>$cateDetail[0]['cate_id']));
                $data['numberOfPages'] = ceil($totProd[0]['totCount']/12) ;
                $data['cateName'] = $data['pageName'] = $cateDetail[0]['cate_name'];
                $data['cateId'] = $cateDetail[0]['cate_id'];
                $data['cateBanner'] = $cateDetail[0]['cate_slug'];
                $viewName = 'categories_prod';
            }
            elseif(!empty($custom_cateDetail)){
                $data['prodList'] = $this->DBfile->get_data('web_products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_categoryid'=>$custom_cateDetail[0]['cate_id'],'p_webid'=>$webRes[0]['w_id']),'','',array(0,12));
                $totProd = $this->DBfile->getCount('web_products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_categoryid'=>$custom_cateDetail[0]['cate_id'],'p_webid'=>$webRes[0]['w_id']));
                $data['numberOfPages'] = ceil($totProd[0]['totCount']/12) ;
                $data['cateName'] = $data['pageName'] = $custom_cateDetail[0]['cate_name'];
                $data['cateId'] = $custom_cateDetail[0]['cate_id'];
                $data['cateBanner'] = $custom_cateDetail[0]['cate_slug'];
                $viewName = 'categories_prod';
            }
            elseif(!empty($soloProdDetail)){
                $data['cateName'] = ($this->DBfile->get_data('categories' , array('cate_id'=>$soloProdDetail[0]['p_categoryid'])))[0]['cate_name'];
                $data['soloProdDetail'] = $soloProdDetail;
                $data['pageName'] = $soloProdDetail[0]['p_name'];
                $viewName = 'product';
            }
            elseif(!empty($custom_soloProdDetail)){
                $data['cateName'] = ($this->DBfile->get_data('web_categories' , array('cate_id'=>$custom_soloProdDetail[0]['p_categoryid'])))[0]['cate_name'];
                $data['soloProdDetail'] = $custom_soloProdDetail;
                $data['pageName'] = $custom_soloProdDetail[0]['p_name'];
                $viewName = 'product';
            }
            else
                redirect(base_url());
        }
        else
            redirect(base_url());
        
        $this->load->view('frontend/header',$data);

        if( $viewName != '' )
            $this->load->view('frontend/'.$viewName,$data);

        $this->load->view('frontend/footer',$data);
    }

    function p($params=''){
        $webRes = $this->DBfile->get_data('website',array('w_id'=>$this->session->userdata['web_id']));
        if(empty($webRes))
            redirect(base_url());

        if(isset($this->session->userdata['c_loggedin']) )
            $data['is_customer'] = 1;
        else
            $data['is_customer'] = 0;

        $data['websiteData'] = $webRes[0];
        $data['cateList'] = $this->DBfile->get_data('categories');

        $website = $this->session->userdata['web_url'];
		$data['checkURL'] = explode('plr/',$website);

        $compArray = $webRes[0]['w_compliancesettings'] != '' ? json_decode($webRes[0]['w_compliancesettings'],true) : array() ;

        $comText = $this->load->view('frontend/compliance/'.$params,$data,true);

        $final_content = str_replace("{BUSINESS-NAME}", !empty($compArray) ? $compArray['business_name'] : $webRes[0]['w_title'] ,$comText);
        $final_content = str_replace("{BUSINESS-EMAIL}", !empty($compArray) ? $compArray['business_email'] : '' ,$final_content);
        $final_content = str_replace("{BUSINESS-PHONE}", !empty($compArray) ? $compArray['business_phone'] : '' ,$final_content);
        $final_content = str_replace("{BUSINESS-URL}", !empty($compArray) ? $compArray['business_url'] : $this->session->userdata['web_url'] ,$final_content);
        $final_content = str_replace("{BUSINESS-WEBSITE}", !empty($compArray) ? $compArray['business_url'] : $this->session->userdata['web_url'] ,$final_content);
        $final_content = str_replace("{BUSINESS-ADDRESS}", !empty($compArray) ? $compArray['business_address'] : '' ,$final_content);

        $data['compliance_text'] = $final_content;
        $data['pageName'] = ucwords( str_replace('-',' ',$params) );
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/footer',$data);
    }

    function index(){
        $webRes = $this->DBfile->get_data('website',array('w_id'=>$this->session->userdata['web_id']));
        if(empty($webRes))
            redirect(base_url());

        if(isset($this->session->userdata['c_loggedin']) )
            $data['is_customer'] = 1;
        else
            $data['is_customer'] = 0;

        $data['websiteData'] = $webRes[0];
        $data['cateList'] = $this->DBfile->get_data('categories');

        $website = $this->session->userdata['web_url'];
		$data['checkURL'] = explode('plr/',$website);
        
        if( $webRes[0]['w_cateid'] == 0 && $webRes[0]['w_dfy'] == 0 ) {
            if( $webRes[0]['w_showonhome'] == 'all' || $webRes[0]['w_showonhome'] == 'pre' ){
                $data['prodList'] = $this->DBfile->get_data('products' , array('p_imagelink !='=>'','p_finalprod !='=>''),'','',array(0,12));
                $totProd = $this->DBfile->getCount('products' , array('p_imagelink !='=>'','p_finalprod !='=>''));
            }
            else{
                $data['prodList'] = $this->DBfile->get_data('web_products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_webid'=>$webRes[0]['w_id']),'','',array(0,12));
                $totProd = $this->DBfile->getCount('web_products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_webid'=>$webRes[0]['w_id']));
            }
            $data['numberOfPages'] = ceil($totProd[0]['totCount']/12) ;
            $data['pageName'] = 'Home';
            $viewName = 'home';
        }
        elseif( $webRes[0]['w_dfy'] != 0 ){
            $cateArr = explode(',',$webRes[0]['w_dfy']);
            $data['prodList'] = $this->DBfile->get_data_limit('products',array('p_imagelink !='=>'','p_finalprod !='=>''),'',array(0,12),'','',array('p_categoryid'=>$cateArr));
            $totProd = $this->DBfile->get_data_limit('products',array('p_imagelink !='=>'','p_finalprod !='=>''),'','','','',array('p_categoryid'=>$cateArr));
            $data['numberOfPages'] = ceil(count($totProd)/12) ;
            $data['pageName'] = 'Home';
            $viewName = 'home';
        }
        else{
            $data['prodList'] = $this->DBfile->get_data('products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_categoryid'=>$webRes[0]['w_cateid']),'','',array(0,12));
            $totProd = $this->DBfile->getCount('products' , array('p_imagelink !='=>'','p_finalprod !='=>'','p_categoryid'=>$webRes[0]['w_cateid']));
            $data['numberOfPages'] = ceil($totProd[0]['totCount']/12) ;
            $data['pageName'] = 'Home';
            $data['cateDetails'] = $this->DBfile->get_data('categories',array('cate_id'=>$webRes[0]['w_cateid']));
            $viewName = 'niche_home';
        }
        $this->load->view('frontend/header',$data);

        if( $viewName != '' )
            $this->load->view('frontend/'.$viewName,$data);

        $this->load->view('frontend/footer',$data);
    }
    
}
?>