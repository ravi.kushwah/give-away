<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
		
	public function index(){
		if(isset($this->session->userdata['loginstatus']) && $this->session->userdata['type'] == 1 )
			redirect(base_url('admin/dashboard'));
		$this->load->view('admin/login');
	}

	private function checkAdminLogin(){
		if(!isset($this->session->userdata['loginstatus']) || $this->session->userdata['type'] != 1 )
			redirect(base_url());
	}

	private function checkValidAJAX(){
		$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
		if( strpos($ref,base_url()) === false )
			die('Unauthorize Access!!');
		if( !isset($_POST) )
			die('Unauthorize Access!!');
			
		$postData = array();
		foreach( $_POST as $key=>$value ){
			$temp = $this->input->post($key,TRUE);
			//$temp = $this->db->escape($temp);
			$postData[$key] = $temp;
		}
		return json_encode($postData);
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}
	
	function dashboard(){
		$this->checkAdminLogin();
		$data['pagename'] = 'Dashboard';
		$data['nicheCount'] = count($this->DBfile->get_data('categories','','cate_id'));
		$data['prodCount'] = count($this->DBfile->get_data('giveaway','','g_id'));
		$data['websiteCount'] = count($this->DBfile->get_data('website','','w_id'));
		$data['userCount'] = count($this->DBfile->get_data('usertbl',array('u_type'=>2),'u_id'));
		$this->load->view('admin/header',$data);
		$this->load->view('admin/dashboard',$data);
		$this->load->view('admin/footer',$data);
	}

	function categories($list=''){
		$this->checkAdminLogin();
		if($list != ''){
			$list = $this->DBfile->get_data('nichetbl');
			$i=0;
			$data = array();
			if(!empty($list)){
				foreach($list as $sList){
					$i++;
					$t = array();
					array_push($t,$i);
					array_push($t,'<span id="niche_'.$sList['n_id'].'">'.$sList['n_title'].'</span>');
					array_push($t,'<span id="desc_'.$sList['n_id'].'">'.$sList['n_desc'].'</span>');
					array_push($t,'<span id="widget_'.$sList['n_id'].'">'.$sList['n_widget'].'</span>');
					array_push($t,'<a style="cursor:pointer;" onclick="editData('.$sList['n_id'].',\'niche\')">'.EDIT_ICON.'</a>');
					array_push($t,'<a style="cursor:pointer;" onclick="deleteData('.$sList['n_id'].',\'niche\')">'.DELETE_ICON.'</a>');
					array_push($data,$t);
				}
			}
			echo json_encode(array('data'=>$data));
			die();
		}
		$data['pagename'] = 'Categories';
		$this->load->view('admin/header',$data);
		$this->load->view('admin/categories',$data);
		$this->load->view('admin/footer',$data);
	}
  
	function productsone($list=''){
		$this->checkAdminLogin();
		if($list != ''){
			$list = $this->DBfile->get_data('giveaway');
			$i=0;
			$data = array();
			if(!empty($list)){
				foreach($list as $sList){
					$u = base_url().'ga/vivek/'.$sList['g_id'];
					$catData = $this->DBfile->get_data('categories' , array('cate_id'=>$sList['g_categories']));
					$cateName = (isset($catData[0]['cate_name']) && !empty($catData[0]['cate_name'])? $catData[0]['cate_name'] : '');
					$edit =  base_url().'admin/addnew/giveaway/'.$sList['g_id'];
					$i++;
					$t = array();
					array_push($t,$i);
					array_push($t,$cateName);
					array_push($t,$sList['g_offer_name']);
					array_push($t,'<a href="'.$u.'" target="_blank">View</a>');
					array_push($t,'<a href="'.$edit.'" target="_blank">Edit</a>');
					array_push($data,$t);
				}
			}
			echo json_encode(array('data'=>$data));
			die();
		}
		$data['pagename'] = 'Give Away';
		$this->load->view('admin/header',$data);
		$this->load->view('admin/products',$data);
		$this->load->view('admin/footer',$data);
	}
function productstwo($list=''){
		$this->checkAdminLogin();
		if($list != ''){
			$list = $this->DBfile->get_data('giveaway_games');
			$i=0;
			$data = array();
			if(!empty($list)){
				foreach($list as $sList){
				    $u = base_url().'ga/vivek/'.$sList['g_id'];
			    	$edit =  base_url().'admin/addnew/giveaway/'.$sList['g_id'];
					$i++;
					$t = array();
					array_push($t,$i);
					array_push($t,$sList['type']);
					array_push($t,$sList['title']);
					array_push($t,'<a href="'.$u.'" target="_blank">View</a>');
					array_push($t,'<a href="'.$edit.'" target="_blank">Edit</a>');
					array_push($data,$t);
				}
			}
			echo json_encode(array('data'=>$data));
			die();
		}
		$data['pagename'] = 'Give Away';
		$this->load->view('admin/header',$data);
		$this->load->view('admin/game_giveaway',$data);
		$this->load->view('admin/footer',$data);
	}

	function addnew($pagetype='categories',$dbid=0){
		$this->checkAdminLogin();
		$data['pagename'] = 'Add New '.ucfirst($pagetype);
		$data['pagetype'] = $pagetype;
		$data['categoriesList'] = $this->DBfile->get_data('categories');

		if( $pagetype == 'users' ){
			$data['userData'] = array();
			if( $dbid != 0 ){
				$u = $this->DBfile->get_data('usertbl',array('u_id'=>$dbid));
				if(!empty($u)){
					$data['userData'] = $u;
					$data['pagename'] = 'Edit User';
				}
			}
		}
		elseif( $pagetype == 'niche' ){
			$data['pagename'] = 'Add New Product '.ucfirst($pagetype);
		}
		elseif( $pagetype == 'products' ){
			$data['resultSet'] = array();
			if( $dbid != 0 ){
				$u = $this->DBfile->get_data('products',array('p_id'=>$dbid));
				if(!empty($u)){
					$data['resultSet'] = $u[0];
					$data['pagename'] = 'Edit Product';
				}
			}
		}
		elseif( $dbid != 0 ){
			$tblName = $pagetype.'tbl';
			$initials = substr($pagetype,0,1);
			$dbidCol = $initials.'_id';
			$res = $this->DBfile->get_data($tblName,array($dbidCol=>$dbid));
			if(empty($res))
				redirect(base_url('dashboard'));

			$data['resultSet'] = $res[0];
		}

		$viewPage = 'addnew_'.$pagetype;
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/'.$viewPage,$data);
		$this->load->view('admin/footer',$data);
	}

	function submitNiche(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		if(isset($postData['delete'])){
			$this->DBfile->delete_data( 'nichetbl', array('n_id'=>$postData['deleteid']));
		}
		elseif(isset($postData['id'])){
			$nicheSlug = slugify($postData['title']);
			$this->DBfile->set_data( 'nichetbl', array('n_title'=>$postData['title'],'n_desc'=>$postData['desc'],'n_widget'=>$postData['widget'],'n_slug'=>$nicheSlug) , array('n_id'=>$postData['id']));
		}
		else{
			$nicheArr = $postData['title'];
			$descArr = $postData['desc'];
			$widgetArr = $postData['widget'];
			for( $i = 0 ; $i < count($nicheArr) ; $i++ ){
				$nicheSlug = slugify($nicheArr[$i]);
				$this->DBfile->put_data( 'nichetbl', array('n_title'=>$nicheArr[$i],'n_desc'=>$descArr[$i],'n_widget'=>$widgetArr[$i],'n_slug'=>$nicheSlug));
			}
		}
		echo '1';
		die();
	}

	
	function users($list=''){
		$this->checkAdminLogin();
		if($list == 'postreq'){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$this->DBfile->put_data( 'usertbl', $postData);
			echo '1';
			die();
		}
		if($list != ''){
			$list = $this->DBfile->get_data('usertbl',array('u_type'=>2));
			$i=0;
			$data = array();
			if(!empty($list)){
				foreach($list as $sList){
					$i++;
					$t = array();
					array_push($t,$i);
					array_push($t,$sList['u_name']);
					array_push($t,$sList['u_email']);
					array_push($t,$sList['u_purchaseddate']);
					array_push($t,$sList['is_fe'] == 1 ? 'FE' : '-' );
					array_push($t,$sList['is_oto1'] == 1 ? 'OTO1' : '-' );
					array_push($t,$sList['is_oto2'] == 1 ? 'OTO2' : '-' );
					array_push($t,$sList['is_oto3'] == 1 ? 'OTO3' : '-' );
					array_push($t,$sList['is_oto4'] == 1 ? 'OTO4' : '-' );
					array_push($t,$sList['is_oto5'] == 1 ? 'OTO5' : '-' );
					array_push($t,$sList['is_oto6'] == 1 ? 'OTO6' : '-' );
					array_push($t,$sList['is_oto7'] == 1 ? 'OTO7' : '-' );
					array_push($t,$sList['is_oto8'] == 1 ? 'OTO8' : '-' );
					array_push($t,'<a href="'.base_url().'admin/addnew/users/'.$sList['u_id'].'">'.EDIT_ICON.'</a>');
					array_push($t,'<a style="cursor:pointer;" onclick="deleteData('.$sList['u_id'].',\'users\')">'.DELETE_ICON.'</a>');
					array_push($data,$t);
				}
			}
			echo json_encode(array('data'=>$data));
			die();
		}
		$data['pagename'] = 'Users';
		$this->load->view('admin/header',$data);
		$this->load->view('admin/users',$data);
		$this->load->view('admin/footer',$data);
	}


	function submitCommonData(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$type = $postData['pageType'];
		if(isset($postData['delete'])){
			$initials = substr($type,0,1);
			$dbidCol = $initials.'_id';
			if($type=='bonus'){
				$res = $this->DBfile->get_data($type.'tbl',array($dbidCol=>$postData['deleteid']));
				if(!empty($res)){
					$upPath = (explode('/application/',__DIR__)[0]).'/assets/upload/';
					unlink($upPath.$res[0]['b_image']);
				}
			}
			$this->DBfile->delete_data( $type.'tbl', array($dbidCol=>$postData['deleteid']) );
		}
		elseif($postData['uniqeid'] != 0){
			if($type == 'product'){

				$this->load->library('Dropbox');
				$upPath = (explode('/application/',__DIR__)[0]).'/assets/products/';
				
				if(!empty($_FILES['p_imagelink']['name'])){
					$config['upload_path'] = $upPath;
					$config['allowed_types'] = '*';
					$config['file_name'] = generateString().getImageExtension($_FILES['p_imagelink']['name']);
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('p_imagelink')){
						$uploadData = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadData['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_imagelink'] = substr($finalResp['url'],0,-4).'raw=1';
						$delPath['p_imagelink'] = $uploadData['full_path'];
					}
					else{
						echo "Failed to upload Image. The reason is : " . $this->upload->display_errors();
						die();
					}
				}

				if(!empty($_FILES['p_finalprod']['name'])){
					$confi1['upload_path'] = $upPath;
					$confi1['allowed_types'] = '*';
					$confi1['file_name'] = generateString().getImageExtension($_FILES['p_finalprod']['name']);
					
					$this->load->library('upload',$confi1);
					$this->upload->initialize($confi1);
					if($this->upload->do_upload('p_finalprod')){
						$uploadDat1 = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadDat1['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_finalprod'] = substr($finalResp['url'],0,-1).'1';
						$delPath['p_finalprod'] = $uploadDat1['full_path'];
					}
					else{
						echo "Failed to upload Zip. The reason is : " . $this->upload->display_errors();
						die();
					}
				}
				
				$uniqeid = $postData['uniqeid'];
				unset($postData['uniqeid']);
				unset($postData['pageType']);

				if(!isset($postData['p_imagelink']) && $postData['p_imagelink_url'] != ''){
					$u = $postData['p_imagelink_url'];
					$postData['p_imagelink'] = substr($u,0,-4).'raw=1';
					unset($postData['p_imagelink_url']);
				}
				else
					unset($postData['p_imagelink_url']);

				if(!isset($postData['p_finalprod']) && $postData['p_finalprod_url'] != ''){
					$u = $postData['p_finalprod_url'];
					$postData['p_finalprod'] = substr($u,0,-1).'1';
					unset($postData['p_finalprod_url']);
				}
				else
					unset($postData['p_finalprod_url']);
					
				$this->DBfile->set_data( 'products',$postData, array('p_id'=>$uniqeid) );

				if(isset($delPath['p_imagelink']))
					unlink($delPath['p_imagelink']);

				if(isset($delPath['p_finalprod']))
					unlink($delPath['p_finalprod']);
			}
			else{
				$initials = substr($type,0,1);
				$dbidCol = $initials.'_id';
				$this->DBfile->set_data( $type.'tbl',$postData['tempArr'], array($dbidCol=>$postData['uniqeid']) );
			}
		}
		elseif($postData['uniqeid'] == 0){
			if($type == 'product'){
				$this->load->library('Dropbox');
				$upPath = (explode('/application/',__DIR__)[0]).'/assets/products/';
				
				if(!empty($_FILES['p_imagelink']['name'])){
					$config['upload_path'] = $upPath;
					$config['allowed_types'] = '*';
					$config['file_name'] = generateString().getImageExtension($_FILES['p_imagelink']['name']);
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('p_imagelink')){
						$uploadData = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadData['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_imagelink'] = substr($finalResp['url'],0,-4).'raw=1';
						$delPath['p_imagelink'] = $uploadData['full_path'];
					}
					else{
						echo "Failed to upload Image. The reason is : " . $this->upload->display_errors();
						die();
					}
				}

				if(!empty($_FILES['p_finalprod']['name'])){
					$confi1['upload_path'] = $upPath;
					$confi1['allowed_types'] = '*';
					$confi1['file_name'] = generateString().getImageExtension($_FILES['p_finalprod']['name']);
					$this->load->library('upload',$confi1);
					$this->upload->initialize($confi1);
					if($this->upload->do_upload('p_finalprod')){
						$uploadDat1 = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadDat1['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_finalprod'] = substr($finalResp['url'],0,-1).'1';
						$delPath['p_finalprod'] = $uploadDat1['full_path'];
					}
					else{
						echo "Failed to upload Zip. The reason is : " . $this->upload->display_errors();
						die();
					}
				}
				unset($postData['uniqeid']);
				unset($postData['pageType']);

				if(!isset($postData['p_imagelink']) && $postData['p_imagelink_url'] != ''){
					$u = $postData['p_imagelink_url'];
					$postData['p_imagelink'] = substr($u,0,-4).'raw=1';
					unset($postData['p_imagelink_url']);
				}
				else
					unset($postData['p_imagelink_url']);

				if(!isset($postData['p_finalprod']) && $postData['p_finalprod_url'] != ''){
					$u = $postData['p_finalprod_url'];
					$postData['p_finalprod'] = substr($u,0,-1).'1';
					unset($postData['p_finalprod_url']);
				}
				else
					unset($postData['p_finalprod_url']);

				$this->DBfile->put_data( 'products', $postData);

				if(isset($delPath['p_imagelink']))
					unlink($delPath['p_imagelink']);

				if(isset($delPath['p_finalprod']))
					unlink($delPath['p_finalprod']);
			}
			else
				$this->DBfile->put_data( $type.'tbl', $postData['tempArr']);
		}
		
		echo '1';
		die();
	}

	function profile(){
		if(isset($_POST['u_name'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			if( $postData['pwd'] != '' ) {
				$pwd = $postData['pwd'];
				unset($postData['pwd']);
				$postData['u_password'] = md5($pwd);
			}
			else
				unset($postData['pwd']);

			$this->DBfile->set_data( 'usertbl', $postData , array('u_id'=>$this->session->userdata['id']) );

			$this->session->userdata['firstname'] = explode($postData['u_name'],' ')[0];
			echo '1';
			die();
		}
		$this->checkAdminLogin();
		$data['pagename'] = 'Profile';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('u_id'=>$this->session->userdata['id']));
		$this->load->view('admin/header',$data);
		$this->load->view('admin/profile',$data);
		$this->load->view('admin/footer',$data);
	}


	function submitUserRecords(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		if(isset($postData['delete'])){
			$this->DBfile->delete_data( 'usertbl', array('u_id'=>$postData['deleteid']));
			$this->DBfile->delete_data( 'shared', array('share_uid'=>$postData['deleteid']));
			echo '1';
			die();
		}
	
		if(isset($postData['u_id'])){
			
			$uid = $postData['u_id'];
			unset($postData['u_id']);
			if(isset($postData['u_password']))
				$postData['u_password'] = md5($postData['u_password']);
			
			if( $uid == 0 ){
				$postData['u_type'] = 2;
				$postData['u_status'] = 1;
				$postData['u_purchaseddate'] = date('d-m-Y h:i:s');
				$this->DBfile->put_data( 'usertbl', $postData );
			}
			else{
				$this->DBfile->set_data( 'usertbl', $postData , array('u_id'=>$uid) );
				$this->DBfile->set_data( 'website', array('w_status'=>$postData['u_status']==1?1:0) , array('w_userid'=>$uid) );
			}
			echo 1;
			die();
		}
	}
}
?>