<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
		
	public function index(){
		$this->checkLoginUser();
		$data['pageName'] = 'Login';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/login');
		$this->load->view('auth/footer');
	}	

	public function forgotpassword(){
		$data['pageName'] = 'Forgot Password';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/forgotpassword');
		$this->load->view('auth/footer');
	}	

    public function signup(){
		$data['pageName'] = 'Sign Up';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/signup');
		$this->load->view('auth/footer');
	}
	public function Page_not_found(){
	    $data['websiteData'] = $this->DBfile->get_data('website' ,array('w_id'=>$_SESSION['web_id']),'','',array(0,1));
		$data['pageName'] = '404 Page Not found';
		$this->load->view('frontend/404',$data);

	}
	
	function checkLoginUser(){
		if(isset($this->session->userdata['loginstatus']) && $this->session->userdata['type'] == 1 )
			redirect(base_url('admin/dashboard'));
		if(isset($this->session->userdata['loginstatus']) && $this->session->userdata['type'] == 2 )
			redirect(base_url('home/dashboard'));
	}

	private function checkUserLoginStatus(){
		if(!isset($this->session->userdata['loginstatus']) || $this->session->userdata['type'] != 2 )
			redirect(base_url());
	}

	private function checkValidAJAX(){
		$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
		if( strpos($ref,base_url()) === false )
			die('Unauthorize Access!!');
		if( !isset($_POST) )
			die('Unauthorize Access!!');
		
		$postData = array();
		foreach( $_POST as $key=>$soloProd ){
			if( $key != 'w_seoextscript' ){
				$temp = $this->input->post($key,TRUE);
				//$temp = $this->db->escape($temp);
				$postData[$key] = $temp;
			}
			else
				$postData[$key] = $soloProd;
		}
		return json_encode($postData);
	}

	function logout(){
		setcookie('check_seo_login', 1 , time() - (86400 * 30 * 24), "/");
		$this->session->sess_destroy();
		redirect(base_url());
	}

	function frontend_logout($w_siteurl){
		$website = $this->session->userdata['web_url'];
		$checkURL = explode('plr/',$website);

		$siteurl = count($checkURL) == 2 ? base_url('plr/'.$w_siteurl) : $this->session->userdata['web_url'];
		$this->session->sess_destroy();
		redirect($siteurl);
	}

	function profile(){
		if(isset($_POST['u_name'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			if( $postData['pwd'] != '' ) {
				$pwd = $postData['pwd'];
				unset($postData['pwd']);
				$postData['u_password'] = md5($pwd);
			}
			else
				unset($postData['pwd']);

			$this->DBfile->set_data( 'usertbl', $postData , array('u_id'=>$this->session->userdata['id']) );
			$this->session->userdata['firstname'] = explode(' ',$postData['u_name'])[0];
			$this->session->userdata['initials'] = substr($postData['u_name'],0,2);
			
			echo '1';
			die();
		}
		$this->checkUserLoginStatus();
		
		$data['pageName'] = 'Profile';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('u_id'=>$this->session->userdata['id']));
		$this->load->view('backend/header',$data);
		$this->load->view('backend/profile',$data);
		$this->load->view('backend/footer',$data);
	}

	function dashboard(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Dashboard';

		// DFY Sites
		$userDetail =  $this->DBfile->get_data('usertbl',array('u_id'=>$this->session->userdata['id']));
		if( $userDetail[0]['is_oto5'] == 1 && $userDetail[0]['dfy_done'] == 0 ){

			$dfysites = $this->DBfile->get_data('dfy');
			foreach($dfysites as $solo){
					$postData['w_title'] = $postData['w_logotext'] = $solo['dfy_name'];
					$postData['w_siteurl'] = strtolower(str_replace(' ','-',$solo['dfy_name'])).'-'.$this->session->userdata['id'];
					$postData['w_userid'] = $this->session->userdata['id'];
					$postData['w_createddate'] = date('d-m-Y H:i:s');
					$postData['w_dfy'] =  $solo['dfy_cate'];
					$postData['w_logofont'] =  $solo['dfy_logo_font'];
					$postData['w_logocolor'] =  $solo['dfy_logo_color'];
					$postData['w_themecolor'] =  $solo['dfy_theme'];
					$postData['w_site_api_key'] = md5(uniqid(rand(), true));
					$postData['w_type'] = 1;
					$this->DBfile->put_data( 'website', $postData);
			}
			$this->DBfile->set_data( 'usertbl', array('dfy_done'=>1), array('u_id'=>$this->session->userdata['id']));
		}
		// DFY Sites
		$data['categoryCount'] = $this->DBfile->getCount('categories');
		$data['giveaway'] = $this->DBfile->getCount('giveaway');
		$data['giveaway_games'] = $this->DBfile->getCount('giveaway_games');
		$data['leadsCount'] = $this->DBfile->getCount('leads',array('w_userid'=>$this->session->userdata['id']));
		$data['training_videos'] = $this->DBfile->getCount('training_videos');
		$data['coupons'] = $this->DBfile->getCount('coupons');
		$data['Contests'] = $this->DBfile->getCount('Contests');
		$data['Gift_Cards'] = $this->DBfile->getCount('Gift_Cards');
		$websiteList = $this->DBfile->get_data_limit('website',array('w_userid'=>$this->session->userdata['id']),'w_id,w_userid,w_createddate,w_title,w_siteurl,addon_domain,w_site_api_key,w_type');
		$data['websiteList'] = $websiteList;
		
		if( count($websiteList) == 0 ){
			$this->load->view('backend/empty',$data);
		}	
		else{
			$this->load->view('backend/header',$data);
			$this->load->view('backend/dashboard',$data);
			$this->load->view('backend/footer',$data);
		}
	}
	
	function empty(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Dashboard';

		
			$this->load->view('backend/empty',$data);
		
	}
	
	function add_new_website($w_id=0){
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id));
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
		$data['cateDetail'] = $this->DBfile->get_data('categories',array('cate_banner'=>1));
		$data['pageName'] = $w_id == 0 ? 'Add New Website' : 'Edit Website';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/add_new_website',$data);
		$this->load->view('backend/footer',$data);
	}
	function add_affiliate_link($w_id=0){
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id));
		$data['affiliate'] = $this->DBfile->get_data('affiliate_link',array('user_id'=>$this->session->userdata['id']));
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
		$data['cateDetail'] = $this->DBfile->get_data('categories',array('cate_banner'=>1));
		$data['pageName'] = $w_id == 0 ? 'Add Affiliate Link ' : 'Edit Affiliate Link ';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/affiliate',$data);
		$this->load->view('backend/footer',$data);
	}
	function checksiteurl($siteUrl=''){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$dArray = array('w_siteurl'=>$postData['w_siteurl']);
		$siteCheck = $this->DBfile->get_data('website',$dArray);
		if( isset($_POST) && $siteUrl == '' ) {
			if(empty($siteCheck))
				echo 1;
			else
				echo $siteCheck[0]['w_id'] == $postData['w_id'] ? 1 : 0 ;
			die();
		}
		else{
			if(empty($siteCheck))
				return 1;
			else
				return $siteCheck[0]['w_id'] == $postData['w_id'] ? 1 : 0 ;
		}
	}

	function submitSetupWebsite(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		if( $this->checksiteurl($postData['w_siteurl']) ){
			$upPath = (explode('/application/',__DIR__)[0]).'/assets/webupload/';
			$config['upload_path'] = $upPath;
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size'] = '1000'; // in KB which is 1 MB
			
			if(!empty($_FILES['w_logourl']['name'])){			
				$config['file_name'] = $_FILES['w_logourl']['name'];
				$this->load->library('upload',$config);
				
				if($this->upload->do_upload('w_logourl')){
					$uploadData = $this->upload->data();
					$filename = $uploadData['file_name'];
					$ext = getImageExtension($_FILES['w_logourl']['name']);
					$clientname = generateString().''.$ext;
					rename( $upPath.$filename , $upPath.$clientname );
					$postData['w_logourl'] = $clientname;
				}
				else{
					echo "Failed to upload <b>" . $config['file_name'] . "</b>. The reason is : " . $this->upload->display_errors();
					die();
				}
			}
	
			if(!empty($_FILES['w_faviconurl']['name'])){
				$config['allowed_types'] = 'jpg|jpeg|png';		
				$config['file_name'] = $_FILES['w_faviconurl']['name'];
				$this->load->library('upload',$config);
				if($this->upload->do_upload('w_faviconurl')){
					$uploadData = $this->upload->data();
					$filename = $uploadData['file_name'];
					$ext = getImageExtension($_FILES['w_faviconurl']['name']);
					$clientname = generateString().''.$ext;
					rename( $upPath.$filename , $upPath.$clientname );
					$postData['w_faviconurl'] = $clientname;
				}
				else{
					echo "Failed to upload <b>" . $config['file_name'] . "</b>. The reason is : " . $this->upload->display_errors();
					die();
				}
			}
	
			$w_id = $postData['w_id'];
			unset($postData['w_id']);
		
			if( $w_id == 0 ){
				$postData['w_title'] = $postData['w_title'] == '' ? $postData['w_siteurl'] : $postData['w_title'] ;
				$postData['w_userid'] = $this->session->userdata['id'];
				$postData['w_createddate'] = date('d-m-Y H:i:s');
				$postData['w_site_api_key'] = $activation = md5(uniqid(rand(), true));
				if(!empty($postData['w_type'])){
				    $this->DBfile->put_data( 'website', $postData);
				    	echo '1';
				}else{
				   	echo 'Tell us, on which GiveAway you want to create your site can\'t be empty.'; 
				}
				
			}
			else{
		    	if(!empty($postData['w_type'])){
				  $this->DBfile->set_data( 'website', $postData, array('w_id'=>$w_id));
				  	echo '1';
				}else{
				   	echo 'Tell us, on which GiveAway you want to create your site can\'t be empty.'; 
				}
            }
			die();
		}
		else{
			echo 'Please, enter different site url. Someone is already using this url.';
			die();
		}	
	}
   
    function submitAdsData(){
        $jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$a_id = $postData['a_id'];
		if( $a_id == 0 ){
			$ins= $this->DBfile->put_data( 'ads', $postData);
		}
		else
			$ins= $this->DBfile->set_data( 'ads', $postData, array('a_id'=>$a_id));
      
        echo '1';
	    die();
    }
   
	function deleteWebsite(){
		if(isset($_POST['webid'])){
			$webRes = $this->DBfile->get_data('website',array('w_id'=>$_POST['webid']));
			$upPath = (explode('/application/',__DIR__)[0]).'/assets/webupload/';
			if( $webRes[0]['w_logourl'] != '' )
				unlink($upPath.$webRes[0]['w_logourl']);

			if( $webRes[0]['w_faviconurl'] != '' )
				unlink($upPath.$webRes[0]['w_faviconurl']);

			$this->DBfile->delete_data( 'website', array('w_id'=>$_POST['webid']));
			$this->DBfile->delete_data( 'customers', array('c_w_id'=>$_POST['webid']));
			$this->DBfile->delete_data( 'product_downloads', array('pd_webid'=>$_POST['webid']));
			$this->DBfile->delete_data( 'customer_transactions', array('trans_webid'=>$_POST['webid']));
			echo 1;
		}
		else
			echo 0;
		die();
	}
    // Ravi has changed the code
    function deleteSiteLogo(){
		if(isset($_POST['webid'])){
			$webRes = $this->DBfile->get_data('website',array('w_id'=>$_POST['webid']));
			$upPath = (explode('/application/',__DIR__)[0]).'/assets/webupload/';
			if($_POST['type']=='logo'){
		    	if( $webRes[0]['w_logourl'] != '' )
    				unlink($upPath.$webRes[0]['w_logourl']);
    			$this->DBfile->set_data( 'website',array('w_logourl'=>''), array('w_id'=>$_POST['webid']));
		        $resp = array('status'=>1,'type'=>'logo');
			}else if($_POST['type']=='favicon'){
		    	if( $webRes[0]['w_faviconurl'] != '' )
				unlink($upPath.$webRes[0]['w_faviconurl']);
			    $this->DBfile->set_data( 'website',array('w_faviconurl'=>''), array('w_id'=>$_POST['webid']));
		        $resp = array('status'=>1,'type'=>'favicon');
			}
		    echo json_encode($resp,JSON_UNESCAPED_SLASHES);
		}
		else
		 echo json_encode(array('status'=>0),JSON_UNESCAPED_SLASHES);
		die();
	}
	function GiveawaysActive(){
    	if(isset($_POST['s_id'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$w_id = $postData['webid'];
			unset($postData['w_id']);
			$webDetail = $this->DBfile->get_data('website',array('w_id'=>$w_id));
	
			if($webDetail[0]['w_type']==1){
          	  $table = 'submit_giveaway';
		    }else if($webDetail[0]['w_type']==5){
	           $table = 'submit_games_giveaway';
		    }else  if($webDetail[0]['w_type']==4){
		        $table = 'submit_gift_cards';
		    }else  if($webDetail[0]['w_type']==3){ 
		        $table = 'submit_contests';
            }else  if($webDetail[0]['w_type']==6){ 
		        $table = 'coupons_giveaway';
            }else  if($webDetail[0]['w_type']==2){ 
		        $table = 'videos_giveaway';
		    }
		     $this->DBfile->set_data($table, array('status'=>$_POST['sts']) , array('s_id'=>$_POST['s_id']) );
	     	echo '1';
			die();
		}
	}
	function web_categories($w_id=0){
		if(isset($_POST['cateid'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$w_id = $postData['webid'];
			unset($postData['w_id']);
			$webDetail = $this->DBfile->get_data('website',array('w_id'=>$w_id),'w_blockcategory');
			
			if( $webDetail[0]['w_blockcategory'] == '' )
				$this->DBfile->set_data( 'website', array('w_blockcategory'=>$postData['cateid']) , array('w_id'=>$w_id) );
			else{
				if( $postData['sts'] == 1 )
				{
					$bCateArr = explode(',',$webDetail[0]['w_blockcategory']);
					$newArr = array_flip($bCateArr);
					unset($newArr[$postData['cateid']]);
					$bCateArr = array_flip($newArr);
					$bCate = implode(',',$bCateArr);
				}
				else{
					$bCate = $webDetail[0]['w_blockcategory'].','.$postData['cateid'];
				}
				$this->DBfile->set_data( 'website', array('w_blockcategory'=>$bCate) , array('w_id'=>$w_id) );
			}	

			echo '1';
			die();
		}

		if( isset($_POST['cate_name']) ){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$cate_id = $postData['cate_id'];
			unset($postData['cate_id']);

			if( $cate_id == 0 )
			{
				$postData['cate_webid'] = $w_id;
				$this->DBfile->put_data('web_categories',$postData);
				echo 2;
			}
			else{
				$this->DBfile->set_data('web_categories',$postData,array('cate_id'=>$cate_id));
				echo 1;	
			}
			die();
		}

		if( isset($_POST['cate_id']) ){
			$custDetails = $this->DBfile->get_data('web_categories',array('cate_id'=>$_POST['cate_id']));
			$temp = array(
				'cate_name'	=>	$custDetails[0]['cate_name'],
				'cate_slug'	=>	$custDetails[0]['cate_slug']
			);
			echo json_encode($temp);
			die();
		}

		if(isset($_POST['delete_cateid'])){
			$this->DBfile->delete_data( 'web_categories', array('cate_id'=>$_POST['delete_cateid'],'cate_webid'=>$w_id));
			$this->DBfile->delete_data( 'web_products', array('p_categoryid'=>$_POST['delete_cateid'],'p_webid'=>$w_id));
			echo 1;
			die();
		}

		if(isset($_POST['customcateid'])){
			$this->DBfile->set_data('web_categories',array('cate_status'=>$_POST['sts']),array('cate_id'=>$_POST['customcateid']));
			echo 1;
			die();
		}

		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'w_id,w_siteurl,w_blockcategory,addon_domain');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));

		$data['categories'] = $this->DBfile->get_data('categories');
		$data['web_categories'] = $this->DBfile->get_data('web_categories',array('cate_webid'=>$data['webDetail'][0]['w_id']));
		$data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ;
		$data['pageName'] = 'Categories Settings';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/web_categories',$data);
		$this->load->view('backend/footer',$data);
	}


	function web_paymentplan($w_id=0){
		if(isset($_POST['paydata'])){
			$this->DBfile->set_data( 'website', array('w_plansettings'=>json_encode($_POST['paydata'])) , array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id) );
			echo '1';
			die();
		}
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'w_plansettings,w_id,w_title,w_siteurl,addon_domain');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));

		$data['pageName'] = 'Plan Settings';
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
		$this->load->view('backend/header',$data);
		$this->load->view('backend/web_paymentplan',$data);
		$this->load->view('backend/footer',$data);
	}
   
	function autoresponder_integration(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Autoresponder Integration';
        $this->load->model('my_model');
		$user_id = $this->session->userdata['id'];
		$res = $this->my_model->select_data(['field'=>'value','table'=>'autoresponder', 'where'=>array( 'm_id' => $user_id, 'mkey' => 'autoresponder' )]);
		
		$aur_nm = array();
		$na = array();
		$customHTMlVal = array();
		if(!empty($res)){
			$a = (array) json_decode( $res[0]['value'] );
		    foreach($a as $k=>$v){
				if($k == 'CustomHTML'){
					array_push($customHTMlVal,$v);
		        } 
				$v = (array) $v;
				if(!empty($v)){
					$na[$k] = $v;
					$aur_nm[] = $k;
				}
			}
		}
		$data['autoresponder'] = $res;
		$data['custom_html'] = (!empty($customHTMlVal) ? $customHTMlVal[0] : $customHTMlVal);
		$data['connect_autoresponder'] = $aur_nm;
		$this->load->view('backend/header',$data);
		$this->load->view('backend/autoresponder_integration',$data);
		$this->load->view('backend/footer',$data);
	}

	function lead_settings($w_id=0){
		if(isset($_POST['w_signuplist'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$this->DBfile->set_data( 'website', $postData , array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id) );
			echo '1';
			die();
		}
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'w_signuplist,customHTMLgiveaways,customHTMLnews,w_buyplanlist,w_newsletterlist,w_id,w_title,w_siteurl');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));

		$autoresponderData = $this->DBfile->get_data('autoresponder',array('m_id'=>$this->session->userdata['id']));
		$na = array();
		if(!empty($autoresponderData)){
			$a = (array) json_decode( $autoresponderData[0]['value'] );
		    foreach($a as $k=>$v){
				if($k == 'CustomHTML'){
					array_push($customHTMlVal,$v);
		        } 
				$v = (array) $v;
				if(!empty($v)){
					$na[$k] = $v;
				}
			}
		}
		$data['autoresponderData'] = $autoresponderData;
		$data['getAr'] = $na;
		$data['pageName'] = 'Lead Settings';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/lead_settings',$data);
		$this->load->view('backend/footer',$data);
	}
    function LeadAdd(){
        $jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$data= $this->DBfile->get_data('website' ,array('w_siteurl'=>$postData['w_userid']),'','',array(0,1));
		if($postData['l_name']!='' && $postData['l_email']!='' && $postData['l_webid']!=''){
		     $postData['w_userid'] = $data[0]['w_userid'];
		     $this->DBfile->put_data( 'leads', $postData);
		     $resp = array('status'=>1,'smg'=>'Send Your Request Successfully');
		     if( isset($_POST['l_email']) ){
                sendToAutoResponder($_POST['l_email'],$_POST['l_name'],$data[0]['w_id'],'w_newsletterlist');
            }
		}else{
		    $resp = array('status'=>0,'smg'=>'Something went wrong');
		}
		echo json_encode($resp,JSON_UNESCAPED_SLASHES);
    }
    function submitGiveaways(){
            $jsonPost = $this->checkValidAJAX();
        	$data= $this->DBfile->get_data('website' ,array('w_siteurl'=>$_POST['siteName']),'','',array(0,1));
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            if($data[0]['w_type']==1){
                $table = 'submit_giveaway';
            }else if($data[0]['w_type']==5){
                $table = "submit_games_giveaway";
            }else if($data[0]['w_type']==4){
                $table = "submit_gift_cards";
            }else if($data[0]['w_type']==3){
                $table = "submit_contests";
            }else if($data[0]['w_type']==2){
                $table = 'videos_giveaway'; 
            }else if($data[0]['w_type']==6){
                $table = 'coupons_giveaway'; 
            }
             	$upPath = (explode('/application/',__DIR__)[0]).'/assets/giveaways/';
    			$config['upload_path'] = $upPath;
    			$config['allowed_types'] = 'jpg|jpeg|png';
    			$config['max_size'] = '1000'; // in KB which is 1 MB
    			if(!empty($_FILES['give_away_image']['name'])){			
    				$config['file_name'] = $_FILES['give_away_image']['name'];
    				$this->load->library('upload',$config);
    				
    				if($this->upload->do_upload('give_away_image')){
    					$uploadData = $this->upload->data();
    					$filename = $uploadData['file_name'];
    					$ext = getImageExtension($_FILES['give_away_image']['name']);
    					$clientname = generateString().''.$ext;
    					rename( $upPath.$filename , $upPath.$clientname );
    					  $data_arr['give_away_image'] = $clientname;
    				}
    				else{
    					echo "Failed to upload <b>" . $config['file_name'] . "</b>. The reason is : " . $this->upload->display_errors();
    					die();
    				}
    			}
    			$data_arr['give_away_url']= $_POST['give_away_url'];
    			$data_arr['give_away__main_prize']= $_POST['give_away__main_prize'];
    			$data_arr['prize_description']= $_POST['prize_description'];
    			$data_arr['end_date']= $_POST['end_date'];
    			$data_arr['country']= $_POST['country'];
    			$data_arr['give_away_categories']= $_POST['give_away_categories'];
    			$data_arr['name']= $_POST['name'];
    			$data_arr['email']= $_POST['email'];
    			$data_arr['w_id']= $data[0]['w_id'];
    			$data_arr['g_offer_name']= $_POST['g_offer_name'];
        	
        	   $res =  $this->DBfile->put_data($table,$data_arr);
        	   if($res){
        	       	$resp = array('status'=>1,'smg'=>'Successfully Submit Give Aways ');
        	   }else{
        	       	$resp = array('status'=>0,'smg'=>'Something went wrong  ');
        	   }
        	     
        } else {
          	$resp = array('status'=>0,'smg'=>'Please Enter Valid Email...');
        }
		echo json_encode($resp,JSON_UNESCAPED_SLASHES);
    }
    function Subscribe(){
        $jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$data= $this->DBfile->get_data('website' ,array('w_siteurl'=>$postData['w_userid']),'','',array(0,1));
		if($postData['s_email']!=''){
		     $postData['w_userid'] = $data[0]['w_userid'];
		     $this->DBfile->put_data( 'subscrib', $postData);
		     $resp = array('status'=>1,'smg'=>'Send Your Request Successfully');
		}else{
		    $resp = array('status'=>0,'smg'=>'Something went wrong');
		}
		echo json_encode($resp,JSON_UNESCAPED_SLASHES);
    }
	function connect_domain($w_id=0){
		if(isset($_POST['addon_domain'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$w_id = $postData['w_id'];
			unset($postData['w_id']);
			$checkDomain = $this->DBfile->get_data('website',array('addon_domain'=>$postData['addon_domain']),'w_id');
			if(empty($checkDomain)){

				/****** Adding Domain as AddOn Domain ***********/
				
				if(count(explode('.' , $postData['addon_domain'])) > 1 ){
					$cpaneluser= C_USERNAME ;
					$cpanelpass= C_PASSWORD;
					$cpanel_skin = 'paper_lantern';
					$domain_url = strtolower(trim($postData['addon_domain']));
					$subdomain = explode('.',$domain_url)[0];
		
					$request = "/frontend/paper_lantern/addon/doadddomain.html?domain=".$domain_url."&subdomain=".$subdomain."&rootdomain=plrsitebuilder.co.in&go=Create&dir=public_html";
					
					$result = '';
					$host = 'localhost';
					$sock = fsockopen($host,2082);
					$authstr = "$cpaneluser:$cpanelpass";
					$pass = base64_encode($authstr);
					$in = "GET $request\r\n";
					$in .= "HTTP/1.0\r\n";
					$in .= "Host:$host\r\n";
					$in .= "Authorization: Basic $pass\r\n";
					$in .= "\r\n";
					fputs($sock, $in);
					while (!feof($sock)) {
						$result .= fgets ($sock,128);
					}
					
					fclose( $sock );
					
					$q_res = explode('has been created',$result);
					
					if( count($q_res) > 1) {
						$this->DBfile->set_data( 'website', array('addon_domain'=>$postData['addon_domain']) , array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id) );
						echo '1';

					}else
						echo 5;// Could not determine the nameserver IP addresses for "'.strtolower($postData['addon_domain']).'" Please make sure that the domain is registered with a valid domain registrar.
				}else
					echo '4'; // Invalid Domain		
				/** Adding Domain as Add On Domain **************/

			}
			else{
				if( $checkDomain[0]['w_id'] == $w_id ){
					$this->DBfile->set_data( 'website', array('addon_domain'=>$postData['addon_domain']) , array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id) );
					echo '3';
				}
				else{
					echo '2';
				}
			}

			
			die();
		}
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'addon_domain,w_id,w_title,w_siteurl');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));

		$data['pageName'] = 'Connect Domain Settings';
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'plr/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
		$this->load->view('backend/header',$data);
		$this->load->view('backend/connect_domain',$data);
		$this->load->view('backend/footer',$data);
	}

	function getProductsToFrontEnd(){
		if(isset($_POST['i'])){
			$start = ($_POST['i'] - 1) * 12;
			if(is_numeric($_POST['gid'])){
			    $websiteData = $this->DBfile->get_data('website' ,array('w_id'=>$_POST['webid']),'','',array(0,1));
			    if($websiteData[0]['w_type']==1){
			        if($websiteData[0]['w_cateid']==0){
			             $prodList = $this->DBfile->get_data('giveaway' ,'' ,'','',array($start,15));
			        }else{
			             $prodList = $this->DBfile->get_data('giveaway' ,array('g_categories'=>$websiteData[0]['w_cateid']) ,'','',array($start,15));
			        }
			       
			    }else  if($websiteData[0]['w_type']==5){
			        $prodList = $this->DBfile->get_data('giveaway_games' , '' ,'','',array($start,15));
			    }else  if($websiteData[0]['w_type']==4){
			        $prodList = $this->DBfile->get_data('Gift_Cards' , '' ,'','',array($start,15));  
			    }else  if($websiteData[0]['w_type']==3){
			          $prodList = $this->DBfile->get_data('Contests' , '' ,'','',array($start,15));  
			    }else  if($websiteData[0]['w_type']==6){
			         $prodList = $this->DBfile->get_data('coupons' , '' ,'','',array($start,15));
	            }else  if($websiteData[0]['w_type']==2){
	                 $prodList = $this->DBfile->get_data('training_videos' , '' ,'','',array($start,15));
	            }
				
			}
			 $str = '';
			if($websiteData[0]['w_type']==1 || $websiteData[0]['w_type']==4 || $websiteData[0]['w_type']==3 || $websiteData[0]['w_type']==5){
			    	if(!empty($prodList)) {
        				foreach($prodList as $soloProd) { 
        				$url = isset($soloProd['g_id']) ? $soloProd['g_id'] : $soloProd['id'];
        				$img =isset($soloProd['g_priview'])? $soloProd['g_priview'] : $soloProd['thumbnail'];
        				$countries = isset($soloProd['g_countries'])!=''? $soloProd['g_countries'] : $soloProd['type'];
        				$published_date = isset($soloProd['g_last_update']) ? $soloProd['g_last_update'] : $soloProd['published_date'];
        				$gName = isset($soloProd['g_offer_name']) ? $soloProd['g_offer_name'] : $soloProd['title'];
        				 $str .= '<div class="col-lg-4 col-md-6 col-sm-6">';
                            $str .= '<div class="plr_news_box">';
                                $str .= '<div class="plr_blog_img plr_animation_box">';
                                    $str .= '<span class="plr_product_list_img plr_animation">';
                                        $str .= '<a href="'.base_url().'ga/'.$websiteData[0]['w_siteurl'].'/single_page/'.$url.'"><img src="'.$img.'" class="plr_animation_img" alt="product-img"></a>';
                                    $str .= '</span>';
                                    $str .= '<div class="plr_blog_btn">';
                                        $str .= '<a href="javascript:;">'.$countries.'</a>';
                                    $str .= '</div>';
                                $str .= '</div>';
                                $str .= '<div class="plr_blog_content">';
                                    $str .= '<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">';
                                    $str .= '<path fill-rule="evenodd"  fill="rgb(121, 121, 121)" d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>';
                                    $str .= '</svg>'.$published_date.'</span>';
                                   $str .= ' <a href="'.base_url().'ga/'. $websiteData[0]['w_siteurl'].'/single_page/'.$url.'"><h5>'.$gName.'</h5></a>';
                                $str .= '</div>';
                            $str .= '</div>';
                        $str .= '</div>';
        				}
        			}
			}else if($websiteData[0]['w_type']==6){
		    	if(!empty($prodList)) {
    				foreach($prodList as $soloProd) { 
    				$url = isset($soloProd['id']) ? $soloProd['id'] : '' ;
    				$img =isset($soloProd['brand_logo'])? $soloProd['brand_logo'] : '';
    				$countries = isset($soloProd['type'])!=''? $soloProd['type'] : '';
    				$published_date = isset($soloProd['end_date']) ? $soloProd['end_date'] : '';
    				$gName = isset($soloProd['title']) ? $soloProd['title'] : '';
    				 $str .= '<div class="col-lg-4 col-md-6 col-sm-6">';
                        $str .= '<div class="plr_news_box">';
                            $str .= '<div class="plr_blog_img plr_animation_box">';
                                $str .= '<span class="plr_product_list_img plr_animation">';
                                    $str .= '<a href="'.base_url().'ga/'.$websiteData[0]['w_siteurl'].'/single_page/'.$url.'"><img src="'.$img.'" class="plr_animation_img" alt="product-img"></a>';
                                $str .= '</span>';
                                $str .= '<div class="plr_blog_btn">';
                                    $str .= '<a href="javascript:;">'.$countries.'</a>';
                                $str .= '</div>';
                            $str .= '</div>';
                            $str .= '<div class="plr_blog_content">';
                                $str .= '<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="19px">';
                                $str .= '<path fill-rule="evenodd"  fill="rgb(121, 121, 121)" d="M9.000,0.149 C4.029,0.149 -0.000,4.178 -0.000,9.149 C-0.000,14.119 4.029,18.149 9.000,18.149 C13.970,18.149 18.000,14.119 18.000,9.149 C17.994,4.181 13.968,0.155 9.000,0.149 ZM12.033,12.182 C11.713,12.501 11.196,12.501 10.876,12.182 L8.421,9.727 C8.268,9.574 8.182,9.366 8.182,9.149 L8.182,4.240 C8.182,3.788 8.548,3.422 9.000,3.422 C9.452,3.422 9.818,3.788 9.818,4.240 L9.818,8.810 L12.033,11.025 C12.352,11.344 12.352,11.862 12.033,12.182 Z"/>';
                                $str .= '</svg>'.$published_date.'</span>';
                               $str .= ' <a href="'.base_url().'ga/'. $websiteData[0]['w_siteurl'].'/single_page/'.$url.'"><h5>'.$gName.'</h5></a>';
                            $str .= '</div>';
                        $str .= '</div>';
                    $str .= '</div>';
    				}
    			}
			}else if($websiteData[0]['w_type']==2){
			    	if(!empty($prodList)) {
    				foreach($prodList as $soloProd) { 
    				$url = isset($soloProd['v_id']) ? $soloProd['v_id'] : '' ;
    				$img =isset($soloProd['v_imagelink'])? $soloProd['v_imagelink'] : '';
    				$gName = isset($soloProd['v_name']) ? $soloProd['v_name'] : '';
    				 $str .= '<div class="col-lg-4 col-md-6 col-sm-6">';
                        $str .= '<div class="plr_news_box">';
                            $str .= '<div class="plr_blog_img plr_animation_box">';
                                $str .= '<span class="plr_product_list_img plr_animation">';
                                    $str .= '<a href="'.base_url().'ga/'.$websiteData[0]['w_siteurl'].'/single_page/'.$url.'"><img src="'.$img.'" class="plr_animation_img" alt="product-img"></a>';
                                $str .= '</span>';
                               $str .= ' <a href="'.base_url().'ga/'. $websiteData[0]['w_siteurl'].'/single_page/'.$url.'"><h5>'.$gName.'</h5></a>';
                            $str .= '</div>';
                        $str .= '</div>';
                    $str .= '</div>';
    				}
    			}
			}
			echo $str;
			die();
		}
	}

	function niche_sites(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Dashboard';
		$cateList = $this->DBfile->get_data('categories',array('cate_banner'=>1));
		$postData = array();
		foreach($cateList as $soloCate){
			$checkCateSite =  $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_cateid'=>$soloCate['cate_id']));
			if(empty($checkCateSite)){
				$postData['w_title'] = $postData['w_logotext'] = $postData['banner_headline'] = $soloCate['cate_name'];
				$postData['w_siteurl'] = $soloCate['cate_slug'].$this->session->userdata['id'];
				$postData['w_userid'] = $this->session->userdata['id'];
				$postData['w_createddate'] = date('d-m-Y H:i:s');
				$postData['w_cateid'] = $soloCate['cate_id'];
				$this->DBfile->put_data( 'website', $postData);
			}
		}
		$websiteList = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_cateid !='=>0),'w_id,w_userid,w_createddate,w_title,w_siteurl');
		$data['websiteList'] = $websiteList;
		
		$this->load->view('backend/header',$data);
		$this->load->view('backend/niche_sites',$data);
		$this->load->view('backend/footer',$data);
	}

	function track_transactions($planId,$webId,$customerId){
		$webRes = $this->DBfile->get_data('website',array('w_id'=>$webId),'w_plansettings');

		$planArr = json_decode($webRes[0]['w_plansettings'],true);
		$planData = json_decode($planArr['plan_'.$planId],true);
		$planAmount = $planData['amount_'.$planId];
		$planName = $planData['planname_'.$planId];

		$dataArr = array(
			'trans_planid'	=>	$planId,
			'trans_webid'	=>	$webId,
			'trans_customerid'	=>	$customerId,
			'trans_mcgross'	=>	$_POST['mc_gross'],
			'trans_amount'	=>	$planAmount,
			'trans_status'	=>	$_POST['payment_status'],
			'trans_record'	=>	json_encode($_POST),
			'trans_date'	=>	date('Y-m-d h:i:s')
		);
		$this->DBfile->put_data('customer_transactions',$dataArr);
		
		if($_POST['payment_status'] == 'Completed'){
			$soloCust = $this->DBfile->get_data('customers',array('c_id'=>$customerId),'c_name,c_email');
			sendToAutoResponder($soloCust[0]['c_email'],$soloCust[0]['c_name'],$webId,'w_buyplanlist');
			$this->DBfile->set_data( 'customers', array('c_plan'=>$planName,'c_planid'=>$planId) , array('c_id'=>$customerId) );
		}
	}

	function web_transactions($w_id=0){
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'w_id,w_siteurl,w_plansettings,addon_domain');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));

		$data['transactions'] = $this->DBfile->get_data('customer_transactions',array('trans_webid'=>$w_id));
		$data['pageName'] = 'Payment Transactions';
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
		$this->load->view('backend/header',$data);
		$this->load->view('backend/web_transactions',$data);
		$this->load->view('backend/footer',$data);
	}

    function ads_setting($w_id=0){
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'w_id,w_siteurl,w_plansettings,addon_domain');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));

		$data['transactions'] = $this->DBfile->get_data('customer_transactions',array('trans_webid'=>$w_id));
		$data['adsSetting'] = $this->DBfile->get_data('ads',array('web_id'=>$this->uri->segment(3)));
		$data['pageName'] = 'Ads Setting';
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
		$this->load->view('backend/header',$data);
		$this->load->view('backend/ads_setting',$data);
		$this->load->view('backend/footer',$data);
	}
	function tutorials(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Tutorials';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/tutorials',$data);
		$this->load->view('backend/footer',$data);
	}


	function access_download($webId,$custId,$prodName){
        $webRes = $this->DBfile->get_data('website',array('w_id'=>$webId));
		if( isset($this->session->userdata['c_loggedin']) ){
			$website = $this->session->userdata['web_url'];
			$data['checkURL'] = explode('plr/',$website);

			$planArr = json_decode($webRes[0]['w_plansettings'],true);

			$getTransactions = $this->DBfile->get_data('customer_transactions',array('trans_webid'=>$webId,'trans_customerid'=>$custId),'trans_planid',array('trans_id','DESC'),1);
            $siteUrl = base_url().'plr/'.$webRes[0]['w_siteurl'];
            
			if(!empty($getTransactions)){
				$planId = $getTransactions[0]['trans_planid'];
				$planData = json_decode($planArr['plan_'.$planId],true);
				$planDownload = $planData['numberofdownloads_'.$planId];
				$downDataArr = array('pd_webid'=>$webId,'pd_custid'=>$custId);
				$checkPreviousDownloads = $this->DBfile->getCount('product_downloads',$downDataArr);
				if( $planDownload == 0 || $checkPreviousDownloads[0]['totCount'] < $planDownload ){
					$productDetails = $this->DBfile->get_data('products' , array('p_urlname'=>$prodName),'p_finalprod,p_id','',1);
					$customproductDetails = $this->DBfile->get_data('web_products' , array('p_urlname'=>$prodName),'p_finalprod,p_id','',1);
					if(!empty($productDetails)){
						$downDataArr['pd_prodid'] = $productDetails[0]['p_id'];
						$recheckProd = $this->DBfile->get_data('product_downloads',$downDataArr);
						if(empty($recheckProd)){
							$downDataArr['pd_date'] = date('Y-m-d h:i:s');
							$downDataArr['pd_type'] = 'pre';
							$this->DBfile->put_data('product_downloads',$downDataArr);
						}
						redirect($productDetails[0]['p_finalprod']);
					}
					elseif(!empty($customproductDetails)){
						$downDataArr['pd_prodid'] = $customproductDetails[0]['p_id'];
						$recheckProd = $this->DBfile->get_data('product_downloads',$downDataArr);
						if(empty($recheckProd)){
							$downDataArr['pd_date'] = date('Y-m-d h:i:s');
							$downDataArr['pd_type'] = 'custom';
							$this->DBfile->put_data('product_downloads',$downDataArr);
						}
						redirect($customproductDetails[0]['p_finalprod']);
					}
					else
                        $msg = 'Sorry, we are not able to find the product.';
				}
				else
                    $msg = 'You have already downloaded '.$checkPreviousDownloads[0]['totCount'].' products out of your '.$planDownload.' limit download as per the plan.';
			}
			else
                $msg = 'It seems that you have to purchased a plan. Check the <a href="'.$siteUrl.'/pricing">pricing</a> page and get a plan';	
		}
		else
            $msg = 'You seems to be logged out. Please, login back and then try to download.';		

        $data['errorMessage'] = $msg;

        if(empty($webRes))
            redirect(base_url());

        if(isset($this->session->userdata['c_loggedin']) )
            $data['is_customer'] = 1;
        else
            $data['is_customer'] = 0;

        $data['websiteData'] = $webRes[0];
        $data['cateList'] = $this->DBfile->get_data('categories');
        
        $this->load->view('frontend/header',$data);
        $this->load->view('frontend/download_error',$data);
        $this->load->view('frontend/footer',$data);
	}
	
	function download_product($prodUrlName){
        $this->checkUserLoginStatus();
        
        $productDetails = $this->DBfile->get_data('training_videos' , array('v_urlname'=>$prodUrlName),'v_finalprod','',1);
        if(!empty($productDetails))
            redirect($productDetails[0]['v_finalprod']);
        else{
			$productDetails = $this->DBfile->get_data('training_videos' , array('v_urlname'=>$prodUrlName),'v_finalprod','',1);
			if(!empty($productDetails))
            	redirect($productDetails[0]['v_finalprod']);
			else
				die('Sorry, we are not able to find the product.');
		}
	}
	
	function agency_users(){
		if(isset($_POST['sts'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
            $sts = $postData['sts'] == 1 ? 1 : 2 ;
			$this->DBfile->set_data( 'usertbl', array('u_status'=>$sts) , array('u_id'=>$_POST['userid']) );
			echo '1';
			die();
		}
		if( isset($_POST['u_name']) ){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$userId = $postData['userid'];
			unset($postData['userid']);
			$checkEmail = $this->DBfile->get_data('usertbl',array('u_email'=>$postData['u_email']));

			if( $userId == 0 )
			{
				if( empty($checkEmail) ){
					$postData['u_purchaseddate'] = date('Y-m-d h:i:s');
					$postData['is_parent'] = $this->session->userdata['id'];
					$postData['u_password'] = md5($postData['u_password']);
					$postData['u_type'] = 2;
					$postData['u_status'] = 1;
					$postData['is_fe'] = 1;
					$postData['is_oto1'] = 1;
					$postData['is_oto2'] = 1;
					$this->DBfile->put_data('usertbl',$postData);
					echo 2;
				}
				else{
					echo 4;
				}
			}
			else{
				if( $postData['u_password'] != '' )
					$postData['u_password'] = md5($postData['u_password']);
				else
					unset($postData['u_password']);

				if( $checkEmail[0]['u_id'] == $userId ){
					$this->DBfile->set_data('usertbl',$postData,array('u_id'=>$userId));
					echo 1;
				}
				else
					echo 4;
					
			}
			die();
		}
		if( isset($_POST['userid']) ){
			$custDetails = $this->DBfile->get_data('usertbl',array('u_id'=>$_POST['userid']));
			$temp = array(
				'u_name'	=>	$custDetails[0]['u_name'],
				'u_email'	=>	$custDetails[0]['u_email']
			);
			echo json_encode($temp);
			die();
		}

		$this->checkUserLoginStatus();
		$data['agency_users'] = $this->DBfile->get_data('usertbl',array('is_parent'=>$this->session->userdata['id']));
		$data['pageName'] = 'Agency Users';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/agency_users',$data);
		$this->load->view('backend/footer',$data);
	}
	function support(){
	    $this->checkUserLoginStatus();
		$data['agency_users'] = $this->DBfile->get_data('usertbl',array('is_parent'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Support Desk';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/support',$data);
		$this->load->view('backend/footer',$data);
	}
	function InterestedGiveaways(){
	    $this->checkUserLoginStatus();
	    $join = array('multiple', array(array('website','website.w_id=leads.l_webid','right')));
		$data['leads'] = $this->DBfile->select_data('leads.*,website.w_siteurl','leads',array('leads.w_userid'=>$this->session->userdata['id']),'','','',$join);
    	$data['pageName'] = 'Interested Giveaways';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/InterestedGiveaways',$data);
		$this->load->view('backend/footer',$data);
	}
	function bonuses(){
	    $this->checkUserLoginStatus();
		$data['agency_users'] = $this->DBfile->get_data('usertbl',array('is_parent'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Bonuses';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/bonuses',$data);
		$this->load->view('backend/footer',$data);
	} 
	function wp_plugin_download(){
	    $this->checkUserLoginStatus();
		$data['agency_users'] = $this->DBfile->get_data('usertbl',array('is_parent'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Wordpress Plugin';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/wp_plugin',$data);
		$this->load->view('backend/footer',$data);
	} 
	function pending_giveaways($id=''){
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_id'=>$id),'w_id,w_siteurl,addon_domain,w_type');

		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));
        if(	$data['webDetail'][0]['w_type']==1){
        	$data['products'] = $this->DBfile->get_data('submit_giveaway',array('w_id'=>$this->uri->segment(3)));
    		$data['pageName'] = 'General GiveAways';
    	
        }else if($data['webDetail'][0]['w_type']==5){
        	$data['products'] = $this->DBfile->get_data('submit_games_giveaway',array('w_id'=>$this->uri->segment(3)));
    		$data['pageName'] = 'Games GiveAways';
    	
        }else if($data['webDetail'][0]['w_type']==3){
        	$data['products'] = $this->DBfile->get_data('submit_contests',array('w_id'=>$this->uri->segment(3)));
    		$data['pageName'] = 'Contests';
    	
        }else if($data['webDetail'][0]['w_type']==4){
        	$data['products'] = $this->DBfile->get_data('submit_gift_cards',array('w_id'=>$this->uri->segment(3)));
    		$data['pageName'] = 'Gift Cards';
       
    	}else if($data['webDetail'][0]['w_type']==2){
            	$data['products'] = $this->DBfile->get_data('videos_giveaway',array('w_id'=>$this->uri->segment(3)));
        		$data['pageName'] = 'Training Videos';
        	
    	}else if($data['webDetail'][0]['w_type']==6){
            	$data['products'] = $this->DBfile->get_data('coupons_giveaway',array('w_id'=>$this->uri->segment(3)));
        		$data['pageName'] = 'Coupons Giveaway';
        	
        }
	    $pageName = 'backend/pending_giveaways';
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
	
		$this->load->view('backend/header',$data);
		$this->load->view($pageName,$data);
		$this->load->view('backend/footer',$data);
	}
    function products($type=''){
		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id']),'w_id,w_siteurl,addon_domain');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));
        if($type=="general-giveaways"){
        	$data['products'] = $this->DBfile->get_data('giveaway');
    		$data['pageName'] = 'General GiveAways';
    		$pageName = 'backend/general_giveaways';
        }else if($type=="games-giveaways"){
        	$data['products'] = $this->DBfile->get_data('giveaway_games');
    		$data['pageName'] = 'Games GiveAways';
    		$pageName = 'backend/games_giveaways_product';
        }else if($type=="training-videos"){
        	$data['training_videos'] = $this->DBfile->get_data('training_videos');
    		$data['pageName'] = 'Training Videos';
    		$pageName = 'backend/training_videos';
        }else if($type=="contests"){
        	$data['Contests'] = $this->DBfile->get_data('Contests');
    		$data['pageName'] = 'Contests';
    		$pageName = 'backend/Contests';
        }else if($type=='gift-cards'){
        	$data['Gift_Cards'] = $this->DBfile->get_data('Gift_Cards');
    		$data['pageName'] = 'Gift Cards';
    		$pageName = 'backend/gift_cards';
        }else{
        	$data['coupons'] = $this->DBfile->get_data('coupons','','','',array(0,500));
    		$data['pageName'] = 'Coupons';
    		$pageName = 'backend/coupons';
        }
	
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
	
		$this->load->view('backend/header',$data);
		$this->load->view($pageName,$data);
		$this->load->view('backend/footer',$data);
	}
	function add_affiliate_url(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);

		$id = $postData['id'];
		unset($postData['id']);

		if( $id == 0 )
		{
			$postData['user_id'] = $this->session->userdata['id'];
			$this->DBfile->put_data('affiliate_link',$postData);
			echo 2;
		}
		else{
		    if($_POST['alibaba'] ){
            $alibaba = $_POST['alibaba'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$alibaba."' WHERE `store` LIKE '%alibaba%'");
            }
            if($_POST['aliexpress'] !=''){
            $aliexpress = $_POST['aliexpress'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$aliexpress."' WHERE `store` LIKE '%aliexpress%'");
            }
            if($_POST['amazon_com'] !=''){
            $amazon_com = $_POST['amazon_com'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$amazon_com."' WHERE `store` LIKE '%amazon_com%'");
            }
            if($_POST['amazon_in'] !=''){
            $amazon_in = $_POST['amazon_in'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$amazon_in."' WHERE `store` LIKE '%amazon_in%'");
            }
            if($_POST['cafago'] !=''){
            $cafago = $_POST['cafago'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$cafago."' WHERE `store` LIKE '%cafago%'");
            }
            if($_POST['discoveryplus'] !=''){
            $discoveryplus = $_POST['discoveryplus'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$discoveryplus."' WHERE `store` LIKE '%discoveryplus%'");
            }
            if($_POST['flipkart'] !=''){
            $flipkart = $_POST['flipkart'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$flipkart."' WHERE `store` LIKE '%flipkart%'");
            }
            if($_POST['geekbuying'] !=''){
            $geekbuying = $_POST['geekbuying'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$geekbuying."' WHERE `store` LIKE '%geekbuying%'");
            }
            if($_POST['macys'] !=''){
            $macys = $_POST['macys'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$macys."' WHERE `store` LIKE '%macys%'");
            }
            if($_POST['paramountplus'] !=''){
            $paramountplus = $_POST['paramountplus'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$paramountplus."' WHERE `store` LIKE '%paramountplus%'");
            }
            if($_POST['pitchground'] !=''){
            $pitchground = $_POST['pitchground'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$pitchground."' WHERE `store` LIKE '%pitchground%'");
            }
            if($_POST['ticketnetwork'] !=''){
            $ticketnetwork = $_POST['ticketnetwork'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$ticketnetwork."' WHERE `store` LIKE '%ticketnetwork%'");
            }
            if($_POST['tomtop'] !=''){
            $tomtop = $_POST['tomtop'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$tomtop."' WHERE `store` LIKE '%tomtop%'");
            }
            if($_POST['vapor'] !=''){
            $vapor = $_POST['vapor'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$vapor."' WHERE `store` LIKE '%vapor%'");
            }
            if($_POST['vetsupply'] !=''){
            $vetsupply = $_POST['vetsupply'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$vetsupply."' WHERE `store` LIKE '%vetsupply%'");
            }
            if($_POST['abebooks'] !=''){
            $abebooks = $_POST['abebooks'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$abebooks."' WHERE `store` LIKE '%abebooks%'");
            }
            if($_POST['casetify'] !=''){
            $casetify = $_POST['casetify'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$casetify."' WHERE `store` LIKE '%casetify%'");
            }
            if($_POST['coinsmart'] !=''){
            $coinsmart = $_POST['coinsmart'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$coinsmart."' WHERE `store` LIKE '%coinsmart%'");
            }
            if($_POST['dhgate'] !=''){
            $dhgate = $_POST['dhgate'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$dhgate."' WHERE `store` LIKE '%dhgate%'");
            }
            if($_POST['examedge'] !=''){
            $examedge = $_POST['examedge'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$examedge."' WHERE `store` LIKE '%examedge%'");
            }
            if($_POST['globalhealing'] !=''){
            $globalhealing = $_POST['globalhealing'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$globalhealing."' WHERE `store` LIKE '%globalhealing%'");
            }
            if($_POST['gshopper'] !=''){
            $gshopper = $_POST['gshopper'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$gshopper."' WHERE `store` LIKE '%gshopper%'");
            }
            if($_POST['hekka'] !=''){
            $hekka = $_POST['hekka'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$hekka."' WHERE `store` LIKE '%hekka%'");
            }
            if($_POST['istockphoto'] !=''){
            $istockphoto = $_POST['istockphoto'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$istockphoto."' WHERE `store` LIKE '%istockphoto%'");
            }
            if($_POST['magix'] !=''){
            $magix = $_POST['magix'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$magix."' WHERE `store` LIKE '%magix%'");
            }
            if($_POST['pariscityvision'] !=''){
            $pariscityvision = $_POST['pariscityvision'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$pariscityvision."' WHERE `store` LIKE '%pariscityvision%'");
            }
            if($_POST['razer'] !=''){
            $razer = $_POST['razer'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$razer."' WHERE `store` LIKE '%razer%'");
            }
            if($_POST['remitly'] !=''){
            $remitly = $_POST['remitly'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$remitly."' WHERE `store` LIKE '%remitly%'");
            }
            if($_POST['shutterstock'] !=''){
            $shutterstock = $_POST['shutterstock'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$shutterstock."' WHERE `store` LIKE '%shutterstock%'");
            }
            if($_POST['springer'] !=''){
            $springer = $_POST['springer'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$springer."' WHERE `store` LIKE '%springer%'");
            }
            if($_POST['tenorshare'] !=''){
            $tenorshare = $_POST['tenorshare'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$tenorshare."' WHERE `store` LIKE '%tenorshare%'");
            }
            if($_POST['ticketliquidator'] !=''){
            $ticketliquidator = $_POST['ticketliquidator'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$ticketliquidator."' WHERE `store` LIKE '%ticketliquidator%'");
            }
            if($_POST['tigerdirect'] !=''){
            $tigerdirect = $_POST['tigerdirect'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$tigerdirect."' WHERE `store` LIKE '%tigerdirect%'");
            }
            if($_POST['vistaprint'] !=''){
            $vistaprint = $_POST['vistaprint'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$vistaprint."' WHERE `store` LIKE '%vistaprint%'");
            }
            if($_POST['appsumo'] !=''){
            $appsumo = $_POST['appsumo'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$appsumo."' WHERE `store` LIKE '%appsumo%'");
            }
            if($_POST['cheapoair'] !=''){
            $cheapoair = $_POST['cheapoair'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$cheapoair."' WHERE `store` LIKE '%cheapoair%'");
            }
            if($_POST['chicme'] !=''){
            $chicme = $_POST['chicme'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$chicme."' WHERE `store` LIKE '%chicme%'");
            }
            if($_POST['corsair'] !=''){
            $corsair = $_POST['corsair'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$corsair."' WHERE `store` LIKE '%corsair%'");
            }
            if($_POST['dell'] !=''){
            $dell = $_POST['dell'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$dell."' WHERE `store` LIKE '%dell%'");
            }
            if($_POST['elfsight'] !=''){
            $elfsight = $_POST['elfsight'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$elfsight."' WHERE `store` LIKE '%elfsight%'");
            }
            if($_POST['englishonlinev'] !=''){
            $englishonlinev = $_POST['englishonlinev'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$englishonlinev."' WHERE `store` LIKE '%englishonlinev%'");
            }
            if($_POST['gearbest'] !=''){
            $gearbest = $_POST['gearbest'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$gearbest."' WHERE `store` LIKE '%gearbest%'");
            }
            if($_POST['higherstandards'] !=''){
            $higherstandards = $_POST['higherstandards'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$higherstandards."' WHERE `store` LIKE '%higherstandards%'");
            }
            if($_POST['kitbag'] !=''){
            $kitbag = $_POST['kitbag'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$kitbag."' WHERE `store` LIKE '%kitbag%'");
            }
            if($_POST['lenovo'] !=''){
            $lenovo = $_POST['lenovo'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$lenovo."' WHERE `store` LIKE '%lenovo%'");
            }
            if($_POST['lightinthebox'] !=''){
            $lightinthebox = $_POST['lightinthebox'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$lightinthebox."' WHERE `store` LIKE '%lightinthebox%'");
            }
            if($_POST['nba'] !=''){
            $nba = $_POST['nba'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$nba."' WHERE `store` LIKE '%nba%'");
            }
            if($_POST['newchic'] !=''){
            $newchic = $_POST['newchic'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$newchic."' WHERE `store` LIKE '%newchic%'");
            }
            if($_POST['nflshop'] !=''){
            $nflshop = $_POST['nflshop'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$nflshop."' WHERE `store` LIKE '%nflshop%'");
            }
            if($_POST['omio'] !=''){
            $omio = $_POST['omio'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$omio."' WHERE `store` LIKE '%omio%'");
            }
            if($_POST['oneplus'] !=''){
            $oneplus = $_POST['oneplus'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$oneplus."' WHERE `store` LIKE '%oneplus%'");
            }
            if($_POST['onetravel'] !=''){
            $onetravel = $_POST['onetravel'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$onetravel."' WHERE `store` LIKE '%onetravel%'");
            }
            if($_POST['philo'] !=''){
            $philo = $_POST['philo'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$philo."' WHERE `store` LIKE '%philo%'");
            }
            if($_POST['photowall'] !=''){
            $photowall = $_POST['photowall'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$photowall."' WHERE `store` LIKE '%photowall%'");
            }
            if($_POST['sage'] !=''){
            $sage = $_POST['sage'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$sage."' WHERE `store` LIKE '%sage%'");
            }
            if($_POST['samueljohnston'] !=''){
            $samueljohnston = $_POST['samueljohnston'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$samueljohnston."' WHERE `store` LIKE '%samueljohnston%'");
            }
            if($_POST['untilgone'] !=''){
            $untilgone = $_POST['untilgone'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$untilgone."' WHERE `store` LIKE '%untilgone%'");
            }
            if($_POST['vapesourcing'] !=''){
            $vapesourcing = $_POST['vapesourcing'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$vapesourcing."' WHERE `store` LIKE '%vapesourcing%'");
            }
            if($_POST['walgreens'] !=''){
            $walgreens = $_POST['walgreens'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$walgreens."' WHERE `store` LIKE '%walgreens%'");
            }
            if($_POST['wondershare'] !=''){
            $wondershare = $_POST['wondershare'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$wondershare."' WHERE `store` LIKE '%wondershare%'");
            }
            if($_POST['zaful'] !=''){
            $zaful = $_POST['zaful'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$zaful."' WHERE `store` LIKE '%zaful%'");
            }
            if($_POST['zulily'] !=''){
            $zulily = $_POST['zulily'];
            $chaData = $this->DBfile->custom_query("UPDATE `coupons` SET `affiliate_link`='".$zulily."' WHERE `store` LIKE '%zulily%'");
        }
                //   echo $this->db->last_query();
			$this->DBfile->set_data('affiliate_link',$postData,array('id'=>$id));
			echo 1;					
		}
		die();
    }
    function custom_products($w_id=0){
		if( isset($_POST['g_offer_name']) ){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$webDetail = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id));
		
			$p_id = $postData['p_id'];
			unset($postData['p_id']);

			if( $p_id == 0 )
			{
				$postData['g_webid'] = $w_id;
				$postData['g_last_update'] = date('M-d-Y');
				$this->DBfile->put_data('web_giveaway',$postData);
				echo 2;
			}
			else{
				$this->DBfile->set_data('web_giveaway',$postData,array('g_id'=>$p_id));
				echo 1;					
			}
			die();
		}
		if( isset($_POST['p_id']) ){
			$prodDetails = $this->DBfile->get_data('web_giveaway',array('g_id'=>$_POST['p_id']));
			echo json_encode($prodDetails[0]);
			die();
		}

		if(isset($_POST['delete_prodid'])){
			$this->DBfile->delete_data( 'product_downloads', array('pd_prodid'=>$_POST['delete_prodid'],'pd_type'=>'custom'));
			$this->DBfile->delete_data( 'web_giveaway', array('g_id'=>$_POST['delete_prodid'],'g_webid'=>$w_id));
			echo 1;
			die();
		}

		$this->checkUserLoginStatus();
		$data['webDetail'] = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'w_id,w_siteurl,addon_domain');
		if( empty($data['webDetail']) )
			redirect(base_url('home/dashboard'));

		$data['custom_products'] = $this->DBfile->get_data('web_giveaway',array('g_webid'=>$w_id));
		$data['web_categories'] = $this->DBfile->get_data('web_categories',array('cate_webid'=>$w_id));
		$data['siteUrl'] = $data['siteUrl'] = !empty($data['webDetail']) ? ($data['webDetail'][0]['addon_domain'] == '' ? base_url().'ga/'.$data['webDetail'][0]['w_siteurl'] : 'https://'.$data['webDetail'][0]['addon_domain']) : '' ; ;
		$data['pageName'] = 'Custom GiveAways';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/custom_products',$data);
		$this->load->view('backend/footer',$data);
	}
	
	function GeneralGiveAways(){
	    $data = $this->DBfile->get_data('giveaway' ,'','','','');
	     $resp = array('status'=>1,'Data'=>$data);
	     echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}

	function getWebsiteSeoData($w_id=0){
		$res = $this->DBfile->get_data('website',array('w_userid'=>$this->session->userdata['id'],'w_id'=>$w_id),'w_title,w_siteurl,w_logourl');
		if(!empty($res))
			echo json_encode($res);
		else
			echo json_encode(array());
	}


	function test(){
		echo '<pre>';
		$uid = 1;
		$userDetail =  $this->DBfile->get_data('usertbl',array('u_id'=>$uid));
		if( $userDetail[0]['is_oto5'] == 1 && $userDetail[0]['dfy_done'] == 0 ){

			$dfysites = $this->DBfile->get_data('dfy');
			foreach($dfysites as $solo){
					$postData['w_title'] = $postData['w_logotext'] = $solo['dfy_name'];
					$postData['w_siteurl'] = strtolower(str_replace(' ','-',$solo['dfy_name'])).'-'.$this->session->userdata['id'];
					$postData['w_userid'] = $this->session->userdata['id'];
					$postData['w_createddate'] = date('d-m-Y H:i:s');
					$postData['w_dfy'] =  $solo['dfy_cate'];
					$postData['w_logofont'] =  $solo['dfy_logo_font'];
					$postData['w_logocolor'] =  $solo['dfy_logo_color'];
					$postData['w_themecolor'] =  $solo['dfy_theme'];
					print_r($postData);
					//$this->DBfile->put_data( 'website', $postData);
			}
			//$this->DBfile->set_data( 'usertbl', array('dfy_done'=>1), array('u_id'=>1));
		}
	}
}
?>