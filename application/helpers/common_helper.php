<?php
function sendUserEmailMandrill($to='', $subject = '', $body = ''){
    $tos[] = array(
    'email' => $to,
    'name' => '',
    'type' => 'to'
    );
    $message = array(
    'html' => $body,	
    'subject' => $subject,
    'from_email' => 'support@giveawaysites.co.in',
    'from_name' => 'Give Away Sites',
    'to' =>$tos,
    ); 	   
    $POSTFIELDS = array(
    'key' => 'NtSxVXvKNB_5JQOjv5bFTw',
    'message' => $message
    );
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://mandrillapp.com/api/1.0/messages/send.json');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($POSTFIELDS));

    $headers = array();
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    curl_close($ch);
}

function generateString($len=8){
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $len; $i++) { 
        $index = rand(0, strlen($characters) - 1); 
        $randomString .= $characters[$index]; 
    } 
  
    return $randomString;
}

function getImageExtension($imgName='a.jpg'){
    $temp = explode('.',$imgName);
    $temp = array_reverse($temp);
    return '.'.$temp[0];
}

function slugify($text){
	
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
      return 'n-a';
    }
    return $text;
  }

function sendToAutoResponder($user_email,$user_name,$w_id,$type){
    $CI =& get_instance();
    $web_res = $CI->DBfile->get_data('website',array('w_id'=>$w_id),$type.',w_userid');
    $w_list_data = $web_res[0][$type] != '' ? json_decode($web_res[0][$type],true) : array();

    if(!empty($w_list_data)){
        if( $w_list_data['ar'] != '' && $w_list_data['ar'] != '0' && $w_list_data['listid'] != '' && $w_list_data['listid'] != '0' )
        {
            $res = $CI->DBfile->get_data('autoresponder',array('m_id' => $web_res[0]['w_userid'], 'mkey' => 'autoresponder'));
            $data = (array) json_decode( $res[0]['value'] );
            if(!empty($data[$w_list_data['ar']])){

                $_POST['email'] = $user_email;
                $_POST['listid'] = $w_list_data['listid'];
                $_POST['name'] = $user_name;

                $api = (array) $data[$w_list_data['ar']];
                $pathArr = explode('application',__DIR__);
                require_once $pathArr[0].'application/controllers/subscriber/subscriber.php';
                $objlist = new subscriber();
                $response = $objlist->switch_responder($api, 'subsCribe', $w_list_data['ar'] );
            }
        }
    }
  }
  
?>